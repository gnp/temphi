<?php

class ElfinderLayout extends UFWLayout {
	function __construct() {
		if (!Auth::isLoggedIn())
			header("Location: " . Auth::getLoginURL());
	}

	function index() {
		Optimize::addFile("css", array(
			//"css/maestro/bootstrap.min.css",
			"css/maestro/ui-lightness/jquery-ui-1.9.2.custom.min.css",
			"css/maestro.css",
			//"css/colorbox.css",
			//"css/maestro/jquery.tagit.css",
			//"css/maestro/jquery.Jcrop.css",
		));

		Core::setParseVar("optimize", Optimize::getHTML());
		
		//Core::setParseVar("user", $_SESSION['User']['username']);
	}

	function __afterLoad() {

	}
}

?>