<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=##CHARSET##" />
		<title>##TITLE##</title>

		##OPTIMIZE##

		<script type="text/javascript" src="/js/tinymce/v4.0.3b/tinymce.min.js"></script>
		<script type="text/javascript" src="/js/tinymce.v3.4.5/customconf.js"></script>

		<script type="text/javascript">
			$(function(){
			   $("[id^=multicheckboxOrderStatus]").multiselect({noneSelectedText: "Order statuses"});
			   $("[id^=multicheckboxOrderClient]").multiselect({noneSelectedText: "Clients"});
			   $("[id^=multicheckboxOrderPayee]").multiselect({noneSelectedText: "Payees"});
			   $("[id^=multicheckboxOrderCity]").multiselect({noneSelectedText: "Departments"});
			   $("[id^=multicheckboxOrderAddition]").multiselect({noneSelectedText: "Additions"});
			   $("[id^=multicheckboxArticleStatus]").multiselect({noneSelectedText: "Additions"});
			   $("[id^=orders_users_additions]").multiselect({noneSelectedText: "Additions"});
			   
			   var sampleTags = [##SAMPLE TAGS##];
				$('#tags').tagit({
				    availableTags: sampleTags,
				    allowSpaces: true,
			    });
			});
		</script>
		
		<!-- elFinder CSS (REQUIRED) -->
		<link rel="stylesheet" type="text/css" media="screen" href="/css/elfinder/v2.1/elfinder.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="/css/elfinder/v2.1/theme.css">

		<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-41755673-1', 'gremonaparty.com');
  ga('send', 'pageview');

</script>
	</head>
	<body>
		<div class="navbar navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container">
					<a class="brand" href="/maestro/">GNP Control Panel</a>
					<ul class="nav pull-right">
						<li>
							<a href="#">Hello, ##USER## [##USER_STATUS.TITLE##]</a>
						</li>
						<li>
							<a href="/logout">Logout</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="container-fluid" style="margin-top: 50px;">
				<div class="row-fluid">
					##NAVI START##
			    	<div class="##LEFT WIDTH##">
			    		<ul class="nav nav-pills nav-stacked">
							##NAVIGATION START##<li class="##ACTIVE##"><a href="/maestro/##URL##"##ADD##>##TEXT##</a></li>##NAVIGATION END##
							<script type="text/javascript">$(document).ready(function(){
								$('.dropdown-toggle').dropdown()
							});</script>
						</ul>
			    	</div>
					##NAVI END##
			    	<div class="##RIGHT WIDTH##">
						##UFW DEBUG##
						##CONTENT##
						##UFW DEVELOPER##
			    	</div>
			  	</div>
			</div>
		</div>
	</body>
</html>