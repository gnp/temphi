<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=##CHARSET##" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    ##SEO##
		<base href="##BASE HREF##" />
		
		##OPTIMIZE CSS##
      <!--link href="/css/style.less" rel="stylesheet/less" /-->
      <link href="/css/style.less.css" rel="stylesheet" /-->
  <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

##GA EXPERIMENT##
      <!--[if IE]>
      <link href="/css/ie.less" rel="stylesheet/less">
      <![endif]-->
    ##OPTIMIZE JS##
	
	
	    <!-- Fav and touch icons -->
      <link rel="icon" type="image/png" href="/favicon.ico" />
	    <link rel="shortcut icon" href="/favicon.ico">
	    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/img/apple-touch-icon-144-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/img/apple-touch-icon-114-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/img/apple-touch-icon-72-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" href="/img/apple-touch-icon-57-precomposed.png">

      <script>
      if (cookiesAllowed()) {
        /* new 
        <!--(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-41755673-1', 'gremonaparty.com');
        ga('send', 'pageview'); */

        /* old */
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-28646215-1']);
        _gaq.push(['_trackPageview']);

        (function() {
          var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
          ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'https://www') + '.google-analytics.com/ga.js';
          var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
      }

</script>
<script src="/js/less.min.js"></script>
	</head>
	<body>
  	<!-- FACEBOOK SDK -->
		<script>
    if (cookiesAllowed()) {
      $("body").children().first().before('<div id="fb-root"></div>');
      (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=487384604659110";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    }
    </script>
    <!-- /FACEBOOK SDK -->
 
<!-- Modal -->
<div id="loginModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <h3 id="myModalLabel">Prijava</h3>
  </div>
  <div class="modal-body">
    <form name="login-form">
      <input id="email" name="email" placeholder="Email" type="email" />
      <input id="password" name="password" placeholder="Geslo" type="password" />
      <input id="submit" type="submit" value="PRIJAVA" />
    </form>
    <a href="#forgotPassModal" data-toggle="modal" onclick="$('#loginModal').modal('hide')">Pozabil geslo?</a>
  </div>
</div>
<div id="forgotPassModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <h3 id="myModalLabel">Pozabljeno geslo</h3>
  </div>
  <div class="modal-body">
    <form name="forgot-pass-form">
      <input id="emailfp" name="email" placeholder="Email" type="email" />
      <input id="submitfp" type="submit" value="POŠLJI NOVO GESLO" />
    </form>
    <a href="#loginModal" data-toggle="modal" onclick="$('#forgotPassModal').modal('hide')">Se želiš prijavit?</a>
  </div>
</div>

    <div class="head-main">
    	<div class="container">
    		<a href="/"><img class="hBackground" src="/img/header-background.jpg"></a>
      </div>
      <div class="main-menu">
        <div class="container">
        	<div class="bgLeft"></div>
        	<div class="bgRight"></div>
          <div class="bgBottomTop"></div>
          <div class="background"></div>
          <div class="menu-logo">
        		<a class="logo" href="/"><img alt="gremonaparty" src="/img/gremonaparty-logo.png" /></a>
          </div>
          <ul>
            ##MENU LINK START##<li class="##ACTIVE##"><a href="##HREF##">##TITLE##</a></li>##SEPARATOR####MENU LINK END##
          </ul>
          ##IF LOGGED IN START##<a class="login" href="/profil"><span>&bull;</span> moj profil</a>##IF LOGGED IN END##
          ##IF LOGGED OUT START##<a class="login" href="#loginModal" data-toggle="modal"><span>&bull;</span> prijava</a>##IF LOGGED OUT END##
        </div>
      </div>
      <div id="minimenu">
        <div class="btn-group">
          <a class="btn btn-large dropdown-toggle" data-toggle="dropdown" href="#" style="padding: 10px;">
            <img src="/img/menuicon.png" style="height: 24px; display: block;" />
          </a>
          <ul class="dropdown-menu">
            ##MENU LINK START##<li class="##ACTIVE##"><a href="##HREF##">##TITLE##</a></li>##SEPARATOR####MENU LINK END##
          </ul>
        </div>
      </div>
    </div>
    
    <!-- class="default"-->
    <div class="container ##CSS PAGE## marTop20" id="maincontainer">
    	<div class="row-fluid">
        <div class="##LEFT WIDTH##" id="leftcontent">
          ##UFW DEBUG##
        	##CONTENT##
        </div>
        <div class="##RIGHT WIDTH##">
          ##CONTENT RIGHT##
        </div>
      </div>
    </div>
    
    ##FOOTER START##
    <div class="foot">
    	<div class="container">
      	<div class="row-fluid">
        	<div class="span3">
          	<div class="about">
              <h3>GREMONAPARTY.COM</h3>
              <p>Na najboljših partyjih na svetu skupaj doživljamo žurerske trenutke, ki se jih bomo spominjali za vedno. Zapri oči, pozabi na vse in se prepusti užitku!</p>
						</div>
          </div>
        	<div class="span3">
          	<div class="col2">
          		<h3>Vprašaj nas</h3>
              <p><a href="mailto:party@gremonaparty.com" title="Piši nam">party@gremonaparty.com</a></p>
              <p>040 148 148</p>
              <p><a href="//facebook.com/gremonaparty" target="_blank" title="Gremonaparty.com na Facebooku">facebook.com/gremonaparty</a></p>
              <p>skype: gremonaparty</p>
          	</div>
          </div>
        	<div class="span6">
            ##SECOND COLUMN##
          </div>
        </div>
        <div class="menu">
        	<a href="/" title="Gremo na party">DOMOV</a>
          <a href="/potovanja" title="Aranžmaji">POTOVANJA</a>
          <a href="/novice" title="Party novice">NOVICE</a>
          <a href="/private-party-pack/3/stran" title="Private party pack">PRIVATE PARTY PACK</a>
          <a href="/party-prevozi/4/stran" title="Party prevozi">PARTY PREVOZ</a>
        	<a href="/kontakt" title="Kontakt">KONTAKT</a>
        	<a href="/splosni-pogoji-poslovanja/5/stran" title="Splošni pogoji">SPLOŠNI POGOJI</a>
        </div>
        <div class="rights">
        	Oblikovanje PSYNAPS DESIGN, izdelava <a href="http://www.spletni-hisnik.si" title="Spletni hišnik">spletni-hisnik.si</a>    <span>2013 &copy; gremonaparty.com</span>
        </div>
      </div>
    </div><!-- /foot -->
    <!-- ##FOOTER END## -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!--script src="/js/less.min.js"></script-->
    <!-- ##UFW DEVELOPER## -->
	</body>
</html>