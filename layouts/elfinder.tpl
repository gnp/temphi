<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=##CHARSET##" />
		<title>Urejevalnik datotek GNP</title>

		##OPTIMIZE##
		
		<!-- jQuery and jQuery UI -->
		<script type="text/javascript" src="/js/maestro/jquery-1.8.3.min.js"></script>
		<script type="text/javascript" src="/js/maestro/jquery-ui-1.9.2.custom.min.js"></script>
		
		<!-- basic functions -->
		<script type="text/javascript" src="/js/maestro/func.js"></script>

		<!-- elFinder JS (REQUIRED) -->
		<script type="text/javascript" src="/js/elfinder/v2.1/elfinder.full.js"></script>

		<!-- elFinder translation (OPTIONAL) -->
		<!--<script type="text/javascript" src="js/i18n/elfinder.ru.js"></script>-->

		<!-- elFinder initialization (REQUIRED) -->
		<script type="text/javascript" charset="utf-8">
	var keyStr = "ABCDEFGHIJKLMNOP" +
           "QRSTUVWXYZabcdef" +
           "ghijklmnopqrstuv" +
           "wxyz0123456789-_" +
           ".";
	
	  function encode64(input) {
	     input = escape(input);
	     var output = "";
	     var chr1, chr2, chr3 = "";
	     var enc1, enc2, enc3, enc4 = "";
	     var i = 0;
	
	     do {
	        chr1 = input.charCodeAt(i++);
	        chr2 = input.charCodeAt(i++);
	        chr3 = input.charCodeAt(i++);
	
	        enc1 = chr1 >> 2;
	        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
	        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
	        enc4 = chr3 & 63;
	
	        if (isNaN(chr2)) {
	           enc3 = enc4 = 64;
	        } else if (isNaN(chr3)) {
	           enc4 = 64;
	        }
	
	        output = output +
	           keyStr.charAt(enc1) +
	           keyStr.charAt(enc2) +
	           keyStr.charAt(enc3) +
	           keyStr.charAt(enc4);
	        chr1 = chr2 = chr3 = "";
	        enc1 = enc2 = enc3 = enc4 = "";
	     } while (i < input.length);
	
	     return output;
	  }
	
	  function decode64(input) {
	     var output = "";
	     var chr1, chr2, chr3 = "";
	     var enc1, enc2, enc3, enc4 = "";
	     var i = 0;
	
	     // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
	     var base64test = /[^A-Za-z0-9\+\/\=]/g;
	     if (base64test.exec(input)) {
	        alert("There were invalid base64 characters in the input text.\n" +
	              "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
	              "Expect errors in decoding.");
	     }
	     input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
	
	     do {
	        enc1 = keyStr.indexOf(input.charAt(i++));
	        enc2 = keyStr.indexOf(input.charAt(i++));
	        enc3 = keyStr.indexOf(input.charAt(i++));
	        enc4 = keyStr.indexOf(input.charAt(i++));
	
	        chr1 = (enc1 << 2) | (enc2 >> 4);
	        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
	        chr3 = ((enc3 & 3) << 6) | enc4;
	
	        output = output + String.fromCharCode(chr1);
	
	        if (enc3 != 64) {
	           output = output + String.fromCharCode(chr2);
	        }
	        if (enc4 != 64) {
	           output = output + String.fromCharCode(chr3);
	        }
	
	        chr1 = chr2 = chr3 = "";
	        enc1 = enc2 = enc3 = enc4 = "";
	
	     } while (i < input.length);
	
	     return unescape(output);
	  }
  
			$(document).ready(function() {
				var elfinder = $('#elfinder').elfinder({
					url : '/elfinder'  // connector URL (REQUIRED)
				});
				
				// create icon
				if ($(".elfinder-buttonset").length > 0) {
					duplicate = $(".elfinder-buttonset").last().clone();
					duplicate.children().first().attr("title", "Select files").children().first().removeClass("elfinder-button-icon-help").addClass("elfinder-button-icon-customaddfiles").css("background-position", "0px -80px");
					$(".elfinder-buttonset").last().after(duplicate);
				}
				
				// add selected files
				$(".elfinder-button-icon-customaddfiles").parent().click(function(){
					var elFinderSelectedFiles = new Array();
					
					$(".elfinder-cwd-filename").each(function(){
						if ($(this).hasClass("ui-state-hover"))
							elFinderSelectedFiles[elFinderSelectedFiles.length] = decode64($(this).parent().attr("id").replace("l1_", "")); // filename ;-)
					});
					
					window.opener.elFinderSelectedFilesHandler(toJSON(elFinderSelectedFiles));
				    window.close();
				});
			});
		</script>
		
		<!-- maestro lyt -->
		<script type="text/javascript" src="/js/maestro/maestro.js"></script>
		
		<!-- elFinder CSS (REQUIRED) -->
		<link rel="stylesheet" type="text/css" media="screen" href="/css/elfinder/v2.1/elfinder.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="/css/elfinder/v2.1/theme.css">
	</head>
	<body>
		<div class="container">
				##UFW DEBUG##
				##CONTENT##
				##UFW DEVELOPER##
		</div>
	</body>
</html>