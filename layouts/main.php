<?php

class MainLayout extends TwigTpl {
	function __construct() {
		if (substr($_SERVER['REQUEST_URI'], 1, 4) != "css/"
			&& substr($_SERVER['REQUEST_URI'], 1, 3) != "js/"
			&& substr($_SERVER['REQUEST_URI'], 1, 6) != "cache/"
			&& substr($_SERVER['REQUEST_URI'], 1, 4) != "img/"
			&& substr($_SERVER['REQUEST_URI'], 1, 6) != "media/"
			&& substr($_SERVER['REQUEST_URI'], 1, 8) != "maestro/"
			&& !in_array($_SERVER['REQUEST_URI'], array("/sitemap.xml", "/sitemap.txt.gz", "/sitemap.xml.gz"))
			&& !in_array(substr($_SERVER['REQUEST_URI'], -4), array(".jpg", ".gif", ".png"))
			&& !DEVELOPING) {
			$data = array(
				"v" => 1,
				"tid" => "UA-" . Settings::get("ga_id"),
				"cid" => session_id(),
				"t" => "pageview",
				"dh" => DOMAIN,
				"dp" => $_SERVER['REQUEST_URI'],
				"dt" => "",
			);

			$d = array();
			foreach ($data AS $k => $v) {
				$d[] = $k . "=" . urlencode($v);
			}

			$ch = curl_init('http://www.google-analytics.com/collect?' . implode("&", $d));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                                                                  
			 
			$result = curl_exec($ch);
		}

		parent::__construct("layouts/main.twig");
	}

	function none() {
		$this->setContent(NULL);
	}
	
	/*
		menu (menus)
		footer (footer)
		without (hideFooter, hideMenu, hideMenuLink)

		index
			with (contentRight, leftWidth, rightWidth, gaExperiment)
				footer
				menu
		full (leftWidth, rightWidth)
			with
		layout120 (leftWidth, rightWidth)
			with
		layout84 (leftWidth, rightWidth)
			with
		layout48 (leftWidth, rightWidth)
			with
		estimateform
			layout84
		contact
			layout84
	*/
	
	function menu() {
		$arr = array(
			array(
				"title" => "{{ __('menu_home') }}",
				"url" => Router::make("index"),
				"urls" => array(
					"/",
				),
				"hidden" => true,
			),
			array(
				"title" => "{{ __('menu_offers') }}",
				"url" => Router::make("offers"),
				"urls" => array(
					"offers", "offer",
				),
			),
			array(
				"title" => "{{ __('news') }}",
				"url" => Router::make("news"),
				"urls" => array(
					"news",
				),
			),
			array(
				"title" => "{{ __('menu_contact') }}",
				"url" => Router::make("contact"),
				"urls" => array(
					"contact",
				),
			),
		);
		
		foreach ($arr AS &$menu) $menu["active"] = in_array(Router::get("name"), $menu['urls']) ? "active " : "";
		
		return array("menus" => $arr);
	}
	
	function footer() {
		return array("footer" => true);
	}
	
	function without() {
		return array("hideFooter" => true, "hideMenu" => true, "hideMenuLink" => true);
	}
	
	function with() {
		return array_merge(array_merge($this->footer(), $this->menu()), array("cookiesAllowed" => isset($_COOKIE['zekom']) && $_COOKIE['zekom'] == "confirm"));
	}
	
	function index() {
		return array_merge($this->with(), array(
			"contentRight" => 
				Core::loadView("video", "default") . 
				Core::loadView("opinions", "default") .
				Core::loadView("enews","mailchimp") .
				Core::loadView("index","news", array('title' => "{{ __('heading_whatsupp') }}", 'length' => 'span6')) .
				Core::loadView("official", "partners"),
			"leftWidth" => "",
			"rightWidth" => "",
			"gaExperiment" => Core::loadView("gaexperiment", "default"),
			"homepage" => true,
			"prehead" => Core::loadView("loginModal", "users"),
		));
	}
	
	function full() {
		return array_merge($this->with(), array(
			"leftWidth" => "span12",
			"rightWidth" => "span0",
		));
	}
	
	function estimateform() {
		return $this->layout84();
	}
	
	function layout120() {
		return array_merge($this->with(), array(
			"leftWidth" => "span12",
			"rightWidth" => "span0",
		));
	}
	
	function layout84() {
		return array_merge($this->with(), array(
			"leftWidth" => "span8",
			"rightWidth" => "span4",
		));
	}

	function layout48() {
		return array_merge($this->with(), array(
			"leftWidth" => "span4",
			"rightWidth" => "span8",
		));
	}
	
	function rightsidebar() {
		return array_merge($this->with(), array(
			"leftWidth" => "span7",
			"rightWidth" => "span4 offset1",
		));
	}
	
	function leftsidebar() {
		return array_merge($this->layout48(), array(
			"contentRight" => Core::loadView("profile", "orders"),
		));
	}
	
	function contact() {
		return $this->layout120();
	}
	
	function __beforeLoad() {
		Optimize::addFile("js", array(
			"js/jquery-1.8.3.min.js",
			"js/jquery-ui-1.9.2.custom.min.js",
			"js/jquery.validationEngine.js",
			"js/colorbox/jquery.colorbox.js",
			"js/maestro/func.js",
			"js/bootstrap.min.js",
			"js/bootstrap-image-gallery.min.js",
			"js/default.js",
			"js/cookie.js",
			"js/ga.js",
			"js/oldbrowser.js",
			"vendor/eternicode/bootstrap-datepicker/js/bootstrap-datepicker.js",
		));

		Optimize::addFile("css", array(
			"css/default.css",
			"css/colorbox.css",
			"css/bootstrap.css",
			"css/bootstrap-responsive.css",
			"css/bootstrap-image-gallery.min.css",
			"vendor/eternicode/bootstrap-datepicker/css/datepicker.css",
		), 2);
	}

	function null() {
		$this->setContent(null);
	}
	
	function __afterLoad() {
		return array(
			"loggedIn" => Auth::isLoggedIn(),
			"optimizeCss" => Optimize::getHTML("css"),
			"optimizeJs" => Optimize::getHTML("js"),
			"secondColumn" => Core::loadView("footer", "partners"),
			"seo" => SEO::getHTML(),
		);
	}
}

?>