<?php

class MaestroLayout extends UFWLayout {
	function __construct() {
		$this->authStatus = Auth::getUser("status_id");

		if (!$this->authStatus || !Auth::isLoggedIn() || !in_array($this->authStatus, array(1,3))) {
			header("Location: " . Auth::getLoginURL());
			die();
		}

		$this->setByStatus();
	}

	function setByStatus() {
		if ($this->authStatus == 1) { // super admin
			$this->arrModules = array(
				"" => "Index",
				"galleries" => "Galleries",
				"games" => "Games",
				"mails" => "Mails",
				"microphones" => "Microphones",
				"news" => "News",
				"offers" => "Offers",
				"orders_bills" => "Bills",
				"polls" => "Polls",
				"pages" => "Pages",
				"settings" => "Settings",
				"translations" => "Translations",
				"users" => "Users",
				"files" => "Files",
				"abbreviations" => "Short URLs",
			);
		} else if ($this->authStatus == 3) { // admin
			$this->arrModules = array(
				"" => "Index",
				"galleries" => "Galleries",
				"games" => "Games",
				"mails" => "Mails",
				"microphones" => "Microphones",
				"news" => "News",
				"offers" => "Offers",
				"orders_bills" => "Bills",
				"polls" => "Polls",
				"pages" => "Pages",
				"translations" => "Translations",
				"users" => "Users",
				"files" => "Files",
				"abbreviations" => "Short URLs",
			);
		} else if ($this->authStatus == 4) { // pr
			$this->arrModules = array(
				"galleries" => "Galleries",
				"news" => "News",
				"polls" => "Polls",
				"pages" => "Pages",
				"translations" => "Translations",
				"files" => "Files",
				"abbreviations" => "Short URLs",
			);
		} else {
			Status::code(404, "You don't have privileges");
		}
	}

	function null() {
		$this->setContent(NULL);
	}

	function index() {
		if (Router::get("name") != 'elfinderFrontend') {
			Optimize::addFile("css", array(
				"css/maestro/bootstrap.min.css",
			));
		} else {
			Core::setGlobal(array("menu" => FALSE));
		}
		
		Optimize::addFile("css", array(
			"css/maestro/ui-lightness/jquery-ui-1.9.2.custom.min.css",
			"css/maestro.css",
			"css/colorbox.css",
			"css/maestro/jquery.tagit.css",
			"css/maestro/jquery.Jcrop.css",
			//"css/bootstrap-datetimepicker.min.css",
		));

		Optimize::addFile("js", array(
			"js/maestro/jquery-1.8.3.min.js",
			"js/maestro/jquery-ui-1.9.2.custom.min.js",
			"js/maestro/bootstrap.min.js",
			"js/maestro/validate.v1.1.min.js",
			"js/maestro/func.js",
			"js/jquerytimepicker.js",
			//"js/bootstrap-datetimepicker.min.js",
			"js/maestro/tablesorter.jquery.js",
			"js/maestro/jquery.tagit.js",
			"js/colorbox/jquery.colorbox.js",
			"js/maestro/jquery.Jcrop.js",
			"js/maestro/jquery.color.js",
			"js/jquery.multiselect.min.js",
			"js/maestro/jqueryfileupload/jquery.fileupload.js",
			"js/maestro/jqueryfileupload/jquery.iframe-transport.js",
			"js/maestro/maestro.js",
		));

		Core::setParseVar("optimize", Optimize::getHTML());
		
		Core::setParseVar("user", $_SESSION['User']['email']);

		Core::loadModule("statuses");
		Core::setParseVar("user_status", Core::getModel("statuses")->get("first", null, array("id" => $_SESSION['User']['status_id'])));
		//Core::setParseVar("user_status", $_SESSION['User']['email']);
	}

	function __afterLoad() {
		if (!isset(Core::$globals["menu"]) || Core::$globals["menu"] == TRUE) {
	
			$pageTitle = "GNP admin panel";
	
			foreach ($this->arrModules AS $key => $val) {
				$this->parse(array(
					"url" => $key,
					"text" => $val,
					"active" => Core::url(1) == $key ? "active" : NULL,
					"add" => $key == "files" ? ' class="popUpFiles"' : NULL,
				), "navigation");
	
				Core::url(1) == $key ? $pageTitle .= ' - ' . $val : NULL;
			}
	
			if (!is_null(Core::url(1)) && Core::url(2) == "") {
				$pageTitle .= ' - List';
			} elseif (Core::url(2) == "edit") {
				$pageTitle .= ' - Edit';
			}  elseif (Core::url(2) == "add") {
				$pageTitle .= ' - Add';
			}
	
			Core::setParseVar("title", $pageTitle);
			Core::setParseVar("sample tags", NULL);
			Core::setParseVar("left width", "span2");
			Core::setParseVar("right width", "span10");
			
			$this->parse(array(
				"navi" => $this->parse(array(
					"navigation" => $this->display("navigation"),
				), "navi"),
			));
		} else {
			Core::setParseVar("title", NULL);
			Core::setParseVar("sample tags", NULL);
			Core::setParseVar("left width", "");
			Core::setParseVar("right width", "span12");
			
			$this->parse(array(
				"navi" => NULL,
			));
		}
	}
}

?>