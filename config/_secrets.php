<?php

ini_set('memory_limit', '512M');

define('PASSHASH', 'e618a5easd246a3sdf3bb7cd8dcc231a'); // set it once and don't change it else you have to reset all passwords in database

Core::configDatabase("Mysql");

require_once "secrets.byhost.php";

define('URL', strtolower(PROTOCOL) . '://' . DOMAIN . "/");

// set defaults
Core::configDefaults(array(
    "site title" => SITE,
    "charset" => "utf-8",
    "keywords" => NULL,
    "description" => NULL,
));

// load helpers
Core::configHelpers(array(
    "Auth",
    "Cache",
    "Image",
    "Mail",
    "Optimize",
    "Paginator",
    "SloDate",
    "Social",
    "Upload",
    "HTML",
    "SEO",
    "JSON",
));

DB::config(Settings::get("db_config"));

SEO::setFacebook(TRUE);
SEO::image(URL . "img/logo_200x200.png");

Settings::set("facebook_appid", 165353900311394);
Settings::set("ga_id", "41755673-1");

// config other modules
Auth::setLoginUrl("/prijava");
Auth::setLogoutUrl("/odjava");
Auth::setSuccessLoginRedirect("/profil");
Auth::setSuccessLogoutRedirect("/");
Auth::setUserField("email");

// payment methods
Settings::set("payment_methods", array(
    1 => array(
        "title" => "UPN",
        "slug" => "upn",
    ),
    2 => array(
        "title" => "Moneta",
        "slug" => "moneta",
    ),
    3 => array(
        "title" => "Paypal",
        "slug" => "paypal",
    ),
));

Settings::set("payment_bills", array(
    1 => array(
        "title" => "Rezervacija",
        "slug" => "reservation",
    ),
    2 => array(
        "title" => "Obroki",
        "slug" => "portions",
    ),
    3 => array(
        "title" => "Celotno plačilo",
        "slug" => "whole",
    )
));

Settings::set("tomorrowland_portions", array(
    /*0 => array(
        "price" => 19.90,
        "due date" => "2014-02-16",
        "num" => "",
        "type" => 1,
    ),*/
    1 => array(
        "price" => 250.00,
        "due date" => "2014-02-17",
        "num" => "1",
        "type" => 2,
    ),
    2 => array(
        "price" => 200.00,
        "due date" => "2014-03-10",
        "num" => "2",
        "type" => 2,
    ),
    3 => array(
        "price" => 150.00,
        "due date" => "2014-04-10",
        "num" => "3",
        "type" => 2,
    ),
));