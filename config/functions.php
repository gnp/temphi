<?php

function makePrice($num, $dec = 2, $currency = " €") {
	return number_format((double)$num, $dec) . $currency;
}

function makeDate($date, $format = "Y-m-d") {
	if (is_null($date) || $date == DT_NULL) return NULL;
	
	return date($format, strtotime($date));
}

function numToString($num) {
	$num = (int)$num;
	return $num > 9999
		? $num
		: ($num > 999
			? "0" . $num
			: ($num > 99
				? "00" . $num
				: ($num > 9
					? "000" . $num
					: ("0000" . $num))));
}
            
function bold($txt, $bold = TRUE) {
	return $bold == TRUE
		? '<b>' . $txt . '</b>'
		: $txt;
}

function nobr($txt) {
	return '<nobr>' . $txt . '</nobr>';
}

function dump($obj) {
	echo "<pre>";
	var_export($obj);
	echo "</pre>";
}

?>