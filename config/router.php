<?php

Router::add("/potovanja", array("module" => "offers", "view" => "all", "layout" => "main:full", "name" => "offers"));
Router::add("/[url]/ponudba/[id:int]", array("module" => "offers", "view" => "one", "layout" => "main:layout84", "name" => "offer"));
Router::add("/novice", array("module" => "news", "view" => "all", "layout" => "main:full", "name" => "news"));
Router::add("/novice/[year:int]", array("module" => "news", "view" => "all", "layout" => "main:full", "name" => "news"));
Router::add("/[url]/novica/[id:int]", array("module" => "news", "view" => "one", "layout" => "main:layout84", "name" => "novica"));
Router::add("/[url]/galerija/album/[id:int]", array("module" => "galleries", "view" => "album", "layout" => "main:full", "name" => "album"));
Router::add("/[url]/[id:int]/galerija", array("module" => "news", "view" => "one", "layout" => "main:rightsidebar", "name" => "galerija"));


Router::add("/profil", array("module" => "users", "view" => "profile", "layout" => "main:leftsidebar", "name" => "profil"));

Router::add("/[module::games]/[view::saveAnswer]", array("layout" => NULL));

Router::add("/[module::users]/[view::getUserData]", array("layout" => NULL));
Router::add("/[module::users]/[view::json]/[what::editprofile|updateprofile|editpassword|updatepassword]", array("layout" => NULL));

Router::add("/narocilnica", array("module" => "orders", "view" => "orderform", "layout" => "main:full", "name" => "narocilnica"));
Router::add("/narocilnica/json/[action:packetchange|addcustomer]", array("module" => "orders", "view" => "json", "layout" => NULL, "name" => "narocilnicajson"));
Router::add("/predracun", array("module" => "orders", "view" => "estimateform", "layout" => "main:none", "name" => "predracun"));
Router::add("/predracun/json/[action:portions]", array("module" => "orders", "view" => "json", "layout" => "main:none", "name" => "predracunjson"));
Router::add("/izbira-placilnega-sredstva", array("module" => "orders", "view" => "paymentform", "layout" => (isset($_POST) ? "main:none" : "main:full"), "name" => "placilna sredstva"));
Router::add("/izbira-placilnega-sredstva/[hash]", array("module" => "orders", "view" => "choosepayment", "layout" => "main:full", "name" => "placilna sredstva hash"));
Router::add("/placaj-z-moneto/[hash]", array("module" => "moneta", "view" => "saveorder", "layout" => "moneta", "name" => "moneta takeoff"));
Router::add("/placaj-s-paypal/[hash]", array("module" => "paypal", "view" => "saveorder", "layout" => "moneta", "name" => "paypal takeoff"));
Router::add("/placaj-z-upn/[hash]", array("module" => "orders_bills", "view" => "upnsepa", "layout" => "main:full", "name" => "upn takeoff"));
Router::add("/placaj-z-braintree/[hash]", array("module" => "braintree", "view" => "startpayment", "layout" => "main:full", "name" => "braintree takeoff"));
Router::add("/potrditev-placila/[provider::paypal|moneta|upn|braintree]/[hash]", array("module" => "orders", "view" => "confirmform", "layout" => "main:full", "name" => "confirmform"));

Router::add("/poglej-narocilo/[id:int]", array("module" => "orders", "view" => "viewform", "layout" => "main:estimateform", "name" => "poglednarocila"));
Router::add("/uredi-narocilo/[hash]", array("module" => "orders", "view" => "editform", "layout" => "main:full", "name" => "uredi narocilo"));
Router::add("/posodobi-narocilo/[hash]", array("module" => "orders", "view" => "updateform", "layout" => NULL, "name" => "posodobi narocilo"));
Router::add("/poglej-pdf/[id:int]", array("module" => "orders", "view" => "outputpdf", "layout" => "main:without", "name" => "view pdf"));


Router::add("/galerija", array("module" => "galleries", "view" => "gallery", "layout" => "main:full", "name" => "galleries"));
Router::add("/kontakt", array("layout" => "main:contact", "module" => "default", "view" => "contact", "name" => "contact"));
Router::add("/private-party-pack/3/stran", array("layout" => "main:contact", "module" => "default", "view" => "privatePartyPack", "name" => "privatePartyPack"));
Router::add("/party-prevozi/4/stran", array("layout" => "main:contact", "module" => "default", "view" => "partyPrevozi", "name" => "partyPrevozi"));
Router::add("/send-contact", array("layout" => NULL, "module" => "default", "view" => "sendContact"));
Router::add("/add-mailchimp", array("layout" => NULL, "module" => "mailchimp", "view" => "addSubscriber"));
Router::add("/404", array("layout" => "main:full", "module" => "default", "view" => "r404", "name" => "404"));

Router::add("/[url]/[id:int]/stran", array("name" => "page", "module" => "pages", "view" => "one", "layout" => "main:layout84"));

// Auth class
Router::add("/prijava", array("module" => "users", "view" => "login", "layout" => "main:full"));
Router::add("/users/takelogin", array("module" => "users", "view" => "takelogin", "layout" => "main"));
Router::add("/odjava", array("module" => "users", "view" => "logout"));
Router::add("/logout", array("module" => "users", "view" => "logout"));
Router::add("/users/login", array("module" => "users", "view" => "takeloginJSON", "layout" => NULL));

// Moneta
Router::add("/moneta/[view:narocilo|potrditev|saveorder]", array("layout" => "moneta", "module" => "moneta"));
Router::add("/paypal/[view::startpayment|confirmpayment|cancelpayment]/[hash]", array("name" => "paypal", "layout" => "moneta", "module" => "paypal"));
Router::add("/braintree/[view::startpayment|confirmpayment|cancelpayment]/[hash]", array("name" => "braintree", "layout" => "main:full", "module" => "braintree"));

// Img cache
Router::add("/cache/img/[type::w|r|h]/[value:int]/*/*/*/*/*/*/*/*/*", array("layout" => NULL, "module" => "default", "view" => "cache"));

// Redirect
Router::add("/redirect/[short]", array("layout" => NULL, "module" => "abbreviations", "view" => "redirect"));

// Redirect
Router::add("/log-error", array("layout" => NULL, "module" => "default", "view" => "logError"));

// Cron
Router::add("/cron/backup/files", array("layout" => null, "module" => "cron", "view" => "backupFiles"));
Router::add("/cron/backup/db", array("layout" => null, "module" => "cron", "view" => "backupDb"));
Router::add("/cron/late-reservation", array("layout" => null, "module" => "cron", "view" => "warnLateReservations"));
Router::add("/cron/set-payed-orders", array("layout" => null, "module" => "cron", "view" => "setPayedOrders"));

// load only non admin routes - one of security steps
if (true || strpos($_SERVER['HTTP_HOST'], "admin") !== false) {
  // Toggle actions
  Router::add("/offers/[view:togglefirstpage|toggletop]/[id:int]/[active::0|1]", array("layout" => "maestro:null", "module" => "offers"));
  Router::add("/[module:users]/[view:toggleenabled]/[id:int]/[active::0|1]", array("layout" => "maestro:null"));
  Router::add("/[module:offers|members|categories|feedbacks|games|microphones|galleries|news|packets|packets_tabs|partners|teams]/[view:togglepublished]/[id:int]/[active::0|1]", array("layout" => "maestro:null"));

  //
  Router::add("/[module::orders_users]/[view::packetchangediff|packetchangedadditions]", array("layout" => "maestro:null"));
  Router::add("/[module::mails]/[view::getjson]/[id:int]", array("layout" => "maestro:null"));
  Router::add("/maestro/[module::orders_users]/[view::confirm|reject|cancel]/[id:int]", array("layout" => "maestro"));
  Router::add("/maestro/[module::orders_bills]/[view::confirm|calculateReturn|postCalculateReturn|mail]/[id:int]", array("layout" => "maestro"));
  Router::add("/maestro/[module::orders_bills]/[view::postCalculateReturn|manualInsert]", array("layout" => NULL));
  Router::add("/maestro/[module::orders]/[view::recalculateBills|generateEstimate|downloadEstimate|generateBill|downloadBill|downloadLog|downloadVoucher|generateVoucher|confirmchanges|cancel|reject|confirm]/[id:int]", array("layout" => NULL));
  Router::add("/maestro/[module::orders]/[view::exportxls]/[ids]", array("layout" => NULL));
  Router::add("/maestro/[module::orders]/[view::generateAllVouchers]", array("layout" => NULL, "module" => "orders"));

  // Update positions
  Router::add("/maestro/[module::galleries|offers]/[view::updatemediapositions]", array("layout" => "maestro:null"));
  Router::add("/maestro/[module::additions|categories|packets|packets_tabs|members|offers]/[view::updatepositions]", array("layout" => "maestro:null"));
  Router::add("/maestro/[module::offers]/[view::position]/[by::index|listing]", array("layout" => "maestro"));

  Router::add("/[module::packets_tabs]/[view::getpredefined]", array("layout" => "maestro:null"));

  // Elfinder
  Router::add("/maestro/files", array("module" => "default", "view" => "elFinderFrontend", "layout" => "elfinder", "name" => "elfinderFrontend"));
  Router::add("/maestro/[module::galleries_pictures|offers_pictures]/[view::insertelfinder]", array("layout" => "maestro:null"));
  Router::add("/elfinder", array("module" => "default", "view" => "elFinderConnector", "layout" => "maestro:null"));

  // Maestro
  Router::add("/maestro/vouchers", array("layout" => "maestro", "module" => "orders", "view" => "vouchers"));
  Router::add("/maestro/[module]/[view::edit|view]/[id:int]", array("layout" => "maestro"));
  Router::add("/maestro/[module]/[view::maestro]/[id:int]", array("layout" => "maestro"));
  Router::add("/maestro/[module]/[view::delete]/[id:int]", array("layout" => "maestro:null"));
  Router::add("/maestro/[module]/[view::add|insert|update]", array("layout" => "maestro"));
  Router::add("/maestro/[module]/[view::maestro]", array("layout" => "maestro"));
  Router::add("/maestro/[module]", array("layout" => "maestro", "view" => "maestro"));
  Router::add("/maestro", array("layout" => "maestro", "view" => "indexmaestro", "module" => "default"));
  
  // Custom maestro
  Router::add("/maestro/[module::packets|packets_tabs|members|orders|orders_users]/[view::add]/[id:int]", array("layout" => "maestro"));
  Router::add("/maestro/[module::mails]/[view::compose|send]", array("layout" => "maestro"));

  Router::add("/loginByUserID/[id:int]", array("module" => "users", "view" => "loginByUserID"));
  Router::add("/tomorrowland/autologin/[hash]", array("module" => "users", "view" => "tomorrowlandautologin"));
  Router::add("/orders/testPortion", array("module" => "orders", "view" => "testPortion", "layout" => NULL));
  Router::add("/sendtomorrowlandmails", array("module" => "users", "view" => "sendtomorrowlandmails"));
}

// Default routing + maestro protection
Router::add("/[module:*|!maestro]/[view:*|!maestro|!add|!insert|!edit|!updata|!delete]", array("layout" => "main"));
	
Router::add("/", array("module" => "offers", "view" => "all", "layout" => "main:full", "name" => "offers"));
// Index
//Router::add("/", array("layout" => "main:index", "module" => "offers", "view" => "index", "name" => "index"));
?>
