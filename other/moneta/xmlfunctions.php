<?php

	//################################################################
	//# MakeOrderHead  D: Order head
	//################################################################ 
	function MakeOrderHead($sTaxNumber, $sFirstName, $sLastName, $sCompanyName, $sStreet, $sHouse, $sPostCode, $sCity, $sCountry, $sTelephone, $sEmail, $sPrice) {
		$sXML = "";
		$EOL = "\r\n";
		$Date = "".gmdate("d.m.Y");
		$sCurrency = "EUR";

		$sXML ="<meta name=\"Order\" content=\"".$EOL.
			"<ORDER>".$EOL.
			"<ORDER_HEAD>".$EOL.
			"  <CustomerTaxNumber>".$sTaxNumber."</CustomerTaxNumber>".$EOL.
			"  <CustomerFirstName>".$sFirstName."</CustomerFirstName>".$EOL.
			"  <CustomerMiddleName></CustomerMiddleName>".$EOL.
			"  <CustomerLastName>".$sLastName."</CustomerLastName>".$EOL.
			"  <CustomerNameSuffix></CustomerNameSuffix>".$EOL.
			"  <CustomerCompanyName>".$sCompanyName."</CustomerCompanyName>".$EOL.
			"  <CustomerStreet>".$sStreet."</CustomerStreet>".$EOL.
			"  <CustomerHouse>".$sHouse."</CustomerHouse>".$EOL.
			"  <CustomerPostCode>".$sPostCode."</CustomerPostCode>".$EOL.
			"  <CustomerCity>".$sCity."</CustomerCity>".$EOL.
			"  <CustomerState></CustomerState>".$EOL.
			"  <CustomerCountry>".$sCountry."</CustomerCountry>".$EOL.
			"  <CustomerTelephone>".$sTelephone."</CustomerTelephone>".$EOL.
			"  <CustomerFax></CustomerFax>".$EOL.
			"  <CustomerEMail>".$sEmail."</CustomerEMail>".$EOL.
			"  <DeliveryFirstName>".$sFirstName."</DeliveryFirstName>".$EOL.
			"  <DeliveryMiddleName></DeliveryMiddleName>".$EOL.
			"  <DeliveryLastName>".$sLastName."</DeliveryLastName>".$EOL.
			"  <DeliveryNameSuffix></DeliveryNameSuffix>".$EOL.
			"  <DeliveryCompanyName>".$sCompanyName."</DeliveryCompanyName>".$EOL.
			"  <DeliveryStreet>".$sStreet."</DeliveryStreet>".$EOL.
			"  <DeliveryHouse>".$sHouse."</DeliveryHouse>".$EOL.
			"  <DeliveryPostCode>".$sPostCode."</DeliveryPostCode>".$EOL.
			"  <DeliveryCity>".$sCity."</DeliveryCity>".$EOL.
			"  <DeliveryState></DeliveryState>".$EOL.
			"  <DeliveryCountry>".$sCountry."</DeliveryCountry>".$EOL.
			"  <DeliveryEMail>".$sEmail."</DeliveryEMail>".$EOL.
			"  <DeliveryTelephone>".$sTelephone."</DeliveryTelephone>".$EOL.
			"  <DeliveryFax></DeliveryFax>".$EOL.
			"  <Currency>".$sCurrency."</Currency>".$EOL.
			"  <Price>".$sPrice."</Price>".$EOL.
			"  <DeliveryPrice>0</DeliveryPrice>".$EOL.
			"  <DateFrom>".$Date."</DateFrom>".$EOL.
			"  <DateTo>".$Date."</DateTo>".$EOL.
			"  <OrderNumberInternal>3dc0</OrderNumberInternal>".$EOL.
			"  <OrderCreated>".$Date."</OrderCreated>".$EOL.
			"  <NotificationDate>".$Date."</NotificationDate>".$EOL.
			"  <DeliveryType>OWN</DeliveryType>".$EOL.
			"</ORDER_HEAD>".$EOL.
			"<ORDER_LINE>";
		return $sXML;
	}
	
	//################################################################
	//# MakeOrderLine  D: add order line
	//################################################################ 
	function MakeOrderLine($sDescription, $sPrice, $sTaxRate, $sQuantity, $sUnit, $sArticleNumber) {
		$sXML = "";
		$EOL = "\r\n";
	 
		$nPriceNoTax    = round((Functions_CDbl($sPrice) * 100) / (100 + Functions_CDbl($sTaxRate)), 2);
		$nPriceSum      = round((Functions_CDbl($sPrice) * Functions_CInt($sQuantity)), 2);    
		$nPriceSumNoTax = round((Functions_CDbl($nPriceSum) * 100) / (100 + Functions_CDbl($sTaxRate)), 2);
		$nPriceTax      = round($nPriceSum - $nPriceSumNoTax, 2);
	  
		$sXML = "<PRODUCT>".$EOL.
			"   <PageDescription>".$sDescription."</PageDescription>".$EOL.
			"   <Price>".$sPrice."</Price>".$EOL.
			"   <PriceNoTax>".$nPriceNoTax."</PriceNoTax>".$EOL.
			"   <TaxRate>".$sTaxRate."</TaxRate>".$EOL.
			"   <Quantity>".$sQuantity."</Quantity>".$EOL.
			"   <PriceSum>".$nPriceSum."</PriceSum>".$EOL.
			"   <PriceSumNoTax>".$nPriceSumNoTax."</PriceSumNoTax>".$EOL.
			"   <PriceTax>".$nPriceTax."</PriceTax>".$EOL.
			"   <Unit>".$sUnit."</Unit>".$EOL.
			"   <ArticleNumber>".$sArticleNumber."</ArticleNumber>".$EOL.
			" </PRODUCT>";
		return $sXML;
	}
	
	//################################################################
	//# MakeOrderEnd  D: Close XML order
	//################################################################ 
	function MakeOrderEnd() {
		$sXML= "";
		$EOL = "\r\n";
		$sXML= "  </ORDER_LINE>".$EOL."</ORDER>\">";
		return $sXML;
	}
		
?>