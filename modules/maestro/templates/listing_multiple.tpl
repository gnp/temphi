##BREADCRUMBS##
<legend>##TITLE##</legend>
##FILTER##
<table class="table table-striped table-condensed" id="maestroList##ID##">
	<thead>
		<tr>
			##TH START##<th>##TXT##</th>##TH END##
			<th>##BTN HEADER START##<a title="##TIT##" href="##URL##" class="btn btn-##CLASS## btn-small" style="font-weight: normal;"##ADD##>##TXT##</a>##BTN HEADER END##</th> 
		</tr>
	</thead>
	<tbody class="##TBODY CLASS##">
		##ROW START##
		<tr class="##CLASS##">
			##TD START##<td>##TXT##</td>##TD END##
			<td>##BTN ROW START##<a title="##TIT##" href="##URL##" class="btn btn-##CLASS## btn-small"##ADD##>##TXT##</a>##BTN ROW END##</td>
		</tr>
		##ROW END##
	</tbody>
</table>
<div class="row" style="margin-left: 0px;">
##LEGEND START##
	<div class="span3">
		<table class="table table-striped table-condensed">
			<tr><th>##LEGEND TITLE##</th></tr>
			##LEGEND ROW START##<tr class="##CLASS##"><td>##TITLE##</td></tr>##LEGEND ROW END##
		</table>
	</div>
##LEGEND END##
</div>
##APPEND##