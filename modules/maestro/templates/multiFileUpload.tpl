<div class="control-group">
    <label class="control-label">Nalaganje datotek</label>
    <div class="controls">
        <input type="file" value="Dodaj datoteke" id="##ID##" class="btn btn-success" name="files" multiple />
        <p>
            <div class="progress progress-success progress-striped active">
                <div class="bar" style="width: 0%;"></div>
            </div>
        </p>
    </div>
</div>