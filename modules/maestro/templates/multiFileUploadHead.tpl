<script>
$(document).ready(function () {
    $('###ID##').fileupload({
        dataType: 'json',
        url: '##URL##',
        autoUpload: true,
        maxNumberOfFiles: ##MAX NUM##,
        formdata: ##FORM DATA##,
	    progressall: function (e, data) {
	    	percent = parseInt(data.loaded / data.total * 100, 10) + '%';
	        $('###ID##').parent().find(".progress .bar").css('width', percent).html(percent);

	        if (percent == '100%') {
	        	setTimeout(function() {
	        		$('###ID##').parent().find(".progress .bar").css('width', '0%').html("");
					$("#maestroFormSubmit").removeClass("disabled");
	        	}, 2500);
	        }
	    },
	    start: function (e, data) {
			$("#maestroFormSubmit").addClass("disabled");
	    }
    }).bind('fileuploaddone', function (e, data) {
		response(data.result);
		//var_dump(data.result.files);
	});
});
</script>