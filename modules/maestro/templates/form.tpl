##BREADCRUMBS##
<legend>##TITLE##</legend>
<form method="POST" enctype="multipart/form-data" action="##ACTION##" class="form-horizontal" name="maestroForm">
    ##CONTENT##
	<div class="form-actions">
		##BUTTONS##
	</div>
</form>