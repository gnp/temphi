<legend>##TITLE## List</legend>
<table class="table table-striped">
	<thead>
		<tr>
			<th>ID</th>
			<th></th>
			##BTN ADD START##<th><a href="/maestro/##MODULE##/add" class="btn btn-success" style="font-weight: normal;">Add</a></th>##BTN ADD END## 
		</tr>
	</thead>
	<tbody>
		##ROW START##
		<tr>
			<td>##ID##</td>
			<td>##TITLE##</td>
			<td>
				<a href="/maestro/##MODULE##/edit/##ID##" class="btn btn-warning">Edit</a>
				<a href="/maestro/##MODULE##/delete/##ID##" class="btn btn-danger" onclick="return confirm('Delete ###ID##?');">Delete</a>
			</td>
		</tr>
		##ROW END##
	</tbody>
</table>