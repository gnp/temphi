<?php

class MaestroController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	// self made maestro router @ToDo
	function index() {
		Auth::isLoggedIn(TRUE);
		
		if (Auth::getUser("status_id") == 2)
			Status::redirect(Router::make("providers-offers-list"));
		
		// array of auto allowed modules
		$modules = array();
		if ($_SESSION['User']['status_id'] == 1)
			$modules = array("teams", "members", "games_answers", "games_options", "games", "polls_answers", "polls_options", "orders", "mails_sents", "feedbacks", "cancels_types", "galleries_videos", "galleries_pictures", "mails", "additions", "users", "statuses", "partners", "news", "polls", "settings", "galleries", "offers", "countries", "cities", "microphones", "categories");

        else Status::code("404");

		// array of auto allowed views
		$views = array("add", "insert", "edit", "update", "delete", "", "maestro", "view", "updatepositions", "updatemediapositions", "exportxls", "cancel", "reject", "confirm");
		$viewsId = array("edit", "delete", "view");

		// default content
		$content = FALSE;

		// if it's permitted action ...
		if (in_array(Core::url(1), $modules) && in_array(Core::url(2), $views)) {
			die("Module maestro! Not good!");
			if (!is_null(Core::url(3)) && Validate::isInt(Core::url(3)))
				Core::setGlobal(array("id" => Core::url(3)));

			if (in_array(Core::url(2), $viewsId) && is_null(Core::getGlobal("id"))){
			} else {
				$content = Core::loadView(is_null(Core::url(2)) ? "maestro" : Core::url(2), Core::url(1), NULL, FALSE);

				switch(Core::url(2)) {
                    case "view":
                        if (!is_array($content))
                            break;
                        
                        $tpl = new Template("view", "maestro");
                        $content = $tpl->parse(array(
                            "title" => $content['title'],
                            "content" => $content['content'],
                        ));
                    break;
					case "add":
						if (!is_array($content))
							break;
						
						$tpl = new Template("form", "maestro");
						$content = $tpl->parse(array(
							"title" => "Add " . $content['title'],
							"content" => $content['content'],
							"action" => Core::url(1) . "/insert",
						)) . $this->{Core::toCamel(Core::url(1))}->genValidateJs();
					break;
					case "edit":
						if (!is_array($content))
							break;
											
						$tpl = new Template("form", "maestro");
						$content = $tpl->parse(array(
							"title" => "Edit " . $content['title'],
							"content" => $content['content'],
							"action" => Core::url(1) . "/update",
						)) . $this->{Core::toCamel(Core::url(1))}->genValidateJs();
					break;
					case "insert":
						if (Validate::isInt(Core::getGlobal("id"))) {
							Debug::addSuccess("Successfully added #" . Core::getGlobal("id") . ".", FALSE);
							header("Location: /maestro/" . Core::url(1) . "/edit/" . Core::getGlobal("id"));
						} else {
							Debug::addError("Error adding.", FALSE);
							header("Location: /maestro/" . Core::url(1));
						}
						die();
					break;
					case "update":
						if (isset($content['redirect']))
							header("Location: " . $content['redirect']);
						else if (isset($content['id']) && Validate::isInt($content['id'])) {
							Debug::addSuccess("Successfully edited #" . $content['id'] . ".", FALSE);
							header("Location: /maestro/" . Core::url(1) . "/edit/" . $content['id']);
						} else {
							Debug::addError("Error editing #" . $content['id'] . ".", FALSE);
							header("Location: /maestro/" . Core::url(1));
						}
						die();
					break;
					case "delete":
						header("Location: /maestro/" . Core::url(1));
						die();
					break;
					case "":
					case "maestro":
						if (isset($content['th'])) {
							$content = $this->makeListing($content);
						} else {
							$tpl = new Template("listing", "maestro");
						
							$content = $tpl->parse(array(
								"btn add" => !isset($content['conf']['btn add']) || $content['conf']['btn add'] == TRUE
									? $tpl->parse(array(
											"module" => Core::url(1),
										), "btn add")
									: NULL,
								"row" => $content['content'],
								"title" => $content['title'],
								"module" => Core::url(1),
							));
						}
					break;
				}
			}
		}

		// if content is null or false/default then return maestro/index.tpl
		if (!$content) {
			$tpl = new Template("index", "maestro");
			return $tpl->display();
		} else // else return content
			return $content;
	}
	
	function makeMaestro($content) {
	    if (isset($content['th'])) {
            $content = $this->makeListing($content);
        } else {
            $tpl = new Template("listing", "maestro");
        
            $content = $tpl->parse(array(
                "btn add" => !isset($content['conf']['btn add']) || $content['conf']['btn add'] == TRUE
                    ? $tpl->parse(array(
                            "module" => Router::get("module"),
                        ), "btn add")
                    : NULL,
                "row" => $content['content'],
                "title" => $content['title'],
                "module" => !isset($content['module']) ? Router::get("module") : $content['module'],
            ));
        }
	}
	
	function makeAdd($content) {
	    $tpl = new Template("form", "maestro");
        return $tpl->parse(array(
            "title" => "Add " . $content['title'],
            "content" => $content['content'],
            "breadcrumbs" => !isset($content['breadcrumbs']) ? $this->makeBreadcrumbs(array(
				Core::toCamel(Router::get("module")) => "/maestro/" . Router::get("module"),
				"Add " . $content['title'] => "#",
			)) : $content['breadcrumbs'],
            "action" => (!isset($content['action']) 
            	? "/maestro/" . Router::get("module") . "/insert"
				: $content['action']),
            "buttons" => isset($content['buttons']) ? $content['buttons'] : ('<input type="submit" value="Save" class="btn btn-success pull-right" id="maestroFormSubmit" />' . '<input type="button" value="Cancel" class="btn btn-warning pull-right" id="maestroFormCancel" style="margin-right: 10px;" />')
        )) . $this->{Core::toCamel(Router::get("module"))}->genValidateJs();
	}
	
	function makeView($content) {
	    $tpl = new Template("view", "maestro");
        return $tpl->parse(array(
            "title" => $content['title'],
            "content" => $content['content'],
        ));
	}
	
	function makeInsert($content) {
        if (Validate::isInt($content['id'])) {
            Debug::addSuccess("Successfully added #" . $content['id'] . ".");
            Status::redirect("/maestro/" . Router::get("module") . "/edit/" . $content['id']);
        } else {
            Debug::addError("Error saving data.");
            Status::redirect("/maestro/" . Router::get("module") . "/maestro");
        }
	}
	
	function makeEdit($content) {
        $tpl = new Template("form", "maestro");
        
        return $tpl->parse(array(
            "title" => "Edit " . $content['title'],
            "content" => $content['content'],
            "breadcrumbs" => !isset($content['breadcrumbs']) ? $this->makeBreadcrumbs(array(
				Core::toCamel(Router::get("module")) => "/maestro/" . Router::get("module"),
				"Edit " . $content['title'] => "#",
			)) : $content['breadcrumbs'],
            "action" => (!isset($content['action']) 
            	? "/maestro/" . Router::get("module") . "/update"
				: $content['action']),
            "buttons" => isset($content['buttons']) ? $content['buttons'] : ('<input type="submit" value="Save" class="btn btn-success pull-right" id="maestroFormSubmit" />' . '<input type="button" value="Cancel" class="btn btn-warning pull-right" id="maestroFormCancel" style="margin-right: 10px;" />')
        )) . $this->{Core::toCamel(Router::get("module"))}->genValidateJs();
	}
	
	function makeUpdate($content) {
        if (isset($content['redirect']))
            header("Location: " . $content['redirect']);
        else if (isset($content['id']) && Validate::isInt($content['id'])) {
            Debug::addSuccess("Successfully edited #" . $content['id'] . ".", FALSE);
            header("Location: /maestro/" . Router::get("module") . "/edit/" . $content['id']);
        } else {
            Debug::addError("Error updating #" . $content['id'] . ".", FALSE);
            header("Location: /maestro/" . Router::get("module"));
        }
        die();
	}
	
	function makeRelated($related = array()) {
        $tpl = new Template("related", "maestro");
        
        foreach ($related AS $url => $text) {
        	if (is_array($text)) {
	            $tpl->parse(array(
	                "url" => isset($text['url']) ? $text['url'] : NULL,
	                "text" => isset($text['text']) ? $text['text'] : NULL,
	                "class" => isset($text['class']) ? $text['class'] : NULL,
	            ), "navi");
        	} else {
	            $tpl->parse(array(
	                "url" => $url,
	                "text" => $text,
	                "class" => NULL,
	            ), "navi");
	        }
    	}
        
        return $tpl->parse(array(
            "navi" => $tpl->display("navi"),
        ));
    }
	
    function makeListing($data = array()){
        if (!is_array($data))
            return $data;
            
        $tpl = new Template("listing_multiple", "maestro");
        
        if (isset($data['th']))
        foreach ($data['th'] AS $th)
            $tpl->parse(array("txt" => $th), "th");
        
        if (isset($data['btn header']))
        foreach ($data['btn header'] AS $btn) {
            if ($btn == "add") {
				//echo "btnadd";die(print_r(debug_backtrace()));
                $btn = array(
                    "txt" => '<i class="icon-plus"></i>',
                    "url" => "/maestro/" . (!isset($data['module']) ? Router::get("module") : $data['module']) . "/add",
                    "action" => "add",
                    "class" => "success",
                    "add" => NULL,
                    "tit" => "Add",
                );
            } else if ($btn == "filter")
                $btn = array(
                    "txt" => '<i class="icon-list-alt"></i>',
                    "url" => "#",
                    "class" => "info",
                    "add" => ' onclick="$(\'.filter\').toggle(\'slow\');"',
                    "tit" => "Filter"
                );
            else if (!is_array($btn)) {
                $tpl->manualAddToPart($btn, "btn header");
                continue;
            }
            if (!isset($btn["tit"]))
                    $btn["tit"] = NULL;
                
            if (!isset($btn["add"]))
                    $btn["add"] = NULL;
                
            $tpl->parse($btn, "btn header");
        }
        
        if (isset($data['row']))
        foreach ($data['row'] AS $row) {
            if (isset($row['td']))
            foreach ($row['td'] AS $td) {
                    $td = array(
                        "txt" => $td
                    );
                    $tpl->parse($td, "td");
            }
            if (isset($row['btn']))
            foreach ($row['btn'] AS $btn) {
                if (is_null($btn))
                    continue;
                
                if ($btn == "delete")
                    $btn = array(
                        "txt" => '<i class="icon-remove"></i>',
                        "url" => "/maestro/" . (!isset($data['module']) ? Router::get("module") : $data['module']) . "/delete/" . $row['td'][0],
                        "id" => $row['td'][0],
                        "class" => "danger btnMaestroAutoDelete",
                        "disabled" => FALSE,
                        "tit" => "Delete"
                    );
                else if ($btn == "update")
                    $btn = array(
                        "txt" => '<i class="icon-pencil"></i>',
                        "url" => "/maestro/" . (!isset($data['module']) ? Router::get("module") : $data['module']) . "/edit/" . $row['td'][0],
                        "id" => $row['td'][0],
                        "class" => "warning",
                        "add" => NULL,
                        "disabled" => FALSE,
                        "tit" => "Edit"
                    );
                else if ($btn == "view")
                    $btn = array(
                        "txt" => '<i class="icon-search"></i>',
                        "url" => "/maestro/" . (!isset($data['module']) ? Router::get("module") : $data['module']) . "/view/" . $row['td'][0],
                        "id" => $row['td'][0],
                        "class" => "info",
                        "add" => NULL,
                        "disabled" => FALSE,
                        "tit" => "View"
                    );
                else if (!is_array($btn)) {
                    $tpl->manualAddToPart($btn, "btn row");
                    continue;
                }
                   
                if (!isset($btn["tit"]))
                    $btn["tit"] = NULL;
                
                if (!isset($btn["add"]))
                    $btn["add"] = NULL;
                
                if (isset($btn['disabled']))
                    $btn['disabled'] = $btn['disabled'] == TRUE ? 'disabled="disabled" ' : $btn['disabled'];
                
                $tpl->parse($btn, "btn row");
            }
            
            $tpl->parse(array(
                "td" => $tpl->display("td"),
                "btn row" => $tpl->display("btn row"),
                "class" => isset($row['class']) ? $row['class'] : NULL,
            ), "row");
        }
            
        if (isset($data['legend']) && !empty($data['legend']) && is_array($data['legend'])) {
            $isArr = FALSE;
            foreach ($data['legend'] AS $i => $legend) {
                if (!Validate::isInt($i)) {
                    foreach ($legend AS $realLegend) {
                        $isArr = TRUE;
                        $tpl->parse($realLegend, "legend row");
                    }
                    $tpl->parse(array("legend row" => $tpl->display("legend row"), "legend title" => $i), "legend");
                } else {
                    $tpl->parse($legend, "legend row");
                }
            }
            if (!$isArr)
                $tpl->parse(array("legend row" => $tpl->display("legend row"), "legend title" => "Legend"), "legend");
            
        } else if (isset($data['legend']) && !empty($data['legend']) && !is_array($data['legend'])) {
           $tpl->manualAddToPart("legend row", $data['legend']);
           $tpl->parse(array("legend row" => $tpl->display("legend row"), "legend title" => $i), "legend");
        }
        
        return $tpl->parse(array(
            "legend" => isset($data['legend']) && !empty($data['legend']) ? $tpl->display("legend") : NULL,
            "title" => isset($data['title']) ? $data['title'] : "List",
            "filter" => isset($data['filter']) ? $data['filter'] : NULL,
            "tbody class" => isset($data['tbody class']) ? $data['tbody class'] : NULL,
            "append" => isset($data['append']) ? $data['append'] : NULL,
            "th" => $tpl->display("th"),
            "btn header" => $tpl->display("btn header"),
            "row" => $tpl->display("row"),
            "id" => isset($data['id']) ? $data['id'] : NULL,
            "breadcrumbs" => isset($data['breadcrumbs']) ? $data['breadcrumbs'] : NULL,
        ));
    }
    
	function makeBreadcrumbs($links = array(), $prepend = NULL) {
		$return = '<ul class="breadcrumb">';
		
		$return .= str_replace(array('<ul class="breadcrumb">', '</ul>'), NULL, $prepend);
		
		foreach ($links AS $title => $href)
			if ($href != "#")
				$return .= '<li><a href="' . $href . '">' . $title . '</a> <span class="divider">/</span></li>';
			else
				$return .= '<li class="active">' . $title . '</li>';
  
		return $return . '</ul>';
	}
}

class MaestroModel extends UFWModel {
	function __construct() {
	}
}

?>