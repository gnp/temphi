<legend>Send mail</legend>
<form method="POST" enctype="multipart/form-data" action="/maestro/mails/send" class="form-horizontal" name="maestroForm">
	<input type="hidden" name="urlrecipient" value="##URL RECIPIENT TYPE##" />
	<input type="hidden" name="urlrecipients" value="##URL RECIPIENTS##" />

	<div class="control-group">
		<label class="control-label" for="mail">Mail</label>
		<div class="controls">
	    	##MAILS LIST##
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="to">To</label>
		<div class="controls">
	    	<input type="text" id="to" name="mails_sent[to]" value="##TO##" class="span6" />
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="from">From</label>
		<div class="controls">
	    	<input type="text" id="from" name="mails_sent[from]" class="span6" />
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="subject">Subject</label>
		<div class="controls">
	    	<input type="text" id="subject" name="mails_sent[subject]" class="span6" />
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="content">Content</label>
		<div class="controls">
	    	<textarea name="mails_sent[content]" id="content" class="mceEditor"></textarea>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="attachment">Attachment</label>
		<div class="controls">
	    	##ATTACHMENTS LIST##
		</div>
	</div>
	
	<div class="form-actions">
		<input type="submit" value="Send" class="btn btn-success pull-right" id="maestroFormSubmit" />
		<input type="button" value="Cancel" class="btn btn-warning pull-right" id="maestroFormCancel" style="margin-right: 10px;" />
	</div>
</form>
<script type="text/javascript">$(document).ready(function(){
	$("#mail_id").change();
});</script>