
<div class="control-group">
	<label class="control-label" for="title">Title</label>
	<div class="controls">
    	<input type="text" id="title" name="mail[title]" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="subject">Subject</label>
	<div class="controls">
    	<input type="text" id="subject" name="mail[subject]" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="identifier">Identifier</label>
	<div class="controls">
    	<input type="text" id="identifier" name="mail[identifier]" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="content">Content</label>
	<div class="controls">
    	<textarea name="mail[content]" id="content" class="mceEditor"></textarea>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="sender">Sender</label>
	<div class="controls">
    	<input type="text" id="sender" name="mail[sender]" class="span6" />
	</div>
</div>
