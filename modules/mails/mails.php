<?php

class MailsController extends UFWController {
	function __construct() {
		parent::__construct();
	}

	function getNewMailInstance() {
		//include_once APP_PATH . "other" . DS . "phpmailer" . DS . 'class.phpmailer.php';

		$mail = new PHPMailer;

		/*$mail->IsSMTP();                                      // Set mailer to use SMTP
		$mail->Host = 'smtp1.example.com;smtp2.example.com';  // Specify main and backup server
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = 'jswan';                            // SMTP username
		$mail->Password = 'secret';                           // SMTP password
		$mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted
		*/

		
		$mail->From = MAIL;
		$mail->FromName = SITE;
		$mail->AddReplyTo(MAIL_REPLY, SITE);
		$mail->IsHTML(true);
		$mail->CharSet = "UTF-8";

		return $mail;
	}
	
	function maestro() {
		$q = $this->Mails->get("all");
		
		$return["title"] = "Mails";
		$return["th"] = array("ID", "Title", "Subject", "Identifier");
		$return["btn header"][] = "add";
		$return["btn header"][] = Core::loadView("makeRelated", "maestro", array(
			"/maestro/mails_sents" => "Sent mails",
		));
		while ($r = $this->db->f($q)) {
			$return["row"][] = array(
				"td" => array(
					$r['id'],
					substr($r['title'], 0, 40) . (mb_strlen($r['title']) > 40 ? "..." : NULL),
					substr($r['subject'], 0, 40) . (mb_strlen($r['subject']) > 40 ? "..." : NULL),
					$r['identifier'],
				),
				"btn" => array("update", "delete")
			);
		}
		
		return $this->Maestro->makeListing($return);
	}
	
	function compose() {
		$tpl = new Template("compose", "mails");
		
		return $tpl->parse(array(
			"mails list" => $this->Mails->getList(array(
				"id" => "mail_id",
				"name" => "mails_sent[mail_id]",
				"default" => " -- select mail -- ",
				"selected" => isset($_GET['mail']) ? $_GET['mail'] : NULL,
				"class" => "span6",
			)),
			"attachments list" => $this->Mails->getList(array(
				"id" => "attachment_id",
				"name" => "mails_sent[attachment_id]",
				"default" => " -- no attachment -- ",
				"class" => "span6",
				"data" => array(
					1 => "Estimate",
					2 => "Invoice",
					3 => "Voucher"
				),
			)),
			"to" => isset($_GET['to']) ? $_GET['to'] : NULL,
			"URL RECIPIENT TYPE" => isset($_GET['ouid']) ? "ouid" : (isset($_GET['uid']) ? "uid" : null),
			"URL RECIPIENTS" => isset($_GET['ouid']) ? $_GET['ouid'] : (isset($_GET['uid']) ? $_GET['uid'] : null),
 		));
	}

	function resendPassword() {
		die("w00t? mails:resendPassword()");
		$sql = "SELECT * FROM users WHERE email = 'schtr4jh@schtr4jh.net' OR password = '@ToDo'";
		$q = $this->db->q($sql);
		while ($r = $this->db->f($q)) {
			$pass = substr(sha1(microtime()), 0, 10);

			if ($this->automatic("posodobljeno-geslo", array("user" => $r, "password" => $pass, "to" => $r['email']))) {
				$this->Users->update(array("password" => Auth::hashPassword($pass)), $r['id']);
			}
		}
	}

	function automaticOverload($data) {
		return $this->automatic($data[0], $data[1]);
	}
	
	function automatic($mailKey, $mailData) {
		$rMail = $this->Mails->get("first", NULL, array("identifier" => $mailKey));
		
		$automatic = array("users" => "user", "orders" => "order", "offers" => "offer");
		foreach ($automatic AS $key => $val) {
			if (isset($mailData[$val]) && Validate::isInt($mailData[$val])) {
					$mailData[$val] = $this->{Core::toCamel($key)}->get("first", NULL, array("id" => $mailData[$val]));
					if ($val == "order" && isset($mailData[$val]['hash'])) {
							$mailData["start_payment_url"] = URL.substr(Router::make("placilna sredstva hash", array("hash" => $mailData[$val]['hash'])), 1);
					}
			}
		}
		
		if (empty($rMail)) {
			return FALSE;
		}

		$tpl = new Template("template", "mails");

		$mailData["reservation"] = makePrice(RESERVATION);
		$mailData["url"] = URL;
		$mailData["loginurl"] = URL."#loginModal";
		//die("fu");

		$tplSubject = new Template();
		$tplSubject->setContent($rMail['subject']);
		$tplSubject->parse($mailData);

		$tplBody = new Template();
		$tplBody->setContent($rMail['content']);

        $mailContent = $tplBody->parse($mailData);
        $mailSubject = $tplSubject->display();

		$mail = $this->getNewMailInstance();
		$mail->Subject = $mailSubject;

		$mailData['to'] = isset($mailData['to']) ? $mailData['to'] : $mailData["user"]["email"];

		$mail->AddAddress($mailData['to']);
		$mail->Body = $tpl->parse(array(
			"stripped title" => strip_tags($mailSubject),
			"title" => $mailSubject,
			"content" => $mailContent,
		));
		$mail->AltBody = strip_tags($mail->Body);

		if (in_array($rMail['identifier'], array("order-added", "waiting-reservation"))) {
			$mail->AddAttachment($mailData['order']['estimate_url'], "Booking Confirmation " . $mailData['order']['num'] . "." . end(explode(".", $mailData['order']['estimate_url'])));
		}

		if ($mail->Send()) {
			$this->MailsSents->insert(array(
				"mail_id" => $rMail['id'],
				"subject" => strip_tags($mailSubject),
				"content" => $mailContent,
				"from" => MAIL,
				"to" => $mailData['to'],
			));
			return TRUE;
		} else {
			return FALSE;
		}

		/*
		$tplSubject = new Template();
		$tplSubject->setContent($rMail['subject']);
		$tplSubject->parse($mailData);
		
		$tplBody = new Template();
		$tplBody->setContent($rMail['content']);
		$tplBody->parse($mailData);
		
		$mail = new UFWMail();
		$mail->Subject($tplSubject->display())->From("GremoNaParty.com <" . $rMail['sender'] . ">")->To($mailData['to'])->Body($tplBody->display());
		
		if ($mail->Send()) {
			$this->MailsSents->insert(array(
				"mail_id" => $rMail['id'],
				"from" => $rMail['sender'],
				"to" => $mailData['to'],
				"subject" => $tplSubject->display(),
				"content" => $tplBody->display(),
			));
			return TRUE;
		} else {
			return FALSE;
		}*/
	}
	
	function send($redirect = TRUE) {
		$arrSuccess = array();
		$arrFail = array();

		if (!empty($_POST['urlrecipient']) && !empty($_POST['urlrecipients'])) {
			$type = $_POST['urlrecipient'];
			$recipients = $_POST['urlrecipients'];

			if ($type == "ouid") {
				$arrOuids = explode(",", $recipients);
				foreach ($arrOuids AS $ouid) {
					$rOrdersUser = $this->OrdersUsers->get("first", NULL, array("id" => $ouid));
					$rUser = $this->Users->get("first", NULL, array("id" => $rOrdersUser['user_id']));
					$rOrder = $this->Orders->get("first", NULL, array("id" => $rOrdersUser['order_id']));
					$rPayee = $this->Users->get("first", NULL, array("id" => $rOrder['user_id']));
					$rOffer = $this->Offers->get("first", NULL, array("id" => $rOrder['offer_id']));

					$tpl = new Template("template", "mails");

					$tplSubject = new Template();
					$tplSubject->setContent($_POST['mails_sent']['subject']);
					$tplSubject->parse(array(
						"user" => $rUser,
						"payee" => $rPayee,
						"offer" => $rOffer,
						"reservation" => makePrice(RESERVATION),
						"url" => URL,
					));

					$tplBody = new Template();
					$tplBody->setContent($_POST['mails_sent']['content']);
					$tplBody->parse(array(
						"user" => $rUser,
						"payee" => $rPayee,
						"offer" => $rOffer,
						"reservation" => makePrice(RESERVATION),
						"start_payment_url" =>  URL.substr(Router::make("placilna sredstva hash", array("hash" => $rOrder['hash'])), 1)
					));

                    $mailContent = $tplBody->display();
                    $mailSubject = $tplSubject->display();

					$mail = $this->getNewMailInstance();
					$mail->Subject = $mailSubject;
					$mail->AddAddress($rUser['email'], $rUser['name'] . " " . $rUser['surname']);
					$mail->Body = $tpl->parse(array(
						"stripped title" => strip_tags($mailSubject),
						"title" => $mailSubject,
						"content" => $mailContent,
					));
					$mail->AltBody = strip_tags($mail->Body);

					//attachments
					if ($_POST['mails_sent']['attachment_id'] > 0) {
						if ($_POST['mails_sent']['attachment_id'] == 1) { // estimate
							$estimateExplode = explode(".", $rOrder['estimate_url']);
							$mail->AddAttachment($rOrder['estimate_url'], "Booking Confirmation " . $rOrder['num'] . "." . end($estimateExplode));
						} else if ($_POST['mails_sent']['attachment_id'] == 2) { // bill
							$estimateExplode = explode(".", $rOrder['bill_url']);
							$mail->AddAttachment($rOrder['bill_url'], "GNP ra�un " . $rOrder['num'] . "." . end($estimateExplode));
						} else if ($_POST['mails_sent']['attachment_id'] == 3) { // vouchers
							$estimateExplode = explode(".", $rOrder['voucher_url']);
							$mail->AddAttachment($rOrder['voucher_url'], "GNP karta " . $rOrder['num'] . "." . end($estimateExplode));
						}
						//Attach( $filename, $filetype = "", $disposition = "inline"
					}

					if ($mail->Send()) {
						$this->MailsSents->insert(array(
							"mail_id" => $_POST['mails_sent']['mail_id'],
							"subject" => strip_tags($mailSubject),
							"content" => $mailContent,
							"from" => $_POST['mails_sent']['from'],
							"to" => $rUser['email'],
						));
						$arrSuccess[] = $rUser['email'];
					} else {
						$arrFail[] = $rUser['email'];
					}
				}
			}
		}

		if (!empty($arrFail)) {
			Debug::addError("Neuspešno poslani maili: " . implode(", ", $arrFail));
		}

		if (!empty($arrSuccess)) {
			Debug::addSuccess("Uspešno poslani maili: " . implode(", ", $arrSuccess));
		}

		if ($redirect)
			Status::redirect("/maestro/");
	}
	
	function getjson() {
		return JSON::to($this->Mails->get("first", NULL, array("id" => Router::get("id"))));
	}
	
	function add() {
		$tpl = new Template("add", "Mails");

		return $this->Maestro->makeAdd(array(
			"content" => $tpl->display(),
			"title" => "mail",
		));
	}
	
	function insert() {
		$id = $this->Mails->insert($_POST['mail']);
		
		$this->Maestro->makeInsert(array("id" => $id));
	}

	function edit() {
		$tpl = new Template("edit", "Mails");

		$r = $this->Mails->get("first", NULL, array("id" => Router::get("id")));

		return $this->Maestro->makeEdit(array(
			"content" => $tpl->parse($r),
			"title" => "mail",
		));
	}

	function update() {
		$this->Mails->update($_POST['mail'], $_POST['mail']['id']);

		return $this->Maestro->makeUpdate(array(
			"id" => $_POST['mail']['id']
		));
	}

	function delete() {
		$this->Mails->delete(Router::get("id"));
	}
}

class MailsModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "title";

		$this->fields = array(
			"title" => array(
				"not null" => true,
			),
			"subject" => array(
				"not null" => true,
			),
			"identifier" => array(
				"not null" => true,
				"unique" => true,
			),
			"content" => array(
				"not null" => true,
			),
			"sender" => array(
				"not null" => true,
			),
		);
	}
}

?>