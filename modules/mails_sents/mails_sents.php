<?php

class MailsSentsController extends UFWController {
	function __construct() {
		parent::__construct();
	}

	function maestro() {
		$q = $this->MailsSents->get("all", NULL, array(), array("order by" => "id DESC", "limit" => "200"));
		
		$return["title"] = "Sent mails";
		$return["th"] = array("ID", "To", "Subject", "Sent");
		while ($r = $this->db->f($q)) {
			$return["row"][] = array(
				"td" => array(
					$r['id'],
					$r['to'],
					$r['subject'],
					$r['datetime'],
				),
				"btn" => array("view"),
			);
		}
		
		return $this->Maestro->makeListing($return);
	}

	function view() {
		$r = $this->MailsSents->get("first", null, array("id" => Router::get("id")));
		$tpl = new Template("view", "mails_sents");
		return $this->Maestro->makeView(array(
			"title" => "Sent email",
			"content" => $tpl->parse(array("mail" => $r)),
		));
	}
}

class MailsSentsModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "";

		$this->fields = array(
			"mail_id" => array(
				"not null" => true,
			),
			"subject" => array(
				"not null" => true,
			),
			"content" => array(
				"not null" => true,
			),
			"from" => array(
				"not null" => true,
			),
			"to" => array(
				"not null" => true,
			),
			"datetime" => array(
				"default" => DT_NOW,
			),

		);
	}
}

?>