<div class="control-group">
	<label class="control-label" for="title">Subject</label>
	<div class="controls">
    	<p>##MAIL.SUBJECT##</p>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="to">To</label>
	<div class="controls">
    	<p>##MAIL.TO##</p>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="from">From</label>
	<div class="controls">
    	<p>##MAIL.FROM##</p>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="sender">Sent</label>
	<div class="controls">
    	<p>##MAIL.DATETIME##</p>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="content">Content</label>
	<div class="controls">
    	##MAIL.CONTENT##
	</div>
</div>