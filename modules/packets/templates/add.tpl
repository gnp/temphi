<input type="hidden" name="packet[offer_id]" value="##OFFER_ID##" />

<div class="control-group">
	<label class="control-label" for="title">Title</label>
	<div class="controls">
    	<input type="text" id="title" name="packet[title]" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="subtitle">Price</label>
	<div class="controls">
		<div class="input-append">
	    	<input type="text" id="price" name="packet[price]" class="span6" />
	    	<span class="add-on">€</span>
		</div>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="published">Published</label>
	<div class="controls">
    	<input type="text" id="published" name="packet[dt_published]" class="jdt span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="ticket">Ticket</label>
	<div class="controls">
		<input type="hidden" name="packet[ticket]" value="-1" />
    	<input type="checkbox" id="ticket" name="packet[ticket]" value="1" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="order_limit">Order limit</label>
	<div class="controls">
		<div class="input-append">
	    	<input type="number" id="order_limit" name="packet[order_limit]" class="span6" />
		</div>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="order_limit_count">Order limit count</label>
	<div class="controls">
		<div class="input-append">
	    	<input type="number" id="order_limit_count" name="packet[order_limit_count]" class="span6" />
		</div>
	</div>
</div>