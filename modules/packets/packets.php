<?php

class PacketsController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
    function maestro($data = array()) {
    	if (!Validate::isInt($data['offer_id']))
			return NULL;
		
        $q = $this->Packets->get("all", NULL, array("offer_id" => $data['offer_id']), array("order by" => "position"));
        
        $return["title"] = "Packets";
        $return["module"] = "packets";
        $return["th"] = array("ID", "Title", "Price", "Status");
		$return["tbody class"] = "sortable";
        $return["btn header"][] = array(
			"txt" => '<i class="icon-plus"/>',
			"url" => "/maestro/packets/add/" . $data['offer_id'],
			"class" => "success"
		);

        while ($r = $this->db->f($q)) {
            $return["row"][] = array(
                "td" => array(
                    $r['id'],
                    $r['title'],
                    $r['price'],
					'<span class="btnToggle btn btn-small" data-url="/packets/togglepublished/' . $r['id'] . '/" data-value="' . $r['dt_published'] . '"><i class="icon-check"></i></span>',
                ),
                "btn" => array("update", "delete")
            );
        }
		
		$tplSort = new Template("onsortupdate", "packets");
		$return['append'] = $tplSort->display();
		
        return $this->Maestro->makeListing($return);
    }
	
	function updatepositions() {
		Core::loadLayout(NULL);
		foreach ($_POST['positions'] AS $position => $id)
			$this->Packets->update(array(
				"position" => $position
			), $id);
	}

	function togglepublished() {
		return $this->Packets->update(array(
			"dt_published" => Router::get("active") ? DT_NOW : DT_NULL,
		), Router::get("id"));
	}
    
    function add() {
        $tpl = new Template("add", "packets");
        
        return $this->Maestro->makeAdd(array(
            "content" => $tpl->parse(array(
				"offer_id" => Router::get("id")
			)),
            "title" => "packet",
            "breadcrumbs" => $this->Maestro->makeBreadcrumbs(array(
				"Offers" => "/maestro/offers",
				"Edit offer" => "/maestro/offers/edit/" . Router::get("id"),
				"Add packet" => "#",
			)),
        ));
    }
    
    function insert() {
        $id = $this->Packets->insert($_POST['packet']);
		
		return $this->Maestro->makeInsert(array("id" => $id));
    }

    function edit() {
        $tpl = new Template("edit", "packets");

        $r = $this->Packets->get("first", NULL, array("id" => Router::get("id")));
		
		$r['cb ticket'] = $r['ticket'] == 1
			? ' checked="checked"'
			: NULL;
		$r['pic display'] = !empty($r['picture']) ? "block" : "none";

        return $this->Maestro->makeEdit(array(
            "content" => $tpl->parse($r) . 
            	$this->PacketsAdditions->edit(array("packet_id" => $r['id'])) .
            	$this->PacketsIncludes->edit(array("packet_id" => $r['id'])) .
            	$this->PacketsCities->edit(array("packet_id" => $r['id'])) .
            	$this->PacketsTabs->maestro(array("packet_id" => $r['id'])),
            "title" => "packet",
            "breadcrumbs" => $this->Maestro->makeBreadcrumbs(array(
				"Offers" => "/maestro/offers",
				"Edit offer" => "/maestro/offers/edit/" . $r['offer_id'],
				"Edit packet" => "#",
			)),
        ));
    }

    function update() {
        $this->Packets->update($_POST['packet'], $_POST['packet']['id']);

        Core::loadView("update", "tags", array("multiple" => "packets", "single" => "packet", "id" => $_POST['packet']['id']));
		
		Core::loadView("update", "PacketsAdditions");
		Core::loadView("update", "PacketsIncludes");
		Core::loadView("update", "PacketsCities");

        return $this->Maestro->makeUpdate(array(
            "id" => $_POST['packet']['id']
        ));
    }

    function delete() {
        $this->Offers->delete(Core::getGlobal("id"));
    }
}

class PacketsModel extends UFWModel {
  function getAvailablePacketsSQL($offer) {
    return "SELECT p.id " .
        "FROM packets p " .
        "INNER JOIN offers o ON o.id = p.offer_id " .
        "LEFT OUTER JOIN orders ord ON (ord.offer_id = o.id AND ord.id IS NULL OR ord.dt_confirmed > '" . DT_NULL . "') " .
        "LEFT OUTER JOIN orders_users ou ON (ou.order_id = ord.id AND ou.packet_id = p.id) " .
        "WHERE p.offer_id = " . $offer . " " . 
        "AND p.dt_published > '" . DT_NULL . "' " . 
        "AND p.dt_published < '" . DT_NOW . "' " .
        "AND o.dt_published > '" . DT_NULL . "' " . 
        "AND o.dt_published < '" . DT_NOW . "' " . 
        "AND o.dt_opened < '" . DT_NOW . "' " . 
        "AND (o.dt_closed > '" . DT_NOW . "' " .
        "OR o.dt_closed = '" . DT_NULL . "') " .
        "GROUP BY p.id, p.order_limit_count " .
        "HAVING (p.order_limit_count < 1 OR COUNT(DISTINCT ou.id) < p.order_limit_count)";
  }

	function __construct() {
		parent::__construct();
		
		$this->mk = "title";

		$this->fields = array(
			"title" => array(
				"not null" => true,
			),
			"price" => array(
				"not null" => true,
			),
			"offer_id" => array(
				"not null" => true,
			),
			"dt_published" => array(
				"default" => DT_NOW,
			),
			"position" => array(
				"default" => 1,
			),
      "ticket" => array(
        "default" => -1,
      ),
      "order_limit" => array(
        "default" => -1,
      ),
      "order_limit_count" => array(
        "default" => -1,
      )
		);

    $this->orderBy = "position";
	}
}

?>