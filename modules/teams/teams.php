<?php

class TeamsController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function index() {
	}
	
	function one() {
	}
	
	function maestro() {
		$q = $this->Teams->get("all");
		
		$return["title"] = "Teams";
		$return["th"] = array("ID", "Title", "Status");
		$return["btn header"][] = "add";
		$return["tbody class"] = "sortable";
		while ($r = $this->db->f($q)) {
			$return["row"][] = array(
				"td" => array(
					$r['id'],
					$r['title'],
					'<span class="btnToggle btn btn-small" data-url="/teams/togglepublished/' . $r['id'] . '/" data-value="' . $r['published'] . '"><i class="icon-check"></i></span>',
				),
				"btn" => array("update", "delete")
			);
		}
		
		return $this->Maestro->makeListing($return);
	}
	
	function togglepublished() {
		return $this->Teams->update(array(
			"published" => Router::get("active"),
		), Router::get("id"));
	}
	
	function add() {
		$tpl = new Template("add", "teams");

		return $this->Maestro->makeAdd(array(
			"content" => $tpl->display(),
			"title" => "team",
		));
	}
	
	function insert() {
		$id = $this->Teams->insert($_POST['team']);
		
		$this->Maestro->makeInsert(array("id" => $id));
	}

	function edit() {
		$tpl = new Template("edit", "teams");

		$r = $this->Teams->get("first", NULL, array("id" => Router::get("id")));
		
		$r["cb published"] = $r['published'] == 1
			? ' checked="checked"'
			: NULL;

		return $this->Maestro->makeEdit(array(
			"content" => $tpl->parse($r) .
            	$this->Members->maestro(array("team_id" => $r['id'])),
			"title" => "team",
		));
	}

	function update() {
		$this->Teams->update($_POST['team'], $_POST['team']['id']);

		return $this->Maestro->makeUpdate(array(
			"id" => $_POST['team']['id']
		));
	}

	function delete() {
		$this->Teams->delete(Router::get("id"));
	}
}

class TeamsModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "title";

		$this->fields = array(
			"title" => array(
				"not null" => true,
			),
			"published" => array(
				"default" => -1,
			),
		);
	}
}

?>