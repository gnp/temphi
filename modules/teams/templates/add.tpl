<div class="control-group">
	<label class="control-label" for="title">Team</label>
	<div class="controls">
    	<input type="text" id="title" name="team[title]" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="enabled">Published</label>
	<div class="controls">
		<input type="hidden" value="-1" name="team[published]" />
    	<input type="checkbox" id="published" name="team[published]" value="1" />
	</div>
</div>