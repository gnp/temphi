<script type="text/javascript" src="/js/contact_form_validation.js"></script>
<div class="row-fluid">
	<div class="span2">
	  <h1>{{ __('header_contact') }}</h1>
	  
	  {{ __('text_contact_sidebar') }}
	</div>
	<div class="span9 offset1">
	  <div id="quick-contact">
    	<div id="form">
        {{ __('content_private_party_pack') }}
        {{ __('form_contact') }}
      </div>
      <div id="success">{{ __('content_contact_success') }}</div>
	  </div>
	</div>
</div>