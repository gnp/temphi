<html>
	<head>
		<title>Predračun za Sensation Praga</title>
	</head>
	<body style="margin: 0; padding: 0; background: #f0f0f0; font-family: Arial, sans-serif; font-size: 14px;">
		<div style="width: 600px; margin: 0 auto; clear: both;">
			<a href="http://gremonaparty.com/" target="_blank"><img src="http://gremonaparty.com/img/mail_header.png" /></a>
			<span style="display: block; clear: both; height: auto; font-size: 30px; margin: 0 auto; margin: 15px; text-transform: uppercase; text-align: center;">Predračun za <b>Sensation Praga</b></span>
			<span style="display: block; clear: both; height: 1px; background: #ccc;"></span>
			<p>Pozdravljena!<br />
			Skladno s tvojo prijavo na Sensation Praga ti v priponki pošiljamo predračun za plačilo.</p>
			
			<br />
			<p><b style="color: #cb181c; text-transform: uppercase;">Opozorilo</b><br />
				Prosimo, upoštevaj rok plačila in pravilno izpolni plačilni nalog. V nasprotnem tvoje nakazilo ne bo zabeleženo v naših evidencah, kar lahko privede do izbrisa tvoje prijave.
			</p>
			
			<p style="font-weight: bold;">Pri tem še posebej pazi na:</p>
			
			<div style="display: block; background: #fafafa; border: 1px solid #ccc; padding: 15px;">
				<b>NAMEN NAKAZILA:</b> napiši naziv dogodka in pripiši ime prijave<br /><br />
				<b>ZNESEK:</b> nakaži točen znesek avansa oz. obroka<br /><br />
				<b>SKLIC/REFERENCA:</b> vpiši točno referenco predračuna, ki jo najdeš v navodilih za plačilo
			</div>
			
			<br /><br />
			<p>Za vprašanja in dodatne informacije smo na voljo na telefonski številki <b style="color: #cb181c;">040 148 148</b> ali e-mailu <b style="color: #cb181c;">party@gremonaparty.com</b>.<br /><br />

			Lep pozdrav ti želi<br />
			Ekipa Gremonaparty.com
			</p>
			<br /><br />
			<span style="display: block; clear: both; height: auto; font-size: 30px; margin: 0 auto; text-transform: uppercase; text-align: center; font-weight: bold;"><b>Enkrat z nami, vedno z nami</span>
			<span style="display: block; clear: both; height: 1px; background: #ccc; margin-top: 15px;"></span>
			<br />
			<a href="#" style="width: 33%; text-align: center; float: left; display: block; text-decoration: none; font-weight: normal; color: #000;">Splošni pogoji</a>
			<a href="#" style="width: 33%; text-align: center; float: left; display: block; text-decoration: none; font-weight: normal; color: #000;">gremonaparty.com</a>
			<a href="#" style="width: 33%; text-align: center; float: left; display: block; text-decoration: none; font-weight: normal; color: #000;">Odjava</a>
			<br /><br /><br />
		</div>
	</body>
</html>