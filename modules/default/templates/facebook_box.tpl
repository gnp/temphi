<div class="mf" id="facebook-box-module">
  <div class="module-head">
      <div class="left">
        <h2>Facebook</h2>
      </div>
  </div>
  <div>
		<div class="fb-like-box" data-href="https://www.facebook.com/gremonaparty" data-width="570" data-show-faces="true" data-stream="false" data-show-border="true" data-header="false"></div>
		<script>
			if (!cookiesAllowed()) {
				$("#facebook-box-module div").last().append('<p>Sprejmi piškotke in se druži z nami tudi na <a href="//www.facebook.com/gremonaparty" target="_blank">Facebooku</a>.</p>');
			}
		</script>
  </div>
</div>