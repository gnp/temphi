<div class="row-fluid">
	<div class="span6">
		<div class="alert alert-warning">
		  <strong>Unconfirmed orders (##UNCONFIRMED NUM##)</strong>
		</div>
		  ##UNCONFIRMED TABLE##
	</div>
	<div class="span6">
		<div class="alert alert-success">
		  <strong>Latest orders</strong>
		</div>
		  ##LATEST TABLE##
	</div>
</div>
<div class="row-fluid" style="margin-top: 30px;">
	<div class="span6">
		<div class="alert alert-danger">
		  <strong>Waiting calculation (##CALCULATION NUM##)</strong>
		</div>
		##CALCULATION TABLE##
	</div>
	<div class="span6">
		<div class="alert alert-info">
		  <strong>Waiting changes (##WAITING NUM##)</strong>
		</div>
		##WAITING TABLE##
	</div>
</div>
<div class="row-fluid" style="margin-top: 30px;">
	<div class="span6">
		<div class="alert alert-danger">
		  <strong>Missing bills (##EMPTY NUM##)</strong>
		</div>
		##EMPTY TABLE##
	</div>
</div>

##TABLE START##
<table class="table table-hover table-stripped table-condensed" id="maestroList##TYPE##">
	<thead><tr><th>ID</th><th>Num</th><th>Offer</th><th>Price</th><th>Datetime</th></thead>
	<tbody>
		##ROW START##<tr><td>##ID##</td><td>##NUM##</td><td>##TITLE##</td><td>##PRICE##</td><td>##DATETIME##</td><td><a href="##HREF##" class="btn btn-##CLASS## btn-mini">View</a></td></tr>##ROW END##
	</tbody>
</table>
##TABLE END##