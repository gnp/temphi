<div class="enews mf" id="gallery-module">
  <div class="module-head">
      <div class="left">
        <h2>{{ __('heading_e-news') }}</h2>
        <img src="/img/icons/enews.png">
      </div>
  </div>
  <div class="grayShadow">
    <div class="content grayBg">
    	<div class="text">
      	Bodi prvi obveščen o <strong>novostih,<br /> akcijah,<br /> ugodnih ponudbah</strong><br /> in najatrativnejših novicah iz sveta eletronske glasbe!
      </div><div class="form">
    		<form id="enews-module">
          <div>
            <input id="name" name="user[name]" placeholder="Ime" type="text" />
          </div>
          <div>
            <input id="surname" name="user[surname]" placeholder="Priimek" type="text" />
          </div>
          <div>
            <input id="email" name="user[email]" placeholder="Email" type="email" />
          </div>
          <div>
            <input class="sendy" type="submit" value="POŠLJI" />
          </div>
        </form>
      </div>
    </div>
  </div>
</div><!-- /gallery-module -->