<div class="module-head topfull">
  <div class="left">
    <h2>{{ __('heading_what_our_passengers_say') }}</h2>
  </div>
  <div class="clear"></div>
</div>
<div class="row-fluid" id="opinions">
  {{ __('index_opinions') }}
</div>