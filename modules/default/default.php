<?php

class DefaultController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function index() {
		Status::code(400);
	}

	function logError() {
		file_put_contents(LOG_PATH . "html/" . sha1(microtime()) . ".log", json_encode($_POST));
		Core::loadModule("Mails");
		$mail = $this->Mails->getNewMailInstance();
		$mail->AddAddress("schtr4jh@schtr4jh.net");
		$mail->Subject = "GNP error";
		$mail->Body = "<pre>" . print_r($_POST, true) . "</pre>";
		$mail->Send();
		die("ok (!ok =))");
	}

	function gaexperiment() {
		if (Router::getUrlData("name") == "index") {
			$tpl = new Template("gaexperiment/cookie", "default");

			return $tpl->display();
		}

		return " ";
	}

	function indexmaestro() {
		$tpl = new Template("maestroindex", "default");

		$sqlLatestOrders = "SELECT off.title, ord.id, ord.price, ord.dt_added AS datetime, ord.num " .
			"FROM orders ord " .
			"INNER JOIN offers off ON off.id = ord.offer_id " .
			"INNER JOIN orders_users ou ON ou.order_id = ord.id " .
			"GROUP BY ord.id " .
			"ORDER BY ord.dt_added DESC " .
			"LIMIT 10";
		$qLatestOrders = $this->db->q($sqlLatestOrders);
		while ($rLatestOrder = $this->db->f($qLatestOrders))
			$tpl->parse(array(
				"id" => $rLatestOrder['id'],
				"num" => $rLatestOrder['num'],
				"title" => $rLatestOrder['title'],
				"price" => makePrice($rLatestOrder['price']),
				"datetime" => date("d.m.Y H:i", strtotime($rLatestOrder['datetime'])),
				"href" => "/maestro/orders/edit/" . $rLatestOrder['id'],
				"class" => "success",
			), "row", "row latest table");

		$sqlUnconfirmedOrders = "SELECT off.title, ord.id, ord.price, ord.dt_added AS datetime, ord.num " .
			"FROM orders ord " .
			"INNER JOIN offers off ON off.id = ord.offer_id " .
			"INNER JOIN orders_users ou ON ou.order_id = ord.id " .
			"WHERE ord.dt_confirmed = '" . DT_NULL . "'" .
			"AND ord.dt_canceled = '" . DT_NULL . "'" .
			"AND ord.dt_rejected = '" . DT_NULL . "'" .
			"GROUP BY ord.id " .
			"ORDER BY ord.dt_added DESC";
		$qUnconfirmedOrders = $this->db->q($sqlUnconfirmedOrders);
		$numUnconfirmedOrders = $this->db->c($qUnconfirmedOrders);
		$i = 0;
		while ($rUnconfirmedOrder = $this->db->f($qUnconfirmedOrders)) {
			$tpl->parse(array(
				"id" => $rUnconfirmedOrder['id'],
				"num" => $rUnconfirmedOrder['num'],
				"title" => $rUnconfirmedOrder['title'],
				"price" => makePrice($rUnconfirmedOrder['price']),
				"datetime" => date("d.m.Y H:i", strtotime($rUnconfirmedOrder['datetime'])),
				"href" => "/maestro/orders/edit/" . $rUnconfirmedOrder['id'],
				"class" => "warning",
			), "row", "row unconfirmed table");
			$i++;
			if ($i >= 10)
				break;
		}

		$sqlCalculationOrders = "SELECT off.title, ord.id, ord.price, ord.num " .
			"FROM orders ord " .
			"INNER JOIN offers off ON off.id = ord.offer_id " .
			"INNER JOIN orders_bills ob ON ob.order_id = ord.id " .
			"GROUP BY ord.id " .
			"HAVING ROUND(SUM(ob.price), 2) <> ROUND(ord.price, 2) " .
			"ORDER BY ord.dt_added DESC";
		$qCalculationOrders = $this->db->q($sqlCalculationOrders);
		$numCalculationOrders = $this->db->c($qCalculationOrders);
		$i = 0;
		while ($rCalculationOrder = $this->db->f($qCalculationOrders)) {
			$tpl->parse(array(
				"id" => $rCalculationOrder['id'],
				"num" => $rCalculationOrder['num'],
				"title" => $rCalculationOrder['title'],
				"price" => makePrice($rCalculationOrder['price']),
				"datetime" => NULL,
				"href" => "/maestro/orders/edit/" . $rCalculationOrder['id'],
				"class" => "danger",
			), "row", "row calculation table");
			$i++;
			if ($i >= 10)
				break;
		}

		$sqlWaitingOrders = "SELECT off.title, ord.id, ord.price, ord.num " .
			"FROM orders ord " .
			"INNER JOIN offers off ON off.id = ord.offer_id " .
			"WHERE ord.dt_locked <> '" . DT_NULL . "'" .
			"GROUP BY ord.id ";
		$qWaitingOrders = $this->db->q($sqlWaitingOrders);
		$numWaitingOrders = $this->db->c($qWaitingOrders);
		$i = 0;
		while ($rWaitingOrder = $this->db->f($qWaitingOrders)) {
			$tpl->parse(array(
				"id" => $rWaitingOrder['id'],
				"num" => $rWaitingOrder['num'],
				"title" => $rWaitingOrder['title'],
				"price" => makePrice($rWaitingOrder['price']),
				"datetime" => NULL,
				"href" => "/maestro/orders/edit/" . $rWaitingOrder['id'],
				"class" => "info",
			), "row", "row waiting table");
			$i++;
			if ($i >= 10)
				break;
		}

		$sqlEmptyOrders = "SELECT off.title, ord.id, ord.price, ord.num " .
			"FROM orders ord " .
			"INNER JOIN offers off ON off.id = ord.offer_id " .
			"WHERE ord.id NOT IN (SELECT DISTINCT(order_id) FROM orders_bills) " .
			"ORDER BY ord.id DESC";
		$qEmptyOrders = $this->db->q($sqlEmptyOrders);
		$numEmptyOrders = $this->db->c($qEmptyOrders);
		$i = 0;
		while ($rEmptyOrder = $this->db->f($qEmptyOrders)) {
			$tpl->parse(array(
				"id" => $rEmptyOrder['id'],
				"num" => $rEmptyOrder['num'],
				"title" => $rEmptyOrder['title'],
				"price" => makePrice($rEmptyOrder['price']),
				"datetime" => NULL,
				"href" => "/maestro/orders/edit/" . $rEmptyOrder['id'],
				"class" => "error",
			), "row", "row empty table");
			$i++;
			if ($i >= 10)
				break;
		}

		return $tpl->parse(array(
			"latest table" => $tpl->parse(array(
				"row" => $tpl->display("row latest table"),
				"type" => "Latest",
			), "table"),
			"unconfirmed table" => $tpl->parse(array(
				"row" => $tpl->display("row unconfirmed table"),
				"type" => "Unconfirmed",
			), "table"),
			"unconfirmed num" => $numUnconfirmedOrders,
			"calculation table" => $tpl->parse(array(
				"row" => $tpl->display("row calculation table"),
				"type" => "Calculation",
			), "table"),
			"waiting num" => $numWaitingOrders,
			"waiting table" => $tpl->parse(array(
				"row" => $tpl->display("row waiting table"),
				"type" => "Waiting",
			), "table"),
			"calculation num" => $numCalculationOrders,
			"empty table" => $tpl->parse(array(
				"row" => $tpl->display("row empty table"),
				"type" => "Empty",
			), "table"),
			"empty num" => $numEmptyOrders,
			"table" => NULL,
		));
	}

	function cacheimg() {
	}
	
	function video() {
		$tpl = new TwigTpl("default/templates/video.twig");
		
		return $tpl;
	}
	
	function opinions() {
		$tpl = new Template("opinions", "default");
		
		return $tpl->display();
	}
	
	function facebook_box() {
		$tpl = new Template("facebook_box", "default");
		
		return $tpl->display();
	}
	
	function r404() {
		$tpl = new Template("404", "default");
		Core::setParseVar("css page", "r404");
		
		return $tpl->display();
	}
	
	function contact() {
		$tpl = new Template("kontakt", "default");
		Core::setParseVar("css page", "contact-page");
		
        SEO::title("Get in touch with GoNParty Team");
		return $tpl->display();
	}
	
	function privatePartyPack() {
		$tpl = new Template("privatePartyPack", "default");
		Core::setParseVar("css page", "contact-page");
		
        SEO::title("Private Party Pack z " . SITE . " ekipo");
		return $tpl->display();
	}
	
	function partyPrevozi() {
		$tpl = new Template("partyPrevozi", "default");
		Core::setParseVar("css page", "contact-page");
		
        SEO::title("Party prevozi z " . SITE . " ekipo");
		return $tpl->display();
	}
	
	function sendContact() {
		$mail = $this->Mails->getNewMailInstance();
		$mail->Subject = "New form " . $_POST['subject'];
		$mail->AddAddress(MAIL_REPLY);
		$mail->Body = $_POST['name'] . "<br />" . $_POST['email'] . "<br />" . $_POST['phone'] . "<br />" . $_POST['message'];
		$mail->AltBody = strip_tags($mail->Body);

		if ($mail->Send())
			return JSON::to(array("success" => true));
		else
			return JSON::to(array("success" => false));
	}
	
	function elFinderFrontend() {
		return '<div id="elfinder"></div>';
	}
	
	function elFinderConnector(){
		$elFinderPath = APP_PATH . "other" . DS . "elfinder" . DS . "v2.1" . DS;
		
		include_once $elFinderPath . 'elFinderConnector.class.php';
		include_once $elFinderPath . 'elFinder.class.php';
		include_once $elFinderPath . 'elFinderVolumeDriver.class.php';
		include_once $elFinderPath . 'elFinderVolumeLocalFileSystem.class.php';
		// Required for MySQL storage connector
		// include_once $elFinderPath . 'elFinderVolumeMySQL.class.php';
		// Required for FTP connector support
		// include_once $elFinderPath . 'elFinderVolumeFTP.class.php';
		
		
		
		$opts = array(
			'debug' => true,
			'roots' => array(
				array(
					'driver'        => 'LocalFileSystem',   // driver for accessing file system (REQUIRED)
					'path'          => WWW_PATH . "media",         // path to files (REQUIRED)
					'root'          => WWW_PATH . "media",         // path to files (REQUIRED)
					'URL'           => URL . "media", // URL to files (REQUIRED)
					'mimeDetect' => $elFinderPath . "mime.types",
					'copyOverwrite' => false,
					'uploadOverwrite' => false,
					'attributes' => array(
						array(  // root must not be hidden
					        'pattern' => '!^/$!',
					        'hidden' => false,
		                    'read' => true,
		                    'write' => true,
		                    'hidden' => false,
		                    'locked' => false,
					    ),
						array(
		                    'read' => true,
		                    'write' => true,
		                    'hidden' => false,
		                    'locked' => false,
	                    )
	                ),
				),
			),
		);
		
		// run elFinder
		$connector = new elFinderConnector(new elFinder($opts));
		$connector->run();
	}

	function cache() {
	    /*
         * 0 - cache
         * 1 - img
         * 2 - w,h
         * 3 - value
         * ...
         */
        // /cache/img/
        $url = NULL;
        $i = 4;
        while (!is_null(Core::url($i))) {
            $url .= (!is_null($url) ? "/" : NULL) . Core::url($i);
            $i++;           
        }

        require_once(APP_PATH . 'other/PHPImageWorkshop/ImageWorkshop.php');

				if (version_compare(PHP_VERSION, '5.3.0') >= 0) {
        			$layer = PHPImageWorkshop\ImageWorkshop::initFromPath(WWW_PATH . rawurldecode($url));
				} else {
					$layer = ImageWorkshop::initFromPath(WWW_PATH . rawurldecode($url));
				}
        if (Core::url(2) == "w") {
	        $thumbWidth = Core::url(3);
			$thumbHeight = null;
			$conserveProportion = true;
			$positionX = 0; // px
			$positionY = 0; // px
			$position = 'MM';
			 
			$layer->resizeInPixel($thumbWidth, $thumbHeight, $conserveProportion, $positionX, $positionY, $position);
        } else if (Core::url(2) == "h") {
	        $thumbWidth = null;
			$thumbHeight = Core::url(3);
			$conserveProportion = true;
			$positionX = 0; // px
			$positionY = 0; // px
			$position = 'MM';
			 
			$layer->resizeInPixel($thumbWidth, $thumbHeight, $conserveProportion, $positionX, $positionY, $position);
        } else if (Core::url(2) == "r") {
        	$layer->rotate(Core::url(3));
        } else
        	die("Image doesn't exists.");

        $dirPath = WWW_PATH . "cache" . DS . "img" . DS . Core::url(2) . DS . Core::url(3) . "/" . rawurldecode($url);
		$filename = end(explode("/", $dirPath));
		$dirPath = substr($dirPath, 0, -strlen($filename));

		$createFolders = true;
		$backgroundColor = null; // transparent, only for PNG (otherwise it will be white if set null)
		$imageQuality = 95; // useless for GIF, usefull for PNG and JPEG (0 to 100%)
 
		$layer->save($dirPath, $filename, $createFolders, $backgroundColor, $imageQuality);

		header('Content-type: image/' . substr(strtolower($filename), -3));
		header('Content-Disposition: filename="' . $filename . '"');


		if (substr(strtolower($filename), -3) == "jpg" || substr(strtolower($filename), -3) == "jpeg") {
			$image = $layer->getResult("ffffff");
			imagejpeg($image, null, 85);
		} else if (substr(strtolower($filename), -3) == "png") {
			$image = $layer->getResult();
			imagepng($image, null, 8);
		} else if (substr(strtolower($filename), -3) == "gif") {
			$image = $layer->getResult("ffffff");
			imagegif($image);
		} else if (substr(strtolower($filename), -3) == "bmp") {
			$image = $layer->getResult("ffffff");
			imagewbmp($image);
		}

		die();/*


       Image::load(rawurldecode($url), WWW_PATH);
        
        if (Core::url(2) == "w")
            Image::resizeToWidth((int)Core::url(3));
        else if (Core::url(2) == "h")
            Image::resizeToHeight((int)Core::url(3));
        else if (Core::url(2) == "r")
            Image::rotate((int)Core::url(3));
        else
            die();

        Image::save($url, WWW_PATH . "cache" . DS . "img" . DS . Core::url(2) . DS . Core::url(3) . DS);
        
        $expire = 60*60*24*365;
        header('Pragma: public');
        header('Cache-Control: maxage=' . $expire);
        header('Expires: ' . gmdate('D, d M Y H:i:s', time()+$expire) . ' GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        
        Image::output();
        
        
        die();*/
	}
}

class DefaultModel extends UFWModel {
	function __construct() {
	}
}

?>