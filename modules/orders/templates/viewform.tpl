<h1>{{ __('heading_view_order') }} - <span class="gray">##OFFER.TITLE## - ##OFFER.SUBTITLE##</span></h1>

<div class="subscriber">
  <h2 class="gray">{{ __('heading_payee_info') }}</h2>
  <div class="grayBg whiteA">
    <div class="basic">
      <p>##PAYEE.NAME## ##PAYEE.SURNAME##</p>
      <p>##PAYEE.ADDRESS##</p>
      <p>##PAYEE.POST##</p>
    </div>
    <div class="num">
      <h3>{{ __('heading_estimate') }}</h3>
      <p>##ORDER.NUM##</p>
    </div>
    <div class="date">
      <h3>{{ __('heading_payment_date') }}</h3>
      <p>##PAYMENT DATE##</p>
    </div>
  </div>
</div>

<div class="calculation">
  <h2 class="gray">{{ __('heading_order_review_and_bills') }}</h2>
  <table>
    <thead>
      <tr><th>{{ __('table_description') }}</th><th>{{ __('table_quantity') }}</th><th>{{ __('table_price') }}</th><th>{{ __('table_sum') }}</th></tr>
    </thead>
    <tbody>
      ##ROW START##<tr><td>##TITLE##</td><td>##QUANTITY##</td><td>##PRICE##</td><td>##SUM##</td></tr>##ROW END##
    </tbody>
    <tfoot>
    	<tr>
      	<td colspan="4">
        	{{ __('your_orders_sum') }}: <span>##SUM TOTAL##</span>
        </td>
      </tr>
    </tfoot>
  </table>
</div>

<div class="grayBg whiteA reservation" style="border: 1px solid #b80004;">
  <div>
    <h3>{{ __('heading_reservation') }}</h3>
    <div class="text">
      {{ __('text_reservation') }}
    </div>
    <div class="price">
      ##RESERVATION PRICE##
    </div>
    <div class="clear"></div>
  </div>
</div>

<div class="separatorLines">
	<div class="col first">
  </div>
  <div class="col second">
  </div>
</div>

<div class="grayBg whiteA rest">
	<h3>{{ __('heading_payment_and_bills') }} <span id="sumRemaining">##SUM REMAINING##</span></h3>
	<div class="text">
		{{ __('text_payment_and_bills') }}
	</div>
	<div class="clear"></div>
</div>

<div class="grayBg whiteA ports">
	##PORTION START##<div class="portion">
	    <div class="portNum">
	      ##TITLE##
	    </div>
      <div class="paymentDate">
        {{ __('title_due_date') }}: <span>##DUE DATE##</span>
      </div>
      <div class="status">
        ##STATUS##&nbsp;
      </div>
	    <div class="sum">
	      ##PRICE## €
	    </div>
	  	<div class="clear"></div>
	</div>##PORTION END##
</div>