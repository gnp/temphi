<form method="POST" action="##ACTION##" data-hash="##ORDER.HASH##" id="estimateform">
<input type="hidden" value="##ORDER.HASH##" name="order_hash" />
<input type="hidden" value="##OFFER.ID##" name="offer_id" />

<h1 class="bl np">{{ __('heading_estimate') }} - <span class="gray">##OFFER.TITLE## - ##OFFER.SUBTITLE##</span></h1>

<ul id="steps">
  <li class="active">1. {{ __('offer_step_insert') }}</li>
  <li class="current">2. {{ __('offer_step_review') }}</li>
    <li class="show-in-skip-payment-variant">3. {{ __('offer_step_payment') }}</li>
    <li class="show-in-skip-payment-variant">4. {{ __('offer_step_confirm') }}</li>
    <li class="hide-in-skip-payment-variant">3. {{ __('offer_step_payment_method') }}</li>
    <li class="hide-in-skip-payment-variant">4. {{ __('offer_step_payment') }}</li>
    <li class="hide-in-skip-payment-variant">5. {{ __('offer_step_confirm') }}</li>
</ul>


<div class="subscriber">
  <h2 class="gray">{{ __('heading_payee_info') }}</h2>
  <div class="grayBg whiteA">
    <div class="basic">
      <p>##PAYEE.NAME## ##PAYEE.SURNAME##</p>
      <p>##PAYEE.ADDRESS##</p>
      <p>##PAYEE.POSTAL##</p>
    </div>
    <div class="num">
      <h3>{{ __('estimate_number') }}</h3>
      <p>##ORDER.NUM##</p>
    </div>
  </div>
</div>

<div class="calculation">
  <h2 class="gray">{{ __('heading_order_review_and_bills') }}</h2>
  <table>
    <thead>
      <tr><th>{{ __('table_description') }}</th><th>{{ __('table_quantity') }}</th><th>{{ __('table_price') }}</th><th>{{ __('table_sum') }}</th></tr>
    </thead>
    <tbody>
      ##ROW START##<tr><td>##TITLE##</td><td>##QUANTITY##</td><td>##PRICE##</td><td>##SUM##</td></tr>##ROW END##
    </tbody>
    <tfoot>
    	<tr>
      	<td colspan="4">
        	{{ __('your_orders_sum') }}: <span>##SUM TOTAL## €</span>
          <!--p class="portions">##MAX PORTIONS##</p-->
        </td>
      </tr>
    </tfoot>
  </table>
</div>

<!--div class="whiteA reservation grayBg" style="border: 1px solid #32d400;">
  <div>
    <h3>{{ __('heading_reservation') }}</h3>
    <div class="text">
      {{ __('text_reservation') }}
    </div>
    <div class="price">
      ##RESERVATION PRICE##
    </div>
    <div class="clear"></div>
  </div>
</div>

<div class="separatorLines">
	<div class="col first">
  </div>
  <div class="col second">
  </div>
</div>

<div class="grayBg whiteA rest">
	<h3>{{ __('heading_payment_and_bills') }} <span id="sumRemaining">##SUM REMAINING##€</span></h3>
	<div class="text">
		{{ __('text_payment_and_bills') }}
	</div>
	<div class="portions">
		{{ __('select_bill_number') }}:
		##PAYMENTS LIST##
	</div>
	<div class="clear"></div>
</div>

<div class="grayBg whiteA ports">
	##PORTION START##<div class="portion">
	    <div class="portNum">
	      ##NUM##. {{ __('bill') }}
	    </div>
	    <div class="paymentDate">
	      {{ __('payment_date') }}: <span>##DUE DATE##</span>
	    </div>
	    <div class="sum">
	      ##PRICE## €
	    </div>
	  	<div class="clear"></div>
	</div>##PORTION END##
</div-->

<div class="last">
  <!--{{ __('text_estimate_last') }}-->
  <input class="button medium whiteText" type="button" id="btnSubmitEstimate" value="{{ __('btn_send_order') }}" /> 
  <div class="clear"></div>
  <div style="text-align:right; font-size:12px; margin-top:10px; color:#777;">{{ __('confirm_terms') }}</div>
</div>
</form>
