<div class="grayBg">
  <div class="orders content">
    <h2>{{ __('heading_my_orders') }}</h2>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <thead>
        <tr>
          <th scope="col">{{ __('table_offer') }}</th>
          <th scope="col">{{ __('table_dt_depart') }}</th>
          <th scope="col">{{ __('table_dt_ordered') }}</th>
          <th scope="col">{{ __('table_status') }}</th>
        </tr>
      </thead>
      <tbody>
        ##ORDER START##<tr class="data">
          <td><h3>##TITLE##</h3></td> <!-- ToDo : link? -->
          <td>##DT_START##</td>
          <td>##DT_ADDED##</td>
          <td>##STATUS##</td>
        </tr>
        <tr class="action">
          <td colspan="4">
          	<div><!-- ToDo : when do we disable buttons? -->
              <a href="##URL VIEW##"><img src="/img/icons/news.png">{{ __('btn_view_order') }}</a>
              ##BTN EDIT ORDER START##<a href="##URL##"><img src="/img/icons/edit.png">{{ __('btn_edit_order') }}</a>##BTN EDIT ORDER END##
              ##BTN PAY ORDER START##<a href="##URL##"><img src="/img/icons/edit.png">{{ __('btn_pay_reservation') }}</a>##BTN PAY ORDER END##<!-- @ToDo add icon -->
              <a href="##URL ESTIMATE##"><img src="/img/icons/pdf.png">{{ __('btn_download_estimate') }}</a>
              ##BTN CANCEL ORDER START##<a href="#"><img src="/img/icons/delete.png">{{ __('btn_cancel_order') }}</a>##BTN CANCEL ORDER END##
            </div>
          </td>
        </tr>##ORDER END##
      </tbody>
    </table>
  </div>
</div>