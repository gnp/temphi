<h1>Izbira plačilnega sredstva</h1>
<p class="intro">Izbiraš lahko med plačilom preko sistema Paypal (tudi za kreditne kartice) in Moneto, ki ti omogočata takojšnjo rezervacijo potovanja, ter plačilom z UPN nalogom na naš bančni račun (v 24 urah od prijave). Katerikoli način izbereš, lahko plačaš rezervacijo v višini ##RESERVATION PRICE## ali pa takoj plačaš celoten znesek aranžmaja in si tako prihraniš nekaj časa in dela. =)
</p>

<h2>PLAČILO REZERVACIJE ##RESERVATION PRICE## ZA ZAGOTOVITEV MEST NA POTOVANJU</h2>
<div class="row-fluid">
	<div class="span8">
    <div class="grayBg paypal">
      <h3>Plačilo s Paypal, Visa in Mastercard</h3>
      <img alt="PAYPAL" src="/img/paypal.png">
      <div class="description">
        <p>Plačilo ##RESERVATION PRICE## za rezervacijo se izvede takoj in ti zagotovi mesto na potovanju zate in tvoje prijatelje.</p>
        <p>Podprto je tudi plačilo s karticama Visa in Mastercard. Postopek je enostaven - po kliku samo sledite korakom in navodilom.</p>
        <p>Lahko pa se odločiš tudi za plačilo celotnega zneska. V tem promeru obkljukaj možnost <i>plačati želim celoten znesek</i>.</p>
        <div class="once_pp">
          <input id="once_pp" type="checkbox" />
          <label for="once_pp">Naenkrat želim plačati celoten znesek ##TOTAL##</label>
        </div>
        <a class="button whiteText big" href="##URL PAYPAL##" title="Plačaj s Paypalom" id="paypalPayment">Plačaj s Paypalom</a>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="grayBg moneta">
      <h3>Plačilo z moneto</h3>
      <img alt="MONETA" src="/img/moneta.png">
      <div class="description">
        <p>Z moneto lahko plačate vsi uporabniki Mobitela, Debitela in Simobila.</p>
        <p>Plačilo ##RESERVATION PRICE## za rezervacijo se izvede takoj in ti zagotovi mesto na potovanju zate in tvoje prijatelje.</p>
        <p>Je enostavno, po kliku samo sledite korakom in navodilom.</p>
        <a class="button whiteText big" href="##URL MONETA##" title="Plačaj z moneto" id="monetaPayment">Plačaj z moneto</a>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
  <div class="span4 grayBg">
  	<div class="content upn">
  		<h3>Plačilo preko položnice</h3>
      <img alt="UPN" src="/img/upn.png">
      <div class="description">
      	<p>Plačilo preko spletne banke, na pošti ali kako drugače je potrebno opraviti v roku 24 ur od prijave.</p>
      </div>
      <div class="clear"></div>
      <div class="once">
      	<input id="once" type="checkbox" />
        <label for="once">Naenkrat želim plačati celoten znesek ##TOTAL##</label>
      </div>
      <a class="button whiteText big" href="##URL UPN##" title="Plačaj z UPN" id="upnPayment">Plačaj z UPN</a>
    </div>
  </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
    $("#monetaPayment, #paypalPayment").click(function(){
      windowHeight = window.screen.availHeight;
      windowWidth = window.screen.availWidth;
      
      monetaWindow = window.open($(this).attr("href") + ($(this).attr("id") == "paypalPayment" && $("#once_pp").is(":checked") ? "?full" : ""),'monetaWindow','top=' + parseInt(windowHeight*0.2) + ',left=' + parseInt(windowWidth*0.2) + ',height=' + parseInt(windowHeight*0.6) + ',width=' + parseInt(windowWidth*0.6) + ',directories=false,resizable=true,menubar=false,toolbar=false');
      monetaWindow.focus();

      return false;
    });
    $("#upnPayment").click(function(){
      redir($(this).attr("href") + ($("#once").is(":checked") ? "?full=1" : ""));

      return false;
    });
	});
</script>
<!-- Google Code for rezervacija Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1044362002;
var google_conversion_language = "sl";

var google_conversion_format = "2";
var google_conversion_color = "ffffff";
var google_conversion_label = "Yj6qCJLQtwUQkub-8QM";

var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript"  
src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt=""  
src="//www.googleadservices.com/pagead/conversion/1044362002/?value=0&amp;label=Yj6qCJLQtwUQkub-8QM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>