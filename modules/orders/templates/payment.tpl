<h1>Izbira plačilnega sredstva</h1>
<p class="intro">Plačilo 20€ za rezervacijo ti zagotovi mesto na potovanju. Izbiraš lahko med moneto, ki ti omogoča takojšno rezervacijo na potovanje ter plačilom prekom položnice na naš TRR.</p>

<h2>PLAČILO REZERVACIJE 20€ ZA ZAGOTOVITEV MEST NA POTOVANJU</h2>
<div class="row-fluid">
	<div class="span4 grayBg">
    <div class="moneta">
      <h3>Plačilo z moneto</h3>
      <img alt="MONETA" src="/img/moneta.png">
      <div class="description">
        <p>Z moneto lahko plačate vsi uporabniki Mobitela, Debitela in Simobila.</p>
        <p>Plačilo 20€ za rezervacijo se izvede takoj in ti zagotovi mesto na potovanju zate in tvoje prijatelje.</p>
        <p>Je enostavno, po kliku samo sledite korakom in navodilom.</p>
        <a class="button whiteText big" href="#" title="Plačaj z moneto" id="monetaPayment">Plačaj z moneto</a>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
  <div class="span4 graybg">
    <div class="moneta">
      <h3>Plačilo s PayPal</h3>
      <img alt="MONETA" src="/img/moneta.png">
      <div class="description">
        <p>Z moneto lahko plačate vsi uporabniki Mobitela, Debitela in Simobila.</p>
        <p>Plačilo 20€ za rezervacijo se izvede takoj in ti zagotovi mesto na potovanju zate in tvoje prijatelje.</p>
        <p>Je enostavno, po kliku samo sledite korakom in navodilom.</p>
        <a class="button whiteText big" href="#" title="Plačaj z moneto" id="monetaPayment">Plačaj z moneto</a>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
  <div class="span4 grayBg">
  	<div class="content upn">
  		<h3>Plačilo preko položnice</h3>
      <img alt="UPN" src="/img/upn.png">
      <div class="description">
      	<p>Plačilo preko spletne banke, na pošti ali kako drugače je potrebno opraviti v roku 24 ur od prijave.</p>
      </div>
      <div class="clear"></div>
      <div class="once">
      	<input id="once" type="checkbox">
        <label for="once">Naenkrat želim plačati rezervacijo in celoten znesek 669,9€</label>
      </div>
      <a class="button whiteText big" href="#" title="Plačaj z UPN">Plačaj z UPN</a>
    </div>
  </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$("#monetaPayment").click(function(){
			windowHeight = window.screen.availHeight;
			windowWidth = window.screen.availWidth;
			
			monetaWindow = window.open('##URL MONETA##','monetaWindow','top=' + parseInt(windowHeight*0.2) + ',left=' + parseInt(windowWidth*0.2) + ',height=' + parseInt(windowHeight*0.6) + ',width=' + parseInt(windowWidth*0.6) + ',directories=false,resizable=true,menubar=false,toolbar=false');
			monetaWindow.focus();

			return false;
		});
	});
</script>