<h1 class="bl">##OFFER.TITLE## - ##OFFER.SUBTITLE##</h1>
##LOCKED START##
<div class="grayBg">
	<div style="width: 45%;" class="pull-left">
    {{ __('text_order_changed') }}
	</div>
	<div style="width: 49%;" class="pull-right">
		<p style="font-size: 20px; font-weight: bold;">{{ __('title_wished_order') }}</p>
		<div>##WISHES##</div>
	</div>
	<div class="clearfix"></div>
</div>
##LOCKED END##
<form method="post" action="##ACTION##" id="orderform" data-hash="##ORDER.HASH##" autocomplete="off">
	<input type="hidden" id="offer_id" name="offer_id" value="##OFFER.ID##" />
	<input type="hidden" id="order_hash" name="order_hash" value="##ORDER.HASH##" />
	<h2>{{ __('heading_payee') }} <span class="gray"></span></h2>
	##PAYEE START##
  <div class="grayBg">
    <div style="" id="payee" class="row-fluid customer" data-hash="##HASH##">
      <div class="span3">
        <h3>{{ __('heading_choose_packet_payee') }}</h3>
        ##PACKETS LIST##
        <ul class="includes" id="orderIncludes_##HASH##">
          ##INCLUDES LIST##
        </ul>
      </div>
      <div class="span3">
        <h3>{{ __('heading_personal_info') }}</h3>
	        <input type="text" name="order[##HASH##][email]" placeholder="{{ __('placeholder_real_email') }}" value="##USER.EMAIL##" disabled="disabled" />
	        <input type="text" name="order[##HASH##][name]" placeholder="{{ __('placeholder_name') }}" value="##USER.NAME##" disabled="disabled" />
	        <input type="text" name="order[##HASH##][surname]" placeholder="{{ __('placeholder_surname') }}" value="##USER.SURNAME##" disabled="disabled" />
	        <input type="text" name="order[##HASH##][address]" placeholder="{{ __('placeholder_address') }}" value="##USER.ADDRESS##" disabled="disabled" />
          <input type="text" name="order[##HASH##][post]" placeholder="{{ __('placeholder_post') }}" value="##USER.POST##" />
          <input type="text" name="order[##HASH##][phone]" placeholder="{{ __('placeholder_phone') }}" value="##USER.PHONE##" />
      </div>
      <div class="span3 departure">
        <h3>{{ __('heading_department') }}</h3>
        <ul id="orderDepartments_##HASH##">
          ##DEPARTMENTS LIST##
        </ul><br />
        <h3>{{ __('heading_notes') }}</h3>
        <textarea class="span12" style="height: 48px; resize: none;" name="order[##HASH##][notes]" placeholder="{{ __('placeholder_notes') }}">##NOTES##</textarea>
      </div>
      <div class="span3 additions">
        <h3>{{ __('heading_additions') }}</h3>
        <ul id="orderAdditions_##HASH##">
          ##ADDITIONS LIST##
        </ul>
      </div>
      <div class="perSum">{{ __('sum_packet_additions') }}: <span id="payment_##HASH##">##PAYMENT##</span></div>
    </div>
	</div>
	##PAYEE END##

	<div id="customers" style="clear: both;">
		<h2>{{ __('heading_choose_packets_friend') }}</h2>
		##CUSTOMER START##
		<div style="position: relative; clear: both; background-color: #ccc; float: left;" id="customer_##HASH##" class="row-fluid customer" data-hash="##HASH##">
	      <div class="grayBg">
	        <div class="row-fluid customer" data-hash="##HASH##">
	          <div class="span3">
	            <h3>{{ __('heading_choose_packet_friend') }}</h3>
	            ##PACKETS LIST##
	            <ul class="includes" id="orderIncludes_##HASH##">
	              ##INCLUDES LIST##
	            </ul>
	          </div>
	          <div class="span3">
	            <h3>{{ __('heading_personal_info') }}</h3>
	        	<input type="text" name="order[##HASH##][email]" placeholder="{{ __('placeholder_real_email') }}" value="##USER.EMAIL##" ##USER DISABLED EMAIL## />
		        <input type="text" name="order[##HASH##][name]" placeholder="{{ __('placeholder_name') }}" value="##USER.NAME##" ##USER DISABLED EMAIL## />
		        <input type="text" name="order[##HASH##][surname]" placeholder="{{ __('placeholder_surname') }}" value="##USER.SURNAME##" ##USER DISABLED EMAIL## />
	          </div>
	          <div class="span3 departure">
	            <h3>{{ __('heading_department') }}</h3>
	            <ul id="orderDepartments_##HASH##">
	              ##DEPARTMENTS LIST##
	            </ul>
		        <h3>{{ __('heading_notes') }}</h3>
		        <textarea class="span12" style="height: 48px; resize: none;" name="order[##HASH##][notes]" placeholder="{{ __('placeholder_notes') }}">##NOTES##</textarea>
	          </div>
	          <div class="span3 additions">
	            <h3>{{ __('heading_additions') }}</h3>
	            <ul id="orderAdditions_##HASH##">
	              ##ADDITIONS LIST##
	            </ul>
	          </div>
	          <div class="perSum">{{ __('sum_packet_additions') }}: <span id="payment_##HASH##">##PAYMENT##</span></div>
	        </div>
	      </div>
		</div>
	    <span style="clear: both; display: block; float: right; margin-bottom: 20px; margin-top: 10px; color: #EF1736;" class="removeCustomer" data-hash="##HASH##">{{ __('btn_cancel_participation') }}</span>
	    <span style="clear: both; display: block; float: right; margin-bottom: 20px; margin-top: 10px; color: #EF1736;" class="removeCustomer" data-hash="##HASH##">{{ __('btn_confirm_participation') }}</span>
		##CUSTOMER END##
		<div class="clearfix"></div>
		
		<input type="button" id="addCustomer" value="{{ __('btn_add_friend') }}" class="button whiteText medium gray" style="float: left; clear: left;" />
		
		<input type="button" id="btnUpdateOrder" value="{{ __('btn_update_order') }}" class="btnToEstimate button whiteText medium" style="float: right;" />
		<span class="sumAll" style="float: right;">{{ __('sum_packets_additions') }}: <span id="payment">##PAYMENT##</span></span>
  	<div class="clear"></div>
	</div>
</form>

##INCLUDES START##<li>##TITLE##</li>##INCLUDES END##
##DEPARTMENTS START##<li><input type="radio" value="##ID##" name="order[##HASH##][department_id]" data-value="##DATA-VALUE##"##CHECKED## /><label>##TITLE##<span>##DT_DEPARTURE##</span></label></li>##DEPARTMENTS END##
##ADDITIONS START##<li><input type="checkbox" value="##ID##" name="order[##HASH##][additions][]" data-value="##DATA-VALUE##" class="ordersUserAddition"##CHECKED## /><label>##TITLE##<span>##VALUE##</span></label></li>##ADDITIONS END##