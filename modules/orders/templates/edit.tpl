<input type="hidden" id="id" name="orders[id]" value="##ID##" />

<div class="control-group">
	<label class="control-label" for="offer">Offer</label>
	<div class="controls">
    	<span>##OFFER##</span>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="status">Status</label>
	<div class="controls">
    	<span>##STATUS##</span>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="">Estimate</label>
	<div class="controls">
      <a href="/maestro/orders/downloadEstimate/##ID##" class="btnDownloadEstimate">Download</a>
      <a href="/maestro/orders/downloadEstimate/##ID##?ext=html" class="btnDownloadEstimate">Download HTML</a>
	</div>
</div>

<div class="control-group">
  <label class="control-label" for="">Bill</label>
  <div class="controls">
      <a href="/maestro/orders/downloadBill/##ID##" class="btnDownloadBill">Download</a>
      <a href="/maestro/orders/downloadBill/##ID##?ext=html" class="btnDownloadBill">Download HTML</a>
  </div>
</div>

<div class="control-group">
  <label class="control-label" for="">Log</label>
  <div class="controls">
      <a href="/maestro/orders/downloadLog/##ID##" class="btnDownloadLog">Download</a>
  </div>
</div>

<div class="control-group">
  <label class="control-label" for="">Voucher</label>
  <div class="controls">
      <a href="/maestro/orders/downloadVoucher/##ID##" class="btnDownloadVoucher">PDF</a>
       <a href="/maestro/orders/downloadVoucher/##ID##?ext=html" class="btnDownloadVoucher">HTML</a>
  </div>
</div>

<div class="control-group">
	<label class="control-label" for="">
		Changes<br />
		<a href="/maestro/orders/confirmchanges/##ID##">Confirm</a>
	</label>
	<div class="controls">
    	<div>##WISHES##</div>
	</div>
</div>