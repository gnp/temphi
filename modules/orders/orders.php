<?php

class OrdersController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function index() {
	}
	
	function one() {
	}

	function outputpdf() {
		$sql = "SELECT o.estimate_url FROM orders o WHERE id = " . Router::get("id") . " AND o.user_id = " . Auth::getUserID();
		$q = $this->db->q($sql);

		if (!$this->db->c($q))
			Status::redirect("/");

		$r = $this->db->f($q);

        $fExpl = explode("/", $r['estimate_url']);

		header('Content-Type: application/pdf');
		header("Content-Transfer-Encoding: Binary");
		header("Content-disposition: attachment; filename=".end($fExpl));
		readfile($r['estimate_url']);
	}
	
	function profile() {
		if (!Auth::isLoggedIn())
			return NULL;

		$tpl = new Template("profile", "orders");
		
		$sql = "SELECT ord.id, ord.hash, of.dt_closed, of.title, DATE_FORMAT(of.dt_start, '%e. %c. %Y') AS dt_start, DATE_FORMAT(ou.dt_added, '%e. %c. %Y') AS dt_added, ou.dt_confirmed, ou.dt_rejected, ou.dt_canceled, ord.dt_finished, ord.dt_payed " .
			"FROM offers of " .
			"INNER JOIN orders ord ON ord.offer_id = of.id " .
			"INNER JOIN orders_users ou ON ou.order_id = ord.id " .
			"WHERE ord.user_id = " . Auth::getUserID() . " " .
			"GROUP BY ord.id";
		$q = $this->db->q($sql);
		while ($r = $this->db->f($q)) {
			$r["status"] = array();
			
			if (!Validate::isNullDate($r['dt_confirmed']))
				$r["status"][] = "confimed";
			
			if (!Validate::isNullDate($r['dt_canceled']))
				$r["status"][] = "canceled";
			
			if (!Validate::isNullDate($r['dt_rejected']))
				$r["status"][] = "rejected";
			
			if (!Validate::isNullDate($r['dt_payed']))
				$r["status"][] = "payed";
			
			if (!Validate::isNullDate($r['dt_finished']))
				$r["status"][] = "finished";
			
			if (empty($r["status"]))
				$r["status"][] = "unconfirmed";
			
			$r["status"] = ucfirst(implode(", ", $r["status"]));
			
			$r["url view"] = Router::make("poglednarocila", array("id" => $r['id']));
			$r["url estimate"] = Router::make("view pdf", array("id" => $r['id']));
			
			$r["btn edit order"] = !Validate::isNullDate($r['dt_finished']) || !Validate::isNullDate($r['dt_payed']) || !Validate::isNullDate($r['dt_rejected']) || !Validate::isNullDate($r['dt_canceled']) || $r['dt_closed'] < DT_NOW
				? NULL
				: $tpl->parse(array("url" => Router::make("uredi narocilo", array("hash" => $r['hash']))), "btn edit order");
			
			$r["btn pay order"] = !Validate::isNullDate($r['dt_confirmed']) || !Validate::isNullDate($r['dt_rejected']) || !Validate::isNullDate($r['dt_canceled'])
				? NULL
				: $tpl->parse(array("url" => Router::make("placilna sredstva hash", array("hash" => $r['hash']))), "btn pay order");
				
			$r["btn cancel order"] = $r['dt_payed'] > DT_NULL || $r['dt_finished'] > DT_NULL || $r['dt_rejected'] > DT_NULL || $r['dt_canceled'] > DT_NULL || (strtotime($r['dt_start']) - 1*24*60*60 < time())
				? NULL
				: $tpl->parse(array("url" => Router::make("cancel order", array("id" => $r['id']))), "btn cancel order");
			
			$tpl->parse($r, "order");
		}
		
		$tpl->commit("order");
		
		return $tpl->display();
	}

	function log(){
		file_put_contents(LOG_PATH . "orders" . DS . date("Y-m-d-H-i-s-u") . "-" . $_SERVER['REMOTE_ADDR'] . ".log", json_encode($_POST) . json_encode($_GET) . json_encode($_SERVER) . json_encode($_SESSION));
	}
	
	function orderform() {
		/*$_POST['offer_id'] = 27;
		$_POST['packets'] = array(
			68 => 1
		);*/

		$this->log();
		// validate data
		if (!isset($_POST['offer_id']) || !Validate::isInt($_POST['offer_id']))
			Status::code(404, "Manjkajo podatki o ponudbi.");
		
		if (!isset($_POST['packets']) || !is_array($_POST['packets']))
			Status::code(404, "Manjkajo podatki o paketih.");
		
		// set data
		$offerID = $_POST['offer_id']; // int
		$packets = $_POST['packets']; // array(id => quantity,...)
		
		// get offer info
		$rOffer = $this->Offers->get("first", NULL, array(
			"id" => $offerID,
			"Offers.dt_published > '" . DT_NULL . "'",
			"Offers.dt_published < '" . DT_NOW . "'",
			"Offers.dt_closed > '" . DT_NOW . "'",
			"Offers.dt_opened < '" . DT_NOW . "'",
		));
		
		if (empty($rOffer))
			Status::code(404, "Ponudba ne obstaja");
		
		$validate = $this->checkOccupied($rOffer['id']);
		
		if ($validate['success'] != TRUE)
			Status::code(404, $validate['text']);
		
		// set template
		$tpl = new TwigTpl("orders/templates/orderform.twig");
		$arrData = array();
		
		// temp variables
		$payee = FALSE;
		$customers = 1;
		$total = 0;
		$payment = 0;
		foreach ($packets AS $packet => $quantity) {
			$validatePacket = $this->checkOccupiedPacket($packet);
			
			if ($validatePacket['success'] != TRUE)
				Status::code(404, $validatePacket['text']);

			while ($quantity > 0 && ($total < $validate['available'] || $validate['available'] == -1)) {
				$hash = sha1(microtime());
				$type = $total == 0
					? "payee"
					: "customers";
				
				// get packet info
				$rPacket = $this->Packets->get("first", NULL, array("id" => $packet));

				$arrData[$type][$hash]["hash"] = $hash;
				$arrData[$type][$hash]["departments"] = $this->getDepartments($packet);
				$arrData[$type][$hash]["additions"] = $this->getAdditions($packet);
				$arrData[$type][$hash]["includes"] = $this->getIncludes($packet);
				$arrData[$type][$hash]["packetsList"] = $this->Packets->getList(array(
					"data" => $this->Packets->getArrList(
						NULL,
						array(
							"offer_id" => $rOffer['id'],
							"ticket" => $rPacket['ticket'],
							"Packets.id IN (" . $this->Packets->getAvailablePacketsSQL($rOffer['id']) . ")"
						),
						NULL,
						"id",
						"title"
					),
					"name" => "order[".$hash."][packet_id]",
					"id" => "packet_id_" . $hash,
					"selected" => $packet,
					"class" => "orderPacket"
				));
				$arrData[$type][$hash]["payee"] = !$payee && Auth::isLoggedIn() ? Auth::getUser() : NULL;
				$arrData[$type][$hash]["num"] = $customers;

				$arrData[$type][$hash]["payment"] = $rPacket['price'];
				
				// sum payment
				$payment += $rPacket['price'];
				
				// change temp variables
				if ($payee == FALSE)
					$payee = TRUE;
				else
					$customers++;

				if ($total == 0)
					$arrData["payee"] = &$arrData[$type][$hash];
				
				$total++;
				$quantity--;
			}
		}

		$arrData['action'] = Router::make("predracun");
		$arrData['payment'] = $payment;
		
		$maxPortions = $this->getMaxPortions(MAX_PORTIONS, $rOffer['dt_start'], DT_NOW);
			
		if($maxPortions > 1) {
			$arrData["maxPortions"] = "Payment possible in $maxPortions installments";
		}
		else {
			$arrData["maxPortions"] = false;
		}
		
		// add JS files
		Optimize::addFile("js", array(
			"js/narocilnica.js?2"
		), "narocilnica");
		
		Optimize::addFile("js", array(
			"js/predracun.js"
		), "predracun");
		
		Core::setParseVar("css page", "order");
		
		// parse and return data
		return $tpl->addData(array("offer" => $rOffer, "form" => $arrData, "steps" => SKIP_PAYMENT_METHOD_SELECTION));
	}

	function estimateform() {
		$this->log();
		$order = $_POST;
		$hash = sha1(microtime());
		$refresh = FALSE;

		// GlobalJourney
		if (FALSE && $order['offer_id'] == Settings::get("tomorrowland_id")) {
			$ok = FALSE;
			foreach ($order["order"] AS $ord) {
				if (in_array($ord['email'], array('schtr4jh@schtr4jh.net','centrih.m@gmail.com','lebar.jasmina@gmail.com','lana.lipecky@yahoo.com','rok.opresnik21@gmail.com','zlhkica@yahoo.com','martina.vertovsek@gmail.com','kaja.perme6@gmail.com','primc.kristinaa@gmail.com','aj.jackd@gmail.com','laura.lukavecki@gmail.com','blazjakopic93@gmail.com','benkovictomaz1988@gmail.com','drtomazydj@gmail.com','mojcagergar@gmail.com','alen.koso@gmail.com','eva.filipic94@gmail.com','maja.vrbnjak@hotmail.com','tinekravanja@gmail.com','dalibor@krejic.net','janko.kucera@gmail.com','simon.vaupotic@hotmail.com','katiia.hartman@gmail.com','golob.gregor@gmail.com','jasna.rovcanin@gmail.com','katka.kavcic@gmail.com','nikolinavrzic@gmail.com','kozuhp@yahoo.com','jzamernik@gmail.com','roksteharnik1@gmail.com','nrgvux@gmail.com','anavalant@gmail.com','simon.skrbinek@gmail.com','taja.oremuz@yahoo.com','omgbunnyz@gmail.com','peter.milosic@gmail.com','vzver@hotmail.com','akul.medd@gmail.com','aleks.delbello1@gmail.com','vero_leo_@hotmail.com','marusa95@gmail.com','bkoprivnik100@gmail.com','natasha.raduha@gmail.com','kaja.ivancic@gmail.com','nika.k6@gmail.com','nejc.jesenicnik@gmail.com','milena.romaric@gmail.com','tomaz212@gmail.com','miha.franchise@gmail.com','misel.jez@student.um.si','miha.sreb@gmail.com','hondaaddict@gmail.com','jurevuc@gmail.com','gasperhozjan@gmail.com','sanjaromaric7@gmail.com','ursa91@gmail.com','matevz.ambrozic@siol.net','nush4aa@gmail.com','matija.knezz@gmail.com','kaja.kovich@gmail.com','Adin.Kurtic@gmail.com','janzekovicjanja78@gmail.com','kristian.eric.sipek@gmail.com','gorazd.jurgec@gmail.com','jure_lisec@yahoo.com','ana.vadnov@gmail.com','deniskouter@gmail.com','leo.novakovic@yahoo.com','zaversnik.tina@gmail.com','mia.vouk@gmail.com','tadeiia.t@gmail.com','marko.lorber11@gmail.com','miha.mlakar2@gmail.com','tamara.lorencic@gmail.com','david.cizmesija@gmail.com','andrej.vesel@hotmail.com','denis.vajngerl@gmail.com','cukzor@gmail.com','luka.kacil@gmail.com','tina.klancnik@hotmail.com','ivo.pistor@gmail.com','klemen.you@gmail.com','gojkovicbostjan@gmail.com','rudi.hirsch1989@gmail.com','icengic4@gmail.com','manjamf@gmail.com','sok.blaz@gmail.com','dejan.lol@hotmail.com','luka.prah@gmail.com','kim.pevec.korsic@gmail.com','alesjakomin@gmail.com','rexrot@hotmail.com'))) {
					$ok = TRUE;
					break;
				}
			}
			$ok = TRUE;

			if (!$ok) {
				return JSON::to(array(
					"success" => FALSE,
					"text" => "Email ni prijavljen na GlobalJourney."
				));
			}
		}

		// catch refresh
		if (!isset($order['order']) || empty($order['order'])) {
			return JSON::to(array(
				"success" => FALSE,
				"text" => "Zaznan je refresh"
			));
			if (isset($_SESSION['last order'])) {
				// redirect?
				$rOrder = $this->Orders->get("first", NULL, array("hash" => $_SESSION['last order']['hash']));
				$rBills = $this->OrdersBills->get("first", NULL, array("order_id" => $rOrder['id']));

				if (!empty($rBills)) {
					Status::redirect(Router::make("poglednarocila", array("id" => $rOrder['id'])));
				} else {
					$order = $_SESSION['last order'];
					$hash = $_SESSION['last order']['hash'];
					$refresh = TRUE;

					Core::__LayoutPrepareLoad("main:layout84");
					Optimize::addFile("js", array(
						"js/narocilnica.js"
					), "narocilnica");
					
					Optimize::addFile("js", array(
						"js/predracun.js"
					), "predracun");
					
					Core::setParseVar("css page", "estimateform".(SKIP_PAYMENT_METHOD_SELECTION ? ' skip-payment-step' : ''));
				}
			} else {
				return JSON::to(array(
					"success" => FALSE,
					"text" => "Prazno naročilo",
				));
			}
		} else {
			$_SESSION['last order'] = $order;
			$_SESSION['last order']['hash'] = $hash;
		}

		$rOffer = $this->Offers->get("first", NULL, array("id" => $order['offer_id']));
		
		if (!isset($order['offer_id']) || !Validate::isInt($order['offer_id']))
			return JSON::to(array(
				"success" => FALSE,
				"text" => "Napačna ponudba",
			));
		
		$rPayee = array();
		$arrSentEmails = array();
		foreach ($order['order'] AS $tempHash => $orderUser) {
			$rUser = $this->Users->get("first", NULL, array("email" => $orderUser['email']));
			
			// register user
			if (empty($rUser)) {
				$pass = substr(sha1(microtime()), 0, 10);
				$userId = -1;
				if (!isset($orderUser['address'])) {// ni plačnik!
					if (!empty($orderUser['email'])) {
						$userId = $this->Users->insert(array(
							"email" => $orderUser['email'],
							"name" => $orderUser['name'],
							"surname" => $orderUser['surname'],
							"password" => Auth::hashPassword($pass),
						));
						
						if (!in_array($orderUser['email'], $arrSentEmails)) {
							$this->Mails->automatic("signup-friend", array("user" => $userId, "offer" => $order['offer_id'], "password" => $pass, "to" => $orderUser['email']));
							$arrSentEmails[] = $orderUser['email'];
							$this->Mailchimp->addSubscriber(array(
								"email" => $orderUser['email'],
								"name" => $orderUser['name'],
								"surname" => $orderUser['surname'],
								"tag" => $rOffer['title'],
							));
						}
					} else {
						$userId = $this->Users->insert(array( // @ToDO ... check email?
							"email" => substr(sha1(microtime()), 0, 10) . "@gnp.si",
							"name" => $orderUser['name'],
							"surname" => $orderUser['surname'],
							"enabled" => -1,
							"password" => Auth::hashPassword($pass),
						));
					}
				} else { // plačnik
					$userId = $this->Users->insert(array(
						"email" => $orderUser['email'],
						"name" => $orderUser['name'],
						"surname" => $orderUser['surname'],
						"address" => $orderUser['address'],
						"post" => $orderUser['post'],
						"phone" => $orderUser['phone'],
						//"city_id" => $orderUser['city_id'],
						"password" => Auth::hashPassword($pass),
					));

					Auth::loginByUserID($userId);
					$this->Mails->automatic("signup-payee", array("user" => $userId, "offer" => $order['offer_id'], "password" => $pass, "to" => $orderUser['email']));
					$arrSentEmails[] = $orderUser['email'];
					$this->Mailchimp->addSubscriber(array(
						"email" => $orderUser['email'],
						"name" => $orderUser['name'],
						"surname" => $orderUser['surname'],
						"tag" => $rOffer['title'],
					));
				}
				
				$rUser = $this->Users->get("first", NULL, array("id" => $userId));
			}
			
			$order['order'][$tempHash]['user_id'] = $rUser['id'];
			
			if (empty($rPayee) && isset($orderUser['address'])) {
				$rPayee = $rUser;
				
				/*$rCity = $this->Cities->get("first", NULL, array("id" => $rPayee['city_id']));
				
				if (!empty($rCity))
					$rPayee['postal'] = $rCity['code'] . " " . $rCity['title'];
				else
					$rPayee['postal'] = NULL;*/
			}
		}
		
		if (empty($rPayee))
			return JSON::to(array(
				"success" => FALSE,
				"text" => "Ne najdem plačnika",
			));
		
		$sqlNumOrders = "SELECT MAX(num) AS num FROM orders o WHERE o.dt_added >= '" . date("Y") . "-01-01 00:00:00'";
		$qNumOrders = $this->db->q($sqlNumOrders);
		$rNumOrders = $this->db->f($qNumOrders);

		if (!empty($rNumOrders['num'])) {
			$tempArr = explode("-", $rNumOrders['num']);
			$realMaxNum = (int)ltrim(end($tempArr), "0");
		} else {
			$realMaxNum = 0;
		}

		if ($refresh == FALSE) {
			$orderID = $this->Orders->insert(array(
				"user_id" => $rPayee['id'],
				"hash" => $hash,
				"offer_id" => $order['offer_id'],
				"user_id" => $rPayee['id'],
				"referer" => isset($_COOKIE['referer']) && $_COOKIE['referer'] ? $_COOKIE['referer'] : '',
				"num" => date("Y") . "-" . numToString($realMaxNum + 1),
			));
		} else {
			$rTempOrder = $this->Orders->get("first", NULL, array("hash" => $_SESSION['last order']['hash']));

			if (empty($rTempOrder))
				return JSON::to(array(
					"success" => FALSE,
					"text" => "Ne najdem naročila?"
				));

			$orderID = $rTempOrder['id'];
		}
		//$orderID = $_POST['order_id'];
		
		$tpl = new Template("estimateform", "orders");
		
		// sumarize data
		$arrSumPackets = array();
		$arrSumAdditions = array();
		foreach ($order['order'] AS $userOrder) {
			if (!isset($userOrder['packet_id'])) {
				return JSON::to(array(
					"success" => FALSE,
					"text" => "Ne najdem paketa?"
				));
			}
			if (isset($arrSumPackets[$userOrder['packet_id']]))
				$arrSumPackets[$userOrder['packet_id']]++;
			else
				$arrSumPackets[$userOrder['packet_id']] = 1;
			
			if (isset($userOrder['additions']))
			foreach ($userOrder['additions'] AS $userAddition) {
				if (isset($arrSumAdditions[$userAddition]))
					$arrSumAdditions[$userAddition]++;
				else
					$arrSumAdditions[$userAddition] = 1;
			}
		}
		
		if (empty($arrSumPackets))
			return JSON::to(array(
				"success" => FALSE,
				"text" => "Izberite vsaj en paket",
			));
		
		// sumarize data
		$sum = 0.0;
		
		// get packets from database
		$sqlPackets = "SELECT p.id, p.title, p.price " .
			"FROM packets p " .
			"INNER JOIN offers o ON o.id = p.offer_id " .
			"WHERE p.offer_id = " . $order['offer_id'] . " " . // only selected offer
			"AND p.id IN (" . implode(",", array_keys($arrSumPackets)) . ")" . // only selected packets
			"AND o.dt_published > '" . DT_NULL . "' " . // is published offer
			"AND o.dt_published < '" . DT_NOW . "'" .
			"AND p.dt_published > '" . DT_NULL . "' " . // is published packet
			"AND p.dt_published < '" . DT_NOW . "'" .
			"AND o.dt_opened > '" . DT_NULL . "' " . // is opened
			"AND o.dt_opened < '" . DT_NOW . "'";
		$qPackets = $this->db->q($sqlPackets);
		while ($rPacket = $this->db->f($qPackets)) {
			$tpl->parse(array(
				"title" => $rPacket['title'],
				"price" => makePrice($rPacket['price']),
				"sum" => makePrice($rPacket['price']*$arrSumPackets[$rPacket['id']]),
				"quantity" => $arrSumPackets[$rPacket['id']],
			), "row");
			$sum += (float)($rPacket['price']*$arrSumPackets[$rPacket['id']]);
		}
		
		if (empty($arrSumPackets))
			$arrSumPackets[-1] = 0;

		if (empty($arrSumAdditions))
			$arrSumAdditions[-1] = 0;

		// get additions from database
		$sqlAdditions = "SELECT pa.id, a.title, pa.value " .
			"FROM additions a " .
			"INNER JOIN packets_additions pa ON pa.addition_id = a.id " .
			"INNER JOIN packets p ON p.id = pa.packet_id " .
			"INNER JOIN offers o ON o.id = p.offer_id " .
			"WHERE p.offer_id = " . $order['offer_id'] . " " . // only selected offer
			"AND p.id IN (" . implode(",", array_keys($arrSumPackets)) . ") " . // only selected packets
			"AND pa.id IN (" . implode(",", array_keys($arrSumAdditions)) . ") " . // only selected additions
			"AND o.dt_published > '" . DT_NULL . "' " . // is published offer
			"AND o.dt_published < '" . DT_NOW . "' " .
			"AND p.dt_published > '" . DT_NULL . "' " . // is published packet
			"AND p.dt_published < '" . DT_NOW . "' " .
			"AND o.dt_opened > '" . DT_NULL . "' " . // is opened
			"AND o.dt_opened < '" . DT_NOW . "'";
		$qAdditions = $this->db->q($sqlAdditions);
		while ($rAddition = $this->db->f($qAdditions)) {
			$tpl->parse(array(
				"title" => $rAddition['title'],
				"price" => makePrice($rAddition['value']),
				"sum" => makePrice($rAddition['value']*$arrSumAdditions[$rAddition['id']]),
				"quantity" => $arrSumAdditions[$rAddition['id']],
			), "row");
			$sum += (float)($rAddition['value']*$arrSumAdditions[$rAddition['id']]);
		}

		// stroški prijavnine
		$sum += PROCESSING_COST;

		$tpl->parse(array(
			"title" => "Booking fee",
			"price" => makePrice(PROCESSING_COST),
			"sum" => makePrice(PROCESSING_COST),
			"quantity" => 1,
		), "row");
		
		$tpl->commit("row");
		
		
		$rPacket = $this->db->f();
		$maxPortions = $this->getMaxPortions(MAX_PORTIONS, $rOffer['dt_start'], DT_NOW);
			
		if($maxPortions > 1) {
			$tpl->parse(array("max portions" => "To pay with installments check bellow."));
		}
		else {
			$tpl->parse(array("max portions" => false));
		}
		
		$maxPortions = $this->getMaxPortions(MAX_PORTIONS, $rOffer['dt_closed'], DT_NOW);

		$arrPortions = array();
		for ($i = 1; $i <= $maxPortions; $i++) {
			$arrPortions[$i] = $i;
		}
		
		// delete orders users
		$qDeleteOrdersUsers = $this->OrdersUsers->get("all", NULL, array("order_id" => $orderID));
		while ($rDeleteOrdersUser = $this->db->f($qDeleteOrdersUsers)) {
			// delete orders users additions
			$qDeleteOrdersUsersAdditions = $this->OrdersUsersAdditions->get("all", NULL, array("orders_user_id" => $rDeleteOrdersUser['id']));
			while ($rDeleteOrdersUserAddition = $this->db->f($qDeleteOrdersUsersAdditions)) {
				$this->OrdersUsersAdditions->delete($rDeleteOrdersUserAddition['id']);
			}
			
			$this->OrdersUsers->delete($rDeleteOrdersUser['id']);
		}
		
		// insert new data
		foreach ($order['order'] AS $tempHash => $orderUser) {
			// insert orders user
			$ordersUserID = $this->OrdersUsers->insert(array(
				"order_id" => $orderID,
				"packet_id" => $orderUser['packet_id'],
				"notes" => $orderUser['notes'],
				"user_id" => $orderUser['user_id'], // updated in first while loop
				"city_id" => isset($orderUser['department_id']) ? $orderUser['department_id'] : -1,
			));
			
			// insert orders users addition
			if (isset($orderUser['additions']))
			foreach ($orderUser['additions'] AS $ordersUsersAddition) {
				$this->OrdersUsersAdditions->insert(array(
					"orders_user_id" => $ordersUserID,
					"addition_id" => $ordersUsersAddition,
				));
			}
		}
		
		$rOrder = $this->Orders->get("first", NULL, array("id" => $orderID));
		
		// update sum
		$this->Orders->update(array(
			"price" => $sum,
			"original" => $sum,
		), $rOrder['id']);
		
		$tpl->parse(array(
			"payee" => array(
				"name" => $rPayee['name'],
				"surname" => $rPayee['surname'],
				"address" => $rPayee['address'],
				"postal" => $rPayee['post'],
			),
			"order" => $rOrder,
// 			"payment date" => date("d.m.Y", strtotime($rOffer['dt_closed'])),
// 			"payment date" => $rOffer['dt_opened'] ."|".$rOffer['dt_published'],
			"payment date" => "BUREK",
			"offer" => $rOffer,
			"sum total" => $sum,
			"sum remaining" => $sum-RESERVATION,
			"payments list" => HTML::select(array(
				"id" => "bills",
				"name" => "bills",
				"options" => $arrPortions,
			)),
			"portion" => Settings::get("tomorrowland_id") == $rOffer['id'] ? $this->tomorrowlandPortions(array(), $sum) : $this->portions(array(
				"portions" => 1,
				"max" => $rOffer['dt_closed'],
				"price" => $sum-RESERVATION,
			)),
			"action" => Router::make("placilna sredstva"),
			"reservation price" => makePrice(RESERVATION),
		));
		
		if ($refresh == FALSE)
			return JSON::to(array(
				"success" => true,
				"html" => $tpl->display(),
				"css" => array(
					"n" => "estimateform".(SKIP_PAYMENT_METHOD_SELECTION ? ' skip-payment-step' : ''),
					"o" => "order",
				),
				"newurl" => Router::make("predracun"),
			));
		else {
			return $tpl->display();
		}
		return $tpl->display();
	}
	
	function paymentform() {
		$this->log();
		$rOrder = $this->Orders->get("first", NULL, array("hash" => isset($_POST['hash']) ? $_POST['hash'] : Router::get("hash")));
		
		if (empty($rOrder))
			Status::code(404, "Manjkajoči podatki o naročilu.");

		if (!Validate::isNullDate($rOrder['dt_confirmed']))
			Status::code(404, "Naročilo je že bilo potrjeno");
		
		if (!Validate::isNullDate($rOrder['dt_rejected']))
			Status::code(404, "Naročilo je bilo zavrnjeno");
		
		if (!Validate::isNullDate($rOrder['dt_canceled']))
			Status::code(404, "Naročilo je bilo preklicano");

		$rOffer = $this->Offers->get("first", NULL, array("id" => $rOrder['offer_id']));
		
		if (empty($rOffer))
			Status::code(404, "Manjkajoči podatki o ponudbi.");
		
		$tpl = new Template("paymentform", "orders");
		
		Core::setParseVar("css page", "payment");

		$numPortions = (isset($_POST['bills'])) ? $_POST['bills'] : 1;
		
		$portions = Settings::get("tomorrowland_id") == $rOffer['id'] ? $this->tomorrowlandPortions(array("arr" => true), $rOrder['price']) : $this->portions(array(
			"portions" => $numPortions,
			"max" => $rOffer['dt_closed'],
			"price" => $rOrder['price'] - RESERVATION,
			"arr" => TRUE, // returns array
		));
		
		// @ToDo - needs delete?
		$qPortions = $this->OrdersBills->get("all", null, array("order_id" => $rOrder['id']));
		$sum = 0;
		$notes = null;
		while ($rPortion = $this->OrdersBills->f($qPortions)) {
			$sum += $rPortion['payed'];
			$notes .= $rPortion['notes'];
			$this->OrdersBills->delete($rPortion['id']);
		}

		if ($numPortions > 1 && date("Y-m-d") > date("Y-m-d", strtotime("-15days", strtotime($rOffer['dt_start'])))) {
			$notes = "Naročilo je oddano manj kot 15 dni do odhoda. " . $notes;
			$this->OrdersBills->insert(array(
				"order_id" => $rOrder['id'],
				"dt_valid" => date("Y-m-d H:i:s", strtotime("+1 days")),
				"price" => $rOrder['price'],
				"type" => 1, // reservation
				"reservation" => 1, // @ToDo - remove, we have type instead
				"payed" => $sum,
				"notes" => $notes,
			));
		} else {
		
		// insert reservation
		$this->OrdersBills->insert(array(
			"order_id" => $rOrder['id'],
			"dt_valid" => date("Y-m-d H:i:s", strtotime("+1 days")),
			"price" => RESERVATION,
			"type" => 1, // reservation
			"reservation" => 1, // @ToDo - remove, we have type instead
			"payed" => $sum,
			"notes" => $notes,
		));
		
		// insert other bills
		/*if ($rOrder['id'] == Settings::get("tomorrowland_id")) {
			foreach (Settings::get("tomorrowland_portions") AS $portion) {
				$this->OrdersBills->insert(array(
					"order_id" => $rOrder['id'],
					"dt_valid" => date("Y-m-d H:i:s", strtotime($portion['due date'])),
					"price" => $portion['price'],
					"type" => 2, // basic portion
				));
			}
		} else {*/
			foreach ($portions AS $portion) {
				$this->OrdersBills->insert(array(
					"order_id" => $rOrder['id'],
					"dt_valid" => date("Y-m-d H:i:s", strtotime($portion['due date'])),
					"price" => $portion['price'],
					"type" => 2, // basic portion
				));
			}
		//}
		}
		
		// @ToDo - preostala logika? je še aktivna ponudba? še ni plačana? itd.
		/*
			pošlji mail s predračunom
			če rezervacija ni plačana v 2 dneh, pošlji mail z opomnikom, predračunom in linkom do plačila rezervacije
			če so bili uporabniki kreirani z emailom, jim pošlji link do urejanja profila
			če so bili uporabniki kreirani samo z imenom in priimkom, pošlji mail plačniku, da vnese email
		*/
		
		//$this->Mails->automatic("send-order", array("user" => $rOrder['user_id'], "offer" => $rOffer));

		if (!isset($_POST)) {
			return $this->choosepayment(isset($_POST['hash']) ? $_POST['hash'] : Router::get("hash"))->__toString();
			return $tpl->parse(array(
				"url moneta" => Router::make("moneta takeoff", array("hash" => $rOrder['hash'])),
				"url paypal" => Router::make("paypal", array("view" => "startpayment", "hash" => $rOrder['hash'])),
				"url upn" => Router::make("upn takeoff", array("hash" => $rOrder['hash'])),
				"reservation price" => bold(makePrice(RESERVATION)),
				"total" => $rOrder['price'],
			));
		} else {
			$this->generateEstimate($rOrder['id']);

			$qOrdersUsers = $this->OrdersUsers->get("all", NULL, array("order_id" => $rOrder['id'], "user_id" => $rOrder['user_id']));
			while ($rOrdersUser = $this->db->f($qOrdersUsers)) {
				$_POST['urlrecipient'] = "ouid";
				$_POST['urlrecipients'] = $rOrdersUser['id'];

				$rMail = $this->Mails->get("first", NULL, array("id" => 9));

				$_POST['mails_sent']['subject'] = $rMail['subject'];
				$_POST['mails_sent']['content'] = $rMail['content'];
				$_POST['mails_sent']['mail_id'] = $rMail['id'];
				$_POST['mails_sent']['from'] = MAIL;
				$_POST['mails_sent']['attachment_id'] = 1;
				$this->Mails->send(FALSE);
			}

			return JSON::to(array(
				"success" => TRUE,
				"html" => $this->choosepayment(isset($_POST['hash']) ? $_POST['hash'] : Router::get("hash"))->__toString(),/*$tpl->parse(array(
					"url moneta" => Router::make("moneta takeoff", array("hash" => $rOrder['hash'])),
					"url paypal" => Router::make("paypal", array("view" => "startpayment", "hash" => $rOrder['hash'])),
					"url upn" => Router::make("upn takeoff", array("hash" => $rOrder['hash'])),
					"reservation price" => bold(makePrice(RESERVATION)),
					"total" => bold(makePrice($rOrder['price'])),
				)),*/
				"css" => array(
					"n" => "payment",
					"o" => "estimateform".(SKIP_PAYMENT_METHOD_SELECTION ? ' skip-payment-step' : ''),
				),
			));
		}
	}

	function choosepayment($orderHash = NULL) {
		//$rOrder = $this->Orders->get("first", NULL, array("hash" => empty($orderHash) ? (isset($_POST['hash']) ? $_POST['hash'] : Router::get("hash")) : $orderHash, "user_id" => Auth::getUserID()));
		$rOrder = $this->Orders->get("first", NULL, array("hash" => empty($orderHash) ? (isset($_POST['hash']) ? $_POST['hash'] : Router::get("hash")) : $orderHash));
		
		if (empty($rOrder))
			Status::code(404, "Manjkajoči podatki o naročilu.");

		if (!Validate::isNullDate($rOrder['dt_confirmed']))
			Status::code(404, "Naročilo je že bilo potrjeno");
		
		if (!Validate::isNullDate($rOrder['dt_rejected']))
			Status::code(404, "Naročilo je bilo zavrnjeno");
		
		if (!Validate::isNullDate($rOrder['dt_canceled']))
			Status::code(404, "Naročilo je bilo preklicano");
		
		$rOffer = $this->Offers->get("first", NULL, array("id" => $rOrder['offer_id']));
		
		if (empty($rOffer))
			Status::code(404, "Manjkajoči podatki o ponudbi.");
		
		Core::setParseVar("css page", "payment");

		if(defined('SKIP_PAYMENT_METHOD_SELECTION')) {
			switch(SKIP_PAYMENT_METHOD_SELECTION) {
				case 'braintree':
					Status::redirect(Router::make("braintree takeoff", array("hash" => $rOrder['hash'])));
					break;
			}
		}

		$data = array(
			"url" => array("moneta" => Router::make("moneta takeoff", array("hash" => $rOrder['hash'])),
			"paypal" => Router::make("paypal", array("view" => "startpayment", "hash" => $rOrder['hash'])),
			"upn" => Router::make("upn takeoff", array("hash" => $rOrder['hash'])),
			"braintree" => Router::make("braintree takeoff", array("hash" => $rOrder['hash']))
			),
			"reservation" => bold(makePrice(RESERVATION)),
			"total" => $rOrder['price'],
			"total_friendly" => makePrice($rOrder['price']),
			"paymenttable" => $this->OffersPaymentMethods->getPaymentTable($rOffer['id']),
			"reservation_title" => "Payment for " . $rOffer['title'] . " package on gonparty.eu",
		);

		return new TwigTpl("modules/orders/templates/paymentform.twig", $data);
	}

	function confirmform() {
        $rOrder = $this->Orders->get("first");
		/*$rOrder = $this->Orders->get("first", null, array("hash" => Router::get("hash")));

		if (empty($rOrder))
			Status::code(404, "Manjkajoči podatki o naročilu.");
		
		if (!Validate::isNullDate($rOrder['dt_rejected']))
			Status::code(404, "Naročilo je bilo zavrnjeno");
		
		if (!Validate::isNullDate($rOrder['dt_canceled']))
			Status::code(404, "Naročilo je bilo preklicano");*/

		$data = array(
			"order" => $rOrder,
			"steps" => SKIP_PAYMENT_METHOD_SELECTION
		);

		Core::setParseVar("css page", "payment confirmform");

		return new TwigTpl("modules/orders/templates/confirmform.twig", $data);
	}

	function viewform($id = NULL) {
		$data = $this->getEstimateData(is_null($id) ? Router::get("id") : $id, is_null($id) ? Auth::getUserID() : FALSE);
		
		if (!$data) {
			if (is_null($id)) {
				Debug::addError("Nimate dostopa do naročila.");
				Status::redirect(Router::make("profil"));
			} else
				return "Ni podatkov";
		}
		
		$tpl = new Template("viewform", "orders");
		
		foreach ($data['orders'] AS $order) {
			$order['price'] = makePrice($order['price']);
			$order['sum'] = makePrice($order['sum']);
			$order['quantity'] = makePrice($order['quantity'], 2, null);
			
			$tpl->parse($order, "row");
		}
		
		$tpl->commit("row");
		
		$j = 0;
		if (isset($data['portions']))
		foreach ($data['portions'] AS $i => $portion) {
			if ($portion['type'] == 1)	continue;
			
			if ($portion['type'] == 2) {
				$j++;
			}

			$portion['due date'] = date("d.m.Y", strtotime($portion['due date']));
			$portion['status'] = $portion['payed'] == $portion['price'] ? "Plačano" : NULL;
			if ($portion['payed'] < $portion['price'] && strtotime($portion['due date']) < strtotime(DT_NOW))
				$portion['status'] = "Zaostalo plačilo";

			$portion['title'] = ($portion['type'] == 2 ? $j . ". " : "") . mb_strtoupper($this->OrdersBills->getTypeById($portion['type']), "UTF-8");
			
			$tpl->parse($portion, "portion");
		}
		
		$tpl->commit("portion");
		
		Core::setParsevar("css page", "estimateform".(SKIP_PAYMENT_METHOD_SELECTION ? ' skip-payment-step' : ''));
		
		$data['sum total'] = $data['sum total'];
		$data['sum remaining'] = makePrice($data['sum remaining']);
		$data['reservation price'] = makePrice($data['reservation price']);
		
		return $tpl->parse($data);
	}

	function editform() {
		$rOrder = $this->Orders->get("first", NULL, array("hash" => Router::get("hash"), "user_id" => Auth::getUserID()));

		if (empty($rOrder))
			Status::code(404, "Order doesn't exist.");

		if (!Validate::isNullDate($rOrder['dt_canceled']))
			Status::code(404, "Order has been canceled.");

		if (!Validate::isNullDate($rOrder['dt_rejected']))
			Status::code(404, "Order has been rejected.");

		if (!Validate::isNullDate($rOrder['dt_payed']))
			Status::code(404, "Order had been payed.");

		if (!Validate::isNullDate($rOrder['dt_finished']))
			Status::code(404, "Order is finished.");

		$rOffer = $this->Offers->get("first", NULL, array("id" => $rOrder['offer_id'], "dt_closed > '" . DT_NOW . "'"));

		if (empty($rOffer))
			Status::code(404, "Offer doesn't exist.");

		$rPayee = $this->Users->get("first", NULL, array("id" => Auth::getUserID(), "id" => $rOrder['user_id']));

		if (empty($rPayee))
			Status::code(404, "User doesn't exist.");

		$qOrdersUsers = $this->OrdersUsers->get(
			"all",
			NULL,
			array(
				"order_id" => $rOrder['id']
			),
			array(
				"order by" => "IF(user_id = " . $rOrder['user_id'] . ", 1, 0) DESC, dt_confirmed DESC, dt_canceled DESC, dt_rejected DESC"
			)
		);

		$tpl = new Template("editform", "orders");
		$sumPayment = 0;

		while ($rOrdersUser = $this->OrdersUsers->f($qOrdersUsers)) {
			$hash = sha1($rOrdersUser['id']);

			// dobumo podatke o izbranem paketu
			$rPacket = $this->Packets->get("first", NULL, array("id" => $rOrdersUser['packet_id']));

			// prikažemo izbran paket in pakete, ki so še na voljo
			$parsedPackets = $this->Packets->getList(array(
				"data" => $this->Packets->getArrList(
					NULL,
					array(
						"offer_id" => $rOffer['id'],
						"ticket" => $rPacket['ticket'], // display only active?
						"dt_published > '" . DT_NULL . "'",
						"dt_published < '" . DT_NOW . "'",
						"(Packets.id IN (" . $this->Packets->getAvailablePacketsSQL($rOffer['id']) . ") OR Packets.id = " . $rPacket['id'] . ")"
					),
					NULL,
					"id",
					"title"
				),
				"name" => "order[##HASH##][packet_id]",
				"id" => "packet_id_" . $hash,
				"selected" => $rOrdersUser['packet_id'],
				"class" => "orderPacket"
			));

			// prikažemo dodatke
			$parsedIncludes = $this->parseIncludes($rOrdersUser['packet_id'], $tpl, NULL, $hash);

			// dobimo uporabnikove podatke
			$rUser = $this->Users->get("first", NULL, array("id" => $rOrdersUser['user_id']));

			// prikažemo uporabnikove podatke
			$parsedCitiesList = $this->Cities->getList(array(
				"disabled" => TRUE,
				"mk" => "CONCAT(code, ' ', title)",
				"selected" => $rUser['city_id'],
			));

			// prikažemo odhodna mesta in izberemo že izbranega
			$parsedDepartments = $this->parseDepartments($rOrdersUser['packet_id'], $tpl, $rOrdersUser['city_id'], $hash);

			// prikažemo doplačila in izberemo že izbrana
			$qSelectedAdditions = $this->OrdersUsersAdditions->get("all", NULL, array("orders_user_id" => $rOrdersUser['id']));
			$arrSelectedAdditions = array();
			while ($rSelectedAddition = $this->db->f($qSelectedAdditions))
				$arrSelectedAdditions[] = $rSelectedAddition['addition_id'];

			$parsedAdditions = $this->parseAdditions($rOrdersUser['packet_id'], $tpl, $arrSelectedAdditions, $hash);
			$sumAdditions = $this->getAdditionsPrice($rOrdersUser['packet_id'], $arrSelectedAdditions);

			$sumPayment += $sumAdditions + $rPacket['price'];

			$tpl->parse(array(
				"packets list" => $parsedPackets,
				"includes list" => $parsedIncludes,
				"additions list" => $parsedAdditions,
				"departments list" => $parsedDepartments,
				"cities list" => $parsedCitiesList,
				"user" => $rUser,
				"user disabled email" => !isset($rUser['email']) || empty($rUser['email']) || $rUser == FALSE ? NULL : 'disabled="disabled"',
				"hash" => $hash,
				"notes" => $rOrdersUser['notes'],
				"payment" => makePrice($sumAdditions + $rPacket['price']),
			), $rOrdersUser['user_id'] == $rOrder['user_id'] ? "payee" : "customer");
		}

		$tpl->commit("payee");
		$tpl->commit("customer");

		Core::setParseVar("css page", "order");

		// add JS files
		Optimize::addFile("js", array(
			"js/editorder.js"
		), "uredi prijavo");

		return $tpl->parse(array(
			"locked" => Validate::isNullDate($rOrder['dt_locked'])
				? NULL
				: $tpl->parse(array(
					"wishes" => nl2br($rOrder['wishes']),
				), "locked"),
			"order" => $rOrder,
			"offer" => $rOffer,
			"action" => Router::make("posodobi narocilo", array("hash" => $rOrder['hash'])),
			"payment" => makePrice($sumPayment),
			"includes" => NULL,
			"additions" => NULL,
			"departments" => NULL,
		));
	}

	function updateform() {
		$rOrder = $this->Orders->get("first", NULL, array("hash" => $_POST['order_hash'], "user_id" => Auth::getUserID()));

		if (empty($rOrder))
			Status::code(404, "Order doesn't exist.");

		if (!Validate::isNullDate($rOrder['dt_canceled']))
			Status::code(404, "Order has been canceled.");

		if (!Validate::isNullDate($rOrder['dt_rejected']))
			Status::code(404, "Order has been rejected.");

		if (!Validate::isNullDate($rOrder['dt_payed']))
			Status::code(404, "Order had been payed.");

		if (!Validate::isNullDate($rOrder['dt_finished']))
			Status::code(404, "Order is finished.");

		$rOffer = $this->Offers->get("first", NULL, array("id" => $rOrder['offer_id'], "dt_closed > '" . DT_NOW . "'"));

		if (empty($rOffer))
			Status::code(404, "Offer doesn't exist.");

		$rPayee = $this->Users->get("first", NULL, array("id" => Auth::getUserID(), "id" => $rOrder['user_id']));

		if (empty($rPayee))
			Status::code(404, "Naročnik ne obstaja.");

		$adminLog = " == " . date("d.m.Y H:i") . " ==";

		foreach ($_POST['order'] AS $hash => $ordersUser) {
			$adminLog .= "\n";
			$rOrdersUser = $this->OrdersUsers->get("first", NULL, array("SHA1(id) = '" . $hash . "'"));
			
			$rUser = $this->Users->get("first", NULL, array("id" => $rOrdersUser['user_id']));
			$adminLog .= ($rOrdersUser['user_id'] == $rOrder['user_id'] ? "Subscriber: " : "Friend: ") . $rUser['name'] . " " . $rUser['surname'] . " " . $rUser['email'] . "\n";

			$rPacket = $this->Packets->get("first", NULL, array("id" => $ordersUser['packet_id']));

			$adminLog .= "Package: " . $rPacket['title'] . "\n";

			if (empty($rUser) && !empty($orderUser['email'])) {
				$pass = substr(sha1(microtime()), 10, 20);
				if (!empty($orderUser['email'])) {
					$userId = $this->Users->insert(array(
						"email" => $orderUser['email'],
						"name" => $orderUser['name'],
						"surname" => $orderUser['surname'],
						"password" => Auth::hashPassword($pass),
					));

					$this->Mails->automatic("signup-friend", array("user" => $userId, "offer" => $order['offer_id'], "password" => $pass, "to" => $orderUser['email']));
				} else {
					$userId = $this->Users->insert(array( // @ToDO ... check email?
						"email" => substr(sha1(microtime()), 0, 10) . "@gnp.si",
						"name" => $orderUser['name'],
						"surname" => $orderUser['surname'],
						"enabled" => -1,
						"password" => Auth::hashPassword($pass),
					));
					
				}

				$rOrdersUser['user_id'] = $userId; // add new orders user
				$this->OrdersUsers->update(array("user_id" => $userId), $rOrdersUser['id']);
			}

			if (!Validate::isNullDate($rOrdersUser['dt_canceled'])) {
				$adminLog .= "Status: canceled\n";
				//continue; // orders_user is canceled
			}

			if (!Validate::isNullDate($rOrdersUser['dt_rejected'])) {
				$adminLog .= "Status: rejected\n";
				//continue; // orders_user is rejected
			}

			// posodobi paket in odhodno mesto
			/*$this->OrdersUsers->update(array(
				"city_id" => isset($ordersUser['department_id']) ? $ordersUser['department_id'] : -1,
				"packet_id" => $ordersUser['packet_id'],
			), $rOrdersUser['id']);*/

			$adminLog .= "Departure city: ";
			if (isset($ordersUser['department_id'])) {
				$rDepartment = $this->Cities->get("first", NULL, array("id" => isset($ordersUser['department_id']) ? $ordersUser['department_id'] : -1));
				$adminLog .= $rDepartment['title'] . "\n";
			} else {
				$adminLog .= "not marked\n";
			}

			// izbriši dodatke
			/*$qOrdersUsersAdditions = $this->OrdersUsersAdditions->get("all", NULL, array("orders_user_id" => $rOrdersUser['id']));
			while ($rOrdersUsersAddition = $this->db->f($qOrdersUsersAdditions))
				$this->OrdersUsersAdditions->delete($rOrdersUsersAddition['id']);*/

			// dodaj dodatke
			$adminLog .= "Additions: ";
			if (isset($ordersUser['additions'])) {
				foreach ($ordersUser['additions'] AS $addition) {
					/*$this->OrdersUsersAdditions->insert(array(
						"orders_user_id" => $rOrdersUser['id'],
						"addition_id" => $addition,
					));*/

					$rPacketAddition = $this->PacketsAdditions->get("first", NULL, array("id" => $addition));
					$rAddition = $this->Additions->get("first", NULL, array("id" => $rPacketAddition['addition_id']));
					$adminLog .= $rAddition['title'] . ", ";
				}
				$adminLog = substr($adminLog, 0, -2) . "\n"; // remove last ', '
			} else {
				$adminLog .= "nope\n";
			}

			$adminLog .= "Notes: ";
			if (isset($ordersUser['notes']) && !empty($ordersUser['notes'])) {
				$adminLog .= strip_tags($ordersUser['notes']);
			} else {
				$adminLog .= "nope\n";
			}
		}

		$this->Orders->update(array(
			"dt_locked" => DT_NOW,
			"wishes" => $adminLog . "\n",
		), $rOrder['id']);
		// @ToDo - zgeneriram predračun in ga pošljem na mail?

		return JSON::to(array(
			"success" => TRUE,
			"url" => $_SERVER['HTTP_REFERER'],
		));
	}

	function getEstimateData($orderId, $user = FALSE) {
		$rOrder = $this->Orders->get("first", NULL, array("id" => $orderId));
		
// 		$rOrder['pin'] = substr($rOrder['hash'], 0, 5);
		
		if (empty($rOrder))
			return FALSE;
		
		$rPayee = $this->Users->get("first", NULL, array("id" => $rOrder['user_id']));
		
		if (empty($rPayee))
			return FALSE;

		if ($user && $user != $rPayee['id'])
			return FALSE;
		
		//$rPayeePostal = $this->Cities->get("first", NULL, array("id" => $rPayee['city_id']));
		//$rPayee['postal'] = $rPayeePostal['code'];
		//$rPayee['post'] = $rPayeePostal['title'];
		
		$rOffer = $this->Offers->get("first", NULL, array("id" => $rOrder['offer_id']));
		
		if (empty($rOffer))
			return FALSE;
		
		$rOfferPostal = $this->Cities->get("first", NULL, array("id" => $rOffer['city_id']));
		$rOffer['postal'] = $rOfferPostal['code'];
		$rOffer['post'] = $rOffer['city'] = $rOfferPostal['title'];

		$rOfferCountry = $this->Countries->get("first", NULL, array("id" => $rOfferPostal['country_id']));
		$rOffer['country'] = $rOfferCountry['title'];
		
		$arrData = array();
		$sum = 0;
		
		$sqlPackets = "SELECT p.title, COUNT(ou.id) AS quantity, p.price, COUNT(ou.id)*p.price AS sum " .
			"FROM packets p " .
			"INNER JOIN orders_users ou ON ou.packet_id = p.id " .
			"INNER JOIN orders o ON o.id = ou.order_id " .
			"WHERE ou.order_id = " . $rOrder['id'] . " " . // order can be confirmed or added only
			"AND ou.dt_rejected = '" . DT_NULL . "' " . 
			"AND ou.dt_canceled = '" . DT_NULL . "' " . 
			"AND o.dt_rejected = '" . DT_NULL . "' " . 
			"AND o.dt_canceled = '" . DT_NULL . "' " . 
			"GROUP BY p.id";
		$qPackets = $this->db->q($sqlPackets);
		while ($rPacket = $this->db->f($qPackets)){
			$arrData["orders"][] = array(
				"title" => $rPacket['title'],
				"price" => $rPacket['price'],
				"sum" => $rPacket['sum'],
				"quantity" => $rPacket['quantity'],
			);
			$sum += $rPacket['sum'];
		}
		
		$sqlAdditions = "SELECT a.title, COUNT(oua.id) AS quantity, pa.value AS price, COUNT(oua.id)*pa.value AS sum " .
			"FROM additions a " .
			"INNER JOIN packets_additions pa ON pa.addition_id = a.id " .
			"INNER JOIN orders_users_additions oua ON oua.addition_id = pa.id " .
			"INNER JOIN orders_users ou ON ou.id = oua.orders_user_id " .
			"INNER JOIN orders o ON o.id = ou.order_id " .
			"WHERE ou.order_id = " . $rOrder['id'] . " " .
			"AND ou.dt_rejected = '" . DT_NULL . "' " . 
			"AND ou.dt_canceled = '" . DT_NULL . "' " . 
			"AND o.dt_rejected = '" . DT_NULL . "' " . 
			"AND o.dt_canceled = '" . DT_NULL . "' " . 
			"GROUP BY pa.id";
		$qAdditions = $this->db->q($sqlAdditions);
		while ($rAddition = $this->db->f($qAdditions)) {
			$arrData["orders"][] = array(
				"title" => $rAddition['title'],
				"price" => $rAddition['price'],
				"sum" => $rAddition['sum'],
				"quantity" => $rAddition['quantity'],
			);
			$sum += $rAddition['sum'];
		}

		// stroški prijavnine
		$arrData["orders"][] = array(
				"title" => "Booking fee",
				"price" => PROCESSING_COST,
				"sum" => PROCESSING_COST,
				"quantity" => 1,
			);
		$sum += PROCESSING_COST;
		
		$sqlPortions = "SELECT ob.price, ob.dt_valid AS 'due date', ob.reservation, ob.type, ob.payed " .
			"FROM orders_bills ob " .
			"WHERE ob.order_id = " . $rOrder['id'] . " " .
			"ORDER BY ob.type ASC, ob.id ASC";
		$qPortions = $this->db->q($sqlPortions);
		$i = 0;
		while ($rPortion = $this->db->f($qPortions)) {
			$arrData['portions'][] = array(
				"num" => $i,
				"due date" => $rPortion['due date'],
				"price" => $rPortion['price'],
				"payed" => $rPortion['payed'],
				"reservation" => $rPortion['reservation'] == 1 ? 1 : -1,
				"type" => $rPortion['type'],
			);
			
			$i++;
		}
		
		return array(
			"portions" => isset($arrData['portions']) ? $arrData['portions'] : NULL,
			"orders" => isset($arrData['orders']) ? $arrData['orders'] : array(),
			"order" => $rOrder,
			"offer" => $rOffer,
			"payee" => $rPayee,
			"reservation price" => RESERVATION,
			"sum remaining" => $sum - RESERVATION,
			"payment date" => date("d.m.Y", strtotime($rOffer['dt_closed'])),
			"sum total" => $sum
		);
	}

	private function getNearest($date, $near = array(5, 15, 25)) {
		$minNear = 0;
		$diffNear = 30;
		foreach ($near AS $i => $n) {
			if (abs(date("d", $date)-$n) < $diffNear) {
				$minNear = $i;
				$diffNear = abs(date("d", $date)-$n);
			}
		}
		return strtotime($near[$minNear] . "." . date("m.Y", $date));
	}

	private function getMaxPortions($portions, $maxDate, $currentDate) {
		if ($portions < 1)
			$portions = 1;
		else if ($portions > MAX_PORTIONS)
			$portions = MAX_PORTIONS;
		
		$daySec = 30*24*60*60; // 30 days
		$maxPortions = round((strtotime($maxDate)-strtotime($currentDate))/$daySec);
		
		if ($maxPortions < 1)
			$maxPortions = 1;
		else if ($maxPortions > MAX_PORTIONS)
			$maxPortions = MAX_PORTIONS;
		
		// rewrite portions
		return $maxPortions < $portions	
			? $maxPortions
			: $portions;
	}

	private function tomorrowlandPortions($settings = array(), $fullPrice = 0) {
		$tpl = new Template("estimateform", "orders");
		$portions = array();
		foreach (Settings::get("tomorrowland_portions") AS $portion) {
			if ($fullPrice && $portion["type"] == 2) {
				$portion["price"] *= floor($fullPrice/550);
			}

			if ($portion["num"] == 3 && $fullPrice > 0) {
				$portion["price"] = $fullPrice - (19.90 + (floor($fullPrice/550) * 450));
			}

			$portions[] = $portion;
			$tpl->parse($portion, "portion");
		}

		if (isset($settings['arr'])) return $portions;

		return $tpl->display("portion");
	}

	private function portions($settings = array()) {
		$portions = $settings["portions"];
		$totalPrice = $settings["price"];
		
		$currentDate = date("Y-m-d", strtotime("+0 days"));
		$maxDate = $settings["max"];
		
		//$portions = 3;
		//$totalPrice = 300;
		//$partyDate = "2013-08-15";
		//$maxDate = "2013-08-01";
		
		$portions = $this->getMaxPortions($portions, $maxDate, $currentDate);
		
		$defaultPerPortion = round($totalPrice/$portions, 2);
		
		$tpl = new Template("estimateform", "orders");
		
		$i = 15;
		do {
			$firstPortionDate = $currentPortionDate = $this->getNearest(strtotime("+" . $i . " days", strtotime($currentDate)));
			$i++;
		} while ($currentPortionDate < strtotime("+15 days", strtotime($currentDate)));
		
		$i = 0;
		do {
			$lastPortionDate = $this->getNearest(strtotime("-" . $i . " days", strtotime($maxDate)));
			$i++;
		} while ($lastPortionDate > strtotime($maxDate));
		
		$sumPortions = 0;
		$arrPortions = array();
		for ($i = 1; $i <= $portions; $i++) {
			$portion = array(
				"num" => $i,
				"due date" => date("Y-m-d H:i:s", $i == 1
					? $firstPortionDate
					: ($i == $portions
						? $lastPortionDate
						: $this->getNearest($currentPortionDate)
					)),
				"price" => $i == $portions ? $totalPrice - $sumPortions : $defaultPerPortion,
			);
			$arrPortions[] = $portion;
			
			$portion['due date'] = date("d.m.Y", strtotime($portion['due date']));
			$tpl->parse($portion, "portion");
			
			$currentPortionDate = strtotime("+1 month", $currentPortionDate);
			$sumPortions += $i == $portions ? $totalPrice - $sumPortions : $defaultPerPortion;
		}
		
		if (isset($settings['arr']))
			return $arrPortions;
		
		return $tpl->display("portion");
	}

	private function checkOccupiedPacket($packet, $numPassengers = 0) {
		$sqlValidatePacket = "SELECT p.order_limit, p.order_limit_count, COUNT(ou.id) AS count_total " .
			"FROM packets p " .
			"LEFT OUTER JOIN orders_users ou ON (ou.packet_id = p.id AND ou.dt_confirmed > '" . DT_NULL . "') " .
			"LEFT OUTER JOIN orders ord ON (ou.order_id = ord.id AND ord.dt_confirmed > '" . DT_NULL . "') " .
			"WHERE p.id = " . $packet . " " .
			"AND p.dt_published <> '" . DT_NULL . "' " .
			"AND p.dt_published < '" . DT_NOW . "' " .
			"GROUP BY p.id";
		$qValidatePacket = $this->db->q($sqlValidatePacket);
		$rValidatePacket = $this->db->f($qValidatePacket);
		
		if (empty($rValidatePacket))
			return array(
				"success" => FALSE,
				"text" => "This package is not active.",
			);
			
		$available = $rValidatePacket['order_limit'] - $numPassengers;
		
		if ($rValidatePacket['order_limit'] > 0 && $rValidatePacket['order_limit'] <= $numPassengers)
			return array(
				"success" => FALSE,
				"text" => "Maximum number of people on one order is " . $rValidatePacket['order_limit'],
				"order_limit" => $rValidatePacket['order_limit'],
				"order_limit_count" => $rValidatePacket['order_limit_count'],
				"count_total" => $rValidatePacket['count_total'],
				"available" => $available,
			);

		if ($rValidatePacket['order_limit_count'] > 0) {
			$sql = "SELECT COUNT(ou.id) AS count " .
				"FROM orders_users ou " .
				"WHERE p.packet_id = " . $packet . " " .
				"AND (ou.dt_confirmed > '0000-00-00 00:00:00' OR (ou.dt_confirmed = '0000-00-00 00:00:00' AND ou.dt_added > '" . date("Y-m-d H:i:s", strtotime("-20 minutes")) . "'))";
			$q = $this->db->q($sql);
			$r = $this->db->f($q);

			$sql2 = "SELECT COUNT(ou.id) AS count " .
				"FROM orders_users ou " .
				"WHERE p.packet_id = " . $packet . " " .
				"AND ou.dt_confirmed > '0000-00-00 00:00:00'";
			$q2 = $this->db->q($sql2);
			$r2 = $this->db->f($q2);

			if ($r["count"] >= $rValidatePacket['order_limit_count'])
				return array(
					"success" => FALSE,
					"text" => "Prijave so " . round($r["count"]/$rValidatePacket['order_limit_count']*100) . "% zasedene. " .
						$r2['count'] . " od " . $rValidatePacket['order_limit_count'] . " je potrjenih, ostali še niso plačali rezervacije." .
						($r2["count"] < $r["count"] ? " Nepotrjene prijave se sprostijo po 20 minutah." : ""),
				);
		}
		
		return array(
			"success" => TRUE,
			"text" => "Ok",
			"order_limit" => $rValidatePacket['order_limit'],
			"order_limit_count" => $rValidatePacket['order_limit_count'],
			"count_total" => $rValidatePacket['count_total'],
			"available" => $available,
		);
	}

	private function checkOccupied($offer, $numPassengers = 0) {
		$sqlValidateOffer = "SELECT o.order_limit, o.order_limit_count, COUNT(ou.id) AS count_total " .
			"FROM offers o " .
			"LEFT OUTER JOIN orders ord ON (ord.offer_id = o.id AND ord.dt_confirmed > '" . DT_NULL . "') " .
			"LEFT OUTER JOIN orders_users ou ON (ou.order_id = ord.id AND ou.dt_confirmed > '" . DT_NULL . "') " .
			"WHERE o.id = " . $offer . " " .
			"AND o.dt_published <> '" . DT_NULL . "'" .
			"AND o.dt_published < '" . DT_NOW . "'" .
			"AND o.dt_opened <> '" . DT_NULL . "'" .
			"AND o.dt_opened < '" . DT_NOW . "'" .
			"GROUP BY o.id";
		$qValidateOffer = $this->db->q($sqlValidateOffer);
		$rValidateOffer = $this->db->f($qValidateOffer);
		
		if (empty($rValidateOffer))
			return array(
				"success" => FALSE,
				"text" => "Ponudba ni aktivna.",
			);
			
		$available = $rValidateOffer['order_limit'] - $numPassengers;
		
		if ($rValidateOffer['order_limit'] > 0 && $rValidateOffer['order_limit'] <= $numPassengers)
			return array(
				"success" => FALSE,
				"text" => "Največje število prijavljenih oseb na naročilnici je " . $rValidateOffer['order_limit'],
				"order_limit" => $rValidateOffer['order_limit'],
				"order_limit_count" => $rValidateOffer['order_limit_count'],
				"count_total" => $rValidateOffer['count_total'],
				"available" => $available,
			);

		if ($rValidateOffer['order_limit_count'] > 0) {
			$sql = "SELECT COUNT(ou.id) AS count " .
				"FROM orders_users ou " .
				"INNER JOIN orders o ON ou.order_id = o.id " .
				"WHERE o.offer_id = " . $offer . " " .
				"AND (ou.dt_confirmed > '0000-00-00 00:00:00' OR (ou.dt_confirmed = '0000-00-00 00:00:00' AND ou.dt_added > '" . date("Y-m-d H:i:s", strtotime("-20 minutes")) . "'))";
			$q = $this->db->q($sql);
			$r = $this->db->f($q);

			$sql2 = "SELECT COUNT(ou.id) AS count " .
				"FROM orders_users ou " .
				"INNER JOIN orders o ON ou.order_id = o.id " .
				"WHERE o.offer_id = " . $offer . " " .
				"AND ou.dt_confirmed > '0000-00-00 00:00:00'";
			$q2 = $this->db->q($sql2);
			$r2 = $this->db->f($q2);

			if ($r["count"] >= $rValidateOffer['order_limit_count'])
				return array(
					"success" => FALSE,
					"text" => "Prijave so " . round($r["count"]/$rValidateOffer['order_limit_count']*100) . "% zasedene. " .
						$r2['count'] . " od " . $rValidateOffer['order_limit_count'] . " je potrjenih, ostali še niso plačali rezervacije." .
						($r2["count"] < $r["count"] ? " Nepotrjene prijave se sprostijo po 20 minutah." : ""),
				);
		}
		
		return array(
			"success" => TRUE,
			"text" => "Ok",
			"order_limit" => $rValidateOffer['order_limit'],
			"order_limit_count" => $rValidateOffer['order_limit_count'],
			"count_total" => $rValidateOffer['count_total'],
			"available" => $available,
		);
	}

	function json() {
		// set template
		$tpl = new Template(isset($_GET['tpl']) ? $_GET['tpl'] : "orderform", "orders");
		
		if (Router::get("action") == "packetchange") {
			if (!Validate::isInt($_POST['packet']) || !Validate::isInt($_POST['department']) || !is_array($_POST['additions']) || !Validate::isAlphaNumeric($_POST['hash']))
				return JSON::to(array(
					"success" => FALSE,
					"text" => "Manjkajoči podatki",
				));
			
			$rPacket = $this->Packets->get("first", NULL, array("id" => $_POST['packet']));

			$return = array();
			$return["departments"] = $this->parseDepartments($_POST['packet'], $tpl, $_POST['department'], $_POST['hash']);
			$return["additions"] = $this->parseAdditions($_POST['packet'], $tpl, $_POST['additions'], $_POST['hash']);
			$return["includes"] = $this->parseIncludes($_POST['packet'], $tpl, $_POST['additions'], $_POST['hash']);
			$return["payment"] = $rPacket['price'];
			
			$return["success"] = TRUE;
			
			return JSON::to($return);
		} else if (Router::get("action") == "addcustomer") {
			if (!isset($_POST['offer']) || !Validate::isInt($_POST['offer']) || !isset($_POST['num']) || !Validate::isInt($_POST['num']))
				return JSON::to(array(
					"success" => FALSE,
					"text" => "Manjkajoči podatki",
				));
			
			$offer = $_POST['offer'];
			$num = $_POST['num'];
			
			$validate = $this->checkOccupied($offer, $num);
			
			if ($validate['success'] != TRUE)
				return JSON::to($validate);
				
			$hash = sha1(microtime());
			
			// get packet info
			$rPacket = $this->Packets->get("first", NULL, array("offer_id" => $_POST['offer']));
			
			$tpl->manualAddToPart($this->parseDepartments($rPacket['id'], $tpl, array(), $hash), "departments list");
			$tpl->manualAddToPart($this->parseAdditions($rPacket['id'], $tpl, array(), $hash), "additions list");
			$tpl->manualAddToPart($this->parseIncludes($rPacket['id'], $tpl, array(), $hash), "includes list");
			
			// parse all data
			$tpl->parse(array(
				"packets list" => $this->Packets->getList(array(
					"data" => $this->Packets->getArrList(
						NULL,
						array(
							"offer_id" => $_POST['offer'],
							"ticket" => $rPacket['ticket'],
							"dt_published > '" . DT_NULL . "'",
							"dt_published < '" . DT_NOW . "'",
							"Packets.id IN (" . $this->Packets->getAvailablePacketsSQL($rPacket['offer_id']) . ")"
						),
						NULL,
						"id",
						"title"
					),
					"name" => "order[##HASH##][packet_id]",
					"id" => "packet_id_" . $hash,
					"default" => " -- izberite paket -- ",
					"selected" => $rPacket['id'],
					"class" => "orderPacket"
				)),
				"cities list" => $this->Cities->getList(array(
					"id" => "city_id_" . $hash,
					"name" => "order[##HASH##][city_id]",
					"default" => " -- izberite pošto  -- "
				)),
				"departments list" => $tpl->display("departments list"),
				"additions list" => $tpl->display("additions list"),
				"includes list" => $tpl->display("includes list"),
				"hash" => $hash,
				"payment" => $rPacket['price'],
				"num" => $_POST['num'],
			), "customer");
			
			return JSON::to(array(
				"success" => TRUE,
				"html" => $tpl->display("customer"),
				"payment" => $rPacket['price'],
			));
		} else if (Router::get("action") == "portions") {
			if (!Validate::isInt($_POST['offer']) || !Validate::isInt($_POST['portions']) || !Validate::isHash($_POST['order']))
				return JSON::to(array(
					"success" => FALSE,
					"text" => "Manjkajoči podatki",
				));
			
			$rOffer = $this->Offers->get("first", NULL, array("id" => $_POST['offer']));
			
			if (empty($rOffer))
				return JSON::to(array(
					"success" => FALSE,
					"text" => "Ponudba ni bila najdena",
				));

			$rOrder = $this->Orders->get("first", NULL, array("hash" => $_POST['order']));

			if (empty($rOrder))
				return JSON::to(array(
					"success" => FALSE,
					"text" => "Naročilo ni najdeno",
				));
			
			return JSON::to(array(
				"success" => TRUE,
				"html" => Settings::get("tomorrowland_id") == $rOffer['id'] ? $this->tomorrowlandPortions(array(), $rOrder['price']) : $this->portions(array(
					"portions" => $_POST['portions'],
					"max" => date("Y-m-d", strtotime($rOffer['dt_closed'])),
					"price" => $rOrder['price'] - RESERVATION, // second check in backend ;-)
				)),
			));
		}
		
		return JSON::to(array(
			"success" => FALSE,
			"text" => "Something went wrong",
		));
	}

	private function getAdditionsPrice($packet, $checked = array()) {
		if (empty($checked))
			return 0;

		$sqlAdditions = "SELECT pa.value " .
			"FROM additions a " .
			"INNER JOIN packets_additions pa ON pa.addition_id = a.id " .
			"WHERE pa.packet_id = " . $packet . " " .
			"AND pa.id IN (" . implode(",", $checked) . ") " .
			"ORDER BY a.position";
		$qAdditions = $this->db->q($sqlAdditions);
		$sum = 0;
		while ($rAddition = $this->db->f($qAdditions)) {
			$sum += $rAddition['value'];
		}
		
		return $sum;
	}

	private function getAdditions($packet) {
		$sqlAdditions = "SELECT pa.id, a.title, pa.value, a.description " .
			"FROM additions a " .
			"INNER JOIN packets_additions pa ON pa.addition_id = a.id " .
			"WHERE pa.packet_id = " . $packet . " " .
			"ORDER BY a.position";
		$qAdditions = $this->db->q($sqlAdditions);
		$arrAdditions = array();
		while ($rAddition = $this->db->f($qAdditions)) $arrAdditions[] = $rAddition;
		return $arrAdditions;
	}

	private function parseAdditions($packet, $tpl, $checked = array(), $hash = NULL) {
		foreach ($this->getAdditions($packet) as $rAddition) {
			$rAddition['checked'] = in_array($rAddition['id'], $checked)
				? ' checked="checked"'
				: NULL;
			$rAddition['hash'] = is_null($hash) ? sha1(microtime()) : $hash;
			$rAddition['dataValue'] = $rAddition['value'] > 0
				? $rAddition['value']
				: 0;
			$rAddition['value'] = $rAddition['value'] > 0
				? makePrice($rAddition['value'])
				: NULL;
			$tpl->parse($rAddition, "additions");
		}
		
		return $tpl->display("additions");
	}

	private function getDepartments($packet) {
		$sqlDepartments = "SELECT c.id, c.title, pc.dt_departure, pc.fee AS value " .
			"FROM cities c " .
			"INNER JOIN packets_cities pc ON pc.city_id = c.id " .
			"WHERE pc.packet_id = " . $packet;
		$qDepartments = $this->db->q($sqlDepartments);
		$arrDepartments = array();
		while ($rDepartment = $this->db->f($qDepartments)) $arrDepartments[] = $rDepartment;
		return $arrDepartments;
	}
	
	private function parseDepartments($packet, $tpl, $checked = NULL, $hash = NULL) {
		foreach ($this->getDepartments($packet) AS $rDepartment) {
			$rDepartment['checked'] = $rDepartment['id'] == $checked
				? ' checked="checked"'
				: NULL;
			$rDepartment['hash'] = is_null($hash) ? sha1(microtime()) : $hash;
			$rDepartment['dt_departure'] = date("H:i", strtotime($rDepartment['dt_departure']));
			$rDepartment['dataValue'] = $rDepartment['value'] > 0
				? $rDepartment['value']
				: 0;
			$rDepartment['value'] = $rDepartment['value'] > 0
				? $rDepartment['value'] . " €"
				: NULL;
			$tpl->parse($rDepartment, "departments");
		}
		
		return $tpl->display("departments");
	}

	private function getIncludes($packet) {
		$sqlIncludes = "SELECT pi.id, a.title " .
			"FROM additions a " .
			"INNER JOIN packets_includes pi ON pi.addition_id = a.id " .
			"WHERE pi.packet_id = " . $packet . " " .
			"ORDER BY a.position";
		$qIncludes = $this->db->q($sqlIncludes);
		$arrIncludes = array();
		while ($rInclude = $this->db->f($qIncludes)) $arrIncludes[] = $rInclude;
		return $arrIncludes;
	}
	
	private function parseIncludes($packet, $tpl, $checked = NULL, $hash = NULL) {
		foreach ($this->getIncludes($packet) AS $rInclude) {
			$rAddition['hash'] = is_null($hash) ? sha1(microtime()) : $hash;
			$tpl->parse($rInclude, "includes");
		}
		
		return $tpl->display("includes");
	}
	
	function maestro() {
		$sqlOffers = "SELECT o.id, o.title " .
            "FROM offers o" .
            (isset($_GET['offer']) && Validate::isInt($_GET['offer']) ? " WHERE o.id = " . $_GET['offer'] : NULL);
        $qOffers = $this->db->q($sqlOffers);
        $return = NULL;
        
        while ($rOffer = $this->db->f($qOffers)) {
            $temp = array();
            
            $tplFilter = new Template("filter", "orders");
            
			// find all clients
            $sqlClients = "SELECT DISTINCT u.id, u.name, u.surname " .
                "FROM users u " .
                "INNER JOIN orders_users ou ON ou.user_id = u.id " .
                "INNER JOIN packets p ON p.id = ou.packet_id " . 
                "WHERE p.offer_id = " . $rOffer['id'];
            $this->db->q($sqlClients);
            $arrClients = array();
            while ($rClient = $this->db->f())
                $arrClients[$rClient['id']] = $rClient['surname'] . " " . $rClient['name'];
            
			// find all payees
            $sqlPayees = "SELECT DISTINCT u.id, u.name, u.surname " .
                "FROM users u " .
                "INNER JOIN orders o ON o.user_id = u.id " .
                "WHERE o.offer_id = " . $rOffer['id'];
            $this->db->q($sqlPayees);
            $arrPayees = array();
            while ($rPayee = $this->db->f())
                $arrPayees[$rPayee['id']] = $rPayee['surname'] . " " . $rPayee['name'];
            
            // find all cities
            $sqlCities = "SELECT DISTINCT c.id, c.title " .
                "FROM cities c " .
                "INNER JOIN packets_cities pc ON pc.city_id = c.id " .
                "INNER JOIN packets p ON p.id = pc.packet_id " .
                "WHERE p.offer_id = " . $rOffer['id'];
            $this->db->q($sqlCities);
            $arrCities = array();
            while ($rCity = $this->db->f())
                $arrCities[$rCity['id']] = $rCity['title'];
            
			// find all additions
            $sqlAdditions = "SELECT DISTINCT a.id, a.title " .
                "FROM additions a " .
                "INNER JOIN packets_additions pa ON pa.addition_id = a.id " .
                "INNER JOIN packets p ON p.id = pa.packet_id " .
                "WHERE p.offer_id = " . $rOffer['id'];
            $this->db->q($sqlAdditions);
            $arrAdditions = array();
            while ($rAddition = $this->db->f())
                $arrAdditions[$rAddition['id']] = $rAddition['title'];
            
            $temp['filter'] = $tplFilter->parse(array(
                "multicheckbox statuses" => $this->Orders->getList(array(
                    "data" => array(
                        1 => "Payed",
                        2 => "Unconfirmed",
                        3 => "Confirmed",
                        4 => "Canceled",
                        5 => "Rejected",
                    ),
                    "name" => "status[]",
                    "multiple" => "multiple",
                    "selected" => isset($_GET['status']) ? $_GET['status'] : -1,
                    "id" => "multicheckboxOrderStatus" . $rOffer['id'],
                )),
                "multicheckbox clients" => $this->Orders->getList(array(
                    "data" => $arrClients,
                    "name" => "client[]",
                    "multiple" => "multiple",
                    "selected" => isset($_GET['client']) ? $_GET['client'] : -1,
                    "id" => "multicheckboxOrderClient" . $rOffer['id'],
                )),
                "multicheckbox payees" => $this->Orders->getList(array(
                    "data" => $arrPayees,
                    "name" => "payee[]",
                    "multiple" => "multiple",
                    "selected" => isset($_GET['payee']) ? $_GET['payee'] : -1,
                    "id" => "multicheckboxOrderPayee" . $rOffer['id'],
                )),
                "multicheckbox cities" => $this->Orders->getList(array(
                    "data" => $arrCities,
                    "name" => "city[]",
                    "multiple" => "multiple",
                    "selected" => isset($_GET['city']) ? $_GET['city'] : -1,
                    "id" => "multicheckboxOrderCity" . $rOffer['id'],
                )),
                "multicheckbox additions" => $this->Orders->getList(array(
                    "data" => $arrAdditions,
                    "name" => "addition[]",
                    "multiple" => "multiple",
                    "selected" => isset($_GET['addition']) ? $_GET['addition'] : -1,
                    "id" => "multicheckboxOrderAddition" . $rOffer['id'],
                )),
                "offer id" => $rOffer['id'],
                "display" => isset($_GET['offer']) ? "block" : "none",
            ));
            
            $filterOrders = array();
            
            if (isset($_GET['status']) && is_array($_GET['status']) && !empty($_GET['status'])) {
                $ors = array();
            
                if (in_array(1, $_GET['status']))
                    $ors[] = "(o.dt_payed > '" . DT_NULL . "' AND ou.dt_confirmed > '" . DT_NULL . "')";
                    
                if (in_array(2, $_GET['status']))
                    $ors[] = "(ou.dt_confirmed = '" . DT_NULL . "' AND ou.dt_rejected = '" . DT_NULL . "' AND ou.dt_canceled = '" . DT_NULL . "')";
                    
                if (in_array(3, $_GET['status']))
                    $ors[] = "(ou.dt_confirmed > '" . DT_NULL . "')";
                    
                if (in_array(4, $_GET['status']))
                    $ors[] = "ou.dt_canceled > '" . DT_NULL . "'";
                    
                if (in_array(5, $_GET['status']))
                    $ors[] = "ou.dt_rejected > '" . DT_NULL . "'";
                
                if (!empty($ors))
                    $filterOrders[] = "(" . implode(" OR ", $ors) . ")";
            }
            
            if (isset($_GET['client']) && is_array($_GET['client']) && !empty($_GET['client'])) {
                $filterOrders[] = "ou.user_id IN (" . implode(",", $_GET['client']) . ")";
            }
            
            if (isset($_GET['payee']) && is_array($_GET['payee']) && !empty($_GET['payee'])) {
                $filterOrders[] = "o.user_id IN (" . implode(",", $_GET['payee']) . ")";
            }
            
            if (isset($_GET['city']) && is_array($_GET['city']) && !empty($_GET['city'])) {
                $filterOrders[] = "ou.city_id IN (" . implode(",", $_GET['city']) . ") AND o.offer_id = " . $rOffer['id'] . "";
            }
            
            if (isset($_GET['addition']) && is_array($_GET['addition']) && !empty($_GET['addition'])) {
                $filterOrders[] = "ou.id IN (SELECT orders_user_id FROM orders_users_additions WHERE addition_id IN (" . implode(",", $_GET['addition']) . ") AND offer_id = " . $rOffer['id'] . ")";
            }

            if (isset($_GET['order_id']) && !empty($_GET['order_id']) && Validate::isInt($_GET['order_id'])) {
                $filterOrders[] = "o.id = " . $_GET['order_id'];
            }

            $sqlOrders = "SELECT o.id, ou.id AS ouid, ou.notes, ou.user_id, o.num, u.name, u.surname, u.address, u.post, c2.title AS city, u.email, u.phone, o.dt_added, ou.dt_confirmed, ou.dt_rejected, ou.dt_canceled, o.dt_confirmed AS odt_confirmed, o.dt_rejected AS odt_rejected, o.dt_canceled AS odt_canceled, o.dt_payed, o.user_id AS ouser_id, p.title AS packet " .
                "FROM orders o " .
                "INNER JOIN orders_users ou ON ou.order_id = o.id " .
                "LEFT OUTER JOIN packets p ON p.id = ou.packet_id " .
                "LEFT OUTER JOIN users u ON u.id = ou.user_id " .
                "LEFT OUTER JOIN cities c2 ON c2.id = ou.city_id " .
                "WHERE o.offer_id = " . $rOffer['id'] . " " .
                (!empty($filterOrders) ? " AND " . implode(" AND ", $filterOrders) . " " : NULL) .
                "ORDER BY ou.dt_rejected ASC, ou.dt_canceled ASC, o.dt_payed ASC, ou.dt_confirmed ASC, ou.dt_added ASC";
            $qOrders = $this->db->q($sqlOrders);
            
            $temp["title"] = $rOffer['title'] . " orders";
            $temp["id"] = $rOffer['id'];
            $temp["th"] = array("OUID", "Order", "Packet", "Name", "Surname", "Address", "Post", "Department", "Email", "Phone", "Additions", "Notes");
            $ordersUserIDs = array();
			
            while ($rOrder = $this->db->f($qOrders)) {
                $ordersUserIDs[] = $rOrder['ouid'];
                $bold = $rOrder['ouser_id'] == $rOrder['user_id'];

                			$sqlAdditions = "SELECT a.title 
												FROM additions a 
												INNER JOIN packets_additions pa ON pa.addition_id = a.id 
												INNER JOIN orders_users_additions oua ON oua.addition_id = pa.id 
												INNER JOIN orders_users ou ON ou.id = oua.orders_user_id 
												WHERE ou.id = " . $rOrder['ouid'];
											$qAdditions = $this->db->q($sqlAdditions);
											$rOrder['additions'] = array();
											while ($rAddition = $this->db->f($qAdditions))
												$rOrder['additions'][] = $rAddition['title'];
											$rOrder['additions'] = implode("<br />", $rOrder['additions']);

                $temp["row"][] = array(
                    "td" => array(
                        $rOrder['ouid'],
                        bold($rOrder['num'], $bold),
                        bold($rOrder['packet'], $bold),
                        bold($rOrder['name'], $bold),
                        bold($rOrder['surname'], $bold),
                        bold($rOrder['address'], $bold),
                        bold($rOrder['city'], $bold),
                        bold($rOrder['post'], $bold),
                        bold($rOrder['email'], $bold),
                        bold($rOrder['phone'], $bold),
                        bold($rOrder['additions'], $bold),
                        bold($rOrder['notes'], $bold),
                    ),
                    "btn" => array(
						HTML::input(array("type" => "checkbox", "value" => $rOrder['ouid'], "name" => "cbOrdersUsers[]", "class" => "pull-right")),
                    	array(
                            "txt" => '<i class="icon-pencil"></i>',
                            "url" => "/maestro/orders/edit/" . $rOrder['id'],
                            "class" => "warning",
                            "tit" => "Edit",
                        ),
                        /*array(
                            "txt" => '<i class="icon-search"></i>',
                            "url" => "/maestro/orders?order_id=" . $rOrder['id'] . "&offer=" . $rOffer['id'] . "&payee[]=" . $rOrder['ouser_id'],
                            "class" => "info",
                            "disabled" => !is_null($rOrder['dt_canceled']),
                            "tit" => "View",
                        ),*/
                        //"update",
                        ($rOrder['ouser_id'] == $rOrder['user_id'] && Validate::isNullDate($rOrder['dt_confirmed']))
                            ? array(
                                "txt" => '<i class="icon-ok"></i>',
                                "url" => "/maestro/orders/confirm/" . $rOrder['id'],
                                "class" => "success",
                                "disabled" => !is_null($rOrder['dt_confirmed']),
                                "tit" => "Confirm",
                            )
                            : NULL,
                        $rOrder['ouser_id'] == $rOrder['user_id'] && Validate::isNullDate($rOrder['dt_confirmed']) && Validate::isNullDate($rOrder['dt_canceled']) && Validate::isNullDate($rOrder['dt_rejected'])
                            ? array(
                                "txt" => '<i class="icon-minus-sign"></i>',
                                "url" => "/maestro/orders/reject/" . $rOrder['id'],
                                "class" => "danger",
                                "disabled" => !is_null($rOrder['dt_rejected']),
                                "tit" => "Reject",
                            )
                            : NULL,
                        ($rOrder['ouser_id'] == $rOrder['user_id'] && !Validate::isNullDate($rOrder['dt_confirmed']))
                            ? array(
                                "txt" => '<i class="icon-stop"></i>',
                                "url" => "/maestro/orders/cancel/" . $rOrder['id'],
                                "class" => "warning",
                                "disabled" => !is_null($rOrder['dt_canceled']),
                                "tit" => "Cancel",
                            )
                            : NULL,
                        ($rOrder['ouser_id'] == $rOrder['user_id'] && Auth::getUser("status_id") == 1)
                            ? array(
                                "txt" => '<i class="icon-remove"></i>',
                                "url" => "/maestro/orders/delete/" . $rOrder['id'],
                                "class" => "danger btnMaestroConfirmClick",
                                "tit" => "Delete",
                            )
                            : NULL,
                        //'<input class="pull-right" type="checkbox" name="order_users[' . $rOrder['ouid'] . ']" value="' . $rOrder['ouid'] . '" />',
                    ),
                    "class" => (!Validate::isNullDate($rOrder['dt_confirmed']))
                        ? (!Validate::isNullDate($rOrder['dt_payed'])
                            ? "success"
                            : NULL)
                        : (!Validate::isNullDate($rOrder['dt_rejected'])
                            ? "error"
                            : (!Validate::isNullDate($rOrder['dt_canceled'])
                                ? "warning"
                                : ((time() - strtotime($rOrder["dt_added"]) > 86400)
                            		? "error blinking"
                            		: "info"
                                ))),
                );
            }

            /*$temp["btn header"][] = array(
                "txt" => '<i class="icon-plus"></i>',
                "url" => "/maestro/orders/add/" . $rOffer['id'],
                "tit" => "Add",
                "class" => "success",
            );*/
			
            $temp["btn header"][] = "filter";

            $temp["btn header"][] = array(
                "txt" => '<i class="icon-file"></i>',
                "url" => "/maestro/orders/exportxls/" . implode(",", $ordersUserIDs),
                "tit" => "Export",
                "add" => NULL,
                "class" => "default",
            );

            $temp["btn header"][] = array(
                "txt" => '<i class="icon-envelope"></i>',
                "url" => "/maestro/mails/compose",
                "tit" => "Send mail",
                "add" => NULL,
                "class" => "default btnComposeMail",
            );

            $temp["btn header"][] = HTML::input(array("type" => "checkbox", "value" => 1, "name" => "cbOrdersUsersAll", "class" => "pull-right"));
            
            /*$temp["btn header"][] = Core::loadView("makeRelated", "maestro", array(
                "#\" class=\"toggleSelectedOrders" => "Toggle all",
                "#\" class=\"confirmSelectedOrders" => "Confirm selected",
                "#\" class=\"rejectSelectedOrders" => "Reject selected",
                "#\" class=\"cancelSelectedOrders" => "Cancel selected",
            ));*/
            
            $ordersUserIDs[] = -1;
            $sqlAdditions = "SELECT a.title, COUNT(oua.id) AS ca " .
                "FROM additions a " .
                "INNER JOIN packets_additions pa ON pa.addition_id = a.id " .
                "INNER JOIN orders_users_additions oua ON oua.addition_id = pa.id " .
                "INNER JOIN orders_users ou ON ou.id = oua.orders_user_id " .
                "INNER JOIN orders o ON o.id = ou.order_id " .
                "WHERE o.offer_id = " . $rOffer['id'] . " " .
                "AND o.dt_confirmed > '" . DT_NULL . "' " . // 3 orders rules
                "AND o.dt_rejected <= '" . DT_NULL . "' " .
                "AND o.dt_canceled <= '" . DT_NULL . "' " .
                "AND ou.dt_confirmed > '" . DT_NULL . "' " . // 3 users orders rules
                "AND ou.dt_rejected <= '" . DT_NULL . "' " .
                "AND ou.dt_canceled <= '" . DT_NULL . "' " .
                (!empty($ordersUserIDs) ? " AND ou.id IN (" . implode(",", $ordersUserIDs) . ") " : NULL) .
                "GROUP BY a.id";
            $qAdditions = $this->db->q($sqlAdditions);
            while ($rAdditions = $this->db->f($qAdditions)) {
                $temp['legend']["Additions"][] = array(
                    "class" => NULL,
                    "title" => $rAdditions['ca'] . "x " . $rAdditions['title'],
                );
            }
            
            $sqlCities = "SELECT c.title, COUNT(ou.id) AS ca " .
                "FROM cities c " .
                "INNER JOIN orders_users ou ON ou.city_id = c.id " .
                "INNER JOIN orders o ON o.id = ou.order_id " .
                "WHERE o.offer_id = " . $rOffer['id'] . " " .
                "AND o.dt_confirmed > '" . DT_NULL . "' " . // 3 orders rules
                "AND o.dt_rejected <= '" . DT_NULL . "' " .
                "AND o.dt_canceled <= '" . DT_NULL . "' " .
                "AND ou.dt_confirmed > '" . DT_NULL . "' " . // 3 users orders rules
                "AND ou.dt_rejected <= '" . DT_NULL . "' " .
                "AND ou.dt_canceled <= '" . DT_NULL . "' " .
                (!empty($ordersUserIDs) ? " AND ou.id IN (" . implode(",", $ordersUserIDs) . ") " : NULL) .
                "GROUP BY c.id";
            $qCities = $this->db->q($sqlCities);
            while ($rCity = $this->db->f($qCities)) {
                $temp['legend']["Cities"][] = array(
                    "class" => NULL,
                    "title" => $rCity['ca'] . "x " . $rCity['title'],
                );
            }
            
            $sqlPackets = "SELECT p.title, COUNT(ou.id) AS ca " .
                "FROM packets p " .
                "INNER JOIN orders_users ou ON ou.packet_id = p.id " .
                "INNER JOIN orders o ON o.id = ou.order_id " .
                "WHERE o.offer_id = " . $rOffer['id'] . " " .
                "AND o.dt_confirmed > '" . DT_NULL . "' " . // 3 orders rules
                "AND o.dt_rejected <= '" . DT_NULL . "' " .
                "AND o.dt_canceled <= '" . DT_NULL . "' " .
                "AND ou.dt_confirmed > '" . DT_NULL . "' " . // 3 users orders rules
                "AND ou.dt_rejected <= '" . DT_NULL . "' " .
                "AND ou.dt_canceled <= '" . DT_NULL . "' " .
                (!empty($ordersUserIDs) ? " AND ou.id IN (" . implode(",", $ordersUserIDs) . ") " : NULL) .
                "GROUP BY p.id";
            $qPackets = $this->db->q($sqlPackets);
            while ($rPacket = $this->db->f($qPackets)) {
                $temp['legend']["Packets"][] = array(
                    "class" => NULL,
                    "title" => $rPacket['ca'] . "x " . $rPacket['title'],
                );
            }
            
            $sqlStats = "SELECT " .
                    "SUM(IF(ord.dt_payed > '" . DT_NULL . "' AND o.dt_confirmed > '" . DT_NULL . "' AND o.dt_confirmed > '" . DT_NULL . "', 1, 0)) AS payedpeople, " .
                    "SUM(IF(o.dt_confirmed > '" . DT_NULL . "', 1, 0)) AS confirmedpeople, " .
                    "SUM(IF(o.dt_canceled > '" . DT_NULL . "', 1, 0)) AS canceledpeople, " .
                    "SUM(IF(o.dt_rejected > '" . DT_NULL . "', 1, 0)) AS rejectedpeople, " .
                    "SUM(IF(o.dt_confirmed = '" . DT_NULL . "' AND o.dt_rejected = '" . DT_NULL . "' AND o.dt_canceled = '" . DT_NULL . "', 1, 0)) AS unconfirmedpeople " .
                "FROM orders_users o " .
                "INNER JOIN orders ord ON ord.id = o.order_id " .
                "INNER JOIN offers of ON of.id = ord.offer_id " .
                "WHERE of.id = " . $rOffer['id'] . " " .
                (!empty($ordersUserIDs) ? " AND o.id IN (" . implode(",", $ordersUserIDs) . ") " : NULL) .
                "GROUP BY of.id";
            $qStats = $this->db->q($sqlStats);
            while ($rStat = $this->db->f($qStats)) {
                $temp['legend']["Statistics"][] = array(
                    "class" => "success",
                    "title" => "Payed: " /*. $rStat['payed'] . "/" */. $rStat['payedpeople'],
                );
                $temp['legend']["Statistics"][] = array(
                    "class" => "info",
                    "title" => "Unconfirmed: " /*. $rStat['unconfirmed'] . "/" */. $rStat['unconfirmedpeople'],
                );
                $temp['legend']["Statistics"][] = array(
                    "class" => "",
                    "title" => "Confirmed: " /*. $rStat['confirmed'] . "/" */. $rStat['confirmedpeople'],
                );
                $temp['legend']["Statistics"][] = array(
                    "class" => "warning",
                    "title" => "Canceled: " /*. $rStat['canceled'] . "/" */. $rStat['canceledpeople'],
                );
                $temp['legend']["Statistics"][] = array(
                    "class" => "error",
                    "title" => "Rejected: " /*. $rStat['rejected'] . "/" */. $rStat['rejectedpeople'],
                );
            }
            
            $return .= Core::loadView("makeListing", "Maestro", $temp);
        }
        
        return $return;
	}

	function exportxls() {
		$file = $this->PhpExcel->exportSimpleOrder(array("orders_users" => explode(",", Router::get("ids"))));
		
		$file_name = 'gnp_export_' . DT_NOW . '.xlsx';
	    $file_url = $file;
	    header('Content-Type: application/octet-stream');
	    header("Content-Transfer-Encoding: Binary"); 
	    header("Content-disposition: attachment; filename=\"".$file_name."\""); 
	    readfile($file_url);
		return;
	}

	function edit() {
		$tpl = new Template("edit", "Orders");

		$r = $this->Orders->get("first", NULL, array("id" => Router::get("id")));

		$sql = "SELECT o.*, of.title AS offer " .
			"FROM orders o " .
			"INNER JOIN offers of ON of.id = o.offer_id " .
			"WHERE o.id = " . Router::get("id");
		$q = $this->db->q($sql);
		
		$r = $this->db->f($q);
		
		$r['status'] = !Validate::isNullDate($r['dt_confirmed'])
			? (!Validate::isNullDate($r['dt_payed'])
				? "Payed " . date("d.m.Y", strtotime($r['dt_payed']))
				: (!Validate::isNullDate($r['dt_finished'])
					? "Finished " . date("d.m.Y", strtotime($r['dt_finished']))
					: "Confirmed " . date("d.m.Y", strtotime($r['dt_confirmed']))
				)
			)
			: (!Validate::isNullDate($r['dt_rejected'])
				? "Rejected " . date("d.m.Y", strtotime($r['dt_rejected']))
				: (!Validate::isNullDate($r['dt_canceled'])
					? "Canceled " . date("d.m.Y", strtotime($r['dt_canceled']))
					: "Waiting for admin's action"
				)
			);

		$r['wishes'] = nl2br($r['wishes']);
		
		return $this->Maestro->makeEdit(array(
			"content" => $tpl->parse($r) .
				$this->OrdersUsers->maestro(array("id" => $r['id'], "title" => $r['offer'])) .
				$this->OrdersBills->maestro(array("id" => $r['id'], "title" => $r['offer'])),
			"title" => "order",
			"buttons" => " ",
		));
	}

	function update() {
		die("02");
		$this->Orders->update($_POST['order'], $_POST['order']['id']);
		return $this->Maestro->makeUpdate(array(
			"id" => $_POST['order']['id']
		));
	}

	function delete() {
		if (Auth::getUser("status_id") != 1) { die("No, no, no =)"); }
		$this->Orders->delete(Router::get("id"));
		Status::redirect($_SERVER['HTTP_REFERER']);
	}
    
    // returns order price of confirmed orders
    function recalculatePrice($orderID) {
    	$rOrder = $this->Orders->get("first", null, array("id" => $orderID));
    	
		$data = $this->getEstimateData($orderID);

		return $data['sum total'];
    }
	
	// returns sum and payed bills
    function sumBills($orderID) {
    	$sum = array("sum" => 0, "payed" => 0, "seized" => 0, "returned" => 0);
		
		$qBills = $this->OrdersBills->get("all", null, array("order_id" => $orderID));
		while ($rBill = $this->db->f($qBills)) {
			if ($rBill['type'] == 1 || $rBill['type'] == 2) {
				$sum["sum"] += $rBill['price'];
				$sum["payed"] += $rBill['payed'];
			} else if ($rBill['type'] == 3) {
				$sum["returned"] += $rBill['price'];
			} else if ($rBill['type'] == 4) {
				$sum["seized"] += $rBill['price'];
			} 
		}
		
		return $sum;
    }

	function calculateReturn($orderId, $conf = array()) {
		$estimateData = $this->getEstimateData($orderId);
		$sumBills = $this->sumBills($orderId);

		$whatToDo = NULL;

		// order values
		$maxValue = isset($conf['maxValue']) ? $conf['maxValue'] : $estimateData['order']['original'];
		$currentValue = isset($conf['currentValue']) ? $conf['currentValue'] : $estimateData['sum total'];

		// expenses
		$reservation = isset($conf['reservation']) ? $conf['reservation'] : RESERVATION;
		$expenses =  isset($conf['expenses']) ? $conf['expenses'] : 0.0;

		// transactions
		$payed = isset($conf['payed']) ? $conf['payed'] : $sumBills["payed"];
		$returned = isset($conf['returned']) ? $conf['returned'] : -$sumBills["returned"];
		$seized = isset($conf['seized']) ? $conf['seized'] : -$sumBills["seized"];

		$saldo = $payed - $returned - $seized; // all money we have current from client
		$toPay = $currentValue - $saldo;

		// other
		$basis = $maxValue - $reservation - $expenses;
		$fullReturn = isset($conf['fullReturn']) ? $conf['fullReturn'] : FALSE;
		$fullReturn = $fullReturn == 1 || $fullReturn == TRUE ? TRUE : FALSE;

		// day difference
		$dtDeadline = isset($conf['dtDeadline']) ? $conf['dtDeadline'] : $estimateData['offer']['dt_start'];
		$dtCalc = isset($conf['dtCalc']) ? $conf['dtCalc'] : DT_NOW;
		$diffDays = (strtotime($dtDeadline) - strtotime($dtCalc)) / (60*60*24);

		// percents
		$perc = 0.0;
		if ($fullReturn == TRUE) {
			$perc = 1.0;
		} else if ($diffDays >= 21) {
			$perc = 0.6;
		} else if ($diffDays >= 15) {
			$perc = 0.4;
		} else if ($diffDays >= 10) {
			$perc = 0.2;
		} else if ($diffDays >= 5) {
			$perc = 0.1;
		} else {
			$perc = 0.0;
		}

		// max return values
		$basisReturn = $basis * $perc;

		$maxBasisReturn = $basisReturn;
		$maxOverpayedReturn = abs($toPay);

		//fix?
		$saldo = abs($saldo) < 0.01 ? 0 : $saldo;
		$seized = abs($seized) < 0.01 ? 0 : $seized;
		$toPay = abs($toPay) < 0.01 ? 0 : $toPay;
		$maxBasisReturn = abs($maxBasisReturn) < 0.01 ? 0 : $maxBasisReturn;
		$maxOverpayedReturn = abs($maxOverpayedReturn) < 0.01 ? 0 : $maxOverpayedReturn;

		$action = array(
			"type" => -1
		);
		if ($toPay == 0) {
			$whatToDo .= "Vse je ok. ";
		} else if ($toPay > 0) { // stranka dolguje denar
			if ($toPay <= $seized) { // imamo dovolj zaseženih sredstev
				$whatToDo .= "Zasežena sredstva zmanjšamo za " . makePrice($toPay) . ". ";
				$action = array(
					"type" => 4,
					"price" => $toPay,
					"payed" => $toPay,
				);
			} else {
				if ($seized > 0) {
					if ($seized >= $toPay) {
						$whatToDo .= "Zasežena sredstva zmanjšamo za " . makePrice($toPay) . ". ";
						$action = array(
							"type" => 4,
							"price" => $toPay,
							"payed" => $toPay,
						);
					} else {
						$whatToDo .= "Zasežena sredstva zmanjšamo za " . makePrice($seized) . ". ";
						$action = array(
							"type" => 4,
							"price" => $seized,
							"payed" => $seized,
						);
					}
				} else {
					$whatToDo .= "Client still needs to pay " . makePrice($toPay) . ". ";
				}
			}
		} else if ($toPay < 0) { // stranki dolgujemo denar
			if ($maxBasisReturn == 0) {
				if ($maxOverpayedReturn > 0.001) {
					$whatToDo .= "[1] Dodamo " . makePrice($maxOverpayedReturn) . " zaseženih sredstev (je stranka preplačala?).";
					$action = array(
						"type" => 4,
						"price" => -$maxOverpayedReturn,
						"payed" => -$maxOverpayedReturn,
					);
				} else if (abs($maxOverpayedReturn) < 0.001) {
					$whatToDo .= "Vse je ok?";
				} else {
					$whatToDo .= "Stranki ne pripada vračilo.";
				}
			} else if ($maxBasisReturn < 0) {
				$whatToDo .= "Vrnili smo " . makePrice($maxBasisReturn) . " preveč";
			} else {
				if ($saldo < $maxBasisReturn) {
					$whatToDo .= "[2] Dodamo " . makePrice($saldo - $currentValue) . " zaseženih sredstev.";
					$action = array(
						"type" => 4,
						"price" => -($saldo - $currentValue),
						"payed" => -($saldo - $currentValue),
					);
				} else if ($maxBasisReturn == $maxOverpayedReturn) {
					$whatToDo .= "[1] Stranki vrnemo " . makePrice($maxBasisReturn) . ".";
					$action = array(
						"type" => 3,
						"price" => -($maxBasisReturn),
						"payed" => -($maxBasisReturn),
					);
				} else if ($maxBasisReturn > $maxOverpayedReturn) {
					$whatToDo .= "[2] Stranki vrnemo " . makePrice($maxOverpayedReturn) . ".";
					$action = array(
						"type" => 3,
						"price" => -($maxOverpayedReturn),
						"payed" => -($maxOverpayedReturn),
					);
				} else if ($maxBasisReturn < $maxOverpayedReturn) {
					$whatToDo .= "[3] Stranki vrnemo " . makePrice($maxBasisReturn) . ".";
					$action = array(
						"type" => 3,
						"price" => -($maxBasisReturn),
						"payed" => -($maxBasisReturn),
					);
				}
			}
		}

		$arr = array(
			"maxValue" => $maxValue,
			"currentValue" => $currentValue,

			"reservation" => $reservation,
			"expenses" => $expenses,

			"payed" => $payed,
			"returned" => $returned,
			"seized" => $seized,

			"saldo" => $saldo,
			"toPay" => $toPay,

			"basis" => $basis,
			"fullReturn" => $fullReturn,
			"fullReturnChecked" => $fullReturn ? ' checked="checked"' : NULL,
			"perc" => $perc,

			"maxBasisReturn" => $maxBasisReturn,
			"maxOverpayedReturn" => $maxOverpayedReturn,

			"dtDeadline" => $dtDeadline,
			"dtCalc" => $dtCalc,
			"diffDays" => $diffDays,

			"whatToDo" => $whatToDo,

			"action" => $action,
		);

		return $arr;
	}

	function generateEstimate($orderID = null) {
		$orderID = is_null($orderID) ? Router::get("id") : $orderID;
		//$file = $this->PhpExcel->generateEstimate($orderID);
		$file = $this->OrdersBills->generateEstimatePDF($orderID);

		if ($file) {
			$this->Orders->update(array(
				"estimate_url" => $file,
			), $orderID);
			
			return JSON::to(array(
				"success" => TRUE,
				"ask" => "Prenesem predračun?",
				"file" => $file,
				"download" => "/maestro/orders/downloadEstimate/" . $orderID
			));
		} else {
			return JSON::to(array(
				"success" => false,
				"text" => "Ne morem generirat predračuna",
			));
		}
	}

	function generateBill($orderID = null) {
		$orderID = is_null($orderID) ? Router::get("id") : $orderID;
		
		//$file = $this->PhpExcel->generateBill($orderID);
		$file = $this->OrdersBills->generateBillPDF($orderID);
		
		if ($file) {
			$this->Orders->update(array(
				"bill_url" => $file,
			), $orderID);
			
			return JSON::to(array(
				"success" => TRUE,
				"ask" => "Prenesem račun?",
				"file" => $file,
				"download" => "/maestro/orders/downloadBill/" . $orderID
			));
		} else {
			return JSON::to(array(
				"success" => false,
				"text" => "Ne morem generirat računa",
			));
		}
	}
	
	function generateVoucher($orderID = null) {
		$orderID = is_null($orderID) ? Router::get("id") : $orderID;
		$file = $this->OrdersBills->generateVoucherPDF($orderID);
	
		if ($file) {
			$this->Orders->update(array(
					"voucher_url" => $file,
			), $orderID);
			
			return JSON::to(array(
					"success" => TRUE,
					"ask" => "Prenesem predracun?",
					"file" => $file,
					"download" => "/maestro/orders/downloadVoucher/" . $orderID
			));
		} else {
			return JSON::to(array(
					"success" => false,
					"text" => "Ne morem generirat predračuna",
			));
		}
	}
	
	
	/**
	 * Generate all vouchers for given ouid's
	 * @deprecated
	 */
	function generateAllVouchers() {
		
		if(isset($_GET['ouid'])) {
			$ouids = explode(',', $_GET['ouid']);
			
			$len = count($ouids);
			
			if($len == 0) return false;
			
			$qOrderID = $this->db->q("SELECT order_id FROM orders_users WHERE id=".trim(stripslashes($ouids[0])));
			$arrOrderID = $this->db->f($qOrderID);
			$orderID = $arrOrderID['order_id'];
			
			$file = $this->OrdersBills->generateVoucherPDF($orderID, $ouids);
			
			if ($file) {
				echo "TICKET CREATED orderID: ".$orderID." | number of tickets: ".count($ouids).BR;
				$this->Orders->update(array(
						"voucher_url" => $file,
				), $orderID);
			}
			else echo "not payed, orderID: ".$orderID." | number of tickets: ".count($ouids).BR;
			
		}
		
	}
	
	function vouchers() {
		
		// get all orderIDs, ouids and hashes for those orderIDs
		// knowledge: https://www.percona.com/blog/2013/10/22/the-power-of-mysqls-group_concat/
		$sql = "SELECT DISTINCT orders_users.order_id as orderID, orders.hash, orders.voucher_url
			GROUP_CONCAT(DISTINCT orders_users.id ORDER BY orders_users.id) AS ouids
			FROM orders_users, orders
			WHERE orders_users.order_id = orders.id
			GROUP BY orderID
			ORDER BY orderID DESC";
		
		$q = $this->db->q($sql);
		$i = 1;
		$created = 0;
		
		while ($r = $this->db->f($q)) {
				
			$orderID = $r['orderID'];
			$ouids = explode(',', $r['ouids']);
			
// 			echo "orderID: $orderID | voucher_url: ".$r['voucher_url'].BR;
			if($i==100) break;
			$i++;
		}
		
		$tpl = new Template("voucherslist", "orders");
		
		$data = array();
		
		
// 		return $tpl->parse(array(
				
// 				));
		
// 		$tpl = new Template("listing_multiple", "maestro");

		$data["th"] = array("OUID", "Order", "Packet", "Name", "Surname", "Address", "Post", "Department", "Email", "Phone", "Additions", "Notes");
		
		$data["row"][] = array(
				"td" => array(
						'ouid',
						'num',
						'packet',
						'name',
						'surname',
						'address',
						'city',
						'post',
						'email',
						'phone',
						'additions',
						'notes'
				));
		
		return $tpl->parse(array(
				"legend" => isset($data['legend']) && !empty($data['legend']) ? $tpl->display("legend") : NULL,
				"title" => isset($data['title']) ? $data['title'] : "Vouchers",
				"filter" => isset($data['filter']) ? $data['filter'] : NULL,
				"tbody class" => isset($data['tbody class']) ? $data['tbody class'] : NULL,
				"th" => $tpl->display("th"),
				"row" => $tpl->display("row"),
				"id" => isset($data['id']) ? $data['id'] : NULL
		));
		
		
	}
	
	// old functioning page
	function vouchersGenerateAllHistory() {
	
		// get all orderIDs, ouids and hashes for those orderIDs
		// knowledge: https://www.percona.com/blog/2013/10/22/the-power-of-mysqls-group_concat/
		$sql = "SELECT DISTINCT orders_users.order_id as orderID, orders.hash, 
			GROUP_CONCAT(DISTINCT orders_users.id ORDER BY orders_users.id) AS ouids
			FROM orders_users, orders
			WHERE orders_users.order_id = orders.id
			GROUP BY orderID
			ORDER BY orderID DESC";
		
		$q = $this->db->q($sql);
		$i = 1;
		$created = 0;
		
		while ($r = $this->db->f($q)) {
				
			$orderID = $r['orderID'];
			$ouids = explode(',', $r['ouids']);
			
			$file = $this->OrdersBills->generateVoucherPDF($orderID, $ouids);
			
// 			echo "orderID: $orderID | packet: ";
			
			if ($file) {
				echo "TICKET CREATED orderID: ".$orderID." | number of tickets: ".count($ouids).BR;
				$created++;
				$this->Orders->update(array(
						"voucher_url" => $file,
				), $orderID);
			}
			else {
				echo "not payed, orderID: ".$orderID." | number of tickets: ".count($ouids).BR;
			}
				
			if($i==100) break;
			$i++;
		}
	
		echo BR."Total number of vouchers created: $created".BR;
	}
	
	function downloadLog() {
		$rOrder = $this->Orders->get("first", null, array("id" => Router::get("id")));
		
		if (empty($rOrder)) {
			return JSON::to(array(
				"success" => false,
				"text" => "Naročilo ne obstaja"
			));
		} else {
	    header('Content-Type: application/octet-stream');
	    header("Content-Transfer-Encoding: Binary"); 
	    header("Content-disposition: attachment; filename=\"".$rOrder['num']."-".time().".html\"");

			function addToLog($text = "") {
				print '<p>' . str_replace("€", "eur", $text) . "</p>\n";
			}

			addToLog("Created: " . date("d.m.Y", strtotime($rOrder['dt_added'])));
			if ($rOrder['dt_confirmed'] > DT_NULL) addToLog("Confirmed: " . date("d.m.Y", strtotime($rOrder['dt_confirmed'])));
			if ($rOrder['dt_canceled'] > DT_NULL) addToLog("Canceled: " . date("d.m.Y", strtotime($rOrder['dt_canceled'])));
			if ($rOrder['dt_rejected'] > DT_NULL) addToLog("Rejected: " . date("d.m.Y", strtotime($rOrder['dt_rejected'])));
			if ($rOrder['dt_payed'] > DT_NULL) addToLog("Payed: " . date("d.m.Y", strtotime($rOrder['dt_payed'])));
			if ($rOrder['dt_finished'] > DT_NULL) addToLog("Finished: " . date("d.m.Y", strtotime($rOrder['dt_finished'])));

			$qBills = $this->OrdersBills->get("all", null, array("order_id" => $rOrder['id']), array("order by" => "id ASC"));
			$i = 0;
			addToLog();
			while ($rBill = $this->db->f($qBills)) {
				echo addToLog(($rBill['type'] == 1 ? "Reservation: " : ("Bill #".$i.": ")) . makePrice($rBill['payed'], 2, "") . "/" . makePrice($rBill['price']) . " [" . ($rBill['dt_confirmed'] > DT_NULL ? date("Y-m-d", strtotime($rBill['dt_confirmed'])) . "/" : "") . date("Y-m-d", strtotime($rBill['dt_valid'])) . "]" . ($rBill['notes'] ? " (" . $rBill['notes'] . ")" : ""));
				$i++;
			}

			return;
		}
	}
	
	function downloadEstimate() {
		$rOrder = $this->Orders->get("first", null, array("id" => Router::get("id")));
		
		if (empty($rOrder))
			return JSON::to(array(
				"success" => false,
				"text" => "Naro�ilo ne obstaja"
			));
		else if (empty($rOrder['estimate_url'])) {
			$json = $this->generateEstimate($rOrder['id']);
			
			$json = JSON::from($json);
			
			if ($json->success == true) {
				Status::redirect($json->download);
			} else {
				return JSON::to(array(
					"success" => false,
					"text" => "Predra�un je potrebno generirat"
				));
			}
		} else {
			$a = explode("/", $rOrder['estimate_url']);
			$file_name = array_pop($a);
	    $file_url = $rOrder['estimate_url'];

			if (isset($_GET['ext']) && $_GET['ext'] == "html") {
				$file_name .= ".html";
				$file_url .= ".html";
			}

	    header('Content-Type: application/octet-stream');
	    header("Content-Transfer-Encoding: Binary"); 
	    header("Content-disposition: attachment; filename=\"".$file_name."\""); 
	    readfile($file_url);

			return;
		}
	}
	
	function downloadBill() {
		$rOrder = $this->Orders->get("first", null, array("id" => Router::get("id")));
		
		if (empty($rOrder))
			return JSON::to(array(
				"success" => false,
				"text" => "Naročilo ne obstaja"
			));
		else if (empty($rOrder['bill_url'])) {
			$json = $this->generateBill($rOrder['id']);
			
			$json = JSON::from($json);
			
			if ($json->success == true) {
				Status::redirect($json->download);
			} else {
				return JSON::to(array(
					"success" => false,
					"text" => "Račun je potrebno generirat"
				));
			}
		} else {
			$a = explode("/", $rOrder['bill_url']);
			$file_name = array_pop($a);
	    $file_url = $rOrder['bill_url'];

			if (isset($_GET['ext']) && $_GET['ext'] == "html") {
				$file_name .= ".html";
				$file_url .= ".html";
			}

	    header('Content-Type: application/octet-stream');
	    header("Content-Transfer-Encoding: Binary"); 
	    header("Content-disposition: attachment; filename=\"".$file_name."\""); 
	    readfile($file_url);
	    
			return;
		}
	}
    
	function downloadVoucher() {
		$rOrder = $this->Orders->get("first", null, array("id" => Router::get("id")));
		
		if (empty($rOrder)) {
			return JSON::to(array(
					"success" => false,
					"text" => "Naro�ilo ne obstaja"
			));
		}
		else if (empty($rOrder['voucher_url'])) {
			$json = $this->generateVoucher($rOrder['id']);
				
			$json = JSON::from($json);
			
			if ($json->success == true) {
				Status::redirect($json->download);
			} else {
				return JSON::to(array(
						"success" => false,
						"text" => "Predra�un je potrebno generirati"
				));
			}
		}
		else {
			$a = explode("/", $rOrder['voucher_url']);
			$file_name = array_pop($a);
			$file_url = $rOrder['voucher_url'];
	
			if (isset($_GET['ext']) && $_GET['ext'] == "html") {
				$file_name .= ".html";
				$file_url .= ".html";
			}
			
			header('Content-Type: application/octet-stream');
			header("Content-Transfer-Encoding: Binary");
			header("Content-disposition: attachment; filename=\"".$file_name."\"");
			readfile($file_url);
	
			return;
		}
	}
	
    function confirm() {
    	$rOrder = $this->Orders->get("first", NULL, array("id" => Router::get("id")));

		if (empty($rOrder))
			Status::code(404, "Naročilo ne obstaja.");
		
		if (!Validate::isNullDate($rOrder['dt_confirmed']))
			Status::code(404, "Naročilo je že potrjeno");

		$qOrdersUsers = $this->OrdersUsers->get("all", NULL, array("order_id" => $rOrder['id']));
		while ($rOrdersUser = $this->db->f($qOrdersUsers)) {
			if (!Validate::isNullDate($rOrdersUser['dt_confirmed']))
				continue;

			$this->OrdersUsers->update(array(
				"dt_canceled" => DT_NULL,
				"dt_rejected" => DT_NULL,
				"dt_confirmed" => DT_NOW,
			), $rOrdersUser['id']);

			// @ToDo - send confirmation mail to user
		}

		$this->Orders->update(array(
			"dt_canceled" => DT_NULL,
			"dt_rejected" => DT_NULL,
			"dt_confirmed" => DT_NOW,
		), $rOrder['id']);

		// @ToDo - send confirmation mail to administrator

        Status::redirect("/maestro/orders/edit/" . $rOrder['id']);
    }
    
    function reject() {
    	$rOrder = $this->Orders->get("first", NULL, array("id" => Router::get("id")));

		if (empty($rOrder))
			Status::code(404, "Naročilo ne obstaja.");
		
		if (!Validate::isNullDate($rOrder['dt_rejected']))
			Status::code(404, "Naročilo je že zavrnjeno");
		
		if (!Validate::isNullDate($rOrder['dt_canceled']))
			Status::code(404, "Naročilo je že preklicano");

		$qOrdersUsers = $this->OrdersUsers->get("all", NULL, array("order_id" => $rOrder['id']));
		while ($rOrdersUser = $this->db->f($qOrdersUsers)) {
			if (!Validate::isNullDate($rOrdersUser['dt_rejected']))
				continue;

			if (!Validate::isNullDate($rOrdersUser['dt_canceled']))
				continue;

			$this->OrdersUsers->update(array(
				"dt_canceled" => DT_NULL,
				"dt_rejected" => DT_NOW,
				"dt_confirmed" => DT_NULL,
			), $rOrdersUser['id']);

			// @ToDo - send reject mail to user
		}

		$this->Orders->update(array(
			"dt_canceled" => DT_NULL,
			"dt_rejected" => DT_NOW,
			"dt_confirmed" => DT_NULL,
		), $rOrder['id']);

		// @ToDo - send reject mail to administrator

        Status::redirect("/maestro/orders/edit/" . $rOrder['id']);
    }
    
    function cancel() {
    	$rOrder = $this->Orders->get("first", NULL, array("id" => Router::get("id")));

		if (empty($rOrder))
			Status::code(404, "Naročilo ne obstaja.");
		
		if (!Validate::isNullDate($rOrder['dt_rejected']))
			Status::code(404, "Naročilo je že zavrnjeno");
		
		if (!Validate::isNullDate($rOrder['dt_canceled']))
			Status::code(404, "Naročilo je že preklicano");

		$qOrdersUsers = $this->OrdersUsers->get("all", NULL, array("order_id" => $rOrder['id']));
		while ($rOrdersUser = $this->db->f($qOrdersUsers)) {
			if (!Validate::isNullDate($rOrdersUser['dt_rejected']))
				continue;

			if (!Validate::isNullDate($rOrdersUser['dt_canceled']))
				continue;

			$this->OrdersUsers->update(array(
				"dt_canceled" => DT_NOW,
				"dt_rejected" => DT_NULL,
				"dt_confirmed" => DT_NULL,
			), $rOrdersUser['id']);

			// @ToDo - send cancel mail to user
		}

		$this->Orders->update(array(
			"dt_canceled" => DT_NOW,
			"dt_rejected" => DT_NULL,
			"dt_confirmed" => DT_NULL,
		), $rOrder['id']);

		// @ToDo - send cancel mail to administrator

        Status::redirect("/maestro/orders/edit/" . $rOrder['id']);
    }

    function confirmchanges() {
    	$rOrder = $this->Orders->get("first", NULL, array("id" => Router::get("id")));

    	$this->Orders->update(array(
    		"wishes" => NULL,
    		"dt_locked" => DT_NULL,
    	), $rOrder['id']);

    	Status::redirect($_SERVER['HTTP_REFERER']);
    }
    
    private function action($type, $ids) {
        // validate action
        if (!in_array($type, array("confirm", "reject", "cancel")))
            return NULL;
        
        // validate ids
        $ids = explode(",", $ids);
        foreach ($ids AS $id)
            if (!Validate::isInt($id))
                return NULL;
        $ids = implode(",", $ids);
        
        return $type . " :: " . $ids;
    }
}

class OrdersModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "";

		$this->fields = array(
			"offer_id" => array(
				"not null" => true,
			),
			"user_id" => array(
				"not null" => true,
			),
			"dt_added" => array(
				"not null" => true,
				"default" => DT_NOW,
			),
			"dt_confirmed" => array(
				"default" => DT_NULL,
			),
			"dt_rejected" => array(
				"default" => DT_NULL,
			),
			"dt_canceled" => array(
				"default" => DT_NULL,
			),
			"dt_payed" => array(
				"default" => DT_NULL,
			),
			"dt_finished" => array(
				"default" => DT_NULL,
			),
			"dt_locked" => array(
				"default" => DT_NULL,
			),
			"dt_notify_reservation" => array(
				"default" => DT_NULL,
			),
			"wishes" => array(
				"default" => NULL,
			),
			"referer" => array(
				"default" => NULL
			),
			"hash" => array(
				"default" => sha1(microtime())
			),
			"price" => array(),
			"num" => array(
				"unique" => TRUE,
			),
			"original" => array(),
			"bill_url" => array(),
			"estimate_url" => array(),
			"voucher_url" => array()
		);
	}

	function getFullPrice($orderID) {
		$ob = new OrdersBillsModel();
		$qBills = $ob->get("all", NULL, array("order_id" => $orderID, "type" => array(1, 2))); // 1, 2 => reservation, bill?

		$price = 0.0;
		while ($rBill = Core::$db->f($qBills))
		{
			$price += $rBill['price'];
		}

		return $price;
	}

	function isPayed($orderID) {
		$ob = new OrdersBillsModel();

		$qBills = $ob->get("all", NULL, array("order_id" => $orderID, "type" => array(1, 2))); // 1, 2 => reservation, bill?
	
		while ($rBill = $this->db->f($qBills))
			if ($rBill['payed'] != $rBill['price'])
				return FALSE;

		return TRUE;
	}

	function isPayedReservation($orderID) {
		$ob = new OrdersBillsModel();

		$rBill = $ob->get("first", NULL, array("order_id" => $orderID, "type" => 1));

		return $rBill && $rBill['payed'] == $rBill['price'];
	}

	function warnLateReservations(&$data) {
		$sql = "SELECT o.id, o.user_id, o.dt_added, o.offer_id " .
			"FROM orders o " .
			"WHERE o.dt_canceled <= '" . DT_NULL . "' " .
			"AND o.dt_rejected <= '" . DT_NULL . "' " .
			"AND o.dt_confirmed <= '" . DT_NULL . "' " .
			"AND o.dt_payed <= '" . DT_NULL . "' " .
			"AND o.dt_finished <= '" . DT_NULL . "' " .
			"AND o.dt_locked <= '" . DT_NULL . "' " .
			"AND o.dt_notify_reservation <= '" . DT_NULL . "' " .
			"AND o.dt_added <= '" . date("Y-m-d H:i:s", strtotime("-2 days")) . "' " .
			"AND o.dt_added >= '" . date("Y-m-d H:i:s", strtotime("-5 days")) . "'";

		$q = $this->q($sql);
		$ok = true;
		$data = array();
		while ($r = $this->f($q)) {
			$mail = Core::loadView("automaticOverload", "mails", array(0 => "waiting-reservation", 1 => array("order" => $r['id'], "offer" => $r['offer_id'], "user" => $r['user_id'])));
			//$mail = $this->Mails->automatic("waiting-reservation", array("order" => $r['id'], "user" => $r['user_id']));

			if (!$mail) {
				$ok = false;
			} else {
				$sql = "UPDATE orders SET dt_notify_reservation = '".DT_NOW."' WHERE id = " . $r['id'];
				$this->q($sql);
				//$this->Orders->update(array("dt_notify_reservation" => DT_NOW), $r['id']);
			}

			$data = $mail;
		}

		return $ok;
	}
}

?>