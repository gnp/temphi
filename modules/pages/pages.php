<?php

class PagesController extends UFWController {
	function __construct() {
		parent::__construct();
	}

	function one() {
		$tpl = new Template("one", "pages");
        
        $r = $this->Pages->get("first", NULL, array("id" => Router::get("id")));
        
        if (!$r)
            Status::code("404", "Page doesn't exists.");

        SEO::title($r['title'] . " - " . SITE);

		return $tpl->parse($r);
	}
	
    function maestro() {
        $q = $this->Pages->get("all");
        
        $return["title"] = "Pages";
        $return["th"] = array("ID", "Title", "URL");
        $return["btn header"][] = "add";
        $prevID = NULL;
        while ($r = $this->db->f($q)) {
            $rowBtn = array("update");
            
            if ($r['system'] != 1)
                $rowBtn[] = "delete";
            
            $return["row"][] = array(
                "td" => array(
                    $r['id'],
                    $r['title'],
                    $r['url'],
                ),
                "btn" => $rowBtn
            );
        }

        return $this->Maestro->makeListing($return);
    }

    function add() {
        $tpl = new Template("add", "pages");

        return $this->Maestro->makeAdd(array(
            "content" => $tpl->display(),
            "title" => "page",
        ));
    }

    function insert() {
        //if (!$this->Pages->isValidInsert($_POST['page']))
         //   Status::code(404, "Page doesn't exist.");
        
        $id = $this->Pages->insert($_POST['page']);

        return $this->Maestro->makeInsert(array("id" => $id));
    }

    function edit() {
        $tpl = new Template("edit", "pages");

        $r = $this->Pages->get("first", NULL, array("id" => Router::get("id")));
        
        //if (!$r || empty($r))
         //   Status::code(404, "Page doesn't exist.");
        
        return $this->Maestro->makeEdit(array(
            "content" => $tpl->parse($r),
            "title" => "page",
        ));
    }

    function update() {
        if (!$this->Pages->isValidUpdate($_POST['page']))
           Status::code(400);
           
        $this->Pages->update($_POST['page'], $_POST['page']['id']);
        
        return $this->Maestro->makeUpdate(array(
            "id" => $_POST['page']['id']
        ));
    }

    function delete() {
        $this->Pages->delete(Core::getGlobal("id"));
    }
}

class PagesModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "username";

		$this->fields = array(
            "title" => array(
                "not null" => TRUE
            ),
            "content" => array(
                "not null" => TRUE
            ),
			"url" => array(
                "not null" => TRUE,
                "unique" => TRUE,
			),
			"system" => array(
				"default" => -1
			),
		);
	}
}

?>