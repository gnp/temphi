<input type="hidden" id="id" name="page[id]" value="##ID##" />

<div class="control-group">
	<label class="control-label" for="title">Title</label>
	<div class="controls">
    	<input type="text" id="title" name="page[title]" value="##TITLE##" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="url">URL</label>
	<div class="controls">
    	<input type="text" id="url" name="page[url]" value="##URL##" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="content">Content</label>
	<div class="controls">
    	<textarea name="page[content]" id="content" class="mceEditor span12" style="height: 800px;">##CONTENT##</textarea>
	</div>
</div>