<?php

class OffersAdditionsController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function edit($data = array()) {
		if (!isset($data['offer_id']) || !Validate::isInt($data['offer_id']))
			return;
		
		$tpl = new Template("edit", "offers_additions");
		
		$sql = "SELECT a.id, a.title, a.value AS defvalue, oa.value AS setvalue, oa.id AS oaid " .
			"FROM additions a " .
			"LEFT OUTER JOIN offers_additions oa ON (oa.addition_id = a.id AND oa.offer_id = " . $data['offer_id'] .  ") " .
			"LEFT OUTER JOIN offers o ON (o.id = oa.offer_id AND o.id = " . $data['offer_id'] .  ") " .
			"ORDER BY a.position ASC";
		$this->db->q($sql);
		
		while ($r = $this->db->f()) {
			$r['cb addition'] = is_null($r['oaid'])
				? NULL
				: ' checked="checked"';
			
			$r['value'] = !is_null($r['oaid'])
				? $r['setvalue']
				: $r['defvalue'];
			
			$r['disabled'] = is_null($r['oaid'])
				? ' disabled="disabled"'
				: NULL;
			
			$tpl->parse($r, "addition");
		}
		
		return $tpl->parse(array(
			"addition" => $tpl->display("addition")
		));
	}

	function update() {
		if (!Validate::isInt($_POST['offer']['id']))
			return;
		
		$sql = "DELETE FROM offers_additions WHERE offer_id = " . $_POST['offer']['id'] . (isset($_POST['offers_addition']) && !empty($_POST['offers_addition']) ? " AND addition_id NOT IN(" . implode(",", array_keys($_POST['offers_addition'])) . ")" : NULL);
		$this->db->q($sql);
		
		if (isset($_POST['offers_addition']))
		foreach ($_POST['offers_addition'] AS $addition_id => $setval) {
			if (!Validate::isInt($addition_id))
				continue;
			
			$sqlCheck = "SELECT id FROM offers_additions WHERE addition_id = " . $addition_id . " AND offer_id = " . $_POST['offer']['id'];
			$qCheck = $this->db->q($sqlCheck);
			$rCheck = $this->db->f($qCheck);
			
			if (empty($rCheck))
				$this->OffersAdditions->insert(array(
					"offer_id" => $_POST['offer']['id'],
					"addition_id" => $addition_id,
					"value" => $setval["value"],
				));
			else
				$this->OffersAdditions->update(array(
					"value" => $setval["value"],
				), $rCheck['id']);
		}
	}
}

class OffersAdditionsModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "";

		$this->fields = array(
			"addition_id" => array(
				"not null" => true,
			),
			"offer_id" => array(
				"not null" => true,
			),
			"value" => array(
			),

		);
	}
}

?>