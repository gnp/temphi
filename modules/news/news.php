<?php
class NewsController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	//Print of all the news
	function all() {
		//if year is not set in URL the current year is set as default
		if(Router::get("year") == NULL)
			Status::redirect(Router::make("news", array("year" => date("Y"))));
		
		$selectedYear = Router::get("year");
		
		$tpl = new Template("all","news");
		
		//last 6 news are exposed on top
		$this->db->q("SELECT id, title, content_short, picture, dt_published
										FROM news
										WHERE dt_published <= NOW()
										ORDER BY dt_published DESC
										LIMIT 6");
		
		$cn = 0;
		while($rNews = $this->db->f()){
			$cn++;
			
			$rNews['parity'] = ($cn%2==0 ? 'even' : 'odd');
			
			$rNews['picture'] = imageNews.$rNews['picture'];
			$rNews['url'] = Router::make("novica", array(
				"url" => SEO::niceUrl($rNews['title']),
				"id" => $rNews['id'],
			));
			
			$tpl->parse($rNews, 'news');
		}
		
		$tpl->commit("news");
		Core::setParsevar("css page", "news");
		
		//prints all the years as links to filter news
		$qYears = $this->db->q("SELECT DISTINCT YEAR(dt_published) AS year
										FROM news
										WHERE dt_published > '1970-00-00'
										ORDER BY YEAR(dt_published) DESC");
		$cn=0;
		$numOfYears = $this->db->c($qYears);
		
		while($rYears = $this->db->f($qYears)) {
			$cn++;
			$rYears['span'] = "<span></span>";
			if($cn >= $numOfYears)
				$rYears['span'] = "";
			
			if($rYears['year'] == $selectedYear)
				$rYears['active'] = "active";
			else
				$rYears['active'] = "";
			
			$rYears['url'] = Router::make("news", array("year" => $rYears['year']));
			
			$tpl->parse($rYears, "years");
		}
	
		
		//prints news for selected year, grouped by months
		//first maximum month is needed. If current year is selected, this month is max, otherwise 12 (december)
		if($selectedYear == date('Y'))
			$maxMonth = date('n');
		else
			$maxMonth = 12;
		
		//for each month in selected year
		for($month=$maxMonth; $month>=1; $month--){
			$qNews = $tpl->db->q("SELECT id, title, dt_published
														FROM news
														WHERE YEAR(dt_published) = $selectedYear
															AND MONTH(dt_published) = $month");
			if($this->db->c()){
				while ($rNews = $this->db->f($qNews)) {
					$rNews['date'] = date("j.n.Y", strtotime($rNews['dt_published']));
					$rNews['url'] = SEO::niceUrl($rNews['title'])."/novica/".$rNews['id'];
					$tpl->parse($rNews, "list");
				}
				$tpl->parse(array(
					"list" => $tpl->display("list"),
					"month name" => SloDate::getNameOfMonth($month),
				), "archive", "archive" . (($month+1)%2));
			}
			else {
				$tpl->parse(array(
					"list" => $tpl->parse(array(),"nolist"),
					"month name" => SloDate::getNameOfMonth($month),
				), "archive", "archive" . (($month+1)%2));
			}
		}
		
		$tpl->commit("years");
		
		SEO::title("Read latest news about best festivals in the world at GoNParty.eu" . SITE);
		SEO::description("Latest news about festivals festivals featuring Techno, House, EDM, Trance and Harder styles music");
		return $tpl->parse(array(
			"archive left" => $tpl->display("archive1"),
			"archive right" => $tpl->display("archive0"),
			"archive" => NULL,
			"nolist" => NULL,
			"news archive" => $this->archive(),
		));
	}

	private function archive() {
		$selectedYear = date("Y");
		
		$tpl = new Template("archive","news");
		
		//prints all the years as links to filter news
		$qYears = $this->db->q("SELECT DISTINCT YEAR(dt_published) AS year
										FROM news
										WHERE dt_published > '1970-00-00'
										ORDER BY YEAR(dt_published) DESC");
		$cn=0;
		$numOfYears = $this->db->c($qYears);
		
		while($rYears = $this->db->f($qYears)) {
			$cn++;
			$rYears['span'] = "<span></span>";
			if($cn >= $numOfYears)
				$rYears['span'] = "";
			
			if($rYears['year'] == $selectedYear)
				$rYears['active'] = "active";
			else
				$rYears['active'] = "";
			
			$rYears['url'] = Router::make("news", array("year" => $rYears['year']));
			
			$tpl->parse($rYears, "years");
		}
	
		
		//prints news for selected year, grouped by months
		//first maximum month is needed. If current year is selected, this month is max, otherwise 12 (december)
		if($selectedYear == date('Y'))
			$maxMonth = date('n');
		else
			$maxMonth = 12;
		
		//for each month in selected year
		for($month=$maxMonth; $month>=1; $month--){
			$qNews = $tpl->db->q("SELECT id, title, dt_published
														FROM news
														WHERE YEAR(dt_published) = $selectedYear
															AND MONTH(dt_published) = $month");
			if($this->db->c()){
				while ($rNews = $this->db->f($qNews)) {
					$rNews['date'] = date("j.n.Y", strtotime($rNews['dt_published']));
					$rNews['url'] = SEO::niceUrl($rNews['title'])."/novica/".$rNews['id'];
					$tpl->parse($rNews, "list");
				}
				$tpl->parse(array(
					"list" => $tpl->display("list"),
					"month name" => SloDate::getNameOfMonth($month),
				), "archive", "archive" . (($month+1)%2));
			}
			else {
				//$tpl->parse(array(
				//	"list" => $tpl->parse(array(),"nolist"),
				//	"month name" => SloDate::getNameOfMonth($month),
				//), "archive", "archive" . (($month+1)%2));
			}
		}
		
		$tpl->commit("years");
		
		return $tpl->parse(array(
			"archive left" => $tpl->display("archive1"),
			"archive right" => $tpl->display("archive0"),
			"archive" => NULL,
			"nolist" => NULL,
		));
	}
	
	//Print of single news
	function one() {
		$q = $this->db->q("SELECT id, title, content, picture, dt_published, content_short
										FROM news
										WHERE id = ".Router::getUrlData("id")."
										AND dt_published <= NOW()
										LIMIT 1");
		
		//If news with ID does not exist, 404
		if(!$this->db->c($q))
			Status::code(404, "Sorry, cant find this article.");
		
		$tpl = new Template("one", "news");
		
		$rNews = $this->db->f();
		
		$rNews['day numeric'] = date("j",strtotime($rNews['dt_published']));
		$rNews['year'] = date("Y",strtotime($rNews['dt_published']));
		
		//Translates day of week and month name from english to slovenian
		$rNews['day'] = date("m",strtotime($rNews['dt_published']));
		$rNews['month'] = date("F",strtotime($rNews['dt_published']));
		
		$rNews['picture'] = imageNews.$rNews['picture'];
		
		$rNews["facebook url"] = "https://gonparty.eu" . Router::make("novica",array(
			"url" => SEO::niceUrl($rNews['title']),
			"id" => $rNews['id'],
		));
		
		$tpl->parse($rNews);
		$tpl->parse(array(
			"news archive" => $this->archive(),
		));
		
		Core::setParseVar("css page", "single-article");
		Core::setParseVar("content right", $this->Offers->sidebar(), TRUE);
		Core::setParseVar("content right", $this->News->index(array("title" => "RELATED NEWS", "currentId" => Router::getUrlData("id"))), TRUE);
		Core::setParseVar("content right", $this->Galleries->index(), TRUE);

		SEO::title($rNews['title'] . " - " . SITE);
		
		return $tpl->display();
	}
	
	function index($var) {
		//If module is shown on single article, the same article shouldn't be on the list of related
		if(isset($var['currentId']))
			$without = " AND id != ".$var['currentId'];
		else
			$without = NULL;
		
		$this->db->q("SELECT id, title, content_short, picture, dt_published
				FROM news
				WHERE dt_published <= NOW()" .
				$without .
				"	ORDER BY dt_published DESC
				LIMIT 4");
		
		$tpl = new Template("index", "news");
		
		$cn = 0;
		while($rNews = $this->db->f()){
			$cn++;
			
			$rNews['picture'] = imageNews.$rNews['picture'];
			$rNews['url'] = Router::make("novica", array(
				"url" => SEO::niceUrl($rNews['title']),
				"id" => $rNews['id'],
			));
			
			if(isset($var['length']))
				$rNews['length'] = $var['length'];
			else
				$rNews['length'] = "span12";
			
			if($cn == 3)
				$rNews['break'] = '</div><div class="row-fluid">';
			else $rNews['break'] = NULL;
			
			$tpl->parse($rNews, 'news');
		}
		
		SEO::title("Top party news on " . SITE);
		
		/*return $tpl->parse(array(
			"news" => $tpl->display("news"),
			"module title" => $var["title"],
		));*/
	}
	
	function sidebar($var) {
		$this->db->q("SELECT id, title, content_short, picture, dt_published
				FROM news
				WHERE dt_published <= NOW()
				ORDER BY dt_published DESC
				LIMIT 3");
		
		$tpl = new Template("sidebar", "news");
		
		while($rNews = $this->db->f()){
			$rNews['picture'] = imageNews.$rNews['picture'];
			$rNews['url'] = Router::make("novica", array(
				"url" => SEO::niceUrl($rNews['title']),
				"id" => $rNews['id'],
			));
			
			$tpl->parse($rNews, 'news');
		}
		
		return $tpl->parse(array(
			"news" => $tpl->display("news"),
			"module title" => $var["title"],
		));
	}

	
	function maestro() {
		$q = $this->News->get("all");
		
		$return["title"] = "News";
		$return["th"] = array("ID", "Title", "Status");
		$return["btn header"][] = "add";
		while ($r = $this->db->f($q)) {
			$return["row"][] = array(
				"td" => array(
					$r['id'],
					$r['title'],
					'<span class="btnToggle btn btn-small" data-url="/news/togglepublished/' . $r['id'] . '/" data-value="' . $r['dt_published'] . '"><i class="icon-check"></i></span>',
				),
				"btn" => array("update", "delete")
			);
		}
		
		return $this->Maestro->makeListing($return);
	}

	function togglepublished() {
		return $this->News->update(array(
			"dt_published" => Router::get("active") ? DT_NOW : DT_NULL,
		), Router::get("id"));
	}
	
	function add() {
		$tpl = new Template("add", "News");

		return $this->Maestro->makeAdd(array(
			"content" => $tpl->parse(array(
				"dt_published" => DT_NOW
			)),
			"title" => "news",
		));
	}
	
	function insert() {
		$id = $this->News->insert($_POST['news']);
		
		$this->Maestro->makeInsert(array("id" => $id));
	}

	function edit() {
		$tpl = new Template("edit", "News");

		$r = $this->News->get("first", NULL, array("id" => Router::get("id")));
		
		$r['picture2'] = "/media/" . $r['picture'];
		$r['pic display'] = !empty($r['picture']) ? "block" : "none";
			
		return $this->Maestro->makeEdit(array(
			"content" => $tpl->parse($r) . Core::loadView("edit", "tags", array("multiple" => "news", "single" => "news", "id" => $r['id'])),
			"title" => "news",
		));
	}

	function update() {
		//$_POST['news']['content'] = stripslashes($_POST['news']['content']);
		
		$this->News->update($_POST['news'], $_POST['news']['id']);

		Core::loadView("update", "tags", array("multiple" => "news", "single" => "news", "id" => $_POST['news']['id']));

		return $this->Maestro->makeUpdate(array(
			"id" => $_POST['news']['id']
		));
	}

	function delete() {
		$this->News->delete(Router::get("id"));
	}
}

class NewsModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->countForms['min'] = "news";
		
		$this->mk = "title";

		$this->fields = array(
			"title" => array(
				"not null" => true,
			),
			"content" => array(
				"not null" => true,
			),
			"content_short" => array(
				"not null" => true,
			),
			"picture" => array(
			),
			"dt_published" => array(
			),
		);
	}
}

?>