<script>
  $(window).load(function() {
    resize(".article .top img", 0.37);
  });
  $(document).ready(function(){
    resize(".article .top img", 0.37);
  });
  $(window).resize(function(){
    resize(".article .top img", 0.37);
  });
</script>
<div class="news row-fluid" id="news-module">
  <div class="module-head span12">
    <div class="left">
      <h2>All news</h2>
      <img src="/img/icons/news.png">
    </div>
  </div>
  ##NEWS START##
  <div class="span6 ##PARITY##">
    <div class="article">
      <div class="top">
        <a href="##URL##"><img src="/cache/img/w/570##PICTURE##" title="##TITLE##" /></a>
        <div class="heading">
          <div class="background"></div>
          <a href="##URL##"><h3>##TITLE##</h3></a>
      	<a class="more" href="##URL##" title="preberi novico">READ MORE <span>></span></a>
        </div>
      </div>
      ##CONTENT_SHORT##
    </div><!-- /article -->
  </div>
  ##NEWS END##
</div><!-- /#news-module -->

##NEWS ARCHIVE##