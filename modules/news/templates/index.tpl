<script>
  $(window).load(function() {
    resize(".news .article img", 0.37);
  });
  $(document).ready(function(){
    resize(".news .article img", 0.37);
  });
  $(window).resize(function(){
    resize(".news .article img", 0.37);
  });
</script>
<div class="news" id="news-module">
  <div class="module-head">
      <div class="left">
      <a class="title" href="/novice" title="poglej vse novice">
        <h2>##MODULE TITLE##</h2>
      </a>
      <a href="/" title="poglej vse novice">
        <img src="/img/icons/news.png">
      </a>
    </div><div class="right">
      <a href="/novice" title="poglej vse novice">All news</a>
    </div>
  </div>
  <div class="row-fluid">
  ##NEWS START##
  ##BREAK##
  <div class="article ##LENGTH##">
    <div class="top">
      <a href="##URL##"><img src="/cache/img/w/570##PICTURE##" title="##TITLE##" /></a>
      <div class="heading">
        <div class="background"></div>
        <a href="##URL##"><h3>##TITLE##</h3></a>
      	<a class="more" href="##URL##" title="preberi novico">PREBERI VEČ <span>></span></a>
      </div>
    </div>
    ##CONTENT_SHORT##
  </div><!-- /article -->
  ##NEWS END##
  </div>
</div><!-- /#news-module -->