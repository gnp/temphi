<div class="control-group">
	<label class="control-label" for="title">Title</label>
	<div class="controls">
    	<input type="text" id="title" name="news[title]" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="content_short">Short content</label>
	<div class="controls">
    	<textarea id="content_short" name="news[content_short]" class="mceEditor"></textarea>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="content">Content</label>
	<div class="controls">
    	<textarea id="content" name="news[content]" class="mceEditor"></textarea>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="picture">Picture</label>
	<div class="controls">
    	<input type="hidden" id="picture" name="news[picture]" />
		<input type="button" class="btn btn-primary elFinderData" value="Open file browser" data-id="" data-files="" data-multi="-1" data-replace="newImageField" data-field="news[picture]" data-url="" />
		<img src="" style="display: none; margin-top: 10px;" id="newImageField" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="dt_published">Published</label>
	<div class="controls">
    	<input type="text" id="dt_published" name="news[dt_published]" class="jdt span6" value="##DT_PUBLISHED##" />
	</div>
</div>