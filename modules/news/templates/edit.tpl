<input type="hidden" id="id" name="news[id]" value="##ID##" />

<div class="control-group">
	<label class="control-label" for="title">Title</label>
	<div class="controls">
    	<input type="text" id="title" name="news[title]" value="##TITLE##" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="content_short">Short content</label>
	<div class="controls">
    	<textarea id="content_short" name="news[content_short]" class="mceEditor">##CONTENT_SHORT##</textarea>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="content">Content</label>
	<div class="controls">
    	<textarea id="content" name="news[content]" class="mceEditor">##CONTENT##</textarea>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="picture">Picture</label>
	<div class="controls">
    	<input type="hidden" id="picture" name="news[picture]" value="##PICTURE##" />
		<input type="button" class="btn btn-primary elFinderData" value="Open file browser" data-id="" data-files="" data-multi="-1" data-replace="newImageField" data-field="news[picture]" data-url="" />
		<img src="##PICTURE2##" id="newImageField" style="display: ##PIC DISPLAY##; margin-top: 10px;" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="dt_published">Published</label>
	<div class="controls">
    	<input type="text" id="dt_published" name="news[dt_published]" value="##DT_PUBLISHED##" class="jdt span6" />
	</div>
</div>