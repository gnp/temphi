<div class="news archive" id="news-module">
  <div class="years">
  	##YEARS START##
  	<a href="##URL##" class="##ACTIVE##">##YEAR##</a>
    ##SPAN##
    ##YEARS END##
  </div>
  
  <h2>NEWS ARCHIVE</h2> <img src="/img/icons/news-archive.png" />
  
  <div class="list row-fluid">
  	<div class="left span6">
      ##ARCHIVE LEFT##
    </div>
    
    <div class="right span6">
      ##ARCHIVE RIGHT##
    </div>
  </div>
</div>
##ARCHIVE START##
<h3>##MONTH NAME##</h3>
  <ul>
    ##LIST START##
    	<li><a href="##URL##"><span class="bullet">►</span>##TITLE## <span class="">##DATE##</span></a></li>
    ##LIST END##
  </ul>
##ARCHIVE END##


##NOLIST START##
  <p class="nolist">UPS. ##MONTH NAME## was very exhausting for us so there is no new articles.</p>
##NOLIST END##