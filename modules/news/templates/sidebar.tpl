<script>
  $(window).load(function() {
    resize(".article .imageBox img", 0.37);
  });
  $(document).ready(function(){
    resize(".article .imageBox img", 0.37);
  });
  $(window).resize(function(){
    resize(".article .imageBox img", 0.37);
  });
</script>
<div class="news row-fluid" id="news-module">
  <div class="module-head span12">
    <div class="left">
      <h2>Party novičke</h2>
      <img src="/img/icons/news.png">
    </div>
  </div>
  ##NEWS START##
  <div class="span12 odd nml">
    <div class="article">
      <div class="top">
      	<div class="imageBox">
        	<div>
        		<a href="##URL##"><img src="/cache/img/w/370##PICTURE##" title="##TITLE##" /></a>
        	</div>
        </div>
        <div class="heading">
          <div class="background"></div>
          <a href="##URL##"><h3>##TITLE##</h3></a>
        </div>
      </div>
      <div class="short-text">##CONTENT_SHORT##</div>
      <div class="more">
        <a href="##URL##" title="preberi novico">PREBERI VEČ</a>
      </div>
    </div><!-- /article -->
  </div>
  ##NEWS END##
</div><!-- /#news-module -->