<script>
  $(window).load(function() {
    resize(".article .top img", 0.37);
  });
  $(document).ready(function(){
    resize(".article .top img", 0.37);
  });
  $(window).resize(function(){
    resize(".article .top img", 0.37);
  });
</script>
<div style="float: left; width: 94%;">
  <div class="article">
    <h1>##TITLE##</h1>
    <div class="top">
    	<img class="main-pic" src="/cache/img/w/724##PICTURE##" title="" />
    </div>
    <div class="date">
      <p>##DAY NUMERIC## ##MONTH## ##YEAR##</p>
    </div>
    <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false" data-width="200px"></div>
    <div class="content">
      ##CONTENT_SHORT##
    </div>
    <div class="content">
      ##CONTENT##
    </div>
  </div><!-- /article -->
	<div class="fb-comments" data-href="##FACEBOOK URL##" data-width="420" data-num-posts="10" data-colorscheme="light" ></div>
  ##NEWS ARCHIVE##
</div>