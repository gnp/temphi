<?php

class OffersCitiesController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function index() {
	}
	
	function one() {
	}
	
	function maestro() {
		$q = $this->OffersCities->get("all");
		
		$return["title"] = "OffersCities";
		$return["th"] = array("ID", "");
		$return["btn header"][] = "add";
		while ($r = $this->db->f($q)) {
			$return["row"][] = array(
				"td" => array(
					$r['id'],
					$r[''],
				),
				"btn" => array("update", "delete")
			);
		}
		return $return;
	}
	
	function add() {
		$tpl = new Template("add", "OffersCities");

		return array(
			"content" => $tpl->display(),
			"title" => "offers_citie",
		);
	}
	
	function insert() {
		$id = $this->OffersCities->insert($_POST['offers_citie']);

		Core::setGlobal(array("id" => $id));
	}

	function edit() {
		$tpl = new Template("edit", "OffersCities");

		$r = $this->OffersCities->get("first", NULL, array("id" => Core::getGlobal("id")));

		return array(
			"content" => $tpl->parse($r),
			"title" => "offers_citie",
		);
	}

	function update() {
		$this->OffersCities->update($_POST['offers_citie'], $_POST['offers_citie']['id']);

		return array(
			"id" => $_POST['offers_citie']['id']
		);
	}

	function delete() {
		$this->OffersCities->delete(Core::getGlobal("id"));
	}
}

class OffersCitiesModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "";

		$this->fields = array(
			"id" => array(
				"not null" => true,
			),
			"offer_id" => array(
				"not null" => true,
			),
			"city_id" => array(
				"not null" => true,
			),

		);
	}
}

?>