
<div class="control-group">
	<label class="control-label" for="id">Id</label>
	<div class="controls">
    	<input type="text" id="id" name="offers_cities[id]" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="offer_id">OfferId</label>
	<div class="controls">
    	<input type="text" id="offer_id" name="offers_cities[offer_id]" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="city_id">CityId</label>
	<div class="controls">
    	<input type="text" id="city_id" name="offers_cities[city_id]" />
	</div>
</div>
