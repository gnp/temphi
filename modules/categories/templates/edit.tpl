<input type="hidden" id="id" name="category[id]" value="##ID##" />

<div class="control-group">
	<label class="control-label" for="title">Category</label>
	<div class="controls">
    	<input type="text" id="title" name="category[title]" value="##TITLE##" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="published">Published</label>
	<div class="controls">
		<input type="hidden" value="-1" name="category[published]" />
    	<input type="checkbox" id="published" name="category[published]" value="1"##CB PUBLISHED## />
	</div>
</div>