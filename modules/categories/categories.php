<?php

class CategoriesController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function index() {
	}
	
	function one() {
	}
	
	function maestro() {
		$q = $this->Categories->get("all", NULL, NULL, array("order by" => array("position")));
		
		$return["title"] = "Categories";
		$return["th"] = array("ID", "Title", "Status");
		$return["btn header"][] = "add";
		$return["tbody class"] = "sortable";
		while ($r = $this->db->f($q)) {
			$return["row"][] = array(
				"td" => array(
					$r['id'],
					$r['title'],
					'<span class="btnToggle btn btn-small" data-url="/categories/togglepublished/' . $r['id'] . '/" data-value="' . $r['published'] . '"><i class="icon-check"></i></span>',
				),
				"btn" => array("update", "delete")
			);
		}
		
		$tplSort = new Template("onsortupdate", "categories");
		$return['append'] = $tplSort->display();
		
		return $this->Maestro->makeListing($return);
	}

	function togglepublished() {
		return $this->Categories->update(array(
			"published" => Router::get("active"),
		), Router::get("id"));
	}
	
	function add() {
		$tpl = new Template("add", "Categories");

		return $this->Maestro->makeAdd(array(
			"content" => $tpl->display(),
			"title" => "category",
		));
	}
	
	function insert() {
		$id = $this->Categories->insert($_POST['category']);
		
		$this->Maestro->makeInsert(array("id" => $id));
	}

	function edit() {
		$tpl = new Template("edit", "Categories");

		$r = $this->Categories->get("first", NULL, array("id" => Router::get("id")));
		
		$r["cb published"] = $r['published'] == 1
			? ' checked="checked"'
			: NULL;

		return $this->Maestro->makeEdit(array(
			"content" => $tpl->parse($r),
			"title" => "category",
		));
	}

	function update() {
		$this->Categories->update($_POST['category'], $_POST['category']['id']);

		return $this->Maestro->makeUpdate(array(
			"id" => $_POST['category']['id']
		));
	}

	function delete() {
		$this->Categories->delete(Router::get("id"));
	}
	
	function updatepositions() {
		foreach ($_POST['positions'] AS $position => $id)
			$this->Categories->update(array(
				"position" => $position
			), $id);
	}
}

class CategoriesModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->countForms['min'] = "category";
		
		$this->mk = "title";

		$this->fields = array(
			"title" => array(
				"not null" => true,
			),
			"position" => array(
			),
			"published" => array(
				"default" => -1,
			),
		);
	}
}

?>