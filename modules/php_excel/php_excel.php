<?php

class PhpExcelController extends UFWController {
	function __construct() {
		parent::__construct();
		
		/** Include path **/
		ini_set('include_path', ini_get('include_path').';'.APP_PATH . "other" . DS . "phpexcel" . DS);
		
		/** PHPExcel */
		include_once APP_PATH . "other" . DS . "phpexcel" . DS . 'PHPExcel.php';
		
		/** PHPExcel_Writer_Excel2007 */
		include_once APP_PATH . "other" . DS . "phpexcel" . DS . 'PHPExcel/Writer/Excel2007.php';
		
		PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);


		//include_once APP_PATH . "other" . DS . "phpexcel" . DS . 'PHPExcel/Writer/PDF.php';
		//include_once APP_PATH . "other" . DS . "phpexcel" . DS . 'PHPExcel/Writer/PDF/dompdf/dompdf_config.inc.php';
		//include_once APP_PATH . "other" . DS . "phpexcel" . DS . 'PHPExcel/Writer/PDF/dompdf/dompdf.php';

		//$rendererName = PHPExcel_Settings::PDF_RENDERER_TCPDF;
		//$rendererLibraryPath = APP_PATH . "other" . DS . "tcpdf" . DS;
		//PHPExcel_Settings::setPdfRenderer($rendererName, $rendererLibraryPath);
	}
	
	/* returns estimate path, gets order id */
	function generateEstimate($orderID) {
		$inputFileName = APP_PATH . 'private' . DS . 'estimates' . DS . 'MASTER_PREDRACUN.xlsx';  
		$objReader = new PHPExcel_Reader_Excel2007();
		//$objReader = new PHPExcel_Reader_Excel2003XML();
		$objPHPExcel = $objReader->load($inputFileName);
		
		$estimateData = $this->Orders->getEstimateData($orderID);
		
		if (!$estimateData)
			return "ni podatkov";
		
		$arr = array();
		foreach ($estimateData['orders'] AS $ord) {
			$arr[] = array(
				1 => count($arr)+1,
				2 => $ord['title'],
				5 => makePrice($ord['quantity'], 1, null),
				6 => makePrice($ord['price'], 2, null),
			);
		}

		//print_r($estimateData);
		//die();
		
		//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 12, $estimateData['offer']['title']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 13, $estimateData['offer']['post']);
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 18, $estimateData['payee']['name'] . " " . $estimateData['payee']['surname']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 19, $estimateData['payee']['address']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 20, $estimateData['payee']['postal'] . " " . $estimateData['payee']['post']);
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 17, $estimateData['order']['num']);
		//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 18, $estimateData['order']['id']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 20, date("d.m.Y", strtotime($estimateData['offer']['dt_end'])));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 36, date("d.m.Y"));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 44, date("d.m.Y"));
		//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 24 + count($arr) + count($estimateData['portions']), $estimateData['order']['num']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 32, "Plačilo aranžmaja " . $estimateData['order']['num']);
		
		//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, (35 + count($arr) - (count($arr)-1)), date("d.m.Y"));
		
		if (count($arr) > 1) {
			$objPHPExcel->getActiveSheet()->insertNewRowBefore(24, count($arr)-1); // add rows
			
			for ($i = 0; $i<count($arr) - 1; $i++) // merge cells
				$objPHPExcel->getActiveSheet()->mergeCells('C' . ($i+24) . ':E' . ($i+24));
		}

		if (count($estimateData['portions'] > 0)) {
			$objPHPExcel->getActiveSheet()->insertNewRowBefore(26 + count($arr) - 1, count($estimateData['portions'])-1); // add rows
		}
			
		foreach ($arr AS $i => $values) {
			foreach ($values AS $j => $value) {
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j, 23+$i, $value);
			}
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 23+$i, "=F" . (23+$i) . "*G" . (23+$i) . "");
		}

		foreach ($estimateData['portions'] AS $i => $portion) { // num, due date, price, reservation
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 23+count($arr)+$i+1, $portion['reservation'] == 1 ? "Rezervacija" : ($portion['num']) . ". obrok");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 23+count($arr)+$i+1, "Rok plačila:");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 23+count($arr)+$i+1, date("d.m.Y", strtotime($portion['due date'])));
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 23+count($arr)+$i+1, $portion['price']);
		}
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 23+count($arr), "=SUM(H23:H" . (23+count($arr)-1) . ")");
		
		return $this->save($objPHPExcel, $orderID . "-estimate-" . time() . ".xlsx", APP_PATH . "private" . DS . "estimates" . DS);
	}
	
	/* returns estimate path, gets order id */
	function generateBill($orderID) {
		$inputFileName = APP_PATH . 'private' . DS . 'bills' . DS . 'MASTER_RACUN.xlsx';  
		$objReader = new PHPExcel_Reader_Excel2007();
		//$objReader = new PHPExcel_Reader_Excel2003XML();
		$objPHPExcel = $objReader->load($inputFileName);
		
		$estimateData = $this->Orders->getEstimateData($orderID);
		
		if (!$estimateData)
			return "ni podatkov";
		
		$arr = array();
		foreach ($estimateData['orders'] AS $ord) {
			$arr[] = array(
				1 => count($arr)+1,
				2 => $ord['title'],
				5 => makePrice($ord['quantity'], 1, null),
				6 => makePrice($ord['price'], 2, null),
			);
		}
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 12, $estimateData['offer']['title']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 13, $estimateData['offer']['post']);
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 17, $estimateData['payee']['name'] . " " . $estimateData['payee']['surname']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 18, $estimateData['payee']['address']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 19, $estimateData['payee']['postal'] . " " . $estimateData['payee']['post']);
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 16, $estimateData['order']['id']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 18, $estimateData['order']['id']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 19, $estimateData['payment date']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 21, date("d.m.Y", strtotime($estimateData['offer']['dt_end'])));
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, (35 + count($arr) - (count($arr)-1)), date("d.m.Y"));
		
		if (count($arr) > 1) {
			$objPHPExcel->getActiveSheet()->insertNewRowBefore(25, count($arr)-1); // add rows
			
			for ($i = 0; $i<count($arr) - 1; $i++) // merge cells
				$objPHPExcel->getActiveSheet()->mergeCells('C' . ($i+25) . ':E' . ($i+25));
		}
			
		foreach ($arr AS $i => $values) {
			//Insert 1 new rows between rows 1 and 2 
			
			foreach ($values AS $j => $value) {
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j, 24+$i, $value);
			}
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 24+$i, "=F" . (24+$i) . "*G" . (24+$i) . "");
		}
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 24+count($arr), "=SUM(H24:H" . (24+count($arr)-1) . ")");
		
		return $this->save($objPHPExcel, $orderID . "-bill-" . time() . ".xlsx", APP_PATH . "private" . DS . "bills" . DS);
	}
	
	private function prepare() {
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		
		PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
		
		// Set properties
		$objPHPExcel->getProperties()->setCreator("GremoNaParty WebApp");
		$objPHPExcel->getProperties()->setLastModifiedBy("GremoNaParty WebApp");
		
		$objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Test Document");
		$objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Test Document");
		$objPHPExcel->getProperties()->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.");
		
		$objPHPExcel->setActiveSheetIndex(0);
		
		return $objPHPExcel;
	}
	
	/* simple export is ready =) */
	function exportSimpleOrder($data = array()) {
		$xslx = $this->prepare();
		
		$sql = "SELECT o.num, u.name, u.surname, u.phone, u.email, p.title, of.title AS otitle, p.title AS ptitle, ou.id AS ouid, u.id AS uid, o.user_id AS ord_user_id, o.id AS order_id " .
			"FROM users u " .
			"INNER JOIN orders_users ou ON ou.user_id = u.id 
INNER JOIN orders o ON o.id = ou.order_id 
INNER JOIN packets p ON p.id = ou.packet_id 
INNER JOIN offers of ON of.id = o.offer_id " .
			"WHERE ou.id IN (" . implode(",", $data['orders_users']) . ") " .
			(isset($data['orders']) && !empty($data['orders']) ? "AND o.id IN (" . implode(",", $data['orders']) . ") " : NULL) .
			(isset($data['offers']) && !empty($data['offers']) ? "AND of.id IN (" . implode(",", $data['offers']) . ") " : NULL) .
			"GROUP BY ou.id " .
			"ORDER BY of.id, o.id, p.id";
		$q = $this->db->q($sql);
		
		$i = 0;
		
		$lastOffer = NULL;
		
		while ($r = $this->db->f($q)) {
			// offer heading
			if ($lastOffer != $r['otitle']) {
				$i++;
				$lastOffer = $r['otitle'];
				$xslx->getActiveSheet()->setCellValueByColumnAndRow(0, $i, $r['otitle']);
				$i++;
				
				$xslx->getActiveSheet()->setCellValueByColumnAndRow(0, $i, "Naročilo");
				$xslx->getActiveSheet()->setCellValueByColumnAndRow(1, $i, "Ime");
				$xslx->getActiveSheet()->setCellValueByColumnAndRow(2, $i, "Priimek");
				$xslx->getActiveSheet()->setCellValueByColumnAndRow(3, $i, "Telefon");
				$xslx->getActiveSheet()->setCellValueByColumnAndRow(4, $i, "Email");
				$xslx->getActiveSheet()->setCellValueByColumnAndRow(5, $i, "Dodatki");
				$xslx->getActiveSheet()->setCellValueByColumnAndRow(6, $i, "Paket");
				$xslx->getActiveSheet()->setCellValueByColumnAndRow(7, $i, "Plačila");
				$i++;
			}

			$sqlAdditions = "SELECT a.title 
				FROM additions a 
				INNER JOIN packets_additions pa ON pa.addition_id = a.id 
				INNER JOIN orders_users_additions oua ON oua.addition_id = pa.id 
				INNER JOIN orders_users ou ON ou.id = oua.orders_user_id 
				WHERE ou.id = " . $r['ouid'];
			$qAdditions = $this->db->q($sqlAdditions);
			$r['additions'] = array();
			while ($rAddition = $this->db->f($qAdditions))
				$r['additions'][] = $rAddition['title'];
			$r['additions'] = implode(", ", $r['additions']);

			$r['payments'] = array();
			if ($r['ord_user_id'] && $r['uid']) {
				$sqlPayments = "SELECT ob.* 
					FROM orders_bills ob
					WHERE ob.order_id = " . $r['order_id'];
				$qPayments = $this->db->q($sqlPayments);
				while ($rPayment = $this->db->f($qPayments))
					$r['payments'][] = date("d.m.Y", strtotime($rPayment['dt_valid'])) . " " . number_format((float)$rPayment['payed'], 2) . "/" . number_format((float)$rPayment['price'], 2);
			}
			
			$xslx->getActiveSheet()->setCellValueByColumnAndRow(0, $i, $r['num']);
			$xslx->getActiveSheet()->setCellValueByColumnAndRow(1, $i, $r['name']);
			$xslx->getActiveSheet()->setCellValueByColumnAndRow(2, $i, $r['surname']);
			$xslx->getActiveSheet()->setCellValueByColumnAndRow(3, $i, $r['phone']);
			$xslx->getActiveSheet()->setCellValueByColumnAndRow(4, $i, $r['email']);
			$xslx->getActiveSheet()->setCellValueByColumnAndRow(5, $i, $r['additions']);
			$xslx->getActiveSheet()->setCellValueByColumnAndRow(6, $i, $r['ptitle']);
			for ($j = 0; $j<count($r['payments']); $j++) {
				$xslx->getActiveSheet()->setCellValueByColumnAndRow($j+7, $i, $r['payments'][$j]);
			}
			$i++;
		}

        $xslx->getActiveSheet()->getColumnDimension('A')->setWidth(10);  //0.33
		$xslx->getActiveSheet()->getColumnDimension('B')->setWidth(10); //12
		$xslx->getActiveSheet()->getColumnDimension('C')->setWidth(10);    //15.29
		$xslx->getActiveSheet()->getColumnDimension('D')->setWidth(25);  //11.71
		$xslx->getActiveSheet()->getColumnDimension('E')->setWidth(25);  //5.14
		$xslx->getActiveSheet()->getColumnDimension('F')->setWidth(20);  //0.5

		return $this->save($xslx);
	}

	private function save($objPHPExcel, $file = NULL, $path = NULL) {
		if (is_null($file))
			$file = microtime() . ".xlsx";
		
		if (is_null($path))
			$path = APP_PATH . "private" . DS;
		
		// Save Excel 2007 file
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save($path . $file);

		//$objWriterPdf = new PHPExcel_Writer_PDF($objPHPExcel);
		//$objWriterPdf->save($path . $file . ".pdf");
		
		return $path . $file;
	}
}

class PhpExcelModel extends UFWModel {
	function __construct() {
		parent::__construct();
	}
}

?>