<?php

class UsersController extends UFWController {
	function __construct() {
		parent::__construct();
	}

	function sendtomorrowlandmails() {
		die("-1");
		$mails = array('schtr4jh@schtr4jh.net','centrih.m@gmail.com','lebar.jasmina@gmail.com','lana.lipecky@yahoo.com','rok.opresnik21@gmail.com','zlhkica@yahoo.com','martina.vertovsek@gmail.com','kaja.perme6@gmail.com','primc.kristinaa@gmail.com','aj.jackd@gmail.com','laura.lukavecki@gmail.com','blazjakopic93@gmail.com','benkovictomaz1988@gmail.com','drtomazydj@gmail.com','mojcagergar@gmail.com','alen.koso@gmail.com','eva.filipic94@gmail.com','maja.vrbnjak@hotmail.com','tinekravanja@gmail.com','dalibor@krejic.net','janko.kucera@gmail.com','simon.vaupotic@hotmail.com','katiia.hartman@gmail.com','golob.gregor@gmail.com','jasna.rovcanin@gmail.com','katka.kavcic@gmail.com','nikolinavrzic@gmail.com','kozuhp@yahoo.com','jzamernik@gmail.com','roksteharnik1@gmail.com','nrgvux@gmail.com','anavalant@gmail.com','simon.skrbinek@gmail.com','taja.oremuz@yahoo.com','omgbunnyz@gmail.com','peter.milosic@gmail.com','vzver@hotmail.com','akul.medd@gmail.com','aleks.delbello1@gmail.com','vero_leo_@hotmail.com','marusa95@gmail.com','bkoprivnik100@gmail.com','natasha.raduha@gmail.com','kaja.ivancic@gmail.com','nika.k6@gmail.com','nejc.jesenicnik@gmail.com','milena.romaric@gmail.com','tomaz212@gmail.com','miha.franchise@gmail.com','misel.jez@student.um.si','miha.sreb@gmail.com','hondaaddict@gmail.com','jurevuc@gmail.com','gasperhozjan@gmail.com','sanjaromaric7@gmail.com','ursa91@gmail.com','matevz.ambrozic@siol.net','nush4aa@gmail.com','matija.knezz@gmail.com','kaja.kovich@gmail.com','Adin.Kurtic@gmail.com','janzekovicjanja78@gmail.com','kristian.eric.sipek@gmail.com','gorazd.jurgec@gmail.com','jure_lisec@yahoo.com','ana.vadnov@gmail.com','deniskouter@gmail.com','leo.novakovic@yahoo.com','zaversnik.tina@gmail.com','mia.vouk@gmail.com','tadeiia.t@gmail.com','marko.lorber11@gmail.com','miha.mlakar2@gmail.com','tamara.lorencic@gmail.com','david.cizmesija@gmail.com','andrej.vesel@hotmail.com','denis.vajngerl@gmail.com','cukzor@gmail.com','luka.kacil@gmail.com','tina.klancnik@hotmail.com','ivo.pistor@gmail.com','klemen.you@gmail.com','gojkovicbostjan@gmail.com','rudi.hirsch1989@gmail.com','icengic4@gmail.com','manjamf@gmail.com','sok.blaz@gmail.com','dejan.lol@hotmail.com','luka.prah@gmail.com','kim.pevec.korsic@gmail.com','alesjakomin@gmail.com','rexrot@hotmail.com');

		$mails = array('matija.vodisek@gmail.com', 'rok.klinar@gmail.com', 'tadej.kokol@gmail.com', '85dejan@gmail.com', 'zala.mueller@gmail.com', 'sisic.roksy@gmail.com', 'nebojsa.vajic@gmail.com');
		$sql = "SELECT u.name, u.surname, u.email FROM users u";
		foreach ($mails AS $mail)
				$this->Mails->automatic("tomorrowlandautologin", array("to" => $mail, "tomorrowlandautologin" => "https://gremonaparty.com/tomorrowland-2014/ponudba/21"));

		die("ok");
	}

	function loginModal() {
		$tpl = new TwigTpl("users/templates/loginmodal.twig");
		return $tpl->display();
	}

	function tomorrowlandautologin() {
		$hash = Router::get("hash");

		$sql = "SELECT u.id FROM users u WHERE SHA1(CONCAT('tojehash', u.email)) = '" . $hash . "'";
		$q = $this->db->q($sql);
		$r = $this->db->f($q);

		if (!empty($r)) {
			Auth::loginByUserID($r['id']);
		}

		Status::redirect("/tomorrowland-2014/ponudba/21");
	}
	
	function profile() {
		if (!Auth::isLoggedIn())
			Status::redirect("/");

		$tpl = new TwigTpl("users/templates/profile.twig");
		
		Core::setParseVar("css page", "profile");
		
		Optimize::addFile("js", array("js/profile.js"), "profile");
		
		return $tpl->parse(array(
			"user" => Auth::getUser(),
			"city" => $this->Cities->get("first", NULL, array("id" => Auth::getUser("city_id"))),
		));
	}
	
	function index() {
		$tpl = new Template("listing", "maestro");

		return $tpl->display();
	}
	
	function news() {
		$tpl = new Template("news","users");
		Core::setParseVar("content right", $tpl->display(), TRUE);
	}

	/*function loginByUserID() {
		Auth::loginByUserID(Router::get("id"));
	}*/
	
	function takeloginJSON() {
		$ok = Auth::login($_POST['username'], $_POST['password']);
		
		return JSON::to(array(
			"success" => $ok,
			"text" => $ok ? "" : "Wrong email or password",
			"redirect" => in_array(Auth::getUser("status_id"), array(1,3,4,5)) ? "/maestro" : "/profil",
		));
	}
	
	function json() {
		if (!Auth::isLoggedIn())
			return JSON::to(array(
				"success" => FALSE,
				"text" => "Login to access this function."
			));
				
		if (Router::get("what") == "editprofile") {
			$tpl = new Template("json/editprofile", "users");
			
			return JSON::to(array(
				"success" => TRUE,
				"html" => $tpl->parse(array_merge(Auth::getUser(), array(
					"city" => $this->Cities->get("first", NULL, array("id" => Auth::getUser("city_id"))),
					"dt_birth" => date("Y-m-d", strtotime(Auth::getUser("dt_birth"))),
					"cities list" => $this->Cities->getList(array(
						"name" => "user[city_id]",
						"selected" => Auth::getUser("city_id"),
						"id" => "city_id",
					)),
				)))
			));
		} else if (Router::get("what") == "updateprofile") {
			if (Validate::isEmpty($_POST, array("name", "surname", "post", "email", "address", "phone", "dt_birth")))
				return JSON::to(array(
					"success" => FALSE,
					"text" => "All fields are requred.",
				));
			
			$this->Users->update($_POST, Auth::getUserID(), TRUE, TRUE, TRUE, NULL, array("name", "surname", "post", "email", "address", "phone", "dt_birth"));
			Auth::updateSessionByUserID();
			
			return JSON::to(array(
				"success" => TRUE,
				"html" => $this->profile(),
			));
		} else if (Router::get("what") == "editpassword") {
			$tpl = new Template("json/editpassword", "users");
			
			return JSON::to(array(
				"success" => TRUE,
				"html" => $tpl->parse(Auth::getUser()),
			));
		} else if (Router::get("what") == "updatepassword") {
			if (Validate::isEmpty($_POST, array("user")) || Validate::isEmpty($_POST["user"], array("old_password", "new_password", "new_password2")))
				return JSON::to(array(
					"success" => FALSE,
					"text" => "All fields are requred.",
				));
			
			if ($_POST['user']['new_password'] != $_POST['user']['new_password2'])
				return JSON::to(array(
					"success" => FALSE,
					"text" => "New passwords has to match.",
				));

			if (!Auth::isValidPassword($_POST['user']['new_password']))
				return JSON::to(array(
					"success" => FALSE,
					"text" => "Password minimum length is " . Auth::getConf("pass", "min"),
				));

			if (Auth::hashPassword($_POST['user']['old_password']) != Auth::getUser("password"))
				return JSON::to(array(
					"success" => FALSE,
					"text" => "Wrong current password",
				));
			
			$this->Users->update(array("password" => Auth::hashPassword($_POST['user']['new_password'])), Auth::getUserID(), TRUE, TRUE, TRUE, NULL, array("password"));
			Auth::updateSessionByUserID();
			
			return JSON::to(array(
				"success" => TRUE,
				"html" => $this->profile(),
			));
		}
		
		return JSON::to(array(
			"success" => FALSE,
			"text" => "No action =)"
		));
	}
	
	function getUserData() {
		if (!Auth::isLoggedIn() && FALSE)
			return JSON::to(array(
				"success" => FALSE,
				"text" => "You have to be logged in to get access",
			));
		
		if (!isset($_POST['email']))
			return JSON::to(array(
				"success" => FALSE,
				"text" => "Email is missing",
			));
		
		$mail = mysql_real_escape_string($_POST['email']);
		
		// get all user's orders
		$sqlOrders = "SELECT o.order_id " .
			"FROM orders_users o " .
			"WHERE o.user_id = " . Auth::getUserID();
		$qOrders = $this->db->q($sqlOrders);
		$arrOrders = array();
		while ($rOrder = $this->db->f($qOrders))
			$arrOrders[] = $rOrder['order_id'];
		
		if (empty($arrOrders))
			return JSON::to(array(
				"success" => FALSE,
				"text" => "This user has never ordered at GoNParty.",
			));
		
		$sqlUser = "SELECT u.name, u.surname, u.email " .
			"FROM users u " .
			//"INNER JOIN orders_users o ON o.user_id = u.id " .
			//"WHERE o.order_id IN (" . implode(",", $arrOrders) . ") " . 
			//"AND u.email = '" . $mail . "'";
			"WHERE u.email = '" . $mail . "'";
		$qUser = $this->db->q($sqlUser);
		$rUser = $this->db->f($qUser);
		
		if (empty($rUser))
			return JSON::to(array(
				"success" => FALSE,
				"text" => "User doesn't exist",
			));
		
		return JSON::to(array(
			"success" => TRUE,
			"user" => $rUser,
		));
	}
	
	function maestro() {
		//Core::generateModules();
		$where = array();
		
		if (!isset($_GET['status']) || empty($_GET['status']))
			$_GET['status'] = array(2);
		
		$where[] = "status_id IN (" . implode(",", $_GET['status']) . ")";
		
		$sql = "SELECT u.id, u.email, u.name, u.surname, c.title, u.enabled " .
			"FROM users u " .
			"INNER JOIN cities c ON c.id = u.city_id " .
			"WHERE " . implode(" AND ", $where);
		$q = $this->db->q($sql);
		
		$tpl = new Template("filter", "users");
		Core::loadModule("Statuses");
		$return['filter'] = $tpl->parse(array(
			"multicheckbox statuses" => $this->Statuses->getList(array(
				"name" => "status[]",
				"multiple" => "multiple",
				"selected" => isset($_GET['status']) ? $_GET['status'] : -1,
				"id" => "multicheckboxArticleStatus",
			)),
		));
		
		$return["title"] = "Users";
		$return["th"] = array("ID", "Email", "Name", "Surname", "City", "Status");
		$return["btn header"][] = "add";
		$return["btn header"][] = array(
			"txt" => '<i class="icon-file"></i>',
			"url" => "/maestro/users/exportxls/" . base64_encode($sql),
			"tit" => "Export",
			"add" => NULL,
			"class" => NULL,
		);
		$return["btn header"][] = Core::loadView("makeRelated", "maestro", array(
			"/maestro/statuses" => "Statuses",
		));
		$prevID = NULL;
		while ($r = $this->db->f($q)) {
			$rowBtn = array("update", "delete");
			
			$return["row"][] = array(
				"td" => array(
					$r['id'],
					$r['email'],
					$r['name'],
					$r['surname'],
					$r['title'],
					'<span class="btnToggle btn btn-small" data-url="/users/toggleenabled/' . $r['id'] . '/" data-value="' . $r['enabled'] . '"><i class="icon-check"></i></span>',
				),
				"btn" => $rowBtn
			);
		}
		
		$return["append"] = Core::loadView("getLogins", "logins");

		return $this->Maestro->makeListing($return);
	}
	
	function toggleenabled() {
		return $this->Users->update(array(
			"enabled" => Router::get("active"),
		), Router::get("id"));
	}

	function add() {
		$tpl = new Template("add", "users");

		Core::loadModule("Statuses");
		Core::loadModule("Cities");

		return $this->Maestro->makeAdd(array(
			"content" => $tpl->parse(array(
				"statuses list" => $this->Statuses->getList(array(
					"id" => "status_id",
					"name" => "user[status_id]",
					"class" => "span6",
				)),
				"cities list" => $this->Cities->getList(array(
					"id" => "city_id",
					"name" => "user[city_id]",
					"class" => "span6",
				)),
			)),
			"title" => "user",
		));
	}

	function insert() {
		$_POST['user']['password'] = Auth::hashPassword($_POST['user']['password']);
		$id = $this->Users->insert($_POST['user']);
		
		$this->Maestro->makeInsert(array("id" => $id));
	}

	function edit() {
		$tpl = new Template("edit", "users");

		$r = $this->Users->get("first", NULL, array("id" => Router::get("id")));
		
		$r['cb enabled'] = $r['enabled'] == 1
			? ' checked="checked"'
			: NULL;
			
		$r["statuses list"] = $this->Statuses->getList(array(
			"id" => "status_id",
			"name" => "user[status_id]",
			"selected" => $r['status_id'],
			"class" => "span6",
		));
		
		$r["cities list"] = $this->Cities->getList(array(
			"id" => "city_id",
			"name" => "user[city_id]",
			"selected" => $r['city_id'],
			"class" => "span6",
		));
		
		return $this->Maestro->makeEdit(array(
			"content" => $tpl->parse($r) . $this->OrdersUsers->getOrdersByUserID($r['id']) . Core::loadView("getLogins", "logins", $r['id']),
			"title" => "user",
		));
	}

	function update() {
		if (empty($_POST['user']['password']))
			unset($_POST['user']['password']);
		else
			$_POST['user']['password'] = Auth::hashPassword($_POST['user']['password']);
			
		$this->Users->update($_POST['user'], $_POST['user']['id']);

		return $this->Maestro->makeUpdate(array(
			"id" => $_POST['user']['id']
		));
	}

	function delete() {
		$this->Users->delete(Router::get("id"));
	}
	
	function exportxls() {
		Core::loadLayout(NULL);
		
		require_once APP_PATH . "other" . DS . "php-excel.class.php";
		
		/*/maestro/users/exportxml/$$SQL$$ */
		
		$sql = base64_decode(Core::url(3));
		$q = $this->db->q($sql);
		$data = array();
		while($r = $this->db->f($q))
			$data[] = $r;

		$xls = new Excel_XML('UTF-8', false, 'User Export');
		$xls->addArray($data);
		
		$file = "cache" . DS . "tmp" . DS . "user_export_" . date("Y-m-d H-i-s") . ".xls";
		
		$xls->generateXML(WWW_PATH . $file);
		
		header("Location: " . DS . $file);
		
		return $file;
	}

	// Auth
	function login() {
		if (Auth::isLoggedIn())
			header("Location: " . Auth::getSuccessLoginRedirect());
			
		$tpl = new Template("login", "users");
		
		return $tpl->display();
	}

	function takelogin() {
		Auth::fullLogin(TRUE, null, null, function(){
			if (in_array(Auth::getUser("status_id"), array(1,3,4,5))) {
				Auth::setSuccessLoginRedirect("/maestro");
			}
		});
	}

	function logout() {
		Auth::fullLogout();
	}

	function forgotpassword() {
		Core::loadLayout(NULL);
		if (!isset($_POST['username']) || !Validate::isMail($_POST['username']))
			return JSON::to(array(
				"success" => false,
				"text" => "Email is not valid",
			));

		$r = $this->Users->get("first", NULL, array("email" => $_POST['username']));

		if (empty($r))
			return JSON::to(array(
				"success" => false,
				"text" => "This email is not in our database",
			));
		else {
			// send mail
			return JSON::to(array(
				"success" => true,
				"text" => "We have sent you an email to this email",
			));
		}
	}
}

class UsersModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "email";

		$this->fields = array(
			"email" => array(
				"not null" => TRUE,
				"unique" => TRUE,
			),
			"password" => array(
			),
			"status_id" => array(
				"not null" => TRUE,
				"default" => 2,
			),
			"city_id" => array( // not in use
				"not null" => TRUE,
				"default" => 1,
			),
			"name" => array(
			),
			"surname" => array(
			),
			"dt_birth" => array(
			),
			"address" => array(
			),
			"phone" => array(
			),
			"post" => array(
			),
			"enabled" => array(
				"default" => 1,
			),
		);
	}
}

?>