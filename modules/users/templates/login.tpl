<form method="POST" action="/users/takelogin" class="form-inline">
	<div class="control-group">
		<label class="control-label" for="username">Email</label>
		<div class="controls">
			<input type="text" id="username" name="username" placeholder="E-mail">
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="password">Password</label>
		<div class="controls">
			<input type="password" id="password" name="password" placeholder="Password">
		</div>
	</div>
	<div class="control-group">
		<div class="controls">
			<button type="submit" class="btn">Sign in</button>
		</div>
	</div>
</form>