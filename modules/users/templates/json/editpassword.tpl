<div class="about" id="profileAbout">
	<h1>Živjo, <span>##NAME##</span></h1>
  <ul class="info" id="userInfo">
    <li><input type="password" placeholder="Old password" name="user[old_password]" /></li>
    <li><input type="password" placeholder="New password" name="user[new_password]" /></li>
    <li><input type="password" placeholder="Confirm new password" name="user[new_password2]" /></li>
  </ul>

  <span class="button-green" id="updatePassword" style="cursor: pointer;">Change password</span>
</div>