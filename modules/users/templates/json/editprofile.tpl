<div class="about" id="profileAbout">
	<h1>{{ __('profile_heading_hi') }}, <span>##NAME##</span></h1>
  <ul class="info" id="userInfo">
    <li><input type="text" value="##NAME##" placeholder="{{ __('placeholder_name') }}" name="user[name]" /></li>
    <li><input type="text" value="##SURNAME##" placeholder="{{ __('placeholder_surname') }}" name="user[surname]" /></li>
    <li><input type="text" value="##EMAIL##" placeholder="{{ __('placeholder_email') }}" name="user[email]" /></li>
    <!--<li><input type="password" placeholder="Novo geslo" name="user[new_password]" /></li>-->
    <li><input type="text" value="##PHONE##" placeholder="{{ __('placeholder_phone') }}" name="user[phone]" /></li>
    <li><input type="text" value="##ADDRESS##" placeholder="{{ __('placeholder_address') }}" name="user[address]" /></li>
    <li><input type="text" value="##POST##" placeholder="{{ __('placeholder_post') }}" name="user[post]" /></li>
    <!--<li>##CITIES LIST##</li>-->
    <li><input type="text" value="##DT_BIRTH##" placeholder="{{ __('placeholder_dt_birth') }}" name="user[dt_birth]" class="jd" /></li>
  </ul>

  <script type="text/javascript">
  $(document).ready(function(){
    $(".jd").datepicker({
      weekStart: 1,
      format: 'yyyy-mm-dd',
      startDate: '1920-01-01',
    });
  });
  </script>
  <span class="button-green" id="updateProfile" style="cursor: pointer;">{{ __('btn_save_data') }}</span>
</div>