<div class="control-group">
	<label class="control-label" for="email">Email</label>
	<div class="controls">
    	<input type="text" id="email" name="user[email]" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="password">Password</label>
	<div class="controls">
    	<input type="password" id="password" name="user[password]" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="status">Status</label>
	<div class="controls">
    	##STATUSES LIST##
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="city">City</label>
	<div class="controls">
    	##CITIES LIST##
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="name">Name</label>
	<div class="controls">
    	<input type="text" id="name" name="user[name]" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="surname">Surname</label>
	<div class="controls">
    	<input type="text" id="surname" name="user[surname]" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="dt_birth">Date of birth</label>
	<div class="controls">
    	<input type="text" id="dt_birth" name="user[dt_birth]" class="jdt span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="address">Address</label>
	<div class="controls">
    	<input type="text" id="address" name="user[address]" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="phone">Phone</label>
	<div class="controls">
    	<input type="text" id="phone" name="user[phone]" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="enabled">Enabled</label>
	<div class="controls">
    	<input type="hidden" id="enabledHidden" name="user[enabled]" value="-1" />
    	<input type="checkbox" id="enabled" name="user[enabled]" value="1" />
	</div>
</div>