<?php

class GalleriesPicturesController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function insertelfinder() {
		$tpl = new Template("mediaedit", "galleries");
		
		foreach ($_POST['files'] AS $file) {
			$file = urldecode(str_replace(array("\\", "//"), "/", trim($file)));
			
			// remove ending /0 because trim doesn't work
			while (substr($file, -2) == "/0")
				$file = substr($file, 0, -2);

			$id = $this->GalleriesPictures->insert(array(
				"gallery_id" => $_POST['id'],
				"url" => $file,
			));
			
			$tpl->parse(array(
				"href" => "/maestro/galleries_pictures/edit/" . $id,
				"src" => "/media/" . $file,
				"id" => "pic_" . $id,
			), "media");
		}
		
		return $tpl->display("media");
	}

	function delete() {
		$this->GalleriesPictures->delete(Router::get("id"));
	}
}

class GalleriesPicturesModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "";

		$this->fields = array(
			"gallery_id" => array(
				"not null" => true,
			),
			"url" => array(
				"not null" => true,
			),
			"main" => array(
				"default" => "-1",
			),
			"thumb" => array(
				"default" => "-1",
			),
			"position" => array(
				"default" => "1",
			),
			"url_main" => array(
				"default" => NULL,
			),
			"url_thumb" => array(
				"default" => NULL,
			),
		);
	}
}

?>