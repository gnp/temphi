<?php

class StatusesController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function index() {
	}
	
	function one() {
	}
	
	function maestro() {
		$q = $this->Statuses->get("all");
		
		$return["title"] = "Statuses";
		$return["th"] = array("ID", "Title");
		$return["btn header"][] = "add";
		while ($r = $this->db->f($q)) {
			$return["row"][] = array(
				"td" => array(
					$r['id'],
					$r['title'],
				),
				"btn" => array("update", "delete")
			);
		}
		
		return $this->Maestro->makeListing($return);
	}
	
	function add() {
		$tpl = new Template("add", "Statuses");

		return $this->Maestro->makeAdd(array(
			"content" => $tpl->display(),
			"title" => "status",
		));
	}
	
	function insert() {
		$id = $this->Statuses->insert($_POST['status']);
		
		$this->Maestro->makeInsert(array("id" => $id));
	}

	function edit() {
		$tpl = new Template("edit", "Statuses");

		$r = $this->Statuses->get("first", NULL, array("id" => Router::get("id")));

		return $this->Maestro->makeEdit(array(
			"content" => $tpl->parse($r),
			"title" => "status",
		));
	}

	function update() {
		$this->Statuses->update($_POST['status'], $_POST['status']['id']);

		return $this->Maestro->makeUpdate(array(
			"id" => $_POST['status']['id']
		));
	}

	function delete() {
		$this->Statuses->delete(Router::get("id"));
	}
}

class StatusesModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->countForms['min'] = "status";
		
		$this->mk = "title";

		$this->fields = array(
			"title" => array(
				"not null" => true,
			),

		);
	}
}

?>