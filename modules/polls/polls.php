<?php

class PollsController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function index() {
	}
	
	function one() {
	}
	
	function maestro() {
		$q = $this->Polls->get("all");
		
		$return["title"] = "Polls";
		$return["th"] = array("ID", "Title", "Status");
		$return["btn header"][] = "add";
		while ($r = $this->db->f($q)) {
			$return["row"][] = array(
				"td" => array(
					$r['id'],
					$r['title'],
					'<span class="btnToggle btn btn-small" data-url="/polls/togglepublished/' . $r['id'] . '/" data-value="' . $r['dt_published'] . '"><i class="icon-check"></i></span>',
				),
				"btn" => array("update", "delete", array(
                    "txt" => '<i class="icon-indent-left"></i>',
					"url" => "/maestro/polls_answers/maestro/" . $r['id'],
					"class" => "info",
                    "tit" => "Participans"
				))
			);
		}
		
		return $this->Maestro->makeListing($return);
	}

	function togglepublished() {
		return $this->Polls->update(array(
			"dt_published" => Router::get("active") ? DT_NOW : DT_NULL,
		), Router::get("id"));
	}
	
	function add() {
		$tpl = new Template("add", "Polls");

		return $this->Maestro->makeAdd(array(
			"content" => $tpl->display(),
			"title" => "poll",
		));
	}
	
	function insert() {
		$id = $this->Polls->insert($_POST['poll']);
		
		$this->Maestro->makeInsert(array("id" => $id));
	}

	function edit() {
		$tpl = new Template("edit", "Polls");

		$r = $this->Polls->get("first", NULL, array("id" => Router::get("id")));
			
		return $this->Maestro->makeEdit(array(
			"content" => $tpl->parse($r) . Core::loadView("edit", "polls_options", array("poll_id" => Router::get("id"))),
			"title" => "poll",
		));
	}

	function update() {
		$this->Polls->update($_POST['poll'], $_POST['poll']['id']);

		Core::loadView("update", "polls_options");

		return $this->Maestro->makeUpdate(array(
			"id" => $_POST['poll']['id']
		));
	}

	function delete() {
		$this->Polls->delete(Router::get("id"));
	}
}

class PollsModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "title";

		$this->fields = array(
			"title" => array(
				"not null" => true,
			),
			"question" => array(
				"not null" => true,
			),
			"dt_published" => array(
			),
		);
	}
}

?>