<div class="control-group">
	<label class="control-label" for="title">Title</label>
	<div class="controls">
    	<input type="text" id="title" name="poll[title]" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="dt_published">Published</label>
	<div class="controls">
    	<input type="text" id="dt_published" name="poll[dt_published]" class="jdt span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="question">Question</label>
	<div class="controls">
    	<input type="text" id="question" name="poll[question]" class="span6" />
	</div>
</div>