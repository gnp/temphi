<input type="hidden" id="id" name="poll[id]" value="##ID##" />

<div class="control-group">
	<label class="control-label" for="title">Title</label>
	<div class="controls">
    	<input type="text" id="title" name="poll[title]" value="##TITLE##" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="dt_published">Published</label>
	<div class="controls">
    	<input type="text" id="dt_published" name="poll[dt_published]" value="##DT_PUBLISHED##" class="jdt span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="question">Question</label>
	<div class="controls">
    	<input type="text" id="question" name="poll[question]" value="##QUESTION##" class="span6" />
	</div>
</div>