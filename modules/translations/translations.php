<?php

class TranslationsController extends UFWController {
  function __construct() {
    parent::__construct();
  }
  
  function maestro() {
    $q = $this->Translations->get("all");
    
    $return["title"] = "Translations";
    $return["th"] = array("ID", "Title", "Key", "Content");
    $return["btn header"][] = "add";
    while ($r = $this->db->f($q)) {
      $return["row"][] = array(
        "td" => array(
          $r['id'],
          $r['title'],
          $r['slug'],
          htmlspecialchars($r['content']),
        ),
        "btn" => array("update", "delete")
      );
    }
    
    return $this->Maestro->makeListing($return);
  }
  
  function add() {
    $tpl = new Template("add", "Translations");

    return $this->Maestro->makeAdd(array(
      "content" => $tpl->display(),
      "title" => "translation",
    ));
  }
  
  function insert() {
    $id = $this->Translations->insert($_POST['translation']);
    
    $this->Maestro->makeInsert(array("id" => $id));
  }

  function edit() {
    $tpl = new Template("edit", "Translations");

    $r = $this->Translations->get("first", NULL, array("id" => Router::get("id")));

    return $this->Maestro->makeEdit(array(
      "content" => $tpl->parse($r),
      "title" => "translation",
    ));
  }

  function update() {
    $this->Translations->update($_POST['translation'], $_POST['translation']['id']);

    return $this->Maestro->makeUpdate(array(
      "id" => $_POST['translation']['id']
    ));
  }

  function delete() {
    $this->Translations->delete(Router::get("id"));
  }
}

class TranslationsModel extends UFWModel {
  private $translations = array();

  function __construct() {
    parent::__construct();
    
    $this->mk = "title";

    $this->fields = array(
      "slug" => array(
        "not null" => TRUE,
        "unique" => TRUE,
      ),
      "title" => array(
      ),
      "content" => array(
      ),
    );
  }

  function getContentBySlug($slug) {
    if (empty($this->translations)) {
      $this->translations = array();
      $qTranslations = $this->get("all");
      while ($rTranslation = $this->f($qTranslations)) {
        $this->translations[$rTranslation['slug']] = $rTranslation['content'];
      }
    }

    return isset($this->translations[$slug])
      ? $this->translations[$slug]
      : $slug;
  }
}