<input type="hidden" id="id" name="translation[id]" value="##ID##" />

<div class="control-group">
  <label class="control-label" for="title">Title</label>
  <div class="controls">
      <input type="text" id="title" name="translation[title]" class="span6" value="##TITLE##" />
  </div>
</div>

<div class="control-group">
  <label class="control-label" for="slug">Slug</label>
  <div class="controls">
      <input type="text" id="slug" name="translation[slug]" class="span6" value="##SLUG##" disabled="disabled" />
  </div>
</div>

<div class="control-group">
  <label class="control-label" for="content">Content</label>
  <div class="controls">
      <textarea id="content" name="translation[content]">##CONTENT##</textarea>
  </div>
</div>