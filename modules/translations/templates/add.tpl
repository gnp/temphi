<div class="control-group">
  <label class="control-label" for="title">Title</label>
  <div class="controls">
      <input type="text" id="title" name="translation[title]" class="span6" />
  </div>
</div>

<div class="control-group">
  <label class="control-label" for="slug">Slug</label>
  <div class="controls">
      <input type="text" id="slug" name="translation[slug]" class="span6" />
  </div>
</div>

<div class="control-group">
  <label class="control-label" for="value">Value</label>
  <div class="controls">
      <textarea id="value" name="translation[content]"></textarea>
  </div>
</div>