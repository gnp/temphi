<?php

class OffersTagsController extends UFWController {
	function __construct() {
		parent::__construct();
	}
}

class OffersTagsModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "";

		$this->fields = array(
			"tag_id" => array(
				"not null" => true,
			),
			"offer_id" => array(
				"not null" => true,
			),

		);
	}
}

?>