<?php

class CountriesController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function index() {
	}
	
	function one() {
	}
	
	function maestro() {
		$q = $this->Countries->get("all");
		
		$return["title"] = "Countries";
		$return["th"] = array("ID", "Title");
		$return["btn header"][] = "add";
		while ($r = $this->db->f($q)) {
			$return["row"][] = array(
				"td" => array(
					$r['id'],
					$r['title'],
				),
				"btn" => array("update", "delete")
			);
		}
		
		return $this->Maestro->makeListing($return);
	}
	
	function add() {
		$tpl = new Template("add", "Countries");

		return $this->Maestro->makeAdd(array(
			"content" => $tpl->display(),
			"title" => "country",
		));
	}
	
	function insert() {
		$id = $this->Countries->insert($_POST['country']);
		
		$this->Maestro->makeInsert(array("id" => $id));
	}

	function edit() {
		$tpl = new Template("edit", "Countries");

		$r = $this->Countries->get("first", NULL, array("id" => Router::get("id")));

		return $this->Maestro->makeEdit(array(
			"content" => $tpl->parse($r),
			"title" => "country",
		));
	}

	function update() {
		$this->Countries->update($_POST['country'], $_POST['country']['id']);

		return $this->Maestro->makeUpdate(array(
			"id" => $_POST['country']['id']
		));
	}

	function delete() {
		$this->Countries->delete(Router::get("id"));
	}
}

class CountriesModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->countForms['min'] = "country";
		
		$this->mk = "title";

		$this->fields = array(
			"title" => array(
				"not null" => true,
			),
		);
	}
}

?>