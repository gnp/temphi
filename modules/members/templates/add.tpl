<input type="hidden" id="id" name="member[team_id]" value="##TEAM_ID##" />

<div class="control-group">
	<label class="control-label" for="title">Member</label>
	<div class="controls">
    	<input type="text" id="title" name="member[title]" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="content">Content</label>
	<div class="controls">
    	<textarea id="content" name="member[content]" class="mceEditor"></textarea>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="picture">Picture</label>
	<div class="controls">
    	<input type="hidden" id="picture" name="member[picture]" />
		<input type="button" class="btn btn-primary elFinderData" value="Open file browser" data-id="" data-files="" data-multi="-1" data-replace="newImageField" data-field="member[picture]" data-url="" />
		<img src="" style="display: none; margin-top: 10px;" id="newImageField" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="published">Published</label>
	<div class="controls">
    	<input type="hidden" id="publishedCB" name="member[published]" value="-1" />
    	<input type="checkbox" id="published" name="member[published]" value="1" />
	</div>
</div>