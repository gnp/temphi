<input type="hidden" id="id" name="member[id]" value="##ID##" />

<div class="control-group">
	<label class="control-label" for="title">Member</label>
	<div class="controls">
    	<input type="text" id="title" name="member[title]" value="##TITLE##" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="content">Content</label>
	<div class="controls">
    	<textarea id="content" name="member[content]" class="mceEditor">##CONTENT##</textarea>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="picture">Picture</label>
	<div class="controls">
    	<input type="hidden" id="picture" name="member[picture]" value="##PICTURE##" />
		<input type="button" class="btn btn-primary elFinderData" value="Open file browser" data-id="" data-files="" data-multi="-1" data-replace="newImageField" data-field="member[picture]" data-url="" />
		<img src="##PICTURE2##" id="newImageField" style="display: ##PIC DISPLAY##; margin-top: 10px;" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="published">Published</label>
	<div class="controls">
    	<input type="hidden" id="publishedCB" name="member[published]" value="-1" />
    	<input type="checkbox" id="published" name="member[published]" value="1"##CB PUBLISHED## />
	</div>
</div>