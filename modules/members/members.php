<?php

class MembersController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function index() {
	}
	
	function one() {
	}
	
	function maestro($data = array()) {
    	if (!Validate::isInt($data['team_id']))
			return NULL;
		
        $q = $this->Members->get("all", NULL, array("team_id" => $data['team_id']), array("order by" => "position"));
		
		$return["title"] = "Members";
        $return["module"] = "members";
		$return["th"] = array("ID", "Member", "Status");
        $return["btn header"][] = array(
			"txt" => '<i class="icon-plus"/>',
			"url" => "/maestro/members/add/" . $data['team_id'],
			"class" => "success"
		);
		$return["tbody class"] = "sortable";
		while ($r = $this->db->f($q)) {
			$return["row"][] = array(
				"td" => array(
					$r['id'],
					$r['title'],
					'<span class="btnToggle btn btn-small" data-url="/members/togglepublished/' . $r['id'] . '/" data-value="' . $r['published'] . '"><i class="icon-check"></i></span>',
				),
				"btn" => array("update", "delete")
			);
		}
		
		$tplSort = new Template("onsortupdate", "members");
		$return['append'] = $tplSort->display();
		
		return $this->Maestro->makeListing($return);
	}

	function togglepublished() {
		return $this->Members->update(array(
			"published" => Router::get("active"),
		), Router::get("id"));
	}
	
	function add() {
		$tpl = new Template("add", "members");
		
		return $this->Maestro->makeAdd(array(
			"content" => $tpl->parse(array(
				"team_id" => Router::get("id"),
			)),
			"title" => "member",
		));
	}
	
	function insert() {
		$id = $this->Members->insert($_POST['member']);
		
		$this->Maestro->makeInsert(array("id" => $id));
	}

	function edit() {
		$tpl = new Template("edit", "members");

		$r = $this->Members->get("first", NULL, array("id" => Router::get("id")));

		$r['cb published'] = $r['published'] == 1
			? ' checked="checked"'
			: NULL;
		
		$r['picture2'] = "/media/" . $r['picture'];
		$r['pic display'] = !empty($r['picture']) ? "block" : "none";
			
		return $this->Maestro->makeEdit(array(
			"content" => $tpl->parse($r),
			"title" => "member",
			"breadcrumbs" => $this->Maestro->makeBreadcrumbs(array(
				"Teams" => "/maestro/teams",
				"Edit team" => "/maestro/teams/edit/" . $r['team_id'],
				"Member" => "#",
			))
		));
	}

	function update() {
		$this->Members->update($_POST['member'], $_POST['member']['id']);

		return $this->Maestro->makeUpdate(array(
			"id" => $_POST['member']['id']
		));
	}

	function delete() {
		$this->Members->delete(Router::get("id"));
	}
	
	function updatepositions() {
		foreach ($_POST['positions'] AS $position => $id)
			$this->Members->update(array(
				"position" => $position
			), $id);
	}
}

class MembersModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "title";

		$this->fields = array(
			"title" => array(
				"not null" => true,
			),
			"content" => array(
				"not null" => true,
			),
			"picture" => array(
			),
			"published" => array(
				"default" => "-1",
			),
			"team_id" => array(
				"not null" => true,
			),
			"position" => array(
				"default" => 1,
			),
		);
	}
}

?>