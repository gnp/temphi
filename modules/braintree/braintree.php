<?php

class BraintreeController extends UFWController {
    function __construct() {
        parent::__construct();

        //Initialize BrainTree
        Braintree_Configuration::environment(BRAINTREE_ENVIRONMENT);
        Braintree_Configuration::merchantId(BRAINTREE_MERCHANT_ID);
        Braintree_Configuration::publicKey(BRAINTREE_PUBLIC_KEY);
        Braintree_Configuration::privateKey(BRAINTREE_PRIVATE_KEY);
    }

    function startpayment() {
        $rOrder = $this->Orders->get("first", NULL, array("hash" => Router::get("hash")));
        
        if (empty($rOrder))
            Status::code(400, "Order is missing");
            
        if ($rOrder['dt_rejected'] > DT_NULL)
            Status::code(400, "Order was rejected");

        if ($rOrder['dt_canceled'] > DT_NULL)
            Status::code(400, "Order was canceled");
        
        if ($this->Orders->isPayedReservation($rOrder['id']))
            Status::code(400, "Reservation payment was successful");

        $rOffer = $this->Offers->get("first", NULL, array("id" => $rOrder['offer_id']));
        if (empty($rOffer))
            Status::code(404, "Manjkajoči podatki o ponudbi.");

        $btPaymentHash = sha1(microtime() . Router::get("hash"));
        $braintreeClientToken = Braintree_ClientToken::generate();

        $this->Braintree->insert(array(
            "order_id" => $rOrder['id'],
            "user_id" => Auth::getUserID(),
            "order_hash" => $rOrder['hash'],
            "braintree_hash" => $btPaymentHash,
            "braintree_client_token" => $braintreeClientToken,
            "state" => "started"
        ));

        $confirmPaymentUrl = Router::make("braintree", array("view" => "confirmpayment", "hash" => $btPaymentHash));

        Core::setParseVar("css page", "payment confirmform");
        return new TwigTpl("modules/braintree/templates/startpayment.twig", array(
            "reservation_price" => makePrice(RESERVATION),
            "total_price" => makePrice($rOrder['price']),
            "full" => (isset($_GET['full']) && $_GET['full'] == 1),
            "confirmPaymentUrl" => $confirmPaymentUrl,
            "braintreeClientToken" => $braintreeClientToken,
            "paymenttable" => $this->OffersPaymentMethods->getPaymentTable($rOffer['id']),
            "steps" => SKIP_PAYMENT_METHOD_SELECTION
        ));
    }

    protected function showPaymentError($orderHash, $message)
    {
        Core::setParseVar("css page", "payment confirmform");
        return new TwigTpl("modules/braintree/templates/paymenterror.twig", array(
            "errorMessage" => $message,
            "back_to_payment_url" => Router::make("braintree takeoff", array("hash" => $orderHash)),
            "steps" => SKIP_PAYMENT_METHOD_SELECTION
        ));
    }

    public function confirmpayment()
    {
        $rBraintree = $this->Braintree->get("first", NULL, array("braintree_hash" => Router::get("hash")));
        $rOrder = $this->Orders->get("first", null, array("id" => $rBraintree['order_id']));
        $price = (isset($_REQUEST['full']) && $_REQUEST['full']) ? $this->Orders->getFullPrice($rOrder['id']) : RESERVATION;
        $this->Braintree->update(array(
            "price" => $price
        ), $rBraintree['id']);
        $rBraintree['price'] = $price;

        $braintreeNonce = $_POST["payment_method_nonce"];

        if (!isset($_POST['payment_method_nonce']) || !$_POST['payment_method_nonce']) {
            Status::code(400, "Missing payment method nonce.");
        }

        if ($_POST['payment_method_nonce'] == $rBraintree['braintree_payment_method_nonce']) {
            //User pressed F5. Load existing transaction.
            $result = Braintree_Transaction::find($rBraintree['braintree_transaction_id']);
        } else {
            //Create a new transaction
            $transactionSettings = array(
                'amount' => number_format($rBraintree['price'], 2, '.', ''),
                'paymentMethodNonce' => $braintreeNonce,
                'options' => array(
                    'submitForSettlement' => true
                )
            );

            if (defined('BRAINTREE_MERCHANT_ACCOUNT_ID') && BRAINTREE_MERCHANT_ACCOUNT_ID) {
                $transactionSettings['merchantAccountId'] = BRAINTREE_MERCHANT_ACCOUNT_ID;
            }

            $result = Braintree_Transaction::sale($transactionSettings);
        }

        //Check for errors
        if (!$result->success) {
            $this->Braintree->update(array(
                "state" => 'error',
                "braintree_payment_method_nonce" => $braintreeNonce,
                "error" => print_r($result->message, true)."\n".print_r($result->params, true)
            ), $rBraintree['id']);

            return $this->showPaymentError($rOrder['hash'], $result->message);
        }

        //If everything went fine, we got a transaction object
        $transaction = $result->transaction;

        //Write what we got to the database
        $this->Braintree->update(array(
            "braintree_transaction_id" => $transaction->id,
            "braintree_payment_method_nonce" => $braintreeNonce,
            "state" => 'BT:'.$transaction->status
        ), $rBraintree['id']);

        //SUBMITTED_FOR_SETTLEMENT means it's practically paid
        if ($transaction->status == Braintree_Transaction::SUBMITTED_FOR_SETTLEMENT) {
            if ($rBraintree['price'] == RESERVATION) {
                $sqlBill = "SELECT ob.id, ob.price, ob.order_id, ob.notes " .
                    "FROM orders_bills ob " .
                    "INNER JOIN braintree bt ON bt.order_id = ob.order_id " .
                    "WHERE ob.reservation = 1 " .
                    "AND bt.id = " . intval($rBraintree['id']) .
                    " AND ob.dt_confirmed = '" . DT_NULL . "'";
                $qBill = $this->db->q($sqlBill);
                $rBill = $this->db->f($qBill);
                
                if (!empty($rBill)) {
                    // update bill
                    $this->OrdersBills->update(array(
                        "payed" => RESERVATION,
                        "notes" => $rBill['notes'] . "Braintree",
                        "dt_confirmed" => DT_NOW,
                    ), $rBill['id']);
                    
                    // update order
                    $this->Orders->update(array(
                        "dt_confirmed" => DT_NOW,
                        "dt_rejected" => DT_NULL,
                        "dt_canceled" => DT_NULL
                    ), $rOrder['id']);
                    
                    $rOffer = $this->Offers->get("first", null, array("id" => $rOrder['offer_id']));
                    
                    // update orders users
                    $qOrdersUsers = $this->OrdersUsers->get("all", NULL, array("order_id" => $rOrder['id'], "dt_confirmed " => DT_NULL, "dt_rejected" => DT_NULL, "dt_canceled" => DT_NULL));
                    while ($rOrdersUser = $this->OrdersUsers->f($qOrdersUsers)) {
                        $this->OrdersUsers->update(array(
                            "dt_confirmed" => DT_NOW,
                            "dt_rejected" => DT_NULL,
                            "dt_canceled" => DT_NULL
                        ), $rOrdersUser['id']);
                        
                        if ($rOrder['user_id'] == $rOrdersUser['user_id'])
                            $this->Mails->automatic("order-confirmed-braintree", array("user" => $rOrdersUser['user_id'], "offer" => $rOffer));
                        else
                            $this->Mails->automatic("order-confirmed-friend", array("user" => $rOrdersUser['user_id'], "offer" => $rOffer));
                    }
                }
            } else if ($rBraintree['price'] == $this->Orders->getFullPrice($rOrder['id'])) {
                $qOrdersBills = $this->OrdersBills->get("all", NULL, array("order_id" => $rOrder['id'], "type" => array(1, 2)));
                while ($rOrdersBill = $this->db->f($qOrdersBills)) {
                    $this->OrdersBills->update(array(
                        "dt_confirmed" => DT_NOW,
                        "payed" => $rOrdersBill['price'],
                        "notes" => $rOrdersBill['notes'] . "Braintree",
                    ), $rOrdersBill['id']);
                }

                // update order
                $this->Orders->update(array(
                    "dt_confirmed" => DT_NOW,
                    "dt_payed" => DT_NOW,
                    "dt_rejected" => DT_NULL,
                    "dt_canceled" => DT_NULL
                ), $rOrder['id']);

                $rOffer = $this->Offers->get("first", null, array("id" => $rOrder['offer_id']));
                
                // update orders users
                $qOrdersUsers = $this->OrdersUsers->get("all", NULL, array("order_id" => $rOrder['id'], "dt_confirmed " => DT_NULL, "dt_rejected" => DT_NULL, "dt_canceled" => DT_NULL));
                while ($rOrdersUser = $this->OrdersUsers->f($qOrdersUsers)) {
                    $this->OrdersUsers->update(array(
                        "dt_confirmed" => DT_NOW,
                        "dt_rejected" => DT_NULL,
                        "dt_canceled" => DT_NULL
                    ), $rOrdersUser['id']);
                    
                    if ($rOrder['user_id'] == $rOrdersUser['user_id']) {
                        $this->Mails->automatic("order-confirmed-braintree", array("user" => $rOrdersUser['user_id'], "offer" => $rOffer));
                        $this->Mails->automatic("order-payed", array("user" => $rOrdersUser['user_id'], "offer" => $rOffer));
                    } else
                        $this->Mails->automatic("order-confirmed-friend", array("user" => $rOrdersUser['user_id'], "offer" => $rOffer));
                }
            }

            Status::redirect(Router::make("confirmform", array('provider' => 'braintree', 'hash' => $rOrder['hash'])));
        } else if ($transaction->status == Braintree_Transaction::PROCESSOR_DECLINED) {
            $this->Braintree->update(array(
                "state" => 'BT:'.$transaction->status,
                "error" => print_r(array(
                    "processorResponseCode" => $transaction->processorResponseCode,
                    "processorResponseText" => $transaction->processorResponseText,
                    "additionalProcessorResponse" => $transaction->additionalProcessorResponse
                ), true)
            ), $rBraintree['id']);
            return $this->showPaymentError($rOrder['hash'], $transaction->processorResponseText);
        } else if ($transaction->status == Braintree_Transaction::GATEWAY_REJECTED) {
            $this->Braintree->update(array(
                "state" => 'BT:'.$transaction->status,
                "error" => print_r(
                    array("gatewayRejectionReason" => $transaction->gatewayRejectionReason),
                    true
                )
            ), $rBraintree['id']);
            return $this->showPaymentError($rOrder['hash'], $transaction->gatewayRejectionReason);
        } else {
            //Unknown error
            return $this->showPaymentError($rOrder['hash'], $transaction->status);
        }

        Status::redirect(Router::make("profil"));
    }
}

class BraintreeModel extends UFWModel {
    function __construct() {
        parent::__construct();
        
        $this->mk = "transaction_id";

        $this->fields = array(
            "order_id" => array(
                "int" => array(),
                "not null" => TRUE
            ),
            "user_id" => array(
                "int" => array(),
                "not null" => TRUE
            ),
            "order_hash" => array(
                "not null" => TRUE
            ),
            "braintree_hash" => array(
                "not null" => TRUE
            ),
            "braintree_client_token" => array(
                "not null" => TRUE
            ),
            "braintree_payment_method_nonce" => array(
            ),
            "braintree_transaction_id" => array(
            ),
            "dt_started" => array(
                "default" => DT_NOW,
                "datetime" => array(),
                "not null" => TRUE
            ),
            "dt_confirmed" => array(
                "default" => DT_NULL,
                "datetime" => array()
            ),
            "price" => array(
            ),
            "state" => array(
                "not null" => TRUE
            ),
            "error" => array()
        );
    }
}

?>
