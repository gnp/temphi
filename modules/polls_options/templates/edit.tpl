<div class="control-group">
	<label class="control-label">Answers <br class="cb" /><input type="button" value="Add" class="btn btn-small btn-info" id="btnNewPollsOption" /></label>
	<div class="controls">
	
		##OPTION START##
		<input type="hidden" value="##ID##" />
    	<input type="text" name="polls_option[##ID##][title]" value="##TITLE##" style="margin-bottom: 5px;" class="span6" />
    	<input type="button" value="Delete" style="margin-top: -6px; margin-left: 5px;" class="btn btn-small btn-warning btnDeletePollsOption" /><br />
    	##OPTION END##
    	<div id="addNewPollsOption"></div>
	</div>
</div>