<?php

class PollsOptionsController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function index() {
	}
	
	function one() {
	}
	
	function maestro() {
		$q = $this->PollsOptions->get("all");
		
		$return["title"] = "PollsOptions";
		$return["th"] = array("ID", "title");
		$return["btn header"][] = "add";
		while ($r = $this->db->f($q)) {
			$return["row"][] = array(
				"td" => array(
					$r['id'],
					$r['title'],
				),
				"btn" => array("update", "delete")
			);
		}
		
		return $this->Maestro->makeListing($return);
	}
	
	function add() {
		if (!Validate::isInt($_POST['poll_id']))
			die();
			
		Core::loadLayout(NULL);
		$tpl = new Template("edit", "polls_options");
		
		$id = $this->PollsOptions->insert(array(
			"poll_id" => $_POST['poll_id'],
			"title" => NULL,
		));
		return $tpl->parse(array(
			"id" => $id,
			"title" => NULL,
		), "option");
	}

	function edit($data = array()) {
		if (!Validate::isInt($data['poll_id']))
			return;
		
		$tpl = new Template("edit", "PollsOptions");
		
		$q = $this->PollsOptions->get("all", NULL, array("poll_id" => $data['poll_id']));
		while ($r = $this->db->f($q))
			$tpl->parse($r, "option");

		return $tpl->parse(array(
			"option" => $tpl->display("option"),
		));
	}

	function update() {
		if (isset($_POST['polls_option']))
			foreach ($_POST['polls_option'] AS $id => $option)
				if (!empty($option['title']))
					$this->PollsOptions->update(array(
						"title" => $option['title'],
					), $id);
				else
					$this->PollsOptions->delete($id);

		return;
	}

	function delete() {
		$this->PollsOptions->delete(Router::get("id"));
	}
}

class PollsOptionsModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "title";

		$this->fields = array(
			"poll_id" => array(
				"not null" => true,
			),
			"title" => array(
			),

		);
	}
}

?>