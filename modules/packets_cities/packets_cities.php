<?php

class PacketsCitiesController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function edit($data = array()) {
		if (!isset($data['packet_id']) || !Validate::isInt($data['packet_id']))
			return;
		
		$tpl = new Template("edit", "packets_cities");
		
		$sql = "SELECT c.id, c.title, pc.dt_departure, pc.id AS pcid, pc.fee " .
			"FROM cities c " .
			"LEFT OUTER JOIN packets_cities pc ON (pc.city_id = c.id AND pc.packet_id = " . $data['packet_id'] . ") " .
			"LEFT OUTER JOIN packets p ON (p.id = pc.packet_id AND p.id = " . $data['packet_id'] . ") " .
			"WHERE c.departure = 1 " .
			"ORDER BY c.title";
		$this->db->q($sql);
		
		while ($r = $this->db->f()) {
			$r['cb city'] = is_null($r['pcid'])
				? NULL
				: ' checked="checked"';
			
			$r['disabled'] = is_null($r['pcid'])
				? ' disabled="disabled"'
				: NULL;
			
			$tpl->parse($r, "city");
		}
		
		return $tpl->parse(array(
			"city" => $tpl->display("city")
		));
	}

	function update() {
		if (!Validate::isInt($_POST['packet']['id']))
			return;
		
		$sql = "DELETE FROM packets_cities WHERE packet_id = " . $_POST['packet']['id'] . (isset($_POST['packets_city']) && !empty($_POST['packets_city']) ? " AND city_id NOT IN(" . implode(",", array_keys($_POST['packets_city'])) . ")" : NULL);
		$this->db->q($sql);
		
		if (isset($_POST['packets_city']))
		foreach ($_POST['packets_city'] AS $city_id => $setval) {
			if (!Validate::isInt($city_id))
				continue;
			
			$sqlCheck = "SELECT id FROM packets_cities WHERE city_id = " . $city_id . " AND packet_id = " . $_POST['packet']['id'];
			$qCheck = $this->db->q($sqlCheck);
			$rCheck = $this->db->f($qCheck);
			
			if (empty($rCheck))
				$this->PacketsCities->insert(array(
					"packet_id" => $_POST['packet']['id'],
					"city_id" => $city_id,
					"dt_departure" => $setval["dt_departure"],
				));
			else
				$this->PacketsCities->update(array(
					"dt_departure" => $setval["dt_departure"],
					"fee" => $setval["fee"],
				), $rCheck['id']);
		}
	}
}

class PacketsCitiesModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "";
		
		$this->countForms['min'] = "packets_city";

		$this->fields = array(
			"city_id" => array(
				"not null" => true,
			),
			"packet_id" => array(
				"not null" => true,
			),
            "dt_departure" => array(
            ),
            "fee" => array(
            ),
		);
	}
}

?>