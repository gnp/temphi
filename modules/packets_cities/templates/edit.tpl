<div class="row-fluid">
	<div class="span6">
		<legend>Departure cities</legend>
		##CITY START##
		<div class="control-group">
			<label class="control-label" for="packets_city_cb_##ID##">##TITLE##</label>
			<div class="controls">
				<div class="input-prepend divCbCity">
					<span class="add-on">
		    			<input type="checkbox" value="1" id="packets_city_cb_##ID##" name="packets_city[##ID##][city_id]"##CB CITY## />
		    		</span>
		    		<input class="jdt" type="text" id="packets_city_##ID##" name="packets_city[##ID##][dt_departure]" value="##DT_DEPARTURE##"##DISABLED##/>&nbsp;
		    		<input type="number" min="0" step="0.01" id="packets_city_##ID##_fee" name="packets_city[##ID##][fee]" value="##FEE##"##DISABLED##/>
				</div>
			</div>
		</div>
		##CITY END##
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".divCbCity span input").change(function(){
			if ($(this).attr("checked") == "checked") {
				$(this).parent().next().attr("disabled", false).next().attr("disabled", false);
			} else {
				$(this).parent().next().attr("disabled", true).next().attr("disabled", true);
			}
		});
	});
</script>