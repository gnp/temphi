<?php

class SettingsController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function index() {
	}
	
	function one() {
	}
	
	function maestro() {
		$q = $this->Settings->get("all");
		
		$return["title"] = "Settings";
		$return["th"] = array("ID", "Title", "Key");
		$return["btn header"][] = "add";
		while ($r = $this->db->f($q)) {
			$return["row"][] = array(
				"td" => array(
					$r['id'],
					$r['title'],
					$r['skey'],
				),
				"btn" => array("update", "delete")
			);
		}
		
		return $this->Maestro->makeListing($return);
	}
	
	function add() {
		$tpl = new Template("add", "Settings");

		return $this->Maestro->makeAdd(array(
			"content" => $tpl->display(),
			"title" => "setting",
		));
	}
	
	function insert() {
		$id = $this->Settings->insert($_POST['setting']);
		
		$this->Maestro->makeInsert(array("id" => $id));
	}

	function edit() {
		$tpl = new Template("edit", "Settings");

		$r = $this->Settings->get("first", NULL, array("id" => Router::get("id")));

		return $this->Maestro->makeEdit(array(
			"content" => $tpl->parse($r),
			"title" => "setting",
		));
	}

	function update() {
		$this->Settings->update($_POST['setting'], $_POST['setting']['id']);

		return $this->Maestro->makeUpdate(array(
			"id" => $_POST['setting']['id']
		));
	}

	function delete() {
		$this->Settings->delete(Router::get("id"));
	}
}

class SettingsModel extends UFWModel {
	private $cacheData = array();
	
	function __construct() {
		parent::__construct();
		
		$this->mk = "title";

		$this->fields = array(
			"title" => array(
				"not null" => true,
			),
			"skey" => array(
				"not null" => true,
			),
			"svalue" => array(
			),

		);
	}
	
	function parse($key, $data = array(), $nl2br = FALSE) {
		$subject = new Template();
		$subject->setContent($this->find($key));
		$subject->parse($data);

		return $nl2br == TRUE
			? nl2br($subject->display()) 
			: $subject->display();
	}
	
	function find($key) {
		if (!isset($this->cacheData[$key]) || empty($this->cacheData)) {
			$q = $this->get("all");
			$this->cacheData = array();
			
			while ($r = $this->f($q)) {
				$this->cacheData[$r['skey']] = $r;
			}
		}

		return isset($this->cacheData[$key]) ? $this->cacheData[$key]['svalue'] : NULL;
	}
	
	function findArr($key) {
		if (!isset($this->cacheData[$key]) || empty($this->cacheData)) {
			$q = $this->get("all");
			$this->cacheData = array();
			
			while ($r = $this->f($q)) {
				$this->cacheData[$r['skey']] = $r;
			}
		}

		return isset($this->cacheData[$key]) ? $this->cacheData[$key] : NULL;
	}
}

?>