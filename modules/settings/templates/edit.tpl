<input type="hidden" id="id" name="setting[id]" value="##ID##" />

<div class="control-group">
	<label class="control-label" for="title">Title</label>
	<div class="controls">
    	<input type="text" id="title" name="setting[title]" value="##TITLE##" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="key">Key</label>
	<div class="controls">
    	<input type="text" id="skey" name="setting[skey]" value="##SKEY##" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="value">Value</label>
	<div class="controls">
    	<textarea id="svalue" name="setting[svalue]" class="mceEditor">##SVALUE##</textarea>
	</div>
</div>
