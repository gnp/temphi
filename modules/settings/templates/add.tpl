<div class="control-group">
	<label class="control-label" for="title">Title</label>
	<div class="controls">
    	<input type="text" id="title" name="setting[title]" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="skey">Key</label>
	<div class="controls">
    	<input type="text" id="skey" name="setting[skey]" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="svalue">Value</label>
	<div class="controls">
    	<textarea id="svalue" name="setting[svalue]" class="mceEditor"></textarea>
	</div>
</div>