<input type="hidden" id="id" name="feedback[id]" value="##ID##" />

<div class="control-group">
	<label class="control-label" for="user_id">User</label>
	<div class="controls">
    	##USERS LIST##
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="datetime">Datetime</label>
	<div class="controls">
    	<input type="text" id="datetime" name="feedback[datetime]" value="##DATETIME##" class="jdt span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="content">Content</label>
	<div class="controls">
    	<textarea id="content" name="feedback[content]" class="mceEditor">##CONTENT##</textarea>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="published">Published</label>
	<div class="controls">
		<input type="hidden" value="-1" name="feedback[published]" />
    	<input type="checkbox" id="published" name="feedback[published]" value="1"##CB PUBLISHED## />
	</div>
</div>