<?php

class FeedbacksController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function index() {
	}
	
	function one() {
	}
	
	function maestro() {
		$q = $this->db->q("SELECT f.id, u.email, f.content, f.published " .
				"FROM feedbacks f " .
				"INNER JOIN users u ON u.id = f.user_id");
		
		$return["title"] = "Feedbacks";
		$return["th"] = array("ID", "User", "Content", "Status");
		while ($r = $this->db->f($q)) {
			$return["row"][] = array(
				"td" => array(
					$r['id'],
					$r['email'],
					substr(strip_tags($r['content']), 0, 50),
					'<span class="btnToggle btn btn-small" data-url="/feedbacks/togglepublished/' . $r['id'] . '/" data-value="' . $r['published'] . '"><i class="icon-check"></i></span>',
				),
				"btn" => array("update", "delete")
			);
		}
		
		return $this->Maestro->makeListing($return);
	}

	function togglepublished() {
		return $this->Feedbacks->update(array(
			"published" => Router::get("active"),
		), Router::get("id"));
	}

	function edit() {
		$tpl = new Template("edit", "Feedbacks");

		$r = $this->Feedbacks->get("first", NULL, array("id" => Router::get("id")));
		
		$r['users list'] = $this->Users->getList(array(
			"name" => "feedback[user_id]",
			"id" => "user_id",
			"selected" => $r['user_id'],
			"class" => "span6",
		));
		
		$r["cb published"] = $r['published'] == 1
			? ' checked="checked"'
			: NULL;
		
		return $this->Maestro->makeEdit(array(
			"content" => $tpl->parse($r),
			"title" => "feedback",
		));
	}

	function update() {
		$this->Feedbacks->update($_POST['feedback'], $_POST['feedback']['id']);

		return $this->Maestro->makeUpdate(array(
			"id" => $_POST['feedback']['id']
		));
	}

	function delete() {
		$this->Feedbacks->delete(Router::get("id"));
	}
}

class FeedbacksModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "";

		$this->fields = array(
			"user_id" => array(
				"not null" => true,
			),
			"datetime" => array(
				"not null" => true,
				"default" => date("Y-m-d H:i:s"),
			),
			"content" => array(
				"not null" => true,
			),
			"published" => array(
				"default" => -1,
			),
		);
	}
}

?>