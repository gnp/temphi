<input type="hidden" id="id" name="orders_user[id]" value="##ID##" />

<div class="control-group">
	<label class="control-label" for="user_id">User</label>
	<div class="controls">
		##USERS LIST##
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="packet_id">Packet</label>
	<div class="controls">
		<input type="hidden" id="temp_packet_id" name="temp_packet_id" value="##PACKET_ID##" />
    	##PACKETS LIST##
    	<span class="help-block" style="display: none;"></span>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="orders_users_additions">Additions</label>
	<div class="controls">
    	##ADDITIONS LIST##
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="orders_users_city">City</label>
	<div class="controls">
    	##CITIES LIST##
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="user_id">Status</label>
	<div class="controls">
    	<span>##STATUS##</span>
	</div>
</div>

<!--
<div class="control-group">
	<label class="control-label" for="dt_added">DtAdded</label>
	<div class="controls">
    	<input type="text" id="dt_added" name="orders_users[dt_added]" value="##DT_ADDED##" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="dt_confirmed">DtConfirmed</label>
	<div class="controls">
    	<input type="text" id="dt_confirmed" name="orders_users[dt_confirmed]" value="##DT_CONFIRMED##" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="dt_rejected">DtRejected</label>
	<div class="controls">
    	<input type="text" id="dt_rejected" name="orders_users[dt_rejected]" value="##DT_REJECTED##" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="dt_canceled">DtCanceled</label>
	<div class="controls">
    	<input type="text" id="dt_canceled" name="orders_users[dt_canceled]" value="##DT_CANCELED##" />
	</div>
</div>
-->