<?php

class OrdersUsersController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function index() {
	}
	
	function one() {
	}
	
	function getOrdersByUserID($uID) {
		$sql = "SELECT of.title AS otitle, o.id AS oid, o.dt_confirmed AS odt_confirmed, o.dt_rejected AS odt_rejected, o.dt_canceled AS odt_canceled, ou.dt_confirmed AS oudt_confirmed, ou.dt_rejected AS oudt_rejected, ou.dt_canceled AS oudt_canceled " .
			"FROM offers of " .
			"INNER JOIN orders o ON o.offer_id = of.id " .
			"INNER JOIN orders_users ou ON ou.order_id = o.id " .
			"WHERE ou.user_id = " . $uID . " " .
			"ORDER BY ou.id";
		$q = $this->db->q($sql);
		
		$temp["title"] = "Orders";
		$temp['module'] = "orders_users";
        $temp["th"] = array("Offer", "Status");
        $temp["btn header"][] = NULL;
       while ($r = $this->db->f($q)) {
       		$class = NULL;
		   	$dtStatus = NULL;
			
			//if ($r['odt_confirmed'] > DT_NULL) {
				if ($r['oudt_confirmed'] > DT_NULL) {
					$class = "success";
					$dtStatus = $r['oudt_confirmed'];
				} else if ($r['oudt_rejected'] > DT_NULL) {
					$class = "error";
					$dtStatus = $r['oudt_rejected'];
				} else if ($r['oudt_canceled'] > DT_NULL) {
					$class = "warning";
					$dtStatus = $r['oudt_canceled'];
				}
			/*} else if ($r['odt_rejected'] > DT_NULL) {
				$class = "error";
				$dtStatus = $r['odt_rejected'];
				
			} else if ($r['oft_canceled'] > DT_NULL) {
				$class = "warning";
				$dtStatus = $r['oft_canceled'];
			}*/
			
            $temp["row"][] = array(
                "td" => array(
                    $r['otitle'],
                    $dtStatus,
                ),
                "class" => $class,
                "btn" => array(array(
                  "txt" => '<i class="icon-pencil"></i>',
                  "url" => "/maestro/orders/edit/" . $r['oid'],
                  "class" => "warning",
                  "tit" => "View"
                ))
            );
        }

		return $this->Maestro->makeListing($temp);
	}
	
	function maestro($data = array()) {
		$sqlOrders = "SELECT ou.notes, o.id, ou.id AS ouid, ou.user_id, o.num, u.name, u.surname, u.address, c.title AS city, c2.title AS city2, u.email, u.phone, o.dt_added, ou.dt_confirmed, ou.dt_rejected, ou.dt_canceled, o.dt_payed, o.user_id AS ouser_id, p.title AS packet " .
            "FROM orders o " .
            "INNER JOIN orders_users ou ON ou.order_id = o.id " .
            "LEFT OUTER JOIN packets p ON p.id = ou.packet_id " .
            "LEFT OUTER JOIN users u ON u.id = ou.user_id " .
            "LEFT OUTER JOIN cities c ON c.id = u.city_id " .
            "LEFT OUTER JOIN cities c2 ON c2.id = ou.city_id " .
            "WHERE o.id = " . $data['id'];
        $qOrders = $this->db->q($sqlOrders);
        
        $temp["title"] = "Orders";
		$temp['module'] = "orders_users";
        $temp["th"] = array("ID", "Order", "Packet", "Name", "Surname", "Address", "City", "Department", "Email", "Phone", "Notes", "Additions");
        $temp["btn header"][] = array(
			"txt" => '<i class="icon-plus"></i>',
			"url" => "/maestro/orders_users/add/" . $data['id'],
			"class" => "success"
		);
        $temp["btn header"][] = array(
            "txt" => '<i class="icon-envelope" /i>',
            "url" => "/maestro/mails/compose",
            "tit" => "Send mail",
            "class" => "default btnComposeMail",
        );
        $temp["btn header"][] = array(
        		"txt" => '<i class="icon-print" /i>',
        		"url" => "/maestro/orders/generateAllVouchers",
        		"tit" => "Generate all vouchers",
        		"class" => "default btnGenerateAllVouchers",
        );

        $ordersUserIDs = array();
        while ($rOrder = $this->db->f($qOrders)) {
          $sqlAdditions = "SELECT a.title 
            FROM additions a 
            INNER JOIN packets_additions pa ON pa.addition_id = a.id 
            INNER JOIN orders_users_additions oua ON oua.addition_id = pa.id 
            INNER JOIN orders_users ou ON ou.id = oua.orders_user_id 
            WHERE ou.id = " . $rOrder['ouid'];
          $qAdditions = $this->db->q($sqlAdditions);
          $rOrder['additions'] = array();
          while ($rAddition = $this->db->f($qAdditions))
            $rOrder['additions'][] = $rAddition['title'];
          $rOrder['additions'] = implode("<br />", $rOrder['additions']);


            $ordersUserIDs[] = $rOrder['ouid'];
            $temp["row"][] = array(
                "td" => array(
                    $rOrder['ouid'],
                    bold($rOrder['num'], $rOrder['ouser_id'] == $rOrder['user_id']),
                    $rOrder['packet'],
                    $rOrder['name'],
                    $rOrder['surname'],
                    $rOrder['address'],
                    $rOrder['city'],
                    $rOrder['city2'],
                    $rOrder['email'],
                    $rOrder['phone'],
                    $rOrder['notes'],
                    $rOrder['additions'],
                ),
                "btn" => array(
					HTML::input(array("type" => "checkbox", "value" => $rOrder['ouid'], "name" => "cbOrdersUsers[]", "class" => "pull-right")),
                    "update",
                    (Validate::isNullDate($rOrder['dt_confirmed'])) // show confirm button if not confirmed
                        ? array(
                            "txt" => '<i class="icon-ok"></i>',
                            "url" => "/maestro/orders_users/confirm/" . $rOrder['ouid'],
                            "class" => "success",
                            "disabled" => !is_null($rOrder['dt_confirmed']),
                            "tit" => "Confirm",
                        )
                        : NULL,
                    Validate::isNullDate($rOrder['dt_confirmed']) && Validate::isNullDate($rOrder['dt_rejected']) && Validate::isNullDate($rOrder['dt_canceled']) // show reject button if not rejected, canceled or confirmed
                        ? array(
                            "txt" => '<i class="icon-minus-sign"></i>',
                            "url" => "/maestro/orders_users/reject/" . $rOrder['ouid'],
                            "class" => "danger",
                            "disabled" => !is_null($rOrder['dt_rejected']),
                            "tit" => "Reject",
                        )
                        : NULL,
                    (!Validate::isNullDate($rOrder['dt_confirmed'])) // show cancel button if confirmed
                        ? array(
                            "txt" => '<i class="icon-stop"></i>',
                            "url" => "/maestro/orders_users/cancel/" . $rOrder['ouid'],
                            "class" => "warning",
                            "disabled" => !is_null($rOrder['dt_canceled']),
                            "tit" => "Cancel",
                        )
                        : NULL,
                ),
                "class" => (!Validate::isNullDate($rOrder['dt_confirmed'])
                    ? (!Validate::isNullDate($rOrder['dt_payed'])
                        ? "success"
                        : NULL)
                    : (!Validate::isNullDate($rOrder['dt_rejected'])
                        ? "error"
                        : (!Validate::isNullDate($rOrder['dt_canceled'])
                            ? "warning"
                            : "info"))),
            );
        }

		
		return $this->Maestro->makeListing($temp);
	}
	
	function add() {
		$id = $this->OrdersUsers->insert(array(
			"order_id" => Router::get("id"),
			"packet_id" => -1,
			"user_id" => -1,
			"city_id" => -1,
		));
		
		$this->Maestro->makeInsert(array("id" => $id));
	}
	
	function insert() {
		$id = $this->OrdersUsers->insert($_POST['orders_user']);
		
		$this->Maestro->makeInsert(array("id" => $id));
	}

	function edit() {
		$tpl = new Template("edit", "OrdersUsers");

		$r = $this->OrdersUsers->get("first", NULL, array("id" => Router::get("id")));

		$r['status'] = !Validate::isNullDate($r['dt_confirmed'])
			? "Confirmed " . date("d.m.Y", strtotime($r['dt_confirmed']))
			: (!Validate::isNullDate($r['dt_rejected'])
				? "Rejected " . date("d.m.Y", strtotime($r['dt_rejected']))
				: (!Validate::isNullDate($r['dt_canceled'])
					? "Canceled " . date("d.m.Y", strtotime($r['dt_canceled']))
					: "Waiting for admin's action"
				)
			);
		
		$rPacket = $this->Packets->get("first", NULL, array("id" => $r['packet_id']));

		if (!$rPacket) {
			$rOrder = $this->Orders->get("first", NULL, array("id" => $r['order_id']));
			$rPacket = $this->Packets->get("first", NULL, array("offer_id" => $rOrder['offer_id']));
			
			
			//$rPacket['id'] = -1;
			//$rPacket['offer_id'] = $rOrder['offer_id'];
		}
		
		$rUser = $this->Users->get("first", NULL, array("id" => $r['user_id']));
		$r['user'] = $rUser['name'] . " " . $rUser['surname'];
		
		$r['packets list'] = $this->Packets->getList(array(
			"id" => "packet_id",
			"name" => "orders_user[packet_id]",
			"selected" => $r['packet_id'],
			"where" => "offer_id = " . $rPacket['offer_id'],
		));
		
		$r['cities list'] = $this->Cities->getList(array(
			"id" => "city_id",
			"name" => "orders_user[city_id]",
			"selected" => $r['city_id'],
			"data" => $this->Cities->getArrList(
				NULL,
				array("id IN (SELECT city_id FROM packets_cities WHERE packet_id = " . $r['packet_id'] . ")")
			),
		));
		
		$r['users list'] = $this->Users->getList(array(
			"id" => "user_id",
			"name" => "orders_user[user_id]",
			"selected" => $r['user_id'],
			"data" => $this->Users->getArrList(
				"id, CONCAT(surname, ' ', name) AS user ",
				NULL,
				array("order by" => "surname"),
				"id",
				"user"
			),
		));
		
		// additions for packet
		$sqlAdditions = "SELECT pa.id, a.title FROM additions a INNER JOIN packets_additions pa ON pa.addition_id = a.id WHERE pa.packet_id = " . $rPacket['id'];
		$qAdditions = $this->PacketsAdditions->q($sqlAdditions);
		$arrAdditions = array();
		while ($rAddition = $this->db->f($qAdditions))
			$arrAdditions[$rAddition['id']] = $rAddition['title'];
		
		// current selected additions
		$sqlAdditions = "SELECT pa.id
		FROM packets_additions pa
		INNER JOIN orders_users_additions oua ON oua.addition_id = pa.id 
		INNER JOIN orders_users ou ON ou.id = oua.orders_user_id
		WHERE pa.packet_id = " . $rPacket['id'] . " AND ou.user_id = " . $r['user_id'];
		$qAdditions = $this->PacketsAdditions->q($sqlAdditions);
		$arrSelectedAdditions = array();
		while ($rAddition = $this->db->f($qAdditions))
			$arrSelectedAdditions[$rAddition['id']] = $rAddition['id'];
		
		$r['additions list'] = $this->Additions->getList(array(
			"id" => "orders_users_additions",
			"name" => "orders_users_additions[]",
			"multiple" => "multiple",
			"data" => $arrAdditions,
			"selected" => $arrSelectedAdditions,
		));
			
		return $this->Maestro->makeEdit(array(
			"content" => $tpl->parse($r),
			"title" => "order",
		));
	}

	function update() {
		//die(print_r($_POST));
		
		$this->OrdersUsers->update($_POST['orders_user'], $_POST['orders_user']['id']);
		
		$sql = "DELETE FROM orders_users_additions WHERE orders_user_id = " . $_POST['orders_user']['id'];
		$this->db->q($sql);
		
		if (isset($_POST['orders_users_additions']))
		foreach ($_POST['orders_users_additions'] AS $addition)
			$this->OrdersUsersAdditions->insert(array(
				"addition_id" => $addition,
				"orders_user_id" => $_POST['orders_user']['id'],
			));
		
		return $this->Maestro->makeUpdate(array(
			"id" => $_POST['orders_user']['id']
		));
	}

	function delete() {
		$this->OrdersUsers->delete(Router::get("id"));
	}
	
	function packetchangediff() {
		$old = $_POST['original'];
		$new = $_POST['updated'];
		
		if (!Validate::isInt($old) || !Validate::isInt($new))
			return;
			
		$sql = "SELECT new.price-old.price AS diff " .
			"FROM packets old " .
			"INNER JOIN packets new ON (new.id = " . $new . " AND old.id = " . $old . ")";
		$q = $this->db->q($sql);
		$r = $this->db->f($q);
		
		return JSON::to($r);
	}
	
	function packetchangedadditions() {
		$packet = $_POST['packet'];
		
		if (!Validate::isInt($packet))
			return;
		
		// additions for packet
		$sqlAdditions = "SELECT a.id, a.title FROM additions a INNER JOIN packets_additions pa ON pa.addition_id = a.id WHERE pa.packet_id = " . $packet;
		$qAdditions = $this->PacketsAdditions->q($sqlAdditions);
		$arrAdditions = array();
		while ($rAddition = $this->db->f($qAdditions))
			$arrAdditions[$rAddition['id']] = $rAddition['title'];
		
		// current selected additions
		$sqlAdditions = "SELECT a.id, a.title FROM additions a INNER JOIN packets_additions pa ON pa.addition_id = a.id INNER JOIN orders_users_additions oua ON oua.addition_id = a.id WHERE pa.packet_id = " . $packet;
		$qAdditions = $this->PacketsAdditions->q($sqlAdditions);
		$arrSelectedAdditions = array();
		while ($rAddition = $this->db->f($qAdditions))
			$arrSelectedAdditions[] = $rAddition['id'];
		
		return $this->Additions->getList(array(
			"id" => "orders_users_additions",
			"name" => "orders_users_additions[]",
			"selected" => $packet,
			"multiple" => "multiple",
			"data" => $arrAdditions,
			"selected" => $arrSelectedAdditions,
		));
	}
	
    function confirm() {
        $this->OrdersUsers->update(array(
        	"dt_confirmed" => DT_NOW,
			"dt_rejected" => DT_NULL,
			"dt_canceled" => DT_NULL,
		), Router::get("id"));
		
		$rOrdersUser = $this->OrdersUsers->get("first", NULL, array("id" => Router::get("id")));
		
		$this->Orders->update(array(
			"dt_confirmed" => DT_NOW,
			"dt_rejected" => DT_NULL,
			"dt_canceled" => DT_NULL,
		), $rOrdersUser['order_id']);
		
		Status::redirect($_SERVER['HTTP_REFERER']);
    }
    
    function reject() {
        $this->OrdersUsers->update(array(
        	"dt_confirmed" => DT_NULL,
			"dt_rejected" => DT_NOW,
			"dt_canceled" => DT_NULL,
		), Router::get("id"));
		
		Status::redirect($_SERVER['HTTP_REFERER']);
    }
    
    function cancel() {
        $this->OrdersUsers->update(array(
        	"dt_confirmed" => DT_NULL,
			"dt_rejected" => DT_NULL,
			"dt_canceled" => DT_NOW,
		), Router::get("id"));

		Status::redirect($_SERVER['HTTP_REFERER']);
    }
}

class OrdersUsersModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "";

		$this->fields = array(
			"order_id" => array(
				"not null" => true,
			),
			"packet_id" => array(
				"not null" => true,
			),
			"user_id" => array(
				"not null" => true,
			),
			"city_id" => array(
			),
			"dt_added" => array(
				"not null" => true,
				"default" => DT_NOW,
			),
			"dt_confirmed" => array(
				"default" => DT_NULL,
			),
			"dt_rejected" => array(
				"default" => DT_NULL,
			),
			"dt_canceled" => array(
				"default" => DT_NULL,
			),
			"notes" => array(
				"default" => NULL,
			),
		);
	}
}

?>