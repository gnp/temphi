<?php

class PacketsAdditionsController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function edit($data = array()) {
		if (!isset($data['packet_id']) || !Validate::isInt($data['packet_id']))
			return;
		
		$tpl = new Template("edit", "packets_additions");
		
		$sql = "SELECT a.id, a.title, a.value AS defvalue, pa.value AS setvalue, pa.id AS paid " .
			"FROM additions a " .
			"LEFT OUTER JOIN packets_additions pa ON (pa.addition_id = a.id AND pa.packet_id = " . $data['packet_id'] .  ") " .
			"LEFT OUTER JOIN packets p ON (p.id = pa.packet_id AND p.id = " . $data['packet_id'] .  ") " .
			"ORDER BY a.position ASC";
		$this->db->q($sql);
		
		while ($r = $this->db->f()) {
			$r['cb addition'] = is_null($r['paid'])
				? NULL
				: ' checked="checked"';
			
			$r['value'] = !is_null($r['paid'])
				? $r['setvalue']
				: $r['defvalue'];
			
			$r['disabled'] = is_null($r['paid'])
				? ' disabled="disabled"'
				: NULL;
			
			$tpl->parse($r, "addition");
		}
		
		return $tpl->parse(array(
			"addition" => $tpl->display("addition")
		));
	}

	function update() {
		if (!Validate::isInt($_POST['packet']['id']))
			return;
		
		$sql = "DELETE FROM packets_additions WHERE packet_id = " . $_POST['packet']['id'] . (isset($_POST['packets_addition']) && !empty($_POST['packets_addition']) ? " AND addition_id NOT IN(" . implode(",", array_keys($_POST['packets_addition'])) . ")" : NULL);
		$this->db->q($sql);
		
		if (isset($_POST['packets_addition']))
		foreach ($_POST['packets_addition'] AS $addition_id => $setval) {
			if (!Validate::isInt($addition_id))
				continue;
			
			$sqlCheck = "SELECT id FROM packets_additions WHERE addition_id = " . $addition_id . " AND packet_id = " . $_POST['packet']['id'];
			$qCheck = $this->db->q($sqlCheck);
			$rCheck = $this->db->f($qCheck);
			
			if (empty($rCheck))
				$this->PacketsAdditions->insert(array(
					"packet_id" => $_POST['packet']['id'],
					"addition_id" => $addition_id,
					"value" => $setval["value"],
				));
			else
				$this->PacketsAdditions->update(array(
					"value" => $setval["value"],
				), $rCheck['id']);
		}
	}
}

class PacketsAdditionsModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "";

		$this->fields = array(
			"addition_id" => array(
				"not null" => true,
			),
			"packet_id" => array(
				"not null" => true,
			),
			"value" => array(
			),

		);
	}
}

?>