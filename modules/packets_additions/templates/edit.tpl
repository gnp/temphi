<!--<div class="row-fluid">
	<div class="span6">-->
		<legend>Additions</legend>
		##ADDITION START##
		<div class="control-group">
			<label class="control-label" for="packets_addition_cb_##ID##">##TITLE##</label>
			<div class="controls">
				<div class="input-prepend divCbAddition">
					<span class="add-on">
		    			<input type="checkbox" value="1" id="packets_addition_cb_##ID##" name="packets_addition[##ID##][addition_id]"##CB ADDITION## />
		    		</span>
		    		<input type="text" id="packets_addition_##ID##" name="packets_addition[##ID##][value]" value="##VALUE##"##DISABLED##/>
				</div>
			</div>
		</div>
		##ADDITION END##
<!--	</div>
</div>-->
<script type="text/javascript">
	$(document).ready(function(){
		$(".divCbAddition span input").change(function(){
			if ($(this).attr("checked") == "checked") {
				$(this).parent().next().attr("disabled", false);
			} else {
				$(this).parent().next().attr("disabled", true);
			}
		});
	});
</script>