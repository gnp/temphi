<?php

class NewsTagsController extends UFWController {
	function __construct() {
		parent::__construct();
	}
}

class NewsTagsModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "";

		$this->fields = array(
			"tag_id" => array(
				"not null" => true,
			),
			"news_id" => array(
				"not null" => true,
			),

		);
	}
}

?>