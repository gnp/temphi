<div class="control-group">
	<label class="control-label" for="title">City</label>
	<div class="controls">
    	<input type="text" id="title" name="city[title]" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="code">Postal code</label>
	<div class="controls">
    	<input type="text" id="code" name="city[code]" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="country_id">Country</label>
	<div class="controls">
    	##COUNTRIES LIST##
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="departure">Departure</label>
	<div class="controls">
    	<input type="hidden" id="departureCB" name="city[departure]" value="-1" />
    	<input type="checkbox" id="departure" name="city[departure]" value="1" />
	</div>
</div>