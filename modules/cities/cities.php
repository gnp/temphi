<?php

class CitiesController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function index() {
	}
	
	function one() {
	}
	
	function maestro() {
		$q = $this->Cities->get("all");
		
		$return["title"] = "Cities";
		$return["th"] = array("ID", "Title");
		$return["btn header"][] = "add";
		while ($r = $this->db->f($q)) {
			$return["row"][] = array(
				"td" => array(
					$r['id'],
					$r['title'],
				),
				"btn" => array("update", "delete")
			);
		}
		
		return $this->Maestro->makeListing($return);
	}
	
	function add() {
		$tpl = new Template("add", "Cities");

		return $this->Maestro->makeAdd(array(
			"content" => $tpl->parse(array(
				"countries list" => $this->Countries->getList(array(
					"id" => "country_id",
					"name" => "city[country_id]",
					"class" => "span6",
				))
			)),
			"title" => "city",
		));
	}
	
	function insert() {
		$id = $this->Cities->insert($_POST['city']);
		
		$this->Maestro->makeInsert(array("id" => $id));
	}

	function edit() {
		$tpl = new Template("edit", "Cities");

		$r = $this->Cities->get("first", NULL, array("id" => Router::get("id")));
		
		$r['countries list'] = $this->Countries->getList(array(
			"id" => "country_id",
			"name" => "city[country_id]",
			"selected" => $r['country_id'],
			"class" => "span6",
		));
		
		$r['cb departure'] = $r['departure'] == 1
			? ' checked="checked"'
			: NULL;
		
		return $this->Maestro->makeEdit(array(
			"content" => $tpl->parse($r),
			"title" => "city",
		));
	}

	function update() {
		$this->Cities->update($_POST['city'], $_POST['city']['id']);

		return $this->Maestro->makeUpdate(array(
			"id" => $_POST['city']['id']
		));
	}

	function delete() {
		$this->Cities->delete(Router::get("id"));
	}
}

class CitiesModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->countForms['min'] = "city";
		
		$this->mk = "title";

		$this->fields = array(
			"country_id" => array(
				"not null" => true,
			),
			"title" => array(
				"not null" => true,
			),
			"code" => array(
				"not null" => true,
			),
			"departure" => array(
				"default" => "-1",
			),

		);
	}
}

?>