<?php

class OffersController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function dateFormat($dtLeave, $dtReturn) {
		$date = null;

		if (date("d.m.Y", strtotime($dtLeave)) != date("d.m.Y", strtotime($dtReturn))) {
			$date .= date("j. ",strtotime($dtLeave));
			$date .= SloDate::getNameOfMonth(date("m",strtotime($dtLeave)),true);
			$date .= ". - ";
		}
		$date .= date("j. ",strtotime($dtReturn));
		$date .= SloDate::getNameOfMonth(date("m",strtotime($dtReturn)), true);
		$date .= date(". Y",strtotime($dtReturn));
		
		return $date;
	}
	
	/* modul ki se prikazuje na desni strani v sidebaru */
	function sidebar($var = array()) {
		$tpl = new Template("sidebar", "offers");
		
		//if offer ID is argument, it skips this offer
		$wr = isset($var['skip']) ? " AND o.id != ".$var['skip'] : NULL;
		
		$qOffers = $this->db->q("SELECT o.id, o.title, o.subtitle, o.dt_start, o.dt_end, o.short_content, o.pickup_line AS pickup, o.picture,
													c.title AS ctitle, co.title AS cotitle
											FROM offers o
											INNER JOIN cities c
												ON o.city_id = c.id
											INNER JOIN countries co
												ON co.id = c.country_id
											WHERE dt_published < NOW()
												AND dt_published > '".DT_NULL."'
												$wr
											LIMIT 2");
		
		while($rOffers = $this->db->f($qOffers)) {
			$rOffers["image"] = images.$rOffers["picture"];
			
			$rOffers["url"] = Router::make("offer",array(
				"url" => SEO::niceUrl($rOffers['title']),
				"id" => $rOffers['id'],
			));
			
			$tpl->parse($rOffers, "party");
		}
		
		//Return
		return $tpl->parse(array(
			"party" => $tpl->display("party")
		));
	}
	
	function index() {
		$q = $this->db->q("SELECT o.id, o.title, o.subtitle, o.dt_start, o.dt_end, o.short_content, o.pickup_line AS pickup, o.picture,
													c.title AS ctitle, co.title AS cotitle
											FROM offers o
											INNER JOIN cities c
												ON o.city_id = c.id
											INNER JOIN countries co
												ON co.id = c.country_id
											WHERE o.firstpage = 1
												AND dt_published < NOW()
												AND dt_published > '".DT_NULL."'
											ORDER BY o.position_index ASC
											LIMIT 3");
				
		$tpl = new Template("index", "offers");
		$i=0;
		while($r = $this->db->f($q)){
			$offerId = $r['id'];
			
			$r["pickup line"] = is_null($r["pickup"]) ? NULL : $tpl->parse($r,"pickup line");
			
			//place is always printed, date only if available
			$r["place and date"] = (is_null($r['dt_start'])) ? $r['ctitle'].", ".$r['cotitle'] : $r['ctitle'].", ".$r['cotitle']." <br /> ".$this->dateFormat($r['dt_start'],$r['dt_end']);
			
			//gets minimum price for the party
			$qMinPrice = $this->db->q("SELECT MIN(p.price) AS price
																	FROM packets p
																	WHERE offer_id = $offerId
																		AND dt_published <= NOW()
																		AND dt_published > '".DT_NULL."'");
			$rMinPrice = $this->db->f($qMinPrice);
			
			//saves the price and if it's not null it shows the price tag
			$r["price"] = (int)$rMinPrice['price'];
			$r["price tag"] = is_null($rMinPrice['price']) ? NULL : $tpl->parse($r, "price tag");
			
			$r["image"] = images.$r["picture"];
			
			$r["url"] = Router::make("offer",array(
				"url" => SEO::niceUrl($r['title']),
				"id" => $r['id'],
			));
			
			$tpl->parse($r, "party");
			$i++;
		}
		
		Core::setParseVar("css page", "");

		SEO::title("Hard Island Festival Shop");
		SEO::description("We travel to the best parties in the world to experience moments we will always remember. Close your eyes, forget about every day life and party with us!");

		return $tpl->parse(array(
			"party" => $tpl->display("party"),
			"party short" => $tpl->display("party short"),
		)) . Core::loadView("index", "games");

	}

	private function getMaxPortions($portions, $maxDate, $currentDate) {
		if ($portions < 1)
			$portions = 1;
		else if ($portions > MAX_PORTIONS)
			$portions = MAX_PORTIONS;
		
		$daySec = 24*60*60;
		$maxPortions = round((strtotime($maxDate)-strtotime($currentDate))/$daySec/30);
		
		if ($maxPortions < 1)
			$maxPortions = 1;
		else if ($maxPortions > MAX_PORTIONS)
			$maxPortions = MAX_PORTIONS;
		
		// rewrite portions
		return $maxPortions < $portions	
			? $maxPortions
			: $portions;
	}
	
	function onesidebar($offerID, $visible = "hidden-phone") {
		$tpl = new TwigTpl("modules/offers/templates/onesidebar.twig");
		
		$sqlPackets = "SELECT p.id, p.title, p.price, o.dt_start, p.order_limit_count, COUNT(DISTINCT ou.id) AS ordered " .
				"FROM packets p " .
				"INNER JOIN offers o ON o.id = p.offer_id " .
				"LEFT OUTER JOIN orders ord ON (ord.offer_id = o.id AND ord.dt_confirmed > '" . DT_NULL . "') " .
				"LEFT OUTER JOIN orders_users ou ON (ou.order_id = ord.id AND ou.packet_id = p.id) " .
				"WHERE p.offer_id = '" . $this->db->escape($offerID) . "' " . 
				"AND p.dt_published > '" . DT_NULL . "' " . 
				"AND p.dt_published < '" . DT_NOW . "' " .
				"AND o.dt_published > '" . DT_NULL . "' " . 
				"AND o.dt_published < '" . DT_NOW . "' " . 
				"AND o.dt_opened < '" . DT_NOW . "' " . 
				"AND (o.dt_closed > '" . DT_NOW . "' " .
				"OR o.dt_closed = '" . DT_NULL . "') " .
				"GROUP BY p.id " .
				"HAVING (p.order_limit_count < 1 OR COUNT(DISTINCT ou.id) < p.order_limit_count)" .
				"ORDER BY p.position";
		
		$qPackets = $this->db->q($sqlPackets);
		$minPrice = null;
		$arrPackets = array();
		$portions = false;
		
		$cp = $this->db->c($qPackets);
		//if (!$this->db->c($qPackets))
		//	return;
		
		$qPackets = $this->db->q($sqlPackets);
		while ($rPacket = $this->db->f($qPackets)) {
			if (!$minPrice || $minPrice > $rPacket['price']) $minPrice = $rPacket['price'];

			$arrPackets[] = array(
				"price" => makePrice($rPacket['price'], 0),
				"portions" => $portions,
				"title" => $rPacket['title']
			);
		}
		
		$maxPortions = $rPacket ? $this->getMaxPortions(MAX_PORTIONS, $rPacket['dt_start'], DT_NOW) : null;
			
		if($maxPortions > 1) {
			$portions = "Payment possible in $maxPortions installments";
		}

		if ($minPrice) $minPrice = makePrice($minPrice, 0);
		
		$rOffer = $this->Offers->get('first', NULL, array('id' => $offerID));
		$rOffer['city'] = $this->Cities->get('first', NULL, array("id" => $rOffer['city_id']));
		$rOffer['country'] = $this->Countries->get('first', NULL, array("id" => $rOffer['city']['country_id']));
 
		return $tpl->parse(array(
			"packets" => $arrPackets,
			"minPrice" => $minPrice,
			"maxPortions" => $portions,
			"offer" => $rOffer,
			"phoneclass" => $visible,
		));
	}
	
	function one() {
		$tpl = new Template("one", "offers");
		
		$qOffer = $this->db->q("SELECT o.id, o.title, o.subtitle, o.dt_start, o.dt_end, o.short_content, o.content, o.picture,
													o.price_text, o.dt_published, o.published, o.pickup_line AS pickup,
													c.title AS ctitle, co.title AS cotitle, o.dt_closed, o.lineup, o.order_limit
									FROM offers o
									INNER JOIN cities c
										ON o.city_id = c.id
									INNER JOIN countries co
										ON co.id = c.country_id
									WHERE o.id = " . Router::get("id"));
		
		$rOffer = $this->db->f($qOffer);

		if (empty($rOffer))
			Status::code(404, "Offer doesnt exist.");

		$rOffer["image"] = images.$rOffer["picture"];
		
		$rOffer["facebook url"] = "https://gonparty.eu" . Router::make("offer",array(
			"url" => SEO::niceUrl($rOffer['title']),
			"id" => $rOffer['id'],
		));
		
		$rOffer["pickup line"] = is_null($rOffer["pickup"]) ? NULL : $tpl->parse($rOffer,"pickup line");
		
		//calculates time difference until party start for countdown
		$diff = getTimeBetween(strtotime($rOffer['dt_start']));
		$arrConfDiff = array(
			/*"y" => array(
				"length" => "years",
				"string" => array(
					0 => "let",
					1 => "leto",
					2 => "leti",
					3 => "leta",
					4 => "leta",
					"more" => "let",
				),
			),*/
			"m" => array(
				"length" => "months",
				"string" => array(
					0 => "months",
					1 => "month",
					2 => "months",
					3 => "months",
					4 => "months",
					"more" => "months"
				),
			),
			"d" => array(
				"length" => "days",
				"string" => array(
					0 => "days",
					1 => "day",
					"more" => "days",
				),
			),
			"h" => array(
				"length" => "hours",
				"string" => array(
					0 => "hours",
					1 => "hour",
					2 => "hours",
					3 => "hours",
					4 => "hours",
					"more" => "hours",
				),
			),
			"i" => array(
				"length" => "minutes",
				"string" => array(
					0 => "minutes",
					1 => "minute",
					2 => "minutes",
					3 => "minutes",
					4 => "minutes",
					"more" => "minutes",
				) 
			),
			/*"s" => array(
				"length" => "seconds",
				"string" => array(
					0 => "sekund",
					1 => "sekunda",
					2 => "sekundi",
					3 => "sekunde",
					4 => "sekunde",
					"more" => "sekund",
				)
			),*/
		);
		foreach ($arrConfDiff AS $key => $confDiff) {
			$tpl->parse(array(
				"length" => $confDiff['length'],
				"title" => isset($confDiff['string'][$diff[$key]])
					? $confDiff['string'][$diff[$key]]
					: $confDiff['string']["more"],
				"time" => $diff[$key]
			), "counter");
		}
		$tpl->commit("counter");
		
		
		$rOffer['days'] = str_pad($diff['d'], 2, '0', STR_PAD_LEFT);
		$rOffer['hours'] = str_pad($diff['h'], 2, '0', STR_PAD_LEFT);
		$rOffer['minutes'] = str_pad($diff['i'], 2, '0', STR_PAD_LEFT);
		
		//place is always printed, date only if available
		$rOffer["place and date"] = (is_null($rOffer['dt_start'])) ? $rOffer['ctitle'].", ".$rOffer['cotitle'] : $rOffer['ctitle'].", ".$rOffer['cotitle']." / <span>".$this->dateFormat($rOffer['dt_start'],$rOffer['dt_end']).'</span>';
		
		$arrSections = array(
			array(
				"title" => "Packages",
				"ticket" => -1,
			),
			array(
				"title" => "Tickets",
				"ticket" => 1,
			),
		);
		
		$selected = NULL;
		$packetExist = false;
		foreach ($arrSections AS $section) {
			$sqlPackets = "SELECT p.id, p.title, p.price, o.dt_start, p.order_limit, p.order_limit_count, COUNT(DISTINCT ou.id) AS ordered, p.ticket " .
				"FROM packets p " .
				"INNER JOIN offers o ON o.id = p.offer_id " .
				"LEFT OUTER JOIN orders ord ON (ord.offer_id = o.id AND ord.dt_confirmed > '" . DT_NULL . "') " .
				"LEFT OUTER JOIN orders_users ou ON (ou.order_id = ord.id AND ou.packet_id = p.id) " .
				"WHERE p.ticket " . ($section['ticket'] == 1 ? " = 1 " : " <> 1 ") .
				"AND p.offer_id = " . $rOffer['id'] . " " . 
				"AND p.dt_published > '" . DT_NULL . "' " . 
				"AND p.dt_published < '" . DT_NOW . "' " .
				"AND o.dt_published > '" . DT_NULL . "' " . 
				"AND o.dt_published < '" . DT_NOW . "' " . 
				"AND o.dt_opened < '" . DT_NOW . "' " . 
				"AND (o.dt_closed > '" . DT_NOW . "' " .
				"OR o.dt_closed = '" . DT_NULL . "') " .
				"GROUP BY p.id " .
				"HAVING (p.order_limit_count < 1 OR COUNT(DISTINCT ou.id) < p.order_limit_count)" .
				"ORDER BY p.position";
			$qPackets = $this->db->q($sqlPackets);
			$numPackets = $this->db->c($qPackets);
			while ($rPacket = $this->db->f($qPackets)) {
				$packetExist = TRUE;
				$w = FALSE;
				// includes
				$sqlIncludes = "SELECT a.title, a.description " .
					"FROM additions a " .
					"INNER JOIN packets_includes pi ON pi.addition_id = a.id " .
					"WHERE pi.packet_id = " . $rPacket['id'] . " " .
					"ORDER BY a.position";
				$qIncludes = $this->db->q($sqlIncludes);
				while ($rInclude = $this->db->f($qIncludes)) {
					$rInclude['questionmark'] = ($rInclude['description'] ? ' <img alt="?" src="/img/icons/questionmark.png" />' : NULL);
					$tpl->parse($rInclude, "includes");
					$w = TRUE;
				}
				//$tpl->commit("includes");

				$w2 = FALSE;
				//additions
				$sqlAdditions = "SELECT a.title, a.description, pa.value " .
					"FROM additions a " .
					"INNER JOIN packets_additions pa ON pa.addition_id = a.id " .
					"WHERE pa.packet_id = " . $rPacket['id'] . " " .
					"ORDER BY a.position";
				$qAdditions = $this->db->q($sqlAdditions);
				while ($rAddition = $this->db->f($qAdditions)) {
					$rAddition['questionmark'] = ($rAddition['description'] ? ' <img alt="?" src="/img/icons/questionmark.png" />' : NULL);
					$rAddition['value'] = makePrice($rAddition['value']);
					$tpl->parse($rAddition, "additions");
					$w2 = TRUE;
				}
				
				//max portions
				
				$maxPortions = $this->getMaxPortions(MAX_PORTIONS, $rPacket['dt_start'], DT_NOW);
					
				if($maxPortions > 1) {
					$portions = "$maxPortions installments";
				}
				else {
					$portions = false;
				}
				
				//$tpl->commit("additions");
				$selected = 0;
				$tpl->parse(array(
					//"includes" => $tpl->display("includes"),
					//"additions" => $tpl->display("additions"),
					"offer_type" => $rPacket['ticket'] == 1 ? 'ticket' : 'packet',
					"packet" => $rPacket,
					"packet price" => makePrice($rPacket['price'], 0),
					"includes" => $tpl->display("includes"),
					"additions" => $tpl->display("additions"),
					"packet tabs" => $this->PacketsTabs->onOffer($rPacket['id']),
					"max portions" => $portions,
					"persons list" => HTML::select(array(
						"class" => "",
						"id" => "",
						"name" => "packets[" . $rPacket['id'] . "]",
						"selected" => $selected,
						"options" => array_merge(array(0 => "0"),range(1, $rPacket['order_limit'] > 0 ? $rPacket['order_limit'] : ($rOffer['order_limit'] > 0 ? $rOffer['order_limit'] : 4))),
					)),
					"last title" => "Limited seats",
					"w" => ($w && $w2) ? 50 : ($w ? 100 : 0 . "; display:none"),
					"w2" => ($w && $w2) ? 50 : ($w2 ? 100 : 0 . "; display:none"),
				), "package");
			}
			if ($numPackets)
				$tpl->parse(array(
					"section title" => $section['title'],
					"package" => $tpl->display("package"),
					"action" => Router::make("narocilnica"),
					"offer id" => $rOffer['id'],
					"section type" => $section['ticket'] == 1 ? 'ticket' : 'packet',
				), "section");
		}

		if ($rOffer['dt_closed'] > DT_NOW)
			$tpl->parse(array(
				"section" => $tpl->display("section"),
				"if packages" => $packetExist ? $tpl->parse(array(), "if packages") : NULL,
				"if not packages" => !$packetExist ? $tpl->parse(array(), "if not packages") : NULL,
			), "sections");
		else {
			$tpl->manualAddToPart(NULL, "sections");
		}
		$tpl->commit("sections");
		
		$rOffer['event gallery'] = $this->Galleries->onOffer($rOffer['id']);
		$rOffer['more info'] = trim(strip_tags($rOffer['content'])) != ""
			? $tpl->parse(array("content" => $rOffer['content']), "more info")
			: NULL;

		$rOffer['lineup block'] = trim(strip_tags($rOffer['lineup'])) != ""
			? $tpl->parse(array(
				"lineup" => $rOffer['lineup'],
				"current url" => Router::currentURL(TRUE)
			), "lineup block")
			: NULL;

		$tpl->parse(array("module enews" => $this->Mailchimp->enewsoffer()));
		
		Core::setParseVar("css page", "the-offer");
		Core::setParseVar("content right", $this->Offers->onesidebar($rOffer['id']) . $this->Offers->sidebar(array("skip" => $rOffer['id'])), TRUE);

		SEO::title("Hard Island " . $rOffer['title']);
		
		$rOffer['basicinfos'] = $this->Offers->onesidebar($rOffer['id'], "visible-phone");

		return array(
			"content" => $tpl->parse($rOffer),
			//"customheader" => $this->offerHeader($rOffer),
			"offerpage" => true,
		);
	}

	function offerHeader($rOffer) {
		$tpl = new TwigTpl("offers/templates/offerheader.twig");

		return $tpl->parse(array(
			"offer" => $rOffer,
		));
	}

	function all() {
		$tpl = new TwigTpl("modules/offers/templates/all.twig");
		
		$qCats = $this->db->q("SELECT c.title, COUNT(o.id) AS offersNum, c.id
									FROM categories c
									LEFT OUTER JOIN offers o
										ON (c.id = o.category_id )
									WHERE c.published = 1
												AND dt_published < NOW()
												AND dt_published > '".DT_NULL."'
									GROUP BY c.id
									HAVING offersNum > 0
									ORDER BY position ASC, offersNum DESC");

		$arrCats = array();
		$arrTitle = array();
		while($rCat = $this->db->f($qCats)) {
			$arrCats[$rCat["id"]] = $rCat;
			$arrTitle[] = $rCat["title"];
			if($rCat["offersNum"] > 0)
				$arrListItems[] = array(
					"title" => $rCat["title"],
					"friendly" => SEO::niceUrl($rCat["title"]),
				);
		}

		$arrListItems = array();
		$empty = 0;

		$arrOffers = array();
		$qOffers = $this->db->q("SELECT MIN(p.price) AS price, o.id, o.category_id, o.title, o.subtitle, o.dt_start, o.dt_end, o.short_content, o.price_text, o.pickup_line AS pickup, c.title AS ctitle, co.title AS cotitle, o.picture AS url
											FROM offers o
											INNER JOIN cities c
												ON o.city_id = c.id
											INNER JOIN countries co
												ON co.id = c.country_id
											INNER JOIN categories ca
												ON o.category_id = ca.id
											LEFT OUTER JOIN packets p
												ON (p.offer_id = o.id
													AND p.dt_published < '".DT_NOW."'
													AND p.dt_published > '".DT_NULL."'
												)
											WHERE o.dt_published < '".DT_NOW."'
												AND o.dt_published > '".DT_NULL."'
											GROUP BY o.id
											ORDER BY o.position_listing ASC");
		while ($rOffer = $this->db->f($qOffers)) {
			$rOffer['cat'] = $arrCats[$rOffer["category_id"]];

			//place is always printed, date only if available
			$rOffer["place_date"] = (is_null($rOffer['dt_start'])) ? $rOffer['ctitle'].", ".$rOffer['cotitle'] : $rOffer['ctitle'].", ".$rOffer['cotitle']." / ".$this->dateFormat($rOffer['dt_start'],$rOffer['dt_end']);
			
			$rOffer["image"] = images.$rOffer["url"];
				
			$rOffer["url"] = Router::make("offer",array(
				"url" => SEO::niceUrl($rOffer['title']),
				"id" => $rOffer['id'],
			));

			$arrOffers[] = $rOffer;
		}

		$qEmptyCats = $this->db->q("SELECT c.title
									FROM categories c
									WHERE published = 1
									ORDER BY position ASC");
		
		if($this->db->c($qEmptyCats) > count($arrCats)) {
			$arrListItems[] = array(
				"title" => "OTHER",
				"friendly" => "ostalo",
				"sep" => '<span></span>',
			);
		}
		
		$arrOtherCats = array();
		while($rCategory = $this->db->f($qEmptyCats)) {
			foreach($arrCats as $cat) {
				if($cat["title"] == $rCategory['title']) {
					$arrOtherCats[] = $rCategory;
					break;
				}
			}
		}

		$moduleEnews = $this->Mailchimp->enewsshort();

		// SEO::title(implode(", ", array_slice($arrTitle, 0, count($arrTitle)-1)) . " and " . array_pop($arrTitle) . " parties with " . SITE);
		SEO::title("Hard Island Festival Shop");
		SEO::description("Experience the best festivals in the world. Don’t think, just GoNParty");
		Core::setParseVar("css page", "offers", false, false);
		
		return $tpl->parse(array(
			"listItems" => $arrListItems,
			"offers" => $arrOffers,
			"moduleEnews" => $moduleEnews,
			"otherCats" => $arrOtherCats,
		));
	}

    function maestro() {
        $q = $this->Offers->get("all");
        
        $return["title"] = "Offers";
        $return["th"] = array("ID", "Title", "Event start", "Published", "Status", "First page", "Top");
        $return["btn header"][] = "add";
        $return["btn header"][] = Core::loadView("makeRelated", "maestro", array(
            "/maestro/additions" => "Additions",
            "/maestro/categories" => "Categories",
            "/maestro/countries" => "Countries",
            "/maestro/cities" => "Cities",
            "/maestro/offers/position/index" => "Set positions on index",
            "/maestro/offers/position/listing" => "Set positions on listing",
        ));
        while ($r = $this->db->f($q)) {
            $return["row"][] = array(
                "td" => array(
                    $r['id'],
                    $r['title'],
                    $r['dt_start'],
                    $r['dt_published'],
					'<span class="btnToggle btn btn-small" data-url="/offers/togglepublished/' . $r['id'] . '/" data-value="' . $r['dt_published'] . '"><i class="icon-check"></i></span>',
					'<span class="btnToggle btn btn-small" data-url="/offers/togglefirstpage/' . $r['id'] . '/" data-value="' . $r['firstpage'] . '"><i class="icon-check"></i></span>',
					'<span class="btnToggle btn btn-small" data-url="/offers/toggletop/' . $r['id'] . '/" data-value="' . $r['top'] . '"><i class="icon-check"></i></span>',
                ),
                "btn" => array("update", "delete", array(
                	"url" => "/maestro/orders/maestro?offer=" . $r['id'],
                	"class" => "info",
                	"txt" => '<i class="icon-globe"></i>',
                ))
            );
        }
        return $this->Maestro->makeListing($return);
    }

    function position() {
		$q = $this->Offers->get("all", NULL, NULL, array("order by" => array("position_" . Router::get("by"))));
		
		$return["title"] = "Set offer's positions";
		$return["th"] = array("ID", "Title");
		$return["btn header"] = array();
		$return["tbody class"] = "sortable";
		while ($r = $this->db->f($q)) {
			$return["row"][] = array(
				"td" => array(
					$r['id'],
					$r['title'],
				),
				"btn" => array(),
			);
		}
		
		$tplSort = new Template("onsortupdate", "offers");
		$return['append'] = $tplSort->parse(array("type" => Router::get("by")));
		
		return $this->Maestro->makeListing($return);
    }

	function updatepositions() {
		if (!in_array($_POST['type'], array("index", "listing")))
			return JSON::to(array("success" => FALSE));

		foreach ($_POST['positions'] AS $position => $id)
			$this->Offers->update(array(
				"position_" . $_POST['type'] => $position
			), $id);
	}
    
	function togglepublished() {
		return $this->Offers->update(array(
			"dt_published" => Router::get("active") ? DT_NOW : DT_NULL,
		), Router::get("id"));
	}
	
	function togglefirstpage() {
		return $this->Offers->update(array(
			"firstpage" => Router::get("active"),
		), Router::get("id"));
	}
	
	function toggletop() {
		return $this->Offers->update(array(
			"top" => Router::get("active"),
		), Router::get("id"));
	}
	
	function mediaEdit($data = array()) {
		if (!isset($data['offer_id']) || !Validate::isInt($data['offer_id']))
			return NULL;
			
		$q = $this->OffersPictures->get("all", NULL, array("offer_id" => $data['offer_id']), array("order by" => "position"));
		
		$tpl = new Template("mediaedit", "offers");
		while ($r = $this->db->f($q)) {
			$tpl->parse(array(
				"delete url" => "/maestro/offers_pictures/delete/" . $r['id'],
				"src" => "/media/" . $r['url'],
				"id" => $r['id'],
			), "media");
		}
		
		return $tpl->parse(array(
			"media" => $tpl->display("media"),
			"offer id" => $data['offer_id'],
		));
	}
    
    function add() {
        $tpl = new Template("add", "Offers");

        $r["cities list"] = $this->Cities->getList(array(
            "id" => "city_id",
            "name" => "offer[city_id]",
            "class" => "span6"
        ));
		
        $r["categories list"] = $this->Categories->getList(array(
            "id" => "category_id",
            "name" => "offer[category_id]",
            "class" => "span6"
        ));
        
        return $this->Maestro->makeAdd(array(
            "content" => $tpl->parse($r),
            "title" => "offer",
        ));
    }
    
    function insert() {
     	$id = $this->Offers->insert($_POST['offer']);
		
		$this->Maestro->makeInsert(array("id" => $id));
    }

    function edit() {
        $tpl = new Template("edit", "Offers");

        $r = $this->Offers->get("first", NULL, array("id" => Router::get("id")));
            
        $r['cb firstpage'] = $r['firstpage'] == 1
            ? ' checked="checked"'
            : NULL;
            
        $r['cb top'] = $r['top'] == 1
            ? ' checked="checked"'
            : NULL;
        
        $r["cities list"] = $this->Cities->getList(array(
            "id" => "city_id",
            "name" => "offer[city_id]",
            "selected" => $r['city_id'],
            "class" => "span6"
        ));
		
        $r["categories list"] = $this->Categories->getList(array(
            "id" => "category_id",
            "name" => "offer[category_id]",
            "class" => "span6",
            "selected" => $r['category_id'],
        ));
		
		$r['picture2'] = "/media/" . $r['picture'];
		$r["pic display"] = empty($r["picture"])
			? "none"
			: "block";

        return $this->Maestro->makeEdit(array(
            "content" => $tpl->parse($r) . 
				$this->mediaEdit(array("offer_id" => $r['id'])) .
            	$this->Packets->maestro(array("offer_id" => $r['id'])) .
                Core::loadView("edit", "tags", array("multiple" => "offers", "single" => "offer", "id" => $r['id'])) .
                Core::loadView("edit", "offers_payment_methods", array("offer_id" => $r['id']))->__toString(),
            "title" => "offer",
        ));
    }

    function update() {
        $this->Offers->update($_POST['offer'], $_POST['offer']['id']);

        Core::loadView("update", "tags", array("multiple" => "offers", "single" => "offer", "id" => $_POST['offer']['id']));
        
        Core::loadView("update", "offers_additions");
        Core::loadView("update", "offers_includes");
        //Core::loadView("update", "offers_cities");
        Core::loadView("update", "offers_includes");
        Core::loadView("update", "offers_payment_methods");

        return $this->Maestro->makeUpdate(array(
            "id" => $_POST['offer']['id'],
        ));
    }

    function delete() {
        $this->Offers->delete(Router::get("id"));
    }
	
	function updatemediapositions() {
		foreach ($_POST['positions'] AS $position => $id)
			$this->OffersPictures->update(array(
				"position" => $position,
			), $id);
	}
}

class OffersModel extends UFWModel {

	function __construct() {
		parent::__construct();
		
		$this->mk = "title";

		$this->fields = array(
			"title" => array(
				"not null" => true,
			),
			"subtitle" => array(
			),
			"picture" => array(
			),
			"city_id" => array(
				"not null" => true,
			),
			"dt_start" => array(
			),
			"dt_end" => array(
			),
			"short_content" => array(
			),
			"content" => array(
			),
			"dt_published" => array(
			),
			"pickup_line" => array(
			),
			"soundcloud" => array(
			),
			"fb_event_url" => array(
			),
			"firstpage" => array(
				"default" => "-1",
			),
			"top" => array(
				"default" => "-1",
			),
			"dt_opened" => array(
			),
			"dt_closed" => array(
			),
			"order_limit" => array(
				"default" => -1,
			),
			"order_limit_count" => array(
				"default" => -1,
			),
			"category_id" => array(),
			"lineup" => array(),
			"position_index" => array("default" => 12321),
			"position_listing" => array("default" => 12321),
		);
	}
}

	function getTimeBetween($first, $second = DT_NOW) {
		// set first 
		$first = Validate::isInt($first)
			? $first
			: strtotime($first);
		$second = Validate::isInt($second)
			? $second
			: strtotime($second);
		
		$diff = $first > $second
			? $first - $second
			: $second - $first;
		
		$arrDiff = array(
			"years" => 0,
			"months" => 0,
			"days" => 0,
			"hours" => 0,
			"minutes" => 0,
			"seconds" => 0
		);
		$arrShort = array(
			"years" => "y",
			"months" => "m",
			"days" => "d",
			"hours" => "h",
			"minutes" => "i",
			"seconds" => "s",
		);
		
		$already = "+";
		foreach ($arrDiff AS $long => $num) {
			$i = 0;
			while (strtotime($already . $i . $long, 0) <= $diff) {
				$i++;
			}
			
			$i--;
			
			if ($i >= 0) {
				$already .= $i . $long . " ";
				unset($arrDiff[$long]);
				$arrDiff[$arrShort[$long]] = $i;
			}
		}
		
		return $arrDiff;
	}

?>