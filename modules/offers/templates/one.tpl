<div style="float: left;">
<script src="/js/jquery.easing.1.3.js"></script>
<script>
	$(window).load(function() {
    resize(".offer .imageBox img", 0.55);
  });
  $(document).ready(function(){
    resize(".offer .imageBox img", 0.55);
  });
  $(window).resize(function(){
    resize(".offer .imageBox img", 0.55);
  });
	
	$(document).ready(function(e) {
		var offset = $(".offers-menu-frame").offset();
		if (offset) {
		var offsetTop = offset.top;
		
    $(document).scroll(function() {
			if($(window).scrollTop() >= offsetTop-35)
				$(".offers-menu-frame").addClass('fixed');
			else
				$(".offers-menu-frame").removeClass('fixed');
		});
		}
  });
	$(document).ready(function(e) {
		//ob kliku na podmeni na strani z vsemi potovanji
		$(".offers-menu a[href^='#']").click(function(e) {
			e.preventDefault();
			
			var id = $(this).attr('href');
			//če je pomeni fixen, je stran za 35px krajša
			if($(".offers-menu-frame").hasClass('fixed')) {
				$('html, body').animate({
						 scrollTop: $(id).offset().top-35-55-33-20+55
				}, 800, 'easeInOutExpo');
			}
			else {
				$('html, body').animate({
						 scrollTop: $(id).offset().top-35-55-33-20
				}, 800, 'easeInOutExpo');
			}
			
			return false;
		});
  });
</script>
<script>
	$(document).ready(function(e) {
		$("select[name^='packets']").change(function(e) {
			empty = true;
			sum = 0;
			table = "<table>";
			
			$(".package select").each(function(index, element) {
				val = $(this).val();
				
				if(val > 0) {
					title = $(this).parent().parent().children('.package-title').children('h2').html();
					
					price = $(this).parent().parent().children('.package-price').children('nobr').html();
					price = price.substring(0, price.indexOf(' '));
					sum += parseFloat(price) * parseFloat(val);
					
					row = "<tr>";
					row += "<td>" + val + "x</td>";
					row += "<td>" + title + "</td>";
					row += "<td><nobr>" + price + " €</nobr></td>";
					row += "</tr>";
					
					table += row;
					
					empty = false;
				}
      });
			
			rowtotal = "<tr class='sum'>";
			rowtotal += "<td colspan='3'>";
			rowtotal += "Total sum: " + sum + " €";
			rowtotal += '<input class="button whiteText" type="submit" value="Confirm" disabled="disabled" />';
			rowtotal += "</td>";
			rowtotal += "</tr>";
			
			table += rowtotal;
			
			table += "</table>";
			
			if(empty) {
				$(".basket-in").hide();
				$(".basket-empty").show();
				$(".basket-in input").attr('disabled', 'disabled');
			}
			else {
				$(".basket-empty").hide();
				
				$(".basket-in table").remove();
				$(".basket-in").append(table);
				
				$(".basket-in").show();
				
				$(".basket-in input").removeAttr('disabled').click(function() {
					$("#offersOneForm").submit();
				});
			}
		});
		
		//ob kliku na podmeni na strani z vsemi potovanji
		$("a[href^='#']").click(function(e) {
			e.preventDefault();
			
			var id = $(this).attr('href');
			$('html, body').animate({
					 scrollTop: $(id).offset().top-35-55-33-20+55
			}, 800, 'easeInOutExpo');
			
			return false;
		});

	});
</script>
<script>
	$(document).ready(function(e) {
		offsetTop = $(".offer .top").offset().top;
		console.log(offsetTop);
		
		maxim = $(".foot").offset().top;
		
		$(window).scroll(function() {
			offsetTop = $(".offer .top").offset().top;
			
			if($(window).width() >= 768) {
				windowTop = $(document).scrollTop();
				
				/*console.log("footer: " + $(".foot").offset().top);
				console.log("window: " + $(".span4 > .floating-info").offset().top);
				console.log("height: " + $(".span4 > .floating-info").outerHeight());
				sum = $(".span4 > .floating-info").offset().top + $(".span4 > .floating-info").outerHeight();
				
				diff = maxim - sum;
				
				console.log(diff);
				
				if(diff < 0)
					$(".floating-info").css("margin-top", diff);
				else
					$(".floating-info").css("margin-top", 0);*/
				
				$(".floating-info").width($(".floating-info").parent('.span4').width())
				
				if(windowTop >= offsetTop)
					$(".floating-info").css('position', 'fixed').css('top', 10);
				else
					$(".floating-info").css('position', 'static');
			}
			else
				$(".floating-info").css('position', 'static');
		});
		$(window).resize(function() {
			offsetTop = $(".offer .intro").offset().top - $(".main-menu ul").height() - 81;
			
			$(".floating-info").width($(".floating-info").parent('.span4').width())
		});
  });
</script>
<script>
	$(document).ready(function(e) {
		$(".incAdd ul li").mouseenter(function() {
      $('.cloud').remove();
			
			if($(this).attr('data-descr')) {
				var cloud = $('<div class="cloud listed"></div>') ;
				var square = $('<div class="square">' + $(this).attr('data-descr') + '</div>').appendTo(cloud);
				var triangle = $('<div class="triangle"></div>').appendTo(cloud);
				
				$(this).children('div').append(cloud);
				
				$(this).mouseleave(function() {
					$('.cloud').remove();
				});
				$(".incAdd").hover(function(e) {
						$('.cloud').remove();
        });
				$(".cloud").click(function() {
						$('.cloud').remove();
				});
			}
		});
	});
</script>
<div class="offer big">
	<div class="top" id="top">
		<h1>##TITLE##</h1>
		<h2>##SUBTITLE##</h2>
  </div>
  
  <img src="##IMAGE##" />
  
  ##BASICINFOS##
  
 	##SECTIONS START##
  <div class="packages two" id="potovanja">
    <div class="grayBg whiteA">
      <h2>{{ __('title_offers_and_trips') }}</h2>
      ##IF PACKAGES START##
      <p class="intro">{{ __('packages_guide') }}</p>
      <p class="intro">{{ __('packages_warning') }}</p>
      ##IF PACKAGES END##
      ##IF NOT PACKAGES START##
      <p class="intro">{{ __('packages_na_warning') }}</p>
      <p class="intro">{{ __('packages_na_guide') }}</p>
      ##IF NOT PACKAGES END##
    </div>
    
    
    ##SECTION START##
    <div class="grayBg whiteA marrb" ID="offerType_##SECTION TYPE##">
      <form method="POST" action="##ACTION##" id="offersOneForm">
        <input type="hidden" name="offer_id" value="##OFFER ID##" />
        <div class="secti">
          <input class="button whiteText" type="submit" value="{{ __('btn_to_orderform') }}" disabled="disabled" />
          <table class="package">
            <thead>
              <tr>
                <th>
                </th>
                <th class="hidden-phone">
                  {{ __('title_packet_includes') }}
                </th>
                <th>
                  {{ __('price') }}
                </th>
                <th>
                  {{ __('number') }}
                </th>
              </tr>
            </thead>
            ##PACKAGE START##
              <tr id="offerType_##OFFER_TYPE####PACKET.ID##">
                <td class="package-title">
                  <h2>##PACKET.TITLE##</h2>
                </td>
                <td class="package-includes hidden-phone incAdd">
                  <ul class="column">
                    ##INCLUDES START##
                    <li data-descr="##DESCRIPTION##"><div class="boxd">##TITLE####QUESTIONMARK##</div></li>
                    ##INCLUDES END##
                  </ul>
                </td>
                <td class="package-price">
                  <nobr>##PACKET PRICE##<span>/person</span></nobr>
                  <!--p class="portions">##MAX PORTIONS##</p-->
                </td>
                <td class="package-number number">
                  ##PERSONS LIST##
                </td>
              </tr>
            ##PACKAGE END##
          </table>
        </div><input class="button whiteText" type="submit" value="{{ __('btn_to_orderform') }}" disabled="disabled" />
      </form>
      <div class="special_package">
    		{{ __('special_package') }}
      </div>
    </div>
    ##SECTION END##
  </div><!-- /.packages -->
	##SECTIONS END##
  
	<div class="intro grayBg whiteA">
		##SHORT_CONTENT##
	</div>
  

	##EVENT GALLERY##
	<!--##UNTIL PARTY END##-->

	##LINEUP BLOCK START##
	<div class="lineup grayBg whiteA" id="line-up">
		<h2>LINE UP</h2>
		<div>##LINEUP##</div>
	</div><!-- /.lineup -->
	##LINEUP BLOCK END##
	
  <div class="moreinfohere"></div>
  
	##MORE INFO START##
	<div class="moreinfo grayBg whiteA" id="info">
		<h2>{{ __('heading_details') }}</h2>
		##CONTENT##
	</div>
	##MORE INFO END##
	  
  ##MODULE ENEWS##
  <div class="mf facebook">
  	<h2>{{ __('heading_chat') }}</h2>
  	<div class="fb-comments" data-href="##FACEBOOK URL##" data-width="420" data-num-posts="10" data-colorscheme="light" ></div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$("select[name^='packets']").change(function(){
			// disable all
			$(".packages form").find("input.button").prop("disabled", true);

			// enable selected button
			var active = null;
			$(".packages form").each(function(){
				form = $(this);
				$(this).find("select").each(function(){
					if ($(this).val() > 0) {
						active = form;
						form.find("input.button").prop("disabled", false);
					}
				});
			});

			if (active == null) { // enable all selects
				$("select[name^='packets']").prop("disabled", false);
			} else {
				$("select[name^='packets']").prop("disabled", true);
				$(active).find("select[name^='packets']").prop("disabled", false);
			}
		});
		$("select[name^='packets']").change();
	});
</script></div>