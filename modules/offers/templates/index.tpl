<script>
  $(window).load(function() {
    resize(".offer .cont img", 0.81);
  });
  $(document).ready(function(){
    resize(".offer .cont img", 0.81);
  });
  $(window).resize(function(){
    resize(".offer .cont img", 0.81);
  });
</script>
<div class="module-head title-offers">
  <div class="left">
    <a class="title" href="/potovanja" title="poglej vsa potovanja">
      <h2>{{ __('title_offers_and_trips') }}</h2>
      <img src="/img/icons/rock-on.png">
    </a>
  </div>
</div>
<div class="row-fluid home-offers">
  ##PARTY START##
  <div class="offer index span4">
    <div class="cont">
      <a class="image" href="##URL##"><img src="/cache/img/w/570##IMAGE##" /></a>
      ##PICKUP LINE START##
      <div class="pickup-line">
        <p>##PICKUP##</p>
      </div>
      ##PICKUP LINE END##
      ##PRICE TAG START##
      <div class="price-tag">
        <div class="background"></div>
        <p class="from">{{ __('price_from') }}</p>
        <p class="price">##PRICE## €</p>
      </div>
      ##PRICE TAG END##
      <div class="main-info">
        <div class="background"></div>
        <a href="##URL##"><h3>##TITLE##</h3></a>
        <a href="##URL##"><p class="subtitle">##SUBTITLE##</p></a>
        <p class="info">##PLACE AND DATE##</p>
        <a class="button bold" href="##URL##">{{ __('btn_go') }}</a>
      </div>
    </div>
  </div><!-- /offer.middle -->
  ##PARTY END##
</div>

<div class="module-head title-offers">
  <div class="center-button">
    <a class="button whiteText big oxygen" href="/potovanja" title="{{ __('title_check_all_offers') }}">{{ __('btn_all_offers') }}</a>
  </div>
</div>