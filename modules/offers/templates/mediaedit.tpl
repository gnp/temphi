<legend>Pictures</legend>

<div class="control-group">
	<label class="control-label">Add media</label>
	<div class="controls">
		<input type="button" class="btn btn-primary elFinderData" value="Open file browser" data-id="##OFFER ID##" data-multi="1" data-files="" data-replace="newImageFieldOffers" data-url="/maestro/offers_pictures/insertelfinder" />
	</div>
</div>

<div id="mediaEdit" class="sortables">
	<ul class="sortable">
	##MEDIA START##
		<li data-id="##ID##">
		    <div style="position: relative;">
		    	<span data-url="##DELETE URL##" class="btn btn-mini btn-danger pull-left btnDeleteGalleryPicture" value="Delete" style="position: absolute; margin-top: 5px; margin-left: 5px;"><i class="icon-remove"></i></span>
		    	<img src="/cache/img/w/130##SRC##" class="mediaEditImg" />
		    </div>
		</li>
	##MEDIA END##
	</ul>
	<div id="newImageFieldOffers"></div>
</div>

<script type="text/javascript">
function onSortUpdate(data) {
	$.post("/maestro/offers/updatemediapositions", {positions: data});
}
</script>