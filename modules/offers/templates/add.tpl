<div class="control-group">
	<label class="control-label" for="title">Title</label>
	<div class="controls">
    	<input type="text" id="title" name="offer[title]" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="subtitle">Subtitle</label>
	<div class="controls">
    	<input type="text" id="subtitle" name="offer[subtitle]" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="category_id">Category</label>
	<div class="controls">
    	##CATEGORIES LIST##
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="picture">Picture</label>
	<div class="controls">
    	<input type="hidden" id="picture" name="offer[picture]" />
		<input type="button" class="btn btn-primary elFinderData" value="Open file browser" data-id="" data-files="" data-multi="-1" data-replace="newImageField" data-field="offer[picture]" data-url="" />
		<img src="" style="display: none; margin-top: 10px;" id="newImageField" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="city_id">City</label>
	<div class="controls">
    	##CITIES LIST##
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="dt_leave">Date start</label>
	<div class="controls">
    	<input type="text" id="dt_leave" name="offer[dt_start]" class="jdt span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="dt_return">Date end</label>
	<div class="controls">
    	<input type="text" id="dt_return" name="offer[dt_end]" class="jdt span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="dt_published">Date published</label>
	<div class="controls">
    	<input type="text" id="dt_published" name="offer[dt_published]" class="jdt span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="dt_opened">Date opened</label>
	<div class="controls">
    	<input type="text" id="dt_opened" name="offer[dt_opened]" class="jdt span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="dt_closed">Date closed</label>
	<div class="controls">
    	<input type="text" id="dt_closed" name="offer[dt_closed]" class="jdt span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="short_content">Short content</label>
	<div class="controls">
    	<textarea id="short_content" name="offer[short_content]" class="mceEditor"></textarea>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="content">Content</label>
	<div class="controls">
    	<textarea id="content" name="offer[content]" class="mceEditor"></textarea>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="lineup">Lineup</label>
	<div class="controls">
    	<textarea id="content" name="offer[lineup]" class="mceEditor"></textarea>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="pickup_line">Pickup line</label>
	<div class="controls">
    	<input type="text" id="pickup_line" name="offer[pickup_line]" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="soundcloud">SoundCloud</label>
	<div class="controls">
		<div class="input-prepend">
			<span class="add-on">http://api.soundcloud.com/tracks/</span>
    		<input type="number" id="soundcloud" name="offer[soundcloud]" />
		</div>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="soundcloud">FB event URL</label>
	<div class="controls">
    	<input type="text" id="fb_event_url" name="offer[fb_event_url]" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="firstpage">Firstpage</label>
	<div class="controls">
    	<input type="hidden" id="firstpageCB" name="offer[firstpage]" value="-1" />
    	<input type="checkbox" id="firstpage" name="offer[firstpage]" value="1" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="top">Top</label>
	<div class="controls">
    	<input type="hidden" id="topCB" name="offer[top]" value="-1" />
    	<input type="checkbox" id="top" name="offer[top]" value="1" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="order_limit">Order limit</label>
	<div class="controls">
    	<input type="number" min="-1" step="1" id="order_limit" name="offer[order_limit]" value="-1" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="order_limit">Count order limit</label>
	<div class="controls">
    	<input type="number" min="-1" step="1" id="order_limit_count" name="offer[order_limit_count]" value="##ORDER_LIMIT_COUNT##" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="total_limit">Total limit</label>
	<div class="controls">
    	<input type="number" min="-1" step="1" id="total_limit" name="offer[total_limit]" value="-1" class="span6" />
	</div>
</div>