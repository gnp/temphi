<!-- SHOULD WE DELETE THIS FILE --><script src="/js/jquery.easing.1.3.js"></script>
<script>
    $(window).load(function() {
    resize(".offer .imageBox img", 0.55);
  });
  $(document).ready(function(){
    resize(".offer .imageBox img", 0.55);
  });
  $(window).resize(function(){
    resize(".offer .imageBox img", 0.55);
  });
	
	$(window).load(function(e) {
		var offset = $(".offers-menu-frame").offset();
		var offsetTop = offset.top;
		
    $(document).scroll(function() {
			if($(window).scrollTop() >= offsetTop-35)
				$(".offers-menu-frame").addClass('fixed');
			else
				$(".offers-menu-frame").removeClass('fixed');
		});
  });
	$(document).ready(function(e) {
		//ob kliku na podmeni na strani z vsemi potovanji
		$(".offers-menu a[href^='#']").click(function(e) {
			e.preventDefault();
			
			var id = $(this).attr('href');
			//če je pomeni fixen, je stran za 35px krajša
			if($(".offers-menu-frame").hasClass('fixed')) {
				$('html, body').animate({
						 scrollTop: $(id).offset().top-35-55-33-20+55
				}, 800, 'easeInOutExpo');
			}
			else {
				$('html, body').animate({
						 scrollTop: $(id).offset().top-35-55-33-20
				}, 800, 'easeInOutExpo');
			}
			
			return false;
		});
  });
</script>
<!--div class="offers-menu-frame">
	<div class="container">
    <div class="offers-menu">
      <ul>
      	##LIST ITEM START##
        <li>##SEP##<a href="###FRIENDLY##">##TITLE##</a></li>
        ##LIST ITEM END##
      </ul>
    </div>
  </div>
</div-->

##CATEGORY START##
<div class="span##SPAN## ##FIRST## category" id="##FRIENDLY TITLE##">
  <h2>##CATEGORYTITLE##</h2>
    ##OFFER START##
    <div class="span##PARTY SPAN## ##FIRST OFFER##">
      <div class="offer index">
        <div class="cont">
        	<div class="imageBox">
          	<div>
          		<a class="image" href="##URL##"><img src="/cache/img/w/570##IMAGE##" /></a>
          	</div>
          </div>
          ##PICKUP LINE START##
          <div class="pickup-line">
            <p>##PICKUP##</p>
          </div>
          ##PICKUP LINE END##
          ##PRICE TAG START##
          <div class="price-tag">
            <div class="background"></div>
            <p class="from">{{ __('price_from') }}</p>
            <p class="price">##PRICE## €</p>
          </div>
          ##PRICE TAG END##
          <div class="main-info">
            <div class="background"></div>
            <a href="##URL##"><h3>##TITLE##</h3></a>
            <a href="##URL##"><p class="subtitle">##SUBTITLE##</p></a>
            <p class="info">##PLACE AND DATE##</p>
            <a class="button" href="##URL##">Več info</a>
          </div>
        </div>
        <div class="fb">
					<div class="fb-like" data-href="https://www.gremonaparty.com##URL##" data-send="false" data-width="150" data-show-faces="true" data-font="arial"></div>
        </div>
      </div><!-- /offer.middle -->
    </div>
    ##OFFER END##
  <div class="clear"></div>
</div>
##CATEGORY END##

##OTHER CATEGORIES START##
<div class="category others span12 first" id="ostalo">
  <h2 id="upcoming">KJE BOMO ŠE ŽURALI?</h2>
	<div class="text span6">
  	<p>Organiziramo tudi potovanja na partyje in festivale, ki trenutno niso objavljeni.</p>
    <p>Naroči se na novice, da boš prvi izvedel o novih aranžmajih v naši ponudbi!</p>
    <div class="list">
    	<p>Gremo tudi na:</p>
      <ul>
      	##OTHER CATEGORY START##<li>##TITLE##</li>##OTHER CATEGORY END##
      </ul>
    </div>
    <p>Na seznamu ni tega, kar te zanima? Klikni <a href="/private-party-pack/3/stran">tu</a> in nam povej svoje želje!</p>
  </div>
	<div class="arrow span2">
  	<img alt=">" src="/img/arrow-right-huge.png" />
  </div>
  <div class="span4">
  	##MODULE ENEWS##
  </div>
</div>
##OTHER CATEGORIES END##