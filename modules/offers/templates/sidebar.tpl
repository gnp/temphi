<!--<div id="floatingCart">
  <h2>Naročilnica</h2>
  <div class="packages-list main grayBg blackBg">
    <div class="package-list hiddenTemplate" onclick="scrollTo('.packages');">
      <p class="tag"></p>
      <p class="price"><nobr></nobr></p>
      <div class="clear"></div>
    </div>
    <input class="button whiteText medium" type="button" value="Na naročilnico" style="width: 100%; margin-top: 15px;" />
  </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
  $(window).scroll(function(){
    if ($(document).scrollTop() > parseInt($(".price-box").first().offset() ? $(".price-box").first().offset().top : 0) + parseInt($(".price-box").first().height()) + parseInt($(".packages-list").first().height()) + 50) {
      $("#floatingCart").addClass("floating");
    } else {
      $("#floatingCart").removeClass("floating");
    }
  });

  $("select[name^='packets']").change(function(){
    $("select[name^='packets']").each(function(){
      if ($(this).find("option:selected").val() > 0) {
        renderFloatingCart();
        return false;
      }
    });
  });

  $(document).delegate("#floatingCart .button", "click", function(){
    $("#offersOneForm .button[type='submit']").click();
  });

  function renderFloatingCart() {
    var total = 0.0;
    var newCart = $("#floatingCart").clone();
    newCart.find(".package-list").each(function(){
      if (!$(this).hasClass("hiddenTemplate"))
        $(this).remove();
    });

    $(".package").each(function(){
      var title = $(this).find("div.title h2").html();
      var price = parseFloat($(this).find("div.price nobr").html());
      var count = parseInt($(this).find("div.number select[name^='packets'] option:selected").val());
      var sumPrice = price * count;

      if (sumPrice) {
        var template = $(newCart).find(".hiddenTemplate").clone().removeClass("hiddenTemplate");
        $(template).find(".price").html(sumPrice + " €");
        $(template).find(".tag").html(count + "x " + title);
        $(newCart).find(".packages-list .button").before(template);
        total += sumPrice;
      }
    });

    if (total > 0) {
      var template = $(newCart).find(".hiddenTemplate").clone().removeClass("hiddenTemplate");
      $(template).find(".price").html(total + " €");
      $(template).find(".tag").html("Skupaj: ");
      $(newCart).find(".packages-list .button").before(template);

      $("#floatingCart").replaceWith(newCart);
      $("#floatingCart").show();
    } else {
      $("#floatingCart").hide();
    }
  }
});
</script>-->