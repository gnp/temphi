<?php

class AbbreviationsController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function maestro() {
		$q = $this->Abbreviations->get("all");
		
		$return["title"] = "Short URLs";
		$return["th"] = array("ID", "URL", "Short", "Clicks");
		$return["btn header"][] = "add";
		while ($r = $this->db->f($q)) {
			$return["row"][] = array(
				"td" => array(
					$r['id'],
					$r['url'],
					"http://gnp.si/" . $r['short'],
					$r['clicks'],
				),
				"btn" => array()
			);
		}
		
		return $this->Maestro->makeListing($return);
	}
	
	function add() {
		$tpl = new Template("add", "abbreviations");

		return $this->Maestro->makeAdd(array(
			"content" => $tpl->parse(array(
				"short" => $this->Abbreviations->getNewUnique(),
			)),
			"title" => "Short URL",
		));
	}
	
	function insert() {
		$id = $this->Abbreviations->insert($_POST['abbreviation']);
		
		$this->Maestro->makeInsert(array("id" => $id));
	}

	function edit() {
		$tpl = new Template("edit", "Abbreviations");

		$r = $this->Abbreviations->get("first", NULL, array("id" => Router::get("id")));
		
		return $this->Maestro->makeEdit(array(
			"content" => $tpl->parse($r),
			"title" => "Short URL",
		));
	}

	function update() {
		$this->Abbreviations->update($_POST['abbreviation'], $_POST['abbreviation']['id']);

		return $this->Maestro->makeUpdate(array(
			"id" => $_POST['abbreviation']['id']
		));
	}

	function delete() {
		$this->Abbreviations->delete(Router::get("id"));
	}

	function redirect() {
		$r = $this->Abbreviations->get("first", NULL, array("short" => Router::get("short")));
		if (empty($r)) {
			Status::rewrite(URL);
		}
		else {
			$this->Abbreviations->update(array(
				"clicks" => $r['clicks']+1,
			), $r['id']);
			Status::rewrite($r['url']);
		}
	}
}

class AbbreviationsModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "url";

		$this->fields = array(
			"url" => array(
				"not null" => true,
			),
			"short" => array(
				"unique" => true,
			),
			"clicks" => array(
				"default" => 0,
			),
		);
	}

	function getNewUnique() {
		$length = 4; // 
		$count = 1;
		$short = NULL;

		do {
			$count++;
			if ($count%10 == 0) {
				$count = 1;
				$length++;
			}
			$short = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, $length);
		} while (is_array($this->get("first", NULL, array("short" => $short))));

		return $short;
	}
}

?>