<input type="hidden" id="id" name="abbreviation[id]" value="##ID##" />

<div class="control-group">
	<label class="control-label" for="url">URL</label>
	<div class="controls">
    	<input type="text" id="url" name="abbreviation[url]" class="span6" value="##URL##" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="short">Short</label>
	<div class="controls">
    	<input type="text" id="short" name="abbreviation[short]" class="span6" value="##SHORT##" />
	</div>
</div>