<?php

class TagsController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function index() {
	}
	
	function one() {
	}
	
	function maestro() {
		$q = $this->Tags->get("all");
		
		$return["title"] = "Tags";
		$return["th"] = array("ID", "title");
		$return["btn header"][] = "add";
		while ($r = $this->db->f($q)) {
			$return["row"][] = array(
				"td" => array(
					$r['id'],
					$r['title'],
				),
				"btn" => array("update", "delete")
			);
		}
		
		return $this->Maestro->makeListing($return);
	}
	
	function add() {
		$tpl = new Template("add", "Tags");

		return $this->Maestro->makeAdd(array(
			"content" => $tpl->display(),
			"title" => "tag",
		));
	}
	
	function insert() {
		$id = $this->Tags->insert($_POST['tag']);
		
		$this->Maestro->makeInsert(array("id" => $id));
	}

	function edit($data = array()) {
		$tpl = new Template("edit", "Tags");
		
		$sql = "SELECT t.title " .
			"FROM tags t " .
			"INNER JOIN " . $data['multiple'] . "_tags o ON o.tag_id = t.id " .
			"WHERE o." . $data['single'] . "_id = " . $data['id'];
		$this->db->q($sql);
		$arrTags = array();
		while ($r = $this->db->f())
			$arrTags[] = $r['title'];
		
		$this->Tags->parseTags();
		
		return $tpl->parse(array(
			"tags" => implode(",", $arrTags),
		));
	}

	function update($data = array()) {
		if (!isset($_POST['tags']))
			return;
		
		Core::loadModule(Core::toCamel($data['multiple'] . "_tags"));
		
		$tags = explode(",", $_POST['tags']);
		$IDs = array();
		foreach ($tags AS $tag) {
			if (empty($tag))
				continue;
				
			$sql = "SELECT id " .
					"FROM tags " .
					"WHERE title = '" . mysql_real_escape_string($tag) . "'";
			$q = $this->db->q($sql);
			$r = $this->db->f();
			$id = NULL;
			
			if (!empty($r)) {
				$id = $r['id'];
			} else
				$id = $this->Tags->insert(array(
					"title" => $tag,
				));

			$sql = "SELECT id " .
					"FROM " . $data['multiple'] . "_tags " .
					"WHERE tag_id = " . $id . " " .
					"AND " . $data['single'] . "_id = " . $data['id'];
			
			$q = $this->db->q($sql);
			$r = $this->db->f();

			if (empty($r)) {
				$this->{Core::toCamel($data['multiple'] . "_tags")}->insert(array(
					"tag_id" => $id,
					$data['single'] . "_id" => $data['id'],
				));
			}
			$IDs[] = $id;
		}

		if(!empty($IDs))
			$this->db->q("DELETE FROM " . $data['multiple'] . "_tags WHERE tag_id NOT IN (" . implode(",", $IDs) . ") AND " . $data['single'] . "_id = " . $data['id']);
		else
			$this->db->q("DELETE FROM " . $data['multiple'] . "_tags WHERE " . $data['single'] . "_id = " . $data['id']);
	}

	function delete() {
		$this->Tags->delete(Router::get("id"));
	}
}

class TagsModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "title";

		$this->fields = array(
			"title" => array(
				"not null" => TRUE,
			),

		);
	}
	
	function parseTags() {
		$q = $this->get("all");
		$arrTags = array();
		while ($r = $this->f($q))
			$arrTags[] = $r['title'];
		Core::setParseVar("sample tags", '"' . implode('","', $arrTags) . '"');
	}
}

?>