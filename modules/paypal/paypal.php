<?php

use PayPal\Core\PPLoggingManager;

class PaypalController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function getAccessToken() {
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "https://" . PAYPAL_ENDPOINT . "/v1/oauth2/token");
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_USERPWD, PAYPAL_CLIENT . ":" . PAYPAL_SECRET);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");

		$result = curl_exec($ch);

		curl_close($ch);

		if(empty($result)) {
			return FALSE;
		} else
		{
		    $json = json_decode($result);
		    return $json->access_token;
		}
	}

	//
	function startpayment() {
		$rOrder = $this->Orders->get("first", NULL, array("hash" => Router::get("hash")));
		
		if (empty($rOrder))
			Status::code(400, "Order is missing");
			
		if ($rOrder['dt_rejected'] > DT_NULL)
			Status::code(400, "Order was rejected");
		
		if ($rOrder['dt_canceled'] > DT_NULL)
			Status::code(400, "Order was canceled");
		
		if ($this->Orders->isPayedReservation($rOrder['id']))
			Status::code(400, "Reservation payment was successful");

		$price = makePrice(Validate::isEmpty($_GET, "full") ? RESERVATION : $this->Orders->getFullPrice($rOrder['id']), 2, "");
		$description = "Payment of Hard Island, ".$price.", reservation #" . $rOrder['id'];
		$ppPaymentHash = sha1(microtime() . Router::get("hash"));
		$accessToken = $this->getAccessToken();
		
		$arrData = array(
			"intent" => "sale",
			"redirect_urls" => array(
				"return_url" => substr(Router::make("paypal", array("view" => "confirmpayment", "hash" => $ppPaymentHash), TRUE),0),
				"cancel_url" => substr(Router::make("paypal", array("view" => "cancelpayment", "hash" => $ppPaymentHash), TRUE),0),
			),
			"payer" => array(
				"payment_method" => "paypal"
			),
			"transactions" => array(
				array(
					"amount" => array(
						"total" => $price,
						"currency" => "EUR"
					),
					"description" => $description
				)
			),
		);

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "https://" . PAYPAL_ENDPOINT . "/v1/payments/payment");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer " . $accessToken, "Content-Type: application/json"));
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, JSON::to($arrData));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 

		$result = curl_exec($ch);
		curl_close($ch);

		if (empty($result)) {
			return "Empty result" . ($result);
		} else {
		    $json = JSON::from($result);

		    if (isset($json->state) && $json->state == "created") {
		    	$this->Paypal->insert(array(
        			"order_id" => $rOrder['id'],
        			"user_id" => Auth::getUserID(),
        			"order_hash" => $rOrder['hash'],
        			"paypal_hash" => $ppPaymentHash,
        			"paypal_id" => $json->id,
        			"status" => "started",
        			"price" => $price,
    			));

    			Status::redirect($json->links[1]->href);
		    } else {
		    	return "SWW";
		    }
		}
	}

	function confirmpayment() {
		$rPaypal = $this->Paypal->get("first", NULL, array("paypal_hash" => Router::get("hash")));
		$rOrder = $this->Orders->get("first", null, array("id" => $rPaypal['order_id']));

		$accessToken = $this->getAccessToken();
		
		$arrData = array(
			"payer_id" => $_GET['PayerID'],
		);

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "https://" . PAYPAL_ENDPOINT . "/v1/payments/payment/" . $rPaypal["paypal_id"] . "/execute/");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer " . $accessToken, "Content-Type: application/json"));
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, JSON::to($arrData));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 

		$result = curl_exec($ch);
		curl_close($ch);

		if (empty($result)) {
		    Debug::addError("Can't finish the order. Please contact GoNParty team on info@gonparty.eu, we're sorry and will help you immediately.");
		} else {
		    $json = JSON::from($result);

		    if (isset($json->state)) { // unknown error
		    	$this->Paypal->update(array(
        			"status" => $json->state,
        			"paypal_payer_id" => $_GET['PayerID'],
    			), $rPaypal["id"]);

			    // reservation payment
			    if ($json->state == "approved") { // yeeey
				    if ($rPaypal['price'] == RESERVATION) {
					    $sqlBill = "SELECT ob.id, ob.price, ob.order_id, ob.notes " .
							"FROM orders_bills ob " .
							"INNER JOIN paypal p ON p.order_id = ob.order_id " .
							"WHERE ob.reservation = 1 " .
							"AND p.paypal_id = '" . mysql_real_escape_string($rPaypal['paypal_id']) . "' " .
							"AND ob.dt_confirmed = '" . DT_NULL . "'";
						$qBill = $this->db->q($sqlBill);
						$rBill = $this->db->f($qBill);
						
						if (!empty($rBill)) {
							// update bill
							$this->OrdersBills->update(array(
								"payed" => RESERVATION,
								"notes" => $rBill['notes'] . "Paypal",
								"dt_confirmed" => DT_NOW,
							), $rBill['id']);
							
							// update order
							$this->Orders->update(array(
								"dt_confirmed" => DT_NOW,
								"dt_rejected" => DT_NULL,
								"dt_canceled" => DT_NULL
							), $rOrder['id']);
							
							$rOffer = $this->Offers->get("first", null, array("id" => $rOrder['offer_id']));
							
							// update orders users
							$qOrdersUsers = $this->OrdersUsers->get("all", NULL, array("order_id" => $rOrder['id'], "dt_confirmed " => DT_NULL, "dt_rejected" => DT_NULL, "dt_canceled" => DT_NULL));
							while ($rOrdersUser = $this->OrdersUsers->f($qOrdersUsers)) {
								$this->OrdersUsers->update(array(
									"dt_confirmed" => DT_NOW,
									"dt_rejected" => DT_NULL,
									"dt_canceled" => DT_NULL
								), $rOrdersUser['id']);
								
								if ($rOrder['user_id'] == $rOrdersUser['user_id'])
									$this->Mails->automatic("order-confirmed-paypal", array("user" => $rOrdersUser['user_id'], "offer" => $rOffer));
								else
									$this->Mails->automatic("order-confirmed-friend", array("user" => $rOrdersUser['user_id'], "offer" => $rOffer));
							}
						}
					} else {
						// fullprice
						if ($rPaypal['price'] == $rOrder['price']) {
							$qOrdersBills = $this->OrdersBills->get("all", NULL, array("order_id" => $rOrder['id'], "type" => array(1, 2)));
							while ($rOrdersBill = $this->db->f($qOrdersBills)) {
								$this->OrdersBills->update(array(
									"dt_confirmed" => DT_NOW,
									"payed" => $rOrdersBill['price'],
									"notes" => $rOrdersBill['notes'] . "Paypal"
								), $rOrdersBill['id']);
							}

							// update order
							$this->Orders->update(array(
								"dt_confirmed" => DT_NOW,
								"dt_payed" => DT_NOW,
								"dt_rejected" => DT_NULL,
								"dt_canceled" => DT_NULL
							), $rOrder['id']);

							$rOffer = $this->Offers->get("first", null, array("id" => $rOrder['offer_id']));
							
							// update orders users
							$qOrdersUsers = $this->OrdersUsers->get("all", NULL, array("order_id" => $rOrder['id'], "dt_confirmed " => DT_NULL, "dt_rejected" => DT_NULL, "dt_canceled" => DT_NULL));
							while ($rOrdersUser = $this->OrdersUsers->f($qOrdersUsers)) {
								$this->OrdersUsers->update(array(
									"dt_confirmed" => DT_NOW,
									"dt_rejected" => DT_NULL,
									"dt_canceled" => DT_NULL
								), $rOrdersUser['id']);
								
								if ($rOrder['user_id'] == $rOrdersUser['user_id']) {
									$this->Mails->automatic("order-confirmed-paypal", array("user" => $rOrdersUser['user_id'], "offer" => $rOffer));
									$this->Mails->automatic("order-payed", array("user" => $rOrdersUser['user_id'], "offer" => $rOffer));
								} else
									$this->Mails->automatic("order-confirmed-friend", array("user" => $rOrdersUser['user_id'], "offer" => $rOffer));
							}
						}
					}
				}

		    	if ($json->state == "pending")
    				Debug::addWarning("Status naročila je <i>$json->state</i>. Ko bo naročilo potrjeno, vas bomo obvestili preko email naslova.");
		    	else if ($json->state == "approved") {
                    Status::redirect(Router::make("confirmform", array('provider' => 'paypal', 'hash' => $rOrder['hash'])));
                } else
		    		Debug::addError("Unknown status: $json->state. Please contact GoNParty team on info@gonparty.eu.");
		    } else {
				Debug::addError("Order confirmation failed. Please contact GoNParty team on info@gonparty.eu.");
		    }
		}

		Status::redirect(Router::make("profil"));
	}

	function cancelpayment() {
		Debug::addError("Order has been canceled. Please check your profile for more information.");
		return Status::redirect("/");
	}

	function ipn() {
		//$rPaypal = $this->Paypal->get("first", NULL, array("payer_id" => $_POST['']));
	}
}

class PaypalModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "paypal_hash";

		$this->fields = array(
			"order_id" => array(
				"int" => array(),
				"not null" => TRUE,
			),
			"user_id" => array(
				"int" => array(),
				"not null" => TRUE,
			),
			"order_hash" => array(
				"not null" => TRUE,
			),
			"price" => array(
				"not null" => TRUE,
			),
			"paypal_hash" => array(
				"not null" => TRUE,
			),
			"paypal_id" => array(
				"not null" => TRUE,
			),
			"paypal_payer_id" => array(
			),
			"status" => array(
				"not null" => TRUE,
			),
			"dt_started" => array(
				"default" => DT_NOW,
			),
			"dt_confirmed" => array(
				"default" => DT_NULL,
			),
		);
	}
}

?>