<?php

class MonetaController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function narocilo() {
		include APP_PATH . "other" . DS . "moneta" . DS . "setup.php";
		include APP_PATH . "other" . DS . "moneta" . DS . "moneta.php";
		include APP_PATH . "other" . DS . "moneta" . DS . "functions.php";  
  
		Functions_ResponseExpires();
  
		//$sMyName = 'http://' . Functions_GetServerVariable('HTTP_HOST') . Functions_GetServerVariable('SCRIPT_NAME');
		$sMyName = "https://gremonaparty.com";
  
		$sConfirmationID = "";
 		$sStatus         = "";
  		$sProviderData   = "";
  
  		$sStatus      = "";
  		$sData        = "";
  
  		// Branje parametra ConfirmationID
  		$sConfirmationID = Functions_RequestString("ConfirmationID", 32);
  		//$sConfirmationID = "09082013100741";
  
  		// kreiranje CMoneta objekta
  		$myMoneta = new CMoneta(MYSQL_SERVER, MYSQL_DATABASE, MYSQL_TABLENAME, MYSQL_USERNAME, MYSQL_PASSWORD);
  
  		// Iskanje ConfirmationID nakupa
  		if (!$myMoneta->FindConfirmationID($sConfirmationID)) {
    		$sStatus = "ConfirmationID ne obstaja.";
		} else {
			$nRefreshCounter = $myMoneta->Get_RefreshCounter();
			$sPurchaseStatus = $myMoneta->Get_PurchaseStatus();
			$sProviderData = $myMoneta->Get_ProviderData();
	
			if ($nRefreshCounter > 60) {
				$sStatus = "Potrditev ni uspela.";
			} else if ($sPurchaseStatus == "vobdelavi") {
				$sStatus = "čakam na potrditev...";
	    	} else if ($sPurchaseStatus == "zavrnjeno") {
				$sStatus = "Potrditvena stran je bila klicana s TARIFFICATIONERROR=1.";
			} else if ($sPurchaseStatus == "potrjeno") {
				$sStatus = "Potrjevanje uspešno.";
				$sData = "<h1>Zahvaljujemo se vam za nakup.</h1>";
	
				$myMoneta->SetPurchaseStatus("prikazano", $sConfirmationID);
			} else {
				$sStatus = "Napaka.";
			}
	
			// povečaj števec osvežitev
			$myMoneta->AddRefreshCounter($sConfirmationID);

            if ($sPurchaseStatus == "potrjeno") {
                Status::redirect(Router::make("confirmform", array('provider' => 'moneta', 'hash' => $rOrder['hash'])));
            }
		}

		// zapremo povezavo na podatkovno bazo
		$myMoneta->Close();

		$return = ($sStatus == "čakam na potrditev..." ? ('<meta http-equiv="refresh" content="3; url='.$sMyName.$_SERVER["REQUEST_URI"] . '" />') : NULL) . $sProviderData . 
		    '<br /><b>Status nakupa:</b> '.$sStatus.'<br />'.$sData.'<br /><br /><br /><a href="index.php">Nazaj</a>';

		error_log("=== Moneta: " . nl2br($return));
	
		// HTML vsebina plačljive strani
		return $return;
	}
	
	/* implemented but not tested */
	function potrditev() {
		include APP_PATH . "other" . DS . "moneta" . DS . "setup.php";
		include APP_PATH . "other" . DS . "moneta" . DS . "moneta.php";
		include APP_PATH . "other" . DS . "moneta" . DS . "functions.php";  
	
		Functions_ResponseExpires();
	  
		// branje vhodnih parametrov
		$sConfirmationID        = Functions_RequestString("ConfirmationID", 32);
		$sConfirmationSignature = Functions_RequestString("ConfirmationSignature", 250);
		$nTarifficationError    = Functions_RequestNumber("TARIFFICATIONERROR", 0, 1, 1);
		$sConfirmationIDStatus  = Functions_RequestString("ConfirmationIDStatus", 32);
		$sIP                    = Functions_GetServerVariable('REMOTE_ADDR');
		$sOutput                = "<error>1</error>";
	
		// preverjanje IP Monete
		if (($sIP == "213.229.249.103") || ($sIP == "213.229.249.104") || ($sIP == "213.229.249.117")) {  
	    	// kreiranje CMoneta objekta
	    	$myMoneta = new CMoneta(MYSQL_SERVER, MYSQL_DATABASE, MYSQL_TABLENAME, MYSQL_USERNAME, MYSQL_PASSWORD);
	  
	    	// zahtevek za status nakupa?
	    	if($sConfirmationIDStatus != "") {
				if($myMoneta->FindConfirmationID($sConfirmationIDStatus)) {
					$sOutput = "<status>" . $myMoneta->Get_PurchaseStatus() . "</status>";
				}
			} else {
				// Iskanje ConfirmationID nakupa in potrjevanje nakupa
				if($myMoneta->FindConfirmationID($sConfirmationID)) {
					$sPurchaseStatus = $myMoneta->Get_PurchaseStatus();
		
					if($sPurchaseStatus == "vobdelavi") {
						if($nTarifficationError == 0) {
							$myMoneta->ConfirmPurchase("potrjeno", $sConfirmationID, $sConfirmationSignature, $nTarifficationError);
							$sOutput = "<error>0</error>";
							
							$sqlBill = "SELECT ob.id, ob.price, ob.order_id " .
								"FROM orders_bills ob " .
								"INNER JOIN moneta m ON m.order_id = ob.order_id " .
								"WHERE ob.reservation = 1 " .
								"AND m.confirmationid = '" . mysql_real_escape_string($sConfirmationID) . "' " .
								"AND ob.dt_confirmed = '" . DT_NULL . "'";
							$qBill = $this->db->q($sqlBill);
							$rBill = $this->db->f($qBill);
							
							if (!empty($rBill)) {
								// update bill
								$this->OrdersBills->update(array(
									"payed" => RESERVATION,
									"notes" => "Moneta",
									"dt_confirmed" => DT_NOW,
								), $rBill['id']);
								
								$rOrder = $this->Orders->get("first", null, array("id" => $rBill['order_id']));
								
								// update order
								$this->Orders->update(array(
									"dt_confirmed" => DT_NOW,
									"dt_rejected" => DT_NULL,
									"dt_canceled" => DT_NULL
								), $rOrder['id']);

								$rOffer = $this->Offers->get("first", null, array("id" => $rOrder['offer_id']));
								
								// update orders users
								$qOrdersUsers = $this->OrdersUsers->get("all", NULL, array("order_id" => $rOrder['id'], "dt_confirmed " => DT_NULL, "dt_rejected" => DT_NULL, "dt_canceled" => DT_NULL));
								$arrSentMails = array();
								while ($rOrdersUser = $this->OrdersUsers->f($qOrdersUsers)) {
									$this->OrdersUsers->update(array(
										"dt_confirmed" => DT_NOW,
										"dt_rejected" => DT_NULL,
										"dt_canceled" => DT_NULL
									), $rOrdersUser['id']);
									
									if (!in_array($rOrdersUser['user_id'], $arrSentMails) || $rOrder['user_id'] == $rOrdersUser['user_id']) {
										$arrSentMails[] = $rOrdersUser['user_id'];
										if ($rOrder['user_id'] == $rOrdersUser['user_id'])
											$this->Mails->automatic("order-confirmed-moneta", array("user" => $rOrdersUser['user_id'], "offer" => $rOffer));
										else
											$this->Mails->automatic("order-confirmed-friend", array("user" => $rOrdersUser['user_id'], "offer" => $rOffer));
									}
								}

								Status::redirect(Router::make("confirmform", array('provider' => 'moneta', 'hash' => $rOrder['hash'])));
							}
						} else {
							$myMoneta->ConfirmPurchase("zavrnjeno", $sConfirmationID, $sConfirmationSignature, $nTarifficationError);
						}
					}
				}
			}
		
	    	// zapremo povezavo na podatkovno bazo
	    	$myMoneta->Close();
		}

		error_log("=== Moneta: " . nl2br($sOutput));

		// izpišemo <error> ali <status>
		return $sOutput;
	}
	
	function saveorder() {
		//Auth::isLoggedIn(TRUE);
		
		//$rOrder = $this->Orders->get("first", NULL, array("hash" => Router::get("hash"), "user_id" => Auth::getUserID()));
		$rOrder = $this->Orders->get("first", NULL, array("hash" => Router::get("hash")));
		
		if (empty($rOrder))
			Status::code(400, "Manjka naročilo");
			
		//if ($rOrder['dt_confirmed'] > DT_NULL)
		//	Status::code(404, "Naročilo je že bilo potrjeno");
		
		if ($rOrder['dt_rejected'] > DT_NULL)
			Status::code(400, "Naročilo je bilo zavrnjeno");
		
		if ($rOrder['dt_canceled'] > DT_NULL)
			Status::code(400, "Naročilo je bilo preklicano");
		
		if ($this->Orders->isPayedReservation($rOrder['id']))
			Status::code(400, "Plačilo rezervacije je bilo uspešno");
		
		//$rMoneta = $this->Moneta->get("first", NULL, array("user_id" => Auth::getUserID(), "order_id" => $rOrder['id'], "purchasestatus" => "vobdelavi"));
		//$rMoneta = $this->Moneta->get("first", NULL, array("order_id" => $rOrder['id'], "purchasestatus" => "vobdelavi"));
		//if (!empty($rMoneta))
		//	Status::code(400, "Plačilo rezervacije je v obdelavi");
		
		include APP_PATH . "other" . DS . "moneta" . DS . "setup.php";
		include APP_PATH . "other" . DS . "moneta" . DS . "moneta.php";
		include APP_PATH . "other" . DS . "moneta" . DS . "functions.php";
		include APP_PATH . "other" . DS . "moneta" . DS . "xmlfunctions.php";

		Functions_ResponseExpires();
		
		$nRezervacija = 1;
		$nSkupnaCena = RESERVATION;
		
		//$city = $this->Cities->get("first", NULL, array("id" => Auth::getUser("city_id")));
		
		$sIme = Auth::getUser("name");
		$sPriimek = Auth::getUser("surname");
		$sDavcna = NULL;
		$sEmail = Auth::getUser("email");
		$sUlica = Auth::getUser("address");
		$sHisnast = NULL;
		$sPosta = NULL;//@$city['code'];
		$sKraj = NULL;//@$city['title'];
		
		// kreiramo xml
		$sXMLData = MakeOrderHead($sDavcna, $sIme, $sPriimek, "", $sUlica, $sHisnast, $sPosta, $sKraj, "", "", $sEmail, $nSkupnaCena);
		
		// dodamo artikel
		$sXMLData = $sXMLData.MakeOrderLine("Rezervacija", RESERVATION, RESERVATION_TAX, 1, "kol", "rez");
		
		// generiramo zaključek naročila
		$sXMLData = $sXMLData.MakeOrderEnd();
		
		// kreiramo CMoneta objekt
		$myMoneta = new CMoneta(MYSQL_SERVER, MYSQL_DATABASE, MYSQL_TABLENAME, MYSQL_USERNAME, MYSQL_PASSWORD);
		
		// dodaj nakup v DB
		$sConfirmationID = $myMoneta->AddMonetaPurchase($sXMLData, array("user_id" => Auth::getUserID(), "order_id" => $rOrder['id']));
		
		// zapri DB
		$myMoneta->Close();
		
		// sestavimo url
		$sTarifficationE  = MONETAURL . "?TARIFFICATIONID=" . TARIFFICATIONID . "&ConfirmationID=" . $sConfirmationID;
		
		Status::redirect($sTarifficationE);
		return '<meta http-equiv="refresh" content="3; url=' . $sTarifficationE . '" /><h4>Poteka preusmeritev na Moneto ...</h4><a href="'.$sTarifficationE.'">Naročilo '.$sTarifficationE.'</a>';
	}
}

class MonetaModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "content";

		$this->fields = array(
			"confirmationid" => array(
				"text" => array(
					"max" => 64,
				),
				"not null" => TRUE,
			),
			"confirmationsignature" => array(
				"text" => array(
					"max" => 255,
				),
				"not null" => TRUE,
			),
			"tarifficationerror" => array(
				"int" => array(),
				"not null" => TRUE,
			),
			"startdate" => array(
				"datetime" => array(),
				"not null" => TRUE,
			),
			"confirmdate" => array(
				"datetime" => array(),
				"not null" => TRUE,
			),
			"refreshcounter" => array(
				"int" => array(),
				"not null" => TRUE,
			),
			"purchasestatus" => array(
				"text" => array(
					"max" => 32,
				),
				"not null" => TRUE,
			),
			"providerdata" => array(
				"text" => array(),
				"not null" => TRUE,
			),
			"order_id" => array(
				"int" => array(),
				"not null" => TRUE,
			),
			"user_id" => array(
				"int" => array(),
				"not null" => TRUE,
			),
		);
	}
}

?>