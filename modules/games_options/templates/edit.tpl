<div class="control-group">
	<label class="control-label">Answers <br class="cb" /><input type="button" value="Add" class="btn btn-small btn-info" id="btnNewGamesOption" /></label>
	<div class="controls">
		
		##OPTION START##
		<input type="hidden" value="##ID##" />
		<input type="hidden" value="-1" name="games_option[##ID##][correct]" />
    	<input type="text" name="games_option[##ID##][title]" value="##TITLE##" class="span6" style="margin-bottom: 5px;" />
		<input type="radio" value="##ID##" style="margin-top: -5px;" name="games_option_correct" ##CB CORRECT## />
    	<input type="button" value="Delete" style="margin-top: -6px; margin-left: 5px;" class="btn btn-small btn-warning btnDeleteGamesOption" /><br />
    	##OPTION END##
    	<div id="addNewGamesOption"></div>
	</div>
</div>