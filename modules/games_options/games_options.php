<?php

class GamesOptionsController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function add() {
		if (!Validate::isInt($_POST['game_id']))
			die();
			
		Core::loadLayout(NULL);
		$tpl = new Template("edit", "games_options");
		
		$id = $this->GamesOptions->insert(array(
			"game_id" => $_POST['game_id'],
			"title" => NULL,
		));
		
		return $tpl->parse(array(
			"id" => $id,
			"title" => NULL,
			"cb correct" => NULL,
		), "option");
	}

	function edit($data = array()) {
		if (!Validate::isInt($data['game_id']))
			return;
		
		$tpl = new Template("edit", "GamesOptions");
		
		$q = $this->GamesOptions->get("all", NULL, array("game_id" => $data['game_id']));
		while ($r = $this->db->f($q)) {
			$r['cb correct'] = $r['correct'] == 1 ? ' checked="checked"' : NULL;
			$tpl->parse($r, "option");
		}

		return $tpl->parse(array(
			"option" => $tpl->display("option"),
		));
	}

	function update() {
		if (isset($_POST['games_option']))
			foreach ($_POST['games_option'] AS $id => $option)
				if (!empty($option['title']))
					$this->GamesOptions->update(array(
						"title" => $option['title'],
						"correct" => $_POST['games_option_correct'] == $id ? 1 : 0,
					), $id);
				else
					$this->GamesOptions->delete($id);

		return;
	}

	function delete() {
		$this->GamesOptions->delete(Router::get("id"));
	}
}

class GamesOptionsModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "title";

		$this->fields = array(
			"game_id" => array(
				"not null" => true,
			),
			"title" => array(
			),
			"correct" => array(
				"default" => -1,
			),
		);
	}
}

?>