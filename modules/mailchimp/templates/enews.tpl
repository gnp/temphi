<script>
	$(document).ready(function(e) {
		//validates form for e-news signup
		$("#enews-form").validationEngine('attach',{scroll: false, onValidationComplete: function(form, status) {
			if(status) {
				var form = "#enews-form";
				
				$('.error').remove();
				$(form + " .sendy").val('{{ __('status_sending') }}').addClass('processing gray');
				
				$.post("/add-mailchimp", $(form).serialize(), function(data){
					data = fromJSON(data);
					
					if (data.success != false) {
						$("#form").slideUp();
						$("#success").slideDown();
					}
					else {
						$(form + ", #fail").slideDown();
						$(form + " .sendy").removeClass('processing gray').val("{{ __('btn_resend') }}");
            if (data.text.length == 0)
						  $(form + " .sendy").after('{{ __('notice_something_went_wrong') }}');
            else
              $(form + " .sendy").after('<p class="error">' + data.text + '</p>');
					}
				});
			}
		}});
	});
</script>
<div class="enews" id="enews-module">
  <div class="row-fluid">
    <div class="grayShadow span8">
      <div class="module-head">
        <div class="left">
          <h2>{{ __('heading_e-news') }}</h2>
          <!--img src="/img/icons/enews.png"-->
        </div>
        <div class="clear"></div>
      </div>
      <div class="content grayBg" id="form">
        <div class="text">
          {{ __('text_e-news_index') }}
        </div><div class="form">
          <form id="enews-form" name="enews-form">
            <div>
              <input class="validate[required]" id="name" name="name" placeholder="{{ __('placeholder_name') }}" type="text" />
            </div>
            <div>
              <input class="validate[required]" id="surname" name="surname" placeholder="{{ __('placeholder_surname') }}" type="text" />
            </div>
            <div>
              <input class="validate[required,custom[email]]" id="email" name="email" placeholder="{{ __('placeholder_email') }}" type="email" />
            </div>
            <div>
              <input class="sendy" id="submitButton" type="submit" value="{{ __('btn_send') }}" />
            </div>
          </form>
        </div>
      </div>
      <div class="content grayBg" id="success">
        {{ __('mailchimp_success_index') }}
      </div>
    </div>
    <div class="span4">
      <div class="module-head">
        <div class="left">
          <h2>{{ __('heading_connect') }}</h2>
        </div>
        <div class="clear"></div>
      </div>
      <div class="row-fluid" id="indexSocial">
        <div class="span4 hidden-phone"><a href="https://www.facebook.com/gremonaparty" target="_blank"><img src="/img/social-index/icon-fb.png"></a></div>
        <div class="span4 hidden-phone"><a href="http://www.youtube.com/user/Gremonapartycom" target="_blank"><img src="/img/social-index/icon-yt.png"></a></div>
        <div class="span4 hidden-phone"><a href="http://instagram.com/gremonaparty" target="_blank"><img src="/img/social-index/icon-insta.png"></a></div>
        <div class="span4 visible-phone"><a href="https://www.facebook.com/gremonaparty" target="_blank"><img src="/img/social-index/icon-fb.png"></a></div>
        <div class="span4 visible-phone"><a href="http://www.youtube.com/user/Gremonapartycom" target="_blank"><img src="/img/social-index/icon-yt.png"></a></div>
        <div class="span4 visible-phone"><a href="http://instagram.com/gremonaparty" target="_blank"><img src="/img/social-index/icon-insta.png"></a></div>
      </div>
    </div>
  </div>
</div><!-- /gallery-module -->