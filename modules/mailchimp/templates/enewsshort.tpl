<script>
	$(document).ready(function(e) {
		//validates form for e-news signup
		$("#enews-form").validationEngine('attach',{scroll: false, onValidationComplete: function(form, status) {
			if(status) {
				var form = "#enews-form";
				
				$('.error').remove();
				$(form + " .sendy").val('Pošiljam').addClass('processing gray');
				
				$.post("/add-mailchimp", $(form).serialize(), function(data){
					data = fromJSON(data);
					
					if (data.success == false) {
						$("#form").slideUp();
						$("#success").slideDown();
					}
					else {
						$(form + ", #fail").slideDown();
						$(form + " .sendy").removeClass('processing gray').val("Pošlji ponovno");
			            if (data.text.length == 0)
							$(form + " .sendy").after('<p class="error">Ups, neki je šlo narobe. Če se ponovi, kopiraj sporočilo in pošlji na party@gremonaparty.com</p>');
			            else
			            	$(form + " .sendy").after('<p class="error">' + data.text + '</p>');
					}
				});
			}
		}});
	});
</script>
<div class="enews mf short" id="enews-module">
  <div class="content grayBg">
  	<div id="form">
      <h2>Sign in for updates</h2>
      <div class="form">
    		<form id="enews-form" name="enews-form">
          <div>
            <input class="validate[required]" id="name" name="name" placeholder="Ime" type="text" />
          </div>
          <div>
            <input class="validate[required]" id="surname" name="surname" placeholder="Priimek" type="text" />
          </div>
          <div>
            <input class="validate[required,custom[email]]" id="email" name="email" placeholder="Email" type="email" />
          </div>
          <div>
            <input class="sendy" id="submitButton" type="submit" value="POŠLJI" />
          </div>
        </form>
      </div>
		</div>
    <div id="success">
    	<h2>Awesome, from know on you will always know where we party!</h2>
      <p>We will update you regularely where we go and which parties we will visit.</p>
    </div>
  </div>
</div><!-- /gallery-module -->