<script>
	$(document).ready(function(e) {
		//validates form for e-news signup
		$("#enews-form").validationEngine('attach',{scroll: false, onValidationComplete: function(form, status) {
			if(status) {
				var form = "#enews-form";
				
				$('.error').remove();
				$(form + " .sendy").val('Pošiljam').addClass('processing gray');
				
				$.post("/add-mailchimp", $(form).serialize(), function(data){
					data = fromJSON(data);
					
					if (data.success != false) {
						$("#form").slideUp();
						$("#success").slideDown();
					}
					else {
						$(form + ", #fail").slideDown();
						$(form + " .sendy").removeClass('processing gray').val("Pošlji ponovno");
			            if (data.text.length == 0)
							$(form + " .sendy").after('<p class="error">Oh now, something went wrong. If this happens again please send us an email to party@gonparty.eu</p>');
			            else
			              $(form + " .sendy").after('<p class="error">' + data.text + '</p>');
					}
				});
			}
		}});
	});
</script>
<div class="enews offer" id="enews-module">
  <div class="grayShadow">
    <div class="content grayBg" id="form">
    	<div class="text" style="margin-top:0">
      	<h3>STAY INFORMED</h3>
				<p>If the packages are not published enter your email and we will inform you as soon as they are online!<br />
Also, if you are not sure if you are coming along for the party, sign up and you will be the first to know our special offers and available spaces.</p>
      </div><div class="form">
    		<form id="enews-form" name="enews-form">
          <div>
            <input class="validate[required]" id="name" name="name" placeholder="Name" type="text" />
          </div>
          <div>
            <input class="validate[required,custom[email]]" id="email" name="email" placeholder="Email" type="email" />
          </div>
          <div>
            <input class="sendy" id="submitButton" type="submit" value="SEND" />
          </div>
        </form>
      </div>
    </div>
    <div class="content grayBg" id="success">
    	<h2>Awesome, from know on you will always know where we party!</h2>
      <p>We will update you regularely where we go and which parties we will visit.</p>
    </div>
  </div>
</div><!-- /gallery-module -->