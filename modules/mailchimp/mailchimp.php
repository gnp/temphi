<?php

use Rezzza\MailChimp\MCAPI;

class MailchimpController extends UFWController {
	function enews() {
		$tpl = new Template("enews", "mailchimp");
		Core::setParseVar("css page", "enews");
		
		return $tpl->display();
	}
	
	function enewsoffer() {
		$tpl = new Template("enewsoffer", "mailchimp");
		Core::setParseVar("css page", "enews");
		
		return $tpl->display();
	}
	
	function enewsshort() {
		$tpl = new Template("enewsshort", "mailchimp");
		Core::setParseVar("css page", "enews");
		
		return $tpl->display();
	}

	function __construct() {
		parent::__construct();
	}
	
	/* mailchimp is ready =) */
	function addSubscriber($data = array()) {
		$data = empty($data) ? $_POST : $data;

		if (!isset($data['email']) || !Validate::isMail($data['email']))
			return FALSE;

		$api = new MCAPI(MAILCHIMP_APIKEY);
		
		$merge_vars = array(
			'FNAME' => isset($data['name']) ? $data['name'] : NULL,
			'LNAME' => isset($data['surname']) ? $data['surname'] : NULL,
			'OPTIN_IP' => isset($data['ip']) ? isset($data['ip']) : $_SERVER['REMOTE_ADDR'],
			"OPTIN_TIME" =>isset($data['date']) ? isset($data['date']) : DT_NOW,
			"TAG" => isset($data['tag']) ? $data['tag'] : NULL,
		);
		
		$retval = $api->listSubscribe(MAILCHIMP_LISTID, $data['email'], $merge_vars, "html", MAILCHIMP_DOUBLEOPTIN);
		
		if ($api->errorCode && $api->errorCode != 214) //214=already on our list. We ignore this and display success message. We don't ignore other errors.
			return die(JSON::to(array(
				"success" => false,
				"code" => $api->errorCode,
				"text" => NULL,
			)));
		else
			return JSON::to(array("success" => true));
	}
}

class MailchimpModel extends UFWModel {
	function __construct() {
		parent::__construct();
	}
}
?>