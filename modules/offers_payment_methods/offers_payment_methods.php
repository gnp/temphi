<?php

class OffersPaymentMethodsController extends UFWController {
	function __construct() {
		parent::__construct();
	}

	function edit($arr = array()) {
		if (!array_key_exists("offer_id", $arr)) return NULL;

		$data["bills"] = Settings::get("payment_bills");
		$data["methods"] = Settings::get("payment_methods");

		foreach ($data["methods"] AS &$method) {
			$method["bills"] = $data["bills"];
			foreach ($method["bills"] AS &$bill) {
				$sql = "SELECT * FROM offers_payment_methods WHERE offer_id = '" . DB::escape($arr['offer_id']) . "' AND method = '" . DB::escape($method['slug']) . "' AND bill = '" . DB::escape($bill['slug']) . "'";
				$q = $this->db->q($sql);
				$r = $this->db->f($q);

				$bill['enabled'] = $r['enabled'];
			}
		}

		return new TwigTpl("modules/offers_payment_methods/templates/edit.twig", $data);
	}

	function update() {
		$sql = "DELETE FROM offers_payment_methods WHERE offer_id = '" . DB::escape($_POST['offer']['id']) . "'";
		$this->db->q($sql);

		foreach ($_POST['payment_methods'] AS $method => $bills) {
			foreach ($bills AS $bill => $value) {
				$this->OffersPaymentMethods->insert(array(
					"method" => $method,
					"bill" => $bill,
					"enabled" => $value,
					"offer_id" => $_POST['offer']['id']
				));
			}
		}
	}
}

class OffersPaymentMethodsModel extends UFWModel {
	function __construct() {
		parent::__construct();

		$this->mk = "method";

		$this->fields = array(
			"method" => array(),
			"bill" => array(),
			"enabled" => array(),
			"offer_id" => array(),
		);
	}

	function getPaymentTable($offer) {
		$arrTable = array();
		$sql = "SELECT * FROM offers_payment_methods WHERE offer_id = '" . DB::escape($offer) . "'";
		$q = DB::q($sql);
		while ($r = DB::f($q)) {
			$arrTable[$r["method"] . $r['bill']] = $r['enabled'] == 1;
		}

		return $arrTable;
	}

	function canPayWith($method, $bill, $offer) {
		$r = $this->OffersPaymentMethods->get("first", NULL, array("method" => $method, "bill" => $bill, "offer_id" => $offer));

		return !empty($r) && $r["enabled"] == 1;
	}
}
	
?>