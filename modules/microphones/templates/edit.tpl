<input type="hidden" id="id" name="microphone[id]" value="##ID##" />

<div class="control-group">
	<label class="control-label" for="content">Content</label>
	<div class="controls">
    	<textarea id="content" name="microphone[content]" class="mceEditor">##CONTENT##</textarea>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="dt_published">Published</label>
	<div class="controls">
    	<input type="text" id="dt_published" name="microphone[dt_published]" value="##DT_PUBLISHED##" class="jdt" />
	</div>
</div>