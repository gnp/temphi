<?php

class MicrophonesController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function index() {
		$tpl = new Template("index", "microphones");
		$qMicrophone = $this->db->q("SELECT content
																	FROM microphones
																	WHERE dt_published <= NOW()
																		AND dt_published > '".DT_NULL."'
																	ORDER BY dt_published DESC
																	LIMIT 1");
		$rMicrophone = $this->db->f($qMicrophone);
		
		//strip <p> tags
		$rMicrophone['content'] = strip_tags($rMicrophone['content'], '<a>');
		
		$tpl->parse($rMicrophone);
		
		return $tpl->display();
	}
	
	function one() {
	}
	
	function maestro() {
		$q = $this->Microphones->get("all");
		
		$return["title"] = "Microphones";
		$return["th"] = array("ID", "Content", "Published", "Status");
		$return["btn header"][] = "add";
		while ($r = $this->db->f($q)) {
			$return["row"][] = array(
				"td" => array(
					$r['id'],
					substr(strip_tags($r['content']), 0, 50),
					$r['dt_published'],
					'<span class="btnToggle btn btn-small" data-url="/microphones/togglepublished/' . $r['id'] . '/" data-value="' . $r['dt_published'] . '"><i class="icon-check"></i></span>',
				),
				"btn" => array("update", "delete")
			);
		}
		
		return $this->Maestro->makeListing($return);
	}

	function togglepublished() {
		return $this->Microphones->update(array(
			"dt_published" => Router::get("active") ? DT_NOW : DT_NULL,
		), Router::get("id"));
	}
	
	function add() {
		$tpl = new Template("add", "microphones");

		return $this->Maestro->makeAdd(array(
			"content" => $tpl->display(),
			"title" => "microphone",
		));
	}
	
	function insert() {
		$id = $this->Microphones->insert($_POST['microphone']);
		
		$this->Maestro->makeInsert(array("id" => $id));
	}

	function edit() {
		$tpl = new Template("edit", "microphones");

		$r = $this->Microphones->get("first", NULL, array("id" => Router::get("id")));
			
		return $this->Maestro->makeEdit(array(
			"content" => $tpl->parse($r),
			"title" => "microphone",
		));
	}

	function update() {
		$this->Microphones->update($_POST['microphone'], $_POST['microphone']['id']);

		return $this->Maestro->makeUpdate(array(
			"id" => $_POST['microphone']['id']
		));
	}

	function delete() {
		$this->Microphones->delete(Router::get("id"));
	}
}

class MicrophonesModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "content";

		$this->fields = array(
			"content" => array(
				"not null" => true,
			),
			"dt_published" => array(
			),
		);
	}
}

?>