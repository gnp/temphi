<?php

class GamesController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function index() {
		//print($this->Users->genValidateJS('game-index'));
		
		$this->db->q("SELECT id, title, content, question, picture AS pic " .
				"FROM games " .
				"WHERE dt_published < '" . DT_NOW . "' AND dt_published > '" . DT_NULL . "'");
		
		$tpl = new Template("index", "games");
		
		$rGame = $this->db->f();
		
		if (empty($rGame))
			return;
		
		$rGame["pic"] = images.$rGame["pic"];
		
		$tpl->parse($rGame);
		
		$this->db->q("SELECT id AS oid, title AS otitle " .
				"FROM games_options " .
				"WHERE game_id = " . $rGame['id'] );
		
		while($rOptions = $this->db->f()){
			$tpl->parse($rOptions, "options");
		}
		
		$tpl->commit("options");
		return $tpl->display();
	}
	
	function saveAnswer() {
		if(isset($_POST['user']['name'])) {
			if(is_numeric($_POST['games_option'])) {
				if(Auth::isLoggedIn()) {
					$userId = Auth::getUserID();
				}
				else {
					if ($result = $this->Users->isValidInsert($_POST['user'])) {
						$userId = $this->Users->insert($_POST['user']);
						$this->Mails->automatic("signup-games", array("user" => $userId));
					} else
						return JSON::to(array("success" => false, "text" => implode(", ",$result)));
				}
				
				$this->GamesAnswers->insert(array("games_option_id"=>$_POST['games_option'], "game_id" => $_POST['game_id'], "user_id" => $userId));
				
				return JSON::to(array("success" => true));
			}
			else
				return JSON::to(array("success" => false, "text" => "Vsi podatki so obvezni, prav tako je obvezno izbrati eno rešitev."));
		}
		return JSON::to(array("success" => false, "text" => "Obrazec ni bil poslan."));
	}
	
	function one() {
	}
	
	function maestro() {
		$q = $this->Games->get("all");
		
		$return["title"] = "Games";
		$return["th"] = array("ID", "Title", "Status");
		$return["btn header"][] = "add";
		while ($r = $this->db->f($q)) {
			$return["row"][] = array(
				"td" => array(
					$r['id'],
					$r['title'],
					'<span class="btnToggle btn btn-small" data-url="/games/togglepublished/' . $r['id'] . '/" data-value="' . $r['dt_published'] . '"><i class="icon-check"></i></span>',
				),
				"btn" => array("update", "delete", array(
					"txt" => '<i class="icon-indent-left"></i>',
					"url" => "/maestro/games_answers/maestro/" . $r['id'],
					"class" => "info",
					"tit" => "Participans"
				))
			);
		}
		
		return $this->Maestro->makeListing($return);
	}

	function togglepublished() {
		return $this->Games->update(array(
			"dt_published" => Router::get("active") ? DT_NOW : DT_NULL,
		), Router::get("id"));
	}
	
	function add() {
		$tpl = new Template("add", "Games");

		return $this->Maestro->makeAdd(array(
			"content" => $tpl->display(),
			"title" => "game",
		));
	}
	
	function insert() {
		$id = $this->Games->insert($_POST['game']);
		
		$this->Maestro->makeInsert(array("id" => $id));
	}

	function edit() {
		$tpl = new Template("edit", "Games");

		$r = $this->Games->get("first", NULL, array("id" => Router::get("id")));

		$r['dt_published'] = date("Y-m-d", strtotime($r['dt_published']));
		$r['picture2'] = "/media/" . $r['picture'];
		$r['pic display'] = !empty($r['picture']) ? "block" : "none";
			
		return $this->Maestro->makeEdit(array(
			"content" => $tpl->parse($r) . Core::loadView("edit", "games_options", array("game_id" => Router::get("id"))),
			"title" => "game",
		));
	}

	function update() {
		$this->Games->update($_POST['game'], $_POST['game']['id']);

		Core::loadView("update", "games_options");

		return $this->Maestro->makeUpdate(array(
			"id" => $_POST['game']['id']
		));
	}

	function delete() {
		$this->Games->delete(Router::get("id"));
	}
}

class GamesModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "title";

		$this->fields = array(
			"title" => array(
				"not null" => true,
			),
			"dt_published" => array(
				"not null" => TRUE,
				"defautl" => DT_NOW,
			),
			"question" => array(
				"not null" => true,
			),
			"content" => array(
				"not null" => true,
			),
			"picture" => array(
			),
		);
	}
}

?>