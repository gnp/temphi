<input type="hidden" id="id" name="game[id]" value="##ID##" />

<div class="control-group">
	<label class="control-label" for="title">Title</label>
	<div class="controls">
    	<input type="text" id="title" name="game[title]" value="##TITLE##" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="content">Content</label>
	<div class="controls">
    	<textarea id="content" name="game[content]" class="mceEditor">##CONTENT##</textarea>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="picture">Picture</label>
	<div class="controls">
    	<input type="hidden" id="picture" name="game[picture]" value="##PICTURE##" />
		<input type="button" class="btn btn-primary elFinderData" value="Open file browser" data-id="" data-files="" data-multi="-1" data-replace="newImageField" data-field="game[picture]" data-url="" />
		<img src="##PICTURE2##" id="newImageField" style="display: ##PIC DISPLAY##; margin-top: 10px;" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="dt_published">Published</label>
	<div class="controls">
    	<input type="text" id="dt_published" name="game[dt_published]" class="jd span6" value="##DT_PUBLISHED##" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="question">Question</label>
	<div class="controls">
    	<input type="text" id="question" name="game[question]" value="##QUESTION##" class="span6" />
	</div>
</div>
