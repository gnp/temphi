<script>
  $(window).load(function() {
    resize(".game .imageBox img", 0.37);
  });
  $(document).ready(function(){
    resize(".game .imageBox img", 0.37);
  });
  $(window).resize(function(){
    resize(".game .imageBox img", 0.37);
  });
	
	$(document).ready(function(e) {
    $("#game-index").validationEngine(
		{	scroll: false, onValidationComplete: function(form, status) {
				if(status) {
					$.post("/games/saveAnswer", $("#game-index").serialize(), function(data){
						data = fromJSON(data);
						
						if (data.success != true) {
							$(".game .content .err").remove();
							$(".game .content .intro").after("<p class='err'>"+data.text+"</p>");
						}
						else {
							$("p.question, #game-index").hide();
							$(".game .content").append("<h3>Fu*k yea, poslano!</h3>"+
							"<p class='succ'>Tvoj odgovor smo si zapomnili. Če boš med srečnimi dobitniki, ti to takoj po žrebu sporočimo.</p>");
						}
					});
				}
			}
		});
  });
</script>
<div class="game grayShadow">
  <h2>NAGRADNA IGRA</h2><a name="game"></a>
  <div class="imageBox">
  	<div>
  		<img title="##TITLE##" src="/cache/img/w/570##PIC##" />
  	</div>
  </div>
  <div class="content grayBg">
    <p class="intro">##TITLE##</p>
    <p class="question">##QUESTION##</p>
    <form id="game-index" method="post">
    	<input type="hidden" name="game_id" value="##ID##" />
      <div class="answer">
      	##OPTIONS START##
        <div>
          <input class="validate[required]" id="option##OID##" name="games_option" type="radio" value="##OID##" />
          <label for="option##OID##">##OTITLE##</label>
        </div>
        ##OPTIONS END##
      </div>
      <div>
        <input class="validate[required]" id="name" name="user[name]" placeholder="Ime" type="text" />
      </div>
      <div>
        <input class="validate[required]" id="surname" name="user[surname]" placeholder="Priimek" type="text" />
      </div>
      <div>
        <input class="validate[required,custom[email]]" id="email" name="user[email]" placeholder="Email" type="email" />
      </div>
      <div>
        <input class="sendy" type="submit" value="POŠLJI" />
      </div>
    </form>
  </div>
</div><!-- /game -->