<div class="control-group">
	<label class="control-label" for="title">Title</label>
	<div class="controls">
    	<input type="text" id="title" name="game[title]" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="content">Content</label>
	<div class="controls">
    	<textarea id="content" name="game[content]" class="mceEditor"></textarea>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="picture">Picture</label>
	<div class="controls">
    	<input type="hidden" id="picture" name="game[picture]" />
		<input type="button" class="btn btn-primary elFinderData" value="Open file browser" data-id="" data-files="" data-multi="-1" data-replace="newImageField" data-field="game[picture]" data-url="" />
		<img src="" style="display: none; margin-top: 10px;" id="newImageField" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="dt_published">Published</label>
	<div class="controls">
    	<input type="text" id="dt_published" name="game[dt_published]" class="jd span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="question">Question</label>
	<div class="controls">
    	<input type="text" id="question" name="game[question]" class="span6" />
	</div>
</div>
