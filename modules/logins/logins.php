<?php

class LoginsController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function getLogins($userId = NULL) {
		if (Validate::isInt($userId)) {
			$tpl = new Template("getlogins", "logins");
			
			$q = $this->Logins->get("all", NULL, array("user_id" => $userId), array("order by" => array("id DESC")));
			
			while ($r = $this->Logins->f($q))
				$tpl->parse($r, "row");
			
			return $tpl->parse(array(
				"row" => $tpl->display("row"),
			));
		} else {
			$tpl = new Template("getlogins2", "logins");
			
			$q = $this->db->q("SELECT l.id, u.email AS user, l.ip, l.datetime AS datetime, l.dt_logged_out AS dt_logged_out FROM logins l INNER JOIN users u ON u.id = l.user_id WHERE u.status_id = 1 ORDER BY id DESC LIMIT 20");
			
			while ($r = $this->Logins->f($q))
				$tpl->parse($r, "row");
			
			return $tpl->parse(array(
				"row" => $tpl->display("row"),
			));
		}
	}
}

class LoginsModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "";

		$this->fields = array(
			"id" => array(
				"not null" => true,
			),
			"user_id" => array(
				"not null" => true,
			),
			"ip" => array(
				"not null" => true,
			),
			"datetime" => array(
				"not null" => true,
			),
			"dt_logged_out" => array(
			),
			"active" => array(
				"default" => 1,
			),
			"hash" => array(
				"not null" => true,
			),

		);
	}
}

?>