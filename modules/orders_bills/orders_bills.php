<?php

class OrdersBillsController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function index() {
	}
	
	function one() {
	}

	function generateEstimatePDF($id = NULL) {
		$orderID = is_null($id) ? Router::get("id") : $id;
		$data = $this->Orders->getEstimateData($orderID);
		
		if (!$data)
			return FALSE;

		$data['dt_now'] = date("d.m.Y", strtotime(DT_NOW));
		$data['www_path'] = WWW_PATH;
		
		$tpl = new Template("estimatepdf", "orders_bills");
		
		foreach ($data['orders'] AS $order) {
			$order['price'] = makePrice($order['price']);
			$order['sum'] = makePrice($order['sum']);
			$order['quantity'] = makePrice($order['quantity'], 2, null);
			
			$tpl->parse($order, "row");
		}
		
		$tpl->commit("row");
		
		$j = 0;
		if (isset($data['portions']))
		foreach ($data['portions'] AS $i => $portion) {
			if ($portion['type'] == 2) {
				$j++;
			}

			$portion['due date'] = date("d.m.Y", strtotime($portion['due date']));
			$portion['title'] = ($portion['type'] == 2 ? $j . ". " : "") . mb_strtoupper($this->OrdersBills->getTypeById($portion['type']), "UTF-8");
			$portion['price'] = makePrice($portion['price']);

			$tpl->parse($portion, "portion");
		}
		
		$tpl->commit("portion");
		
		$data['sum total'] = makePrice($data['sum total']);
		$data['sum remaining'] = makePrice($data['sum remaining']);
		$data['reservation price'] = makePrice($data['reservation price']);
		
		// generate pdf
		ini_set('max_execution_time', 120);
		
		require_once(APP_PATH . 'other/html2pdf/html2pdf.class.php');
	
    $html2pdf = new HTML2PDF('P','A4','en', true, 'UTF-8', array(0, 0, 0, 0));
    $html2pdf->setDefaultFont('Tahoma');

    $htmlData = $tpl->parse($data);
		
		try {
			$html2pdf->WriteHTML($htmlData);
		} catch (Exception $r) {
			Status::code(404, "Error when generating estimate");//var_dump($r);
		}

		$paymentsData = '<table style="margin: 0; padding: 0; width: 60%; margin-left: 48px;">';
		$payments = $this->generatePaymentsPNG($orderID);
		$i = 1;
		foreach ($payments AS $payment) {
			//$paymentsData .= '<tr><td style="width: 25%;"><p><b>'.($i == 1 ? "Rezervacija" : ($i-1 . ". obrok")).'</b></p></td><td style="width: 75%"><br /><img src="' . $payment . '" style"width: 600px; height: 350px;" width="600" height="350" /></td></tr>';
			$i++;
		}
		$paymentsData .= '</table>';
		$html2pdf->WriteHTML($paymentsData);

		$rOrder = $this->Orders->get("first", NULL, array("id" => $id));
		$filename = APP_PATH . "private" . DS . "estimates" . DS . $rOrder['num'] . "-" . time() . ".pdf";

		$filenameHtml = APP_PATH . "private" . DS . "estimates" . DS . $rOrder['num'] . "-" . time() . ".pdf.html";
		file_put_contents($filenameHtml, $htmlData);

    $html2pdf->Output($filename, "F");

    $this->Orders->update(array(
    	"estimate_url" => $filename,
    ), $rOrder['id']);

    return $filename;
	}

	function generateBillPDF($id = NULL) {
		$data = $this->Orders->getEstimateData(is_null($id) ? Router::get("id") : $id);
		
		if (!$data)
			return FALSE;

		$data['dt_now'] = date("d.m.Y", strtotime(DT_NOW));
		$data['dt_executed'] = date("d.m.Y", strtotime("+1day", strtotime($data['offer']['dt_end'])));
		$data['www_path'] = WWW_PATH;
		
		$tpl = new Template("billpdf", "orders_bills");
		
		foreach ($data['orders'] AS $order) {
			$order['price'] = makePrice($order['price']);
			$order['sum'] = makePrice($order['sum']);
			$order['quantity'] = makePrice($order['quantity'], 2, null);
			
			$tpl->parse($order, "row");
		}
		
		$tpl->commit("row");
		
		$j = 0;
		if (isset($data['portions']))
		foreach ($data['portions'] AS $i => $portion) {
			if ($portion['type'] == 2) {
				$j++;
			}

			$portion['due date'] = date("d.m.Y", strtotime($portion['due date']));
			$portion['title'] = ($portion['type'] == 2 ? $j . ". " : "") . mb_strtoupper($this->OrdersBills->getTypeById($portion['type']), "UTF-8");
			$portion['price'] = makePrice($portion['price']);

			$tpl->parse($portion, "portion");
		}
		
		$tpl->commit("portion");
		
		$data['sum total'] = makePrice($data['sum total']);
		$data['sum remaining'] = makePrice($data['sum remaining']);
		$data['reservation price'] = makePrice($data['reservation price']);
		
		// generate pdf
		ini_set('max_execution_time', 120);
		
		require_once(APP_PATH . 'other/html2pdf/html2pdf.class.php');
	
	    $html2pdf = new HTML2PDF('P','A4','en', true, 'UTF-8', array(0, 0, 0, 0));
	    $html2pdf->setDefaultFont('Tahoma');
			
	    $htmlData = $tpl->parse($data);
	
			$html2pdf->WriteHTML($htmlData);
	
			$rOrder = $this->Orders->get("first", NULL, array("id" => $id));
			$filename = APP_PATH . "private" . DS . "bills" . DS . $rOrder['num'] . "-" . time() . ".pdf";
			$filenameHtml = APP_PATH . "private" . DS . "bills" . DS . $rOrder['num'] . "-" . time() . ".pdf.html";
			file_put_contents($filenameHtml, $htmlData);
	
	    $html2pdf->Output($filename, "F");
	
	    $this->Orders->update(array(
	    	"bill_url" => $filename,
	    ), $rOrder['id']);
	
	    return $filename;
	}

	function generatePaymentsPNG($order) {
		$rOrder = $this->Orders->get("first", NULL, array("id" => $order));

		if (empty($rOrder))
			Status::code(404, "Ne najdem naročila");

		$rPayee = $this->Users->get("first", NULL, array("id" => $rOrder['user_id']));

		if (empty($rPayee))
			Status::code(404, "Ne najdem naročnika");

		$qBills = $this->OrdersBills->get("all", NULL, array("order_id" => $rOrder['id'], "type" => array(1,2)), array("order by" => "type ASC, dt_valid ASC"));

		$i = 1;
		$files = array();
		while ($rBill = $this->db->f($qBills)) {
			$arrTexts = array(
				array(40, 31, 'SI56 XXXX XXXX XXXX XXX (' . $rPayee['name'] . ' ' . $rPayee['surname'] . ')'),
				array(40, 75, "00-" .  $rOrder['num'] . "-" . $i),
				array(40, 120, $rPayee['name'] . ' ' . $rPayee['surname']),
				array(40, 165, $rPayee['post']),
				array(40, 209, "PRCP - Plačilo izdatkov"),
				array(370, 120, $rPayee['address']),
				array(370, 165, "Slovenija - 705"),
				array(370, 209, "Plačilo aranžmaja " . $rOrder['num'] . "-" . $i),

				array(40, 272, makePrice($rBill['price'])),
				array(40, 316, 'SI56 0481 0011 3762 882'),
				array(40, 360, 'SI'),
				array(82, 360, '00'),
				array(124, 360, $rOrder['num'] . "-" . $i),
				array(40, 405, 'RENTOR Mario Benić s.p.'),
				array(40, 449, '8250 Brežice'),

				array(370, 272, date("d.m.Y", strtotime($rBill['dt_valid']))),
				array(484, 272, 'KBMASI2X'),
				array(370, 405, 'CPB 26a'),
				array(370, 449, 'Slovenija - 705'),
			);
			
			try {
		    $img = imagecreatefrompng(WWW_PATH . "img" . DS . "upnsepa.png");
		    $black = imagecolorallocate($img, 0, 0, 0);
		    $font = 3;

		    foreach ($arrTexts AS $t) {
		    	imagettftext($img, 10, 0, $t[0], $t[1]+11, $black, WWW_PATH . "css" . DS . "fonts" . DS . "Oxygen-Regular.ttf", $t[2]);
		    }

		    $path = APP_PATH . "private" . DS . "payments" . DS . microtime() . ".png";
		    imagepng($img, $path);
		    $files[] = $path;
			} catch (Exception $r) {
				//var_dump($r);
			}

	    $i++;
    }
	   
	  return $files;
	}

	/**
	 * Generate Voucher PDF
	 * @id = ID
	 * @pageNum - number of the page to create
	 * @numOfPages - number of all pages in a document
	 */
	function generateVoucherPDF($id = NULL, $ouids) {
		$data = $this->Orders->getEstimateData(is_null($id) ? Router::get("id") : $id);
		
		if (!$data)
			return FALSE;
		
// 		dump($data['offer']['title']); die();
		
		// generate just those which have been payed!!
		if(is_null($data['portions'][0]['payed'])) return FALSE;
	
		$data['dt_now'] 			= date("d.m.Y", strtotime(DT_NOW));
		$data['dt_executed'] 		= date("d.m.Y", strtotime("+1day", strtotime($data['offer']['dt_end'])));
		$data['www_path'] 			= WWW_PATH;
		$data['sum total'] 			= makePrice($data['sum total']);
		$data['packet name'] 		= $data['orders'][0]['title'];
		$data['ticket price'] 		= makePrice($data['orders'][0]['price']);
		$data['sum remaining'] 		= makePrice($data['sum remaining']);
		$data['reservation price'] 	= makePrice($data['reservation price']);
		
		// generate pdf
		ini_set('max_execution_time', 120);
	
		require_once(APP_PATH . 'other/html2pdf/html2pdf.class.php');
	
		$html2pdf = new HTML2PDF('P','A4','en', true, 'UTF-8', array(0, 0, 0, 0));
		$html2pdf->setDefaultFont('Tahoma');
		
		$len = count($ouids);
		foreach ($ouids as $i => $ouid) {
			// ticket specific pin must be generated separately for each page !
			$data['order']['pin'] = $ouid . substr($data['order']['hash'], $i*4, 4);
			
			$tpl = new Template("voucherpdf", "orders_bills");
	        $htmlData = $tpl->parse($data);
	        $html2pdf->writeHTML($htmlData);
	        if($i < $len-1) {
	        	$data['offer']['firstpage'] = false;
	        	$html2pdf->pdf->AddPage();
	        }
		}
		
		$rOrder = $this->Orders->get("first", NULL, array("id" => $id));
		$filename = APP_PATH . "private" . DS . "vouchers" . DS . $rOrder['num'] . "-" . time() . ".pdf";
// 		$filenameHtml = APP_PATH . "private" . DS . "vouchers" . DS . $rOrder['num'] . "-" . time() . ".pdf.html";
// 		$aa = file_put_contents($filenameHtml, $htmlData);
	
		$html2pdf->Output($filename, "F");
		
// 		$this->Orders->update(array(
// 				"voucher_url" => $filename,
// 		), $rOrder['id']);
		
		return $filename;
	}
	
	function generateVoucherPDFSinglePage($id = NULL, $pageNum = 0, $numOfPages = 0) {
		$data = $this->Orders->getEstimateData(is_null($id) ? Router::get("id") : $id);
		
		if (!$data)
			return FALSE;
		
		dump($data['portions'][0]['payed']); echo BR;
		
		// generate just those which have been payed!!
		if(is_null($data['portions'][0]['payed'])) return false;
	
		$data['dt_now'] = date("d.m.Y", strtotime(DT_NOW));
		$data['dt_executed'] = date("d.m.Y", strtotime("+1day", strtotime($data['offer']['dt_end'])));
		$data['www_path'] = WWW_PATH;
	
		$tpl = new Template("voucherpdf", "orders_bills");
	
		$data['sum total'] = makePrice($data['sum total']);
		$data['sum remaining'] = makePrice($data['sum remaining']);
		$data['reservation price'] = makePrice($data['reservation price']);
		
		// cena !!
		$data['ticket price'] = makePrice($data['orders'][0]['price']);
		
		$data['order']['pin'] = $ouid . substr($data['order']['hash'], $i*4, 4);
	
		// generate pdf
		ini_set('max_execution_time', 120);
	
		require_once(APP_PATH . 'other/html2pdf/html2pdf.class.php');
	
		$html2pdf = new HTML2PDF('P','A4','en', true, 'UTF-8', array(0, 0, 0, 0));
		$html2pdf->setDefaultFont('Tahoma');
			
		$htmlData = $tpl->parse($data);
		$html2pdf->writeHTML($htmlData);
	
		$rOrder = $this->Orders->get("first", NULL, array("id" => $id));
		$filename = APP_PATH . "private" . DS . "vouchers" . DS . $rOrder['num'] . "-" . time() . ".pdf";
// 		$filenameHtml = APP_PATH . "private" . DS . "vouchers" . DS . $rOrder['num'] . "-" . time() . ".pdf.html";
// 		$aa = file_put_contents($filenameHtml, $htmlData);
	
		$html2pdf->Output($filename, "F");
	
		$this->Orders->update(array(
				"voucher_url" => $filename,
		), $rOrder['id']);
	
		return $filename;
	}
	
	function upnsepa() {
		$rOrder = $this->Orders->get("first", NULL, array("hash" => Router::get("hash")));

		if (empty($rOrder))
			Status::code(404, "Manjka naročilo");
		
		if (!Validate::isNullDate($rOrder['dt_rejected']))
			Status::code(404, "Naročilo je bilo zavrnjeno");
		
		if (!Validate::isNullDate($rOrder['dt_canceled']))
			Status::code(404, "Naročilo je bilo preklicano");

		$rPayee = $this->Users->get("first", NULL, array("id" => $rOrder['user_id']));

		if (empty($rPayee))
			Status::code(404, "Cant find Payee");

		$price = RESERVATION;
		if (isset($_GET['full']) && $_GET['full'] == 1) {
			$price = $rOrder['price'];
		}

		Core::setParseVar("css page", "payment");

        return new TwigTpl("modules/orders_bills/templates/upnsepa.twig", array(
            "payee" => $rPayee,
            "referenca" => array(
                "string" => "Packet payment " . $rOrder['num'] . "-1",
                "long" => "00-" .  $rOrder['num'] . "-1",
                "short" => $rOrder['num'] . "-1",
            ),
            "price" => bold(makePrice($price)),
            "date" => date("d.m.Y", strtotime("+1 days", strtotime($rOrder['dt_added']))),
            "content_order_payment_upn" => array(
                "[pdfurl]" => Router::make("view pdf", array('id' => $rOrder['id'])),
            ),
        ));
	}

	function postCalculateReturn() {
		return JSON::to(array(
			"success" => TRUE,
			"html" => $this->OrdersBills->calculateReturn($_POST['id'], $_POST),
		));
	}

	function calculateReturn($id = NULL, $post = array()) {
		$tpl = new Template("return", "orders_bills");
		$id = is_null($id) ? Router::get("id") : $id;

		$data = $this->Orders->calculateReturn(Router::get("id"), $post);
		$data["id"] = Router::get("id");

		return $tpl->parse($data);
	}

	function maestroAll() {
		$return["title"] = "Bills";
		$return["module"] = "orders_bills";
		$return["th"] = array("Offer", "Order", "Payee", "Added", "Confirmed", "Status");
		$return["btn header"] = array();

		$sqlOffers = "SELECT o.id, o.title 
			FROM offers o 
			WHERE o.dt_start > '" . date("Y-m-d", strtotime("-6months")) . "'
			ORDER BY IF(o.dt_start > '" . date("Y-m-d", strtotime("-1days")) . "', 0, 1) ASC, o.dt_start ASC";
		$qOffers = $this->db->q($sqlOffers);
		while ($rOffer = $this->db->f($qOffers)) {
			$sqlOrders = "SELECT o.num, o.dt_confirmed, o.dt_canceled, o.dt_rejected, o.id, o.user_id, o.dt_added
				FROM orders o 
				WHERE o.offer_id = " . $rOffer['id'] . "
				ORDER BY o.dt_rejected ASC, o.dt_canceled ASC, o.dt_confirmed DESC";
			$qOrders = $this->db->q($sqlOrders);
			while ($rOrder = $this->db->f($qOrders)) {
				$reservation = false;
				$all = true;
				$late = false;

				$qAllBills = $this->db->q("SELECT * FROM orders_bills WHERE order_id = " . $rOrder['id']);
				while ($rBill = $this->db->f($qAllBills)) {
					if ($rBill['type'] == 1 && $rBill['dt_confirmed'] > DT_NULL)
						$reservation = true;

					if ($rBill['dt_confirmed'] <= DT_NULL)
						$all = false;

					if ($rBill['dt_confirmed'] <= DT_NULL && $rBill['dt_valid'] < DT_NOW)
						$late = true;
				}

				$class = null;
				$status = null;
				if ($late) {
					if ($reservation) {
						$class = "error";
						$status = "Waiting for instalment payment";
					} else {
						$class = "warning";
						$status = "Waiting for reservation";
					}
				} else {
					if ($all) {
						$class = "success";
						$status = "Paid";
					} else if (!$reservation) {
						$status = "Waiting for payment";
					} else {
						$status = "OK";
					}
				}

				$rPayee = $this->Users->get("first", null, array("id" => $rOrder['user_id']));

				$return["row"][] = array(
					"td" => array(
						$rOffer['title'],
						nobr($rOrder['num']),
						$rPayee['name'] . " " . $rPayee['surname'] . "<br />" . $rPayee['email'],
						nobr(makeDate($rOrder['dt_added'])),
						nobr(makeDate($rOrder['dt_confirmed'])),
						$status,
					),
					"btn" => array(array(
						"class" => "warning",
						"tit" => "View",
						"txt" => '<i class="icon-pencil"></i>',
	          "url" => "/maestro/orders/edit/" . $rOrder['id'],
					)),
					"class" => $class,
				);
			}
		}

		return $this->Maestro->makeListing($return);
	}
	
	function maestro($data = array()) {
		if (empty($data)) {
			return $this->maestroAll();
		}

		$rOrder = $this->Orders->get("first", NULL, array("id" => $data['id']));

		$sql = "SELECT ob.id, ob.type, ob.price, ob.payed, ob.notes, ob.dt_valid, ob.dt_confirmed " .
			"FROM orders_bills ob " .
			"WHERE ob.order_id = " . $data['id'];
		$q = $this->db->q($sql);
		
		$return["title"] = "Bills";
		$return["module"] = "orders_bills";
		$return["th"] = array("ID", "Ref", "Valid", "Type", "Price", "Payed", "", "Notes");
		$return["btn header"][] = $this->Maestro->makeRelated(array(
			array(
				"url" => "/maestro/orders_bills/calculateReturn/" . Router::get("id"),
				"text" => "Calculate return",
			),
			array(
				"url" => "/maestro/orders_bills/add?order_id=" . Router::get("id"),
				"text" => "Add",
			),
			array(
				"text" => "Generate estimate",
	            "url" => "/maestro/orders/generateEstimate/" . Router::get("id"),
				"class" => "btnGenerateEstimate",
			),
			array(
				"text" => "Generate bill",
	            "url" => "/maestro/orders/generateBill/" . Router::get("id"),
				"class" => "btnGenerateBill",
			),
			array(
				"text" => "Send estimate",
	            "url" => "/maestro/orders/sendEstimate/" . Router::get("id"),
			),
			array(
				"text" => "Send bill",
	            "url" => "/maestro/orders/sendBill/" . Router::get("id"),
			)
		));

		$sum = 0;
		$sumPayed = 0;
		while ($r = $this->db->f($q)) {
			$sum += $r['price'];
			$sumPayed += $r['payed'];
			$return["row"][] = array(
				"td" => array(
					$r['id'],
					$rOrder['num'],
					$r['dt_valid'],
					$this->OrdersBills->getTypeById($r['type']),
					makePrice($r['price']),
					makePrice($r['payed']),
					$r['dt_confirmed'],
					substr($r['notes'], 0, 40),
				),
				"btn" => array(
					"update",
					(empty($r['dt_confirmed']) || $r['dt_confirmed'] == DT_NULL
						? array(
							"class" => "success",
							"tit" => "Confirm",
							"txt" => '<i class="icon-ok"></i>',
		          "url" => "/maestro/orders_bills/confirm/" . $r['id'],
						)
						: NULL
					),
					($r['dt_confirmed'] > date("Y-m-d H:i:s", strtotime("-3minute"))
						? array(
							"class" => "success",
							"tit" => "Send thanks email",
							"txt" => '<i class="icon-envelope"></i>',
		          "url" => "/maestro/orders_bills/mail/" . $r['id'],
						)
						: NULL
					),
					((float)$r['price'] == 0.0 || TRUE
						? "delete"
						: NULL
					)
				),
				"class" => $r['price'] <= $r['payed'] ? "success" : ($r['dt_valid'] < DT_NOW ? "error" : "warning")
			);
		}
		
		$returnData = $this->Orders->calculateReturn(Router::get("id"));

		/*$return["title"] .= " (Payed: " . makePrice($sumPayed) . 
			" × Calculated: " . makePrice($this->Orders->recalculatePrice($data['id'])) . 
			" × Bills: " . makePrice($sum) . 
			" × Return: " . ($returnData['return']) . ")";*/
		$return["title"] .= " × Status: " . $returnData['whatToDo'];
		
		return $this->Maestro->makeListing($return);
	}

	function mail() {
		 // order-payment #15 || order-confirmed-upn #12 + order-confirmed-friend #11 || order.payed #16
		$orderBillID = Router::get("id");

		$rOrdersBill = $this->OrdersBills->get("first", null, array("id" => $orderBillID));
		$rOrder = $this->Orders->get("first", null, array("id" => $rOrdersBill['order_id']));
		$rPayee = $this->Users->get("first", null, array("id" => $rOrder['user_id']));
		$rOrdersUser = $this->OrdersUsers->get("first", null, array("order_id" => $rOrder['id'], "user_id" => $rPayee['id']));

		if (!$rOrder['dt_payed'] > DT_NULL) {
			Status::redirect("/maestro/mails/compose?ouid=" . $rOrdersUser['id'] . "&mail=16");
		} else if ($rOrdersBill['type'] == 1) {
			Status::redirect("/maestro/mails/compose?ouid=" . $rOrdersUser['id'] . "&mail=12");
		} else if ($rOrdersBill['type'] == 2) {
			Status::redirect("/maestro/mails/compose?ouid=" . $rOrdersUser['id'] . "&mail=15");
		} else {
			Status::redirect("/maestro/mails/compose?ouid=" . $rOrdersUser['id']);
		}
	}
	
	function confirm() {
		$r = $this->OrdersBills->get("first", NULL, array("id" => Router::get("id")));
		
		$this->OrdersBills->update(array(
			"payed" => $r['payed'] == 0 ? $r['price'] : $r['payed'],
			"dt_confirmed" => DT_NOW,
		), $r['id']);

		$qAllBills = $this->OrdersBills->get("all", null, array("order_id" => $r['order_id']));
		$allPayed = true;
		while ($rBill = $this->db->f($qAllBills)) {
			if ($rBill['dt_confirmed'] <= DT_NULL) {
				$allPayed = false;
				break;
			}
		}

		if ($allPayed) {
			$this->Orders->update(array("dt_payed" => DT_NOW), $r['order_id']);
		}

		Status::redirect($_SERVER['HTTP_REFERER']);
	}
	
	function add() {
		$tpl = new Template("add", "orders_bills");

		return $this->Maestro->makeAdd(array(
			"content" => $tpl->parse(array(
					"order_id" => $_GET['order_id'],
					"types list" => $this->OrdersBills->generateTypesList(array(
						"name" => 'orders_bill[type]',
						"id" => 'type',
					))
				)),
			"title" => "bill",
		));
	}

	function manualInsert() {
		$id = $this->OrdersBills->insert(array(
			"price" => $_POST['price'],
			"payed" => $_POST['payed'],
			"type" => $_POST['type'],
			"order_id" => $_POST['order_id'],
			"dt_valid" => DT_NOW,
		));

		if (Validate::isInt($id)) {
			return JSON::to(array(
				"success" => TRUE,
				"url" => "/maestro/orders/edit/" . $_POST['order_id'],
			));
		} else {
			return JSON::to(array(
				"success" => FALSE,
				"text" => "Napaka!",
			));
		}
	}
	
	function insert() {
		$id = $this->OrdersBills->insert($_POST['orders_bill']);
		
		Status::redirect("/maestro/orders/edit/" . $_POST['orders_bill']['order_id']);
	}

	function edit() {
		$tpl = new Template("edit", "OrdersBills");

		$r = $this->OrdersBills->get("first", NULL, array("id" => Router::get("id")));

		$r['types list'] = $this->OrdersBills->generateTypesList(array(
			"name" => 'orders_bill[type]',
			"id" => 'type',
			"selected" => $r['type'],
		));

		return $this->Maestro->makeEdit(array(
			"content" => $tpl->parse($r),
			"title" => "Bill",
		));
	}

	function update() {
		if ($_POST['orders_bill']['price'] <= $_POST['orders_bill']['payed'] && ($_POST['orders_bill']['dt_confirmed'] == DT_NULL || empty($_POST['orders_bill']['dt_confirmed'])))
			$_POST['orders_bill']['dt_confirmed'] = DT_NOW;
		else {
			$_POST['orders_bill']['dt_confirmed'] = DT_NULL;
		}
		
		$this->OrdersBills->update($_POST['orders_bill'], $_POST['orders_bill']['id']);

		return $this->Maestro->makeUpdate(array(
			"id" => $_POST['orders_bill']['id']
		));
	}

	function delete() {
		$this->OrdersBills->delete(Router::get("id"));
	}
}

class OrdersBillsModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "";

		$this->orderBy = "id ASC";

		$this->fields = array(
			"order_id" => array(
				"not null" => true,
			),
			"dt_added" => array(
				"not null" => true,
				"default" => DT_NOW,
			),
			"dt_valid" => array(
			),
			"dt_confirmed" => array(
				"default" => DT_NULL,
			),
			"price" => array(
			),
			"payed" => array(
				"default" => 0,
			),
			"notes" => array(
			),
			"reservation" => array(
				"default" => -1,
			),
			"dt_override" => array(
			),
			"bill_id" => array(
				"default" => 1, // because of mysql error
			),
			"url" => array(
				"default" => 1, // because of mysql error
			),
			"type" => array(
				"default" => 2,
			), // 1 reservation, 2 portion, 3 return, 4 hold // @ToDo!!!
		);
	}

	function generateTypesList($conf = array()) {
		$conf["options"] = $this->getArrTypesList();

		return HTML::select($conf);
	}

	function getArrTypesList() {
		return array(
			2 => "Instalment",
			1 => "Reservation",
			3 => "Return",
			4 => "Withheld funds"
		);
	}

	function getTypeById($id) {
		$arr = $this->getArrTypesList();
		return isset($arr[$id]) ? $arr[$id] : " - select account type - ";
	} 
}

?>