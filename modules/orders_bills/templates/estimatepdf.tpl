<table style="margin: 0; padding: 0; width: 80%; margin-left: 80px; margin-top: 60px;">
	<tr>
		<td style="width: 100%">
			<table style="margin: 0; padding: 0; width: 100%;">
				<tr>
					<td style="width: 60%">
						<!--<img style="width: 90%;" src="##WWW_PATH##img/logo_white.png" />-->
					</td>
					<td style="width: 40%">
						<span style="font-weight: bold;">Sender</span><br />
						Getit d.o.o.<br />
						<br />
						Doverska 37<br />
						Split<br />
						Croatia<br />
						<br />
						<br />
						<br />
						
					</td>
				</tr>
			</table>
			<br /><br />
		</td>
	</tr>
	<tr>
		<td>
			<span style="font-weight: bold;" >BOOKING CONFIRMATION - VOUCHER</span><br />
			<span style="font-weight: bold;" >Thank you for the trust you have shown by booking through GoNParty booking system.<br />
			You have given us a chance to do everything for the best party experience<br />
            you can have. We will send you further information soon.</span>
			<br /><br /><br />
		</td>
	</tr>
	<tr>
		<td>
			<table style="margin: 0; padding: 0; width: 100%;">
				<tr>
					<td style="width: 60%;">
						<span style="font-weight: bold;" >Customer</span><br />
						##PAYEE.NAME## ##PAYEE.SURNAME##<br />
						##PAYEE.ADDRESS##<br />
						##PAYEE.POST##
					</td>
					<td style="width: 40%;">
						<span style="font-weight: bold;" >Booking number.:</span>##ORDER.NUM##<br />
						<br /><br />
						<!--<span style="font-weight: bold;" >Payment deadline:</span> ##PAYMENT DATE##<br />-->
					</td>
				</tr>
			</table>
			<br /><br />
		</td>
	</tr>
	<tr>
		<td>
			<span style="font-weight: bold;" >Booking information</span>
			<table style="margin: 0; padding: 0; width: 100%;">
				<tr style="background-color: #623d9c; color: #ffffff;">
					<td style="width: 10%;">#</td>
					<td style="width: 45%;">Description</td>
					<td style="width: 15%;">Quantity</td>
					<td style="width: 15%;">Price</td>
					<td style="width: 15%;">Sum</td>
				</tr>
				##ROW START##<tr style="background-color: #f0e6ff;">
					<td style="width: 10%;">1</td>
					<td style="width: 45%;">##TITLE##</td>
					<td style="width: 15%;">##QUANTITY##</td>
					<td style="width: 15%;">##PRICE##</td>
					<td style="width: 15%;">##SUM##</td>
				</tr>##ROW END##
				<tr style="background-color: #623d9c; color: #ffffff;">
					<td colspan="3"></td>
					<td style="width: 15%;">Total</td>
					<td style="width: 15%;">##SUM TOTAL##</td>
				</tr>
			</table>
			<br />
		</td>
	</tr>
	<!--<tr>
		<td>
			<span style="font-weight: bold;" >Instalments</span>
			<table style="margin: 0; padding: 0; width: 100%;">
				<tr style="background-color: #623d9c; color: #ffffff;">
					<td style="width: 10%;">#</td>
					<td style="width: 45%;">Description</td>
					<td style="width: 30%;">Payment deadline</td>
					<td style="width: 15%;">Sum</td>
				</tr>
				##PORTION START##<tr style="background-color: #f0e6ff;">
					<td style="width: 10%;">1</td>
					<td style="width: 45%;">##TITLE##</td>
					<td style="width: 30%;">##DUE DATE##</td>
					<td style="width: 15%;">##PRICE##</td>
				</tr>##PORTION END##
				<tr style="background-color: #623d9c; color: #ffffff;">
					<td colspan="2"></td>
					<td style="width: 30%;">Total</td>
					<td style="width: 15%;">##SUM TOTAL##</td>
				</tr>
			</table>
			<br />
		</td>
	</tr>-->
	<tr>
		<td>
			NOTE: VAT is included in the price and is not displayed according to applicable legislation.
			<br /><br />
		</td>
	</tr>
	<tr>
		<td>
    			The booking is fully confirmed after the full payment for the booking is completed.<br />
The booking confirmation becomes a formal document for the ordered booking and is<br />
regarded as an invoice for provided services. First by finishing the booking process online,<br />
then by completing the payment the traveller agrees to and accepts the Terms and Conditions<br />
of Getit d.o.o..<br /><br />
		</td>
	</tr>
	<tr>
		<td>
			For all questions and requests we are available on email info@hardisland.com.
			<br /><br /><br />
		</td>
	</tr>
	<tr>
		<td>
			<table style="margin: 0; padding: 0; width: 100%;">
				<tr>
					<td style="width: 60%;">
						<span style="font-weight: bold;" >The booking confirmation is issued by:</span><br />
						GoNParty.eu in the name of Getit d.o.o.<br />
						Date of issue: ##DT_NOW##
						<br /><br /><br /><br /><br /><br />
					</td>
					<td style="width: 40%;">
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>