<table style="margin: 0; padding: 0; width: 80%; margin-left: 80px; margin-top: 60px; font-family:'Nexa', Arial, Helvetica, sans-serif;">
	<tr>
  	<td>
    	<img style="width: 100%;" src="http://dev.gonparty.eu/img/hardisland.png" />
    </td>
  </tr>
</table>
<table style="margin: 0; padding: 0; width: 80%; margin-left: 80px; margin-top: 60px; font-family:Arial, Helvetica, sans-serif;">
  <tr>
  	<td style="width:60%">
    	<table style="width:100%">
        <tr>
          <td style="width:100%;margin:0;padding:0;">
            <h2 style="font-weight:bold;font-size:20px;">HARD ISLAND FESTIVAL 2015</h2>
            
            <span style="font-size:20px;line-height:24px;margin:0;padding:0 0 20px;">
              <br /><b>3.-6. July 2015</b><br />
              <b>Kalypso Klub Zrce beach, Croatia</b><br />
              Issued by: <b>GoNParty</b><br />
            </span><br />
          </td>
        </tr>
        <tr>
          <td style="width:100%;margin:0;padding:0;">
            <span style="font-size:20px;line-height:24px;margin:0;padding:0 0 20px;">
              <br />Price: <b>##TICKET PRICE##</b><br />
              Order no.: <b>##ORDER.NUM##</b><br />
              Client name: <b>##PAYEE.NAME## ##PAYEE.SURNAME##</b><br />
            </span><br /><br />
          </td>
        </tr>
        <tr>
          <td style="width:100%">
            <br /><span style="font-weight:bold">Minimum age:</span> The minimum age requirement for this<br />
            event is 18+ with exception of everyone, who will<br />
            celebrate their 18th birthday in 2015, but not before<br />
            Hard Island Festival.
          </td>
        </tr>
        <tr>
          <td style="width:100%;padding-top:20px">
            <span style="font-weight:bold">Entry times:</span><br />
            <table style="width:100%">
              <tr>
                <td style="width:30%">
                  Friday:<br />
                  Saturday:<br />
                  Sunday:<br />
                  Monday:
                </td>
                <td style="width:50%">
                  9:00 - 5:00<br />
                  9:00 - 5:00<br />
                  9:00 - 5:00<br />
                  9:00 - 11:00
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td style="width:100%;padding-top:20px;">
            <span style="font-weight:bold">Attention:</span> Every ticket has a pin-code unique to the<br />
            client order number/name. Do not try to copy this ticket,<br />
            because each combination can be used only once.<br />
            Regarding questions write us at <span style="font-weight:bold">info@hardisland.com.</span>
          </td>
        </tr>
        <tr>
          <td style="width:100%;padding-top:20px;">
            <span style="font-weight:bold">Terms and conditions:</span><br />
            http://hardisland.com/tandc
          </td>
        </tr>
      </table>
    </td>
    <td style="width:15%; padding-left:5%; vertical-align:top;">
    	<table>
      	<tr>
        	<td>
      			<h1>##OFFER.TITLE## VOUCHER</h1>
      		</td>
        </tr>
      	<tr>
        	<td>
      			<p style="font-size:16px;"><b>##PACKET NAME##</b></p><br />
      		</td>
        </tr>
        <tr>
        	<td style="border:4px solid #000; text-align:center; width:15%; padding:30px; font-size:16px;">
          		<p style="font-size:11px;">Voucher no.</p>
            	<b>##ORDER.PIN##</b>
          	</td>
        </tr>
        <tr>
        	<td style="width:100%">
          		<br /><br />The voucher is not valid without the<br /> correct pin-code<br /><br />

						You will exchange this ticket for an<br /> appropriate bracelet<br /><br />

						We will check your ID before the exchange<br /><br />

						Ticket, accommodation, upgrades & boat<br /> parties have a separate voucher
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>