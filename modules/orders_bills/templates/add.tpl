<input type="hidden" id="order_id" name="orders_bill[order_id]" value="##ORDER_ID##" />

<div class="control-group">
	<label class="control-label" for="dt_valid">Valid</label>
	<div class="controls">
		<input type="text" id="dt_valid" name="orders_bill[dt_valid]" class="jdt" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="type">Type</label>
	<div class="controls">
		##TYPES LIST##
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="price">Price</label>
	<div class="controls">
		<input type="text" id="price" name="orders_bill[price]" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="payed">Payed</label>
	<div class="controls">
		<input type="text" id="payed" name="orders_bill[payed]" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="notes">Notes</label>
	<div class="controls">
    	<textarea id="notes" name="orders_bill[notes]"></textarea>
	</div>
</div>