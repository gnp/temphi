<input type="hidden" id="id" name="orders_bill[id]" value="##ID##" />

<div class="control-group">
	<label class="control-label" for="dt_added">Added</label>
	<div class="controls">
    	<span>##DT_ADDED##</span>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="dt_valid">Valid</label>
	<div class="controls">
		<input type="text" id="dt_valid" name="orders_bill[dt_valid]" value="##DT_VALID##" class="jdt" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="type">Type</label>
	<div class="controls">
		##TYPES LIST##
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="dt_confirmed">Confirmed</label>
	<div class="controls">
		<input type="hidden" id="dt_confirmed" name="orders_bill[dt_confirmed]" value="##DT_CONFIRMED##" />
    	<span>##DT_CONFIRMED##</span>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="price">Price</label>
	<div class="controls">
		<input type="text" id="price" name="orders_bill[price]" value="##PRICE##" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="payed">Payed</label>
	<div class="controls">
		<input type="text" id="payed" name="orders_bill[payed]" value="##PAYED##" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="notes">Notes</label>
	<div class="controls">
    	<textarea id="notes" name="orders_bill[notes]">##NOTES##</textarea>
	</div>
</div>