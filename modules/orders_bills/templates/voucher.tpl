<table style="margin: 0; padding: 0; width: 80%; margin-left: 80px; margin-top: 60px;">
	<tr>
  	<td>
    	<img style="width: 100%;" src="http://dev.gonparty.eu/img/hardisland.png" />
    </td>
  </tr>
  <tr>
  	<td style="width:60%">
    	<h2>HARD ISLAND FESTIVAL 2015</h2><br /><br />
      
      <span style="font-weight:bold">3.-6. July 2015</span><br />
      <span style="font-weight:bold">Kalypso Klub Zrce beach, Croatia</span><br />
      Issued by: <span style="font-weight:bold">GoNParty</span><br />
    </td>
  </tr>
  <tr>
  	<td style="width:60%">
      Price: <span style="font-weight:bold">##SUM TOTAL##</span><br />
      Order no.: <span style="font-weight:bold">##ORDER.NUM##</span><br />
      Client name: <span style="font-weight:bold">##PAYEE.NAME## ##PAYEE.SURNAME##</span><br />
    </td>
  </tr>
  <tr>
  	<td style="width:60%">
      <span style="font-weight:bold">Minimum age:</span> The minimum age requirement for this<br />
      event is 18+ with exception of everyone, who will<br />
      celebrate their 18th birthday in 2015, but not before<br />
      Hard Island Festival.
    </td>
  </tr>
  <tr>
  	<td style="width:60%">
      <span style="font-weight:bold">Entry times:</span><br />
      <table style="width:100%">
      	<tr>
        	<td style="width:50%">
          	Friday:<br />
            Saturday:<br />
            Sunday:<br />
            Monday:
          </td>
          <td style="width:50%">
          	9:00 - 5:00<br />
            9:00 - 5:00<br />
            9:00 - 5:00<br />
            9:00 - 11:00
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
  	<td style="width:60%">
      <span style="font-weight:bold">Attention:</span> Every ticket has a pin-code unique to the<br />
      client order number/name. Do not try to copy this ticket,<br />
      because each combination can be used only once.<br />
      Regarding questions write us at <span style="font-weight:bold">info@hardisland.com.</span>
    </td>
  </tr>
  <tr>
  	<td style="width:60%">
      <span style="font-weight:bold">Terms and conditions:</span><br />
      http://hardisland.com/tandc
    </td>
  </tr>
</table>