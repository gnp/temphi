<div class="row-fluid">
  <div class="span4 grayBg">
  	<div class="content upn">
  		<h3>Plačilo preko položnice</h3>
      <img alt="UPN" src="/img/upn.png">
      <div class="description">
      	<p>Plačilo ##PRICE## za rezervacijo ti zagotovi mesto na potovanju.</p>
      	<p>Plačilo preko spletne banke, na pošti ali kako drugače je potrebno opraviti v roku 24 ur od prijave.</p>
      </div>
      <div class="clear"></div>
    </div>
  </div>
  <div class="span8 grayBg">
<div class="upnsepa payee">
	<div class="type">Plačnik</div>
	<div class="con">
		<div class="wOneHalf">
			<label>IBAN:</label>
			<span>SI56 XXXX XXXX XXXX XXX (##PAYEE.NAME## ##PAYEE.SURNAME##)</span>
			<label>Referenca:</label>
			<span>##REFERENCA LONG##</span>
			<label>Naziv:</label>
			<span>##PAYEE.NAME## ##PAYEE.SURNAME##</span>
			<label>Kraj:</label>
			<span>##PAYEE.POST##</span>
			<label>Koda namena:</label>
			<span>PRCP - Plačilo izdatkov</span>
		</div>
		<div class="wOneHalf">
			<div class="stamp"></div>
			<label>Naslov:</label>
			<span>##PAYEE.ADDRESS##</span>
			<label>Država:</label>
			<span>Slovenija - 705</span>
			<label>Namen / rok plačila:</label>
			<span>##REFERENCA STRING##</span>
		</div>
	</div>
	<div style="clear: both;"></div>
</div>
<div class="upnsepa gnp">
	<div class="type">Prejemnik</div>
	<div class="con">
		<div class="wOneHalf">
			<label>Znesek:</label>
			<span>##PRICE##</span>
			<label>IBAN:</label>
			<span>SI56 0481 0011 3762 882</span>
			<label>Referenca:</label>
			<span style="clear: left; width: 8%; margin-right: 2%;">SI</span>
			<span style="clear: none; width: 8%; margin-right: 2%;">00</span>
			<span style="clear: none; width: 60%;">##REFERENCA SHORT##</span>
			<label>Naziv:</label>
			<span>RENTOR Mario Benić s.p.</span>
			<label>Kraj:</label>
			<span>8250 Brežice</span>
		</div>
		<div class="wOneHalf">
			<div class="wOneHalf" style="width: 35%;">
				<label>Datum plačila:</label>
				<span>##PAYMENT DATE##</span>
			</div>
			<div class="wOneHalf" style="width: 60%;">
				<label>BIC banke prejemnika:</label>
				<span>KBMASI2X</span>
			</div>
			<label style="margin: 48px 0px 31px 40px;">UPN - Univerzalni plačilni nalog</label>
			<label>Naslov:</label>
			<span>CPB 26a</span>
			<label>Država:</label>
			<span>Slovenija - 705</span>
		</div>
	</div>
	<div style="clear: both;"></div>
</div>
  </div>
</div>