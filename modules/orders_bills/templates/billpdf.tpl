<table style="margin: 0; padding: 0; width: 80%; margin-left: 80px; margin-top: 60px; max-width: 400px; width: 600px;">
	<tr>
		<td style="width: 100%">
			<table style="margin: 0; padding: 0; width: 100%;">
				<tr>
					<td style="width: 60%">
						<img style="width: 90%;" src="##WWW_PATH##img/logo_white.png" />
					</td>
					<td style="width: 40%">
						<span style="font-weight: bold;">Pošiljatelj</span><br />
						RENTOR, Turizem in poslovne storitve<br />
						Mario Benić s.p.<br />
						CPB 26a<br />
						8250 Brežice<br />
						web: www.gremonaparty.com<br />
						e-mail: party@gremonaparty.com<br />
						T: 040 148 148<br />
						IBAN: Si56 0481 0011 3762 882<br />
						ID. ŠT. ZA DDV: Si81835078
					</td>
				</tr>
			</table>
			<br /><br />
		</td>
	</tr>
	<tr>
		<td>
			<span style="font-weight: bold;">VOUCHER </span><br />
			<span style="font-weight: bold;">Aranžma: </span>##OFFER.TITLE##<br />
			<span style="font-weight: bold;">Destinacija: </span>##OFFER.CITY##, ##OFFER.COUNTRY##
			<br /><br /><br />
		</td>
	</tr>
	<tr>
		<td>
			<table style="margin: 0; padding: 0; width: 100%;">
				<tr>
					<td style="width: 60%;">
						<span style="font-weight: bold;" >Prejemnik</span><br />
						##PAYEE.NAME## ##PAYEE.SURNAME##<br />
						##PAYEE.ADDRESS##<br />
						##PAYEE.POST##
					</td>
					<td style="width: 40%;">
						<span style="font-weight: bold;" >Račun št.:</span>##ORDER.NUM##<br />
						<span style="font-weight: bold;" >Veza: Predračun št.:</span>##ORDER.NUM##<br />
						<br />
						<span style="font-weight: bold;" >Rok plačila:</span> plačano<br />
						<span style="font-weight: bold;" >Način plačila:</span> nakazilo<br />
						<span style="font-weight: bold;" >Datum opravljene storitve:</span> ##DT_EXECUTED##
					</td>
				</tr>
			</table>
			<br /><br />
		</td>
	</tr>
	<tr>
		<td>
			<span style="font-weight: bold;" >Podatki o naročilu</span>
			<table style="margin: 0; padding: 0; width: 100%;">
				<tr style="background-color: #623d9c; color: #ffffff;">
					<td style="width: 10%;">#</td>
					<td style="width: 45%;">Opis</td>
					<td style="width: 15%;">Količina</td>
					<td style="width: 15%;">Cena</td>
					<td style="width: 15%;">Vrednost</td>
				</tr>
				##ROW START##<tr style="background-color: #f0e6ff;">
					<td style="width: 10%;">1</td>
					<td style="width: 45%;">##TITLE##</td>
					<td style="width: 15%;">##QUANTITY##</td>
					<td style="width: 15%;">##PRICE##</td>
					<td style="width: 15%;">##SUM##</td>
				</tr>##ROW END##
				<tr style="background-color: #623d9c; color: #ffffff;">
					<td colspan="3"></td>
					<td style="width: 15%;">Skupaj</td>
					<td style="width: 15%;">##SUM TOTAL##</td>
				</tr>
			</table>
			<br />
		</td>
	</tr>
	<tr>
		<td>
			OPOMBA: DDV je obračunan in ni prikazan skladno s 98. členom ZDDV-1 – posebna ureditev.
			<br /><br />
		</td>
	</tr>
	<tr>
		<td>
			Za vsa vaša vprašanja, želje in predloge smo dosegljivi na: party@gremonaparty.com ali 040 148 148.
			<br /><br /><br />
		</td>
	</tr>
	<tr>
		<td>
			<table style="margin: 0; padding: 0; width: 100%;">
				<tr>
					<td style="width: 60%;">
						<span style="font-weight: bold;" >Predračun izdal:</span><br />
						RENTOR Mario Benić s.p.<br />
						Zastopnik: Mario Benić<br />
						Kraj izdaje: Brežice<br />
						<br /><br /><br /><br /><br /><br />
					</td>
					<td style="width: 40%;">
						<span style="font-weight: bold;" >Podpis in žig:</span><br /><br />
						<img style="width: 80%;" src="##WWW_PATH##img/podpis_zig.png" />
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>