<form id="calculateForm">
	<input type="hidden" id="id" value="##ID##" name="id" />


	<div class="row-fluid">
		<div class="span4">
			<div class="control-group">
				<label class="control-label" for="dtDeadline">Deadline date</label>
				<div class="controls">
			    	<input type="text" id="dtDeadline" name="dtDeadline" class="span8" value="##DTDEADLINE##" />
				</div>
			</div>
		</div>

		<div class="span4">
			<div class="control-group">
				<label class="control-label" for="dtCalc">Calculation date</label>
				<div class="controls">
			    	<input type="text" id="dtCalc" name="dtCalc" class="span8" value="##DTCALC##" />
				</div>
			</div>
		</div>

		<div class="span4">
			<div class="control-group">
				<label class="control-label" for="diffDays">Days difference</label>
				<div class="controls">
			    	<input type="text" id="diffDays" name="diffDays" class="span8" value="##DIFFDAYS##" />
				</div>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span4">
			<div class="control-group">
				<label class="control-label" for="maxValue">Original price</label>
				<div class="controls">
			    	<input type="text" id="maxValue" name="maxValue" class="span8" value="##MAXVALUE##" />
				</div>
			</div>
		</div>

		<div class="span4">
			<div class="control-group">
				<label class="control-label" for="currentValue">Current price</label>
				<div class="controls">
			    	<input type="text" id="currentValue" name="currentValue" class="span8" value="##CURRENTVALUE##" />
				</div>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span4">
			<div class="control-group">
				<label class="control-label" for="reservation">Reservation</label>
				<div class="controls">
			    	<input type="text" id="reservation" name="reservation" class="span8" value="##RESERVATION##" />
				</div>
			</div>
		</div>

		<div class="span4">
			<div class="control-group">
				<label class="control-label" for="expenses">Expenses</label>
				<div class="controls">
			    	<input type="text" id="expenses" name="expenses" class="span8" value="##EXPENSES##" />
				</div>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span4">
			<div class="control-group">
				<label class="control-label" for="payed">Payed</label>
				<div class="controls">
			    	<input type="text" id="payed" name="payed" class="span8" value="##PAYED##" />
				</div>
			</div>
		</div>

		<div class="span4">
			<div class="control-group">
				<label class="control-label" for="returned">Returned</label>
				<div class="controls">
			    	<input type="text" id="returned" name="returned" class="span8" value="##RETURNED##" />
				</div>
			</div>
		</div>

		<div class="span4">
			<div class="control-group">
				<label class="control-label" for="seized">Seized</label>
				<div class="controls">
			    	<input type="text" id="seized" name="seized" class="span8" value="##SEIZED##" />
				</div>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span4">
			<div class="control-group">
				<label class="control-label" for="saldo">Saldo</label>
				<div class="controls">
			    	<input type="text" id="saldo" name="saldo" class="span8" value="##SALDO##" />
				</div>
			</div>
		</div>

		<div class="span4">
			<div class="control-group">
				<label class="control-label" for="topay">To pay</label>
				<div class="controls">
			    	<input type="text" id="topay" name="topay" class="span8" value="##TOPAY##" />
				</div>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span4">
			<div class="control-group">
				<label class="control-label" for="basis">Basis</label>
				<div class="controls">
			    	<input type="text" id="basis" name="basis" class="span8" value="##BASIS##" />
				</div>
			</div>
		</div>

		<div class="span4">
			<div class="control-group">
				<label class="control-label" for="perc">Percents</label>
				<div class="controls">
			    	<input type="text" id="perc" name="perc" class="span8" value="##PERC##" />
				</div>
			</div>
		</div>

		<div class="span4">
			<div class="control-group">
				<label class="control-label" for="fullReturn">Full return</label>
				<div class="controls">
			    	<input type="checkbox" id="fullReturn" name="fullReturn" value="1" ##FULLRETURNCHECKED## />
				</div>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span4">
			<div class="control-group">
				<label class="control-label" for="maxbasisreturn">Max basis return</label>
				<div class="controls">
			    	<input type="text" id="maxbasisreturn" name="maxbasisreturn" class="span8" value="##MAXBASISRETURN##" />
				</div>
			</div>
		</div>

		<div class="span4">
			<div class="control-group">
				<label class="control-label" for="maxoverpayedreturn">Max overpayed return</label>
				<div class="controls">
			    	<input type="text" id="maxoverpayedreturn" name="maxoverpayedreturn" class="span8" value="##MAXOVERPAYEDRETURN##" />
				</div>
			</div>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="return">Notes</label>
		<div class="controls">
	    	<span>##WHATTODO##</span>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span4">
			<div class="control-group">
				<div class="controls">
			    	<input type="button" id="calculate" name="calculate" class="btn btn-success span12" value="Calculate return" />
				</div>
			</div>
		</div>

		<div class="span4">
			<div class="control-group">
				<div class="controls">
			    	<input type="button" id="addbill" name="addbill" class="btn btn-danger span12" value="Add bill" />
				</div>
			</div>
		</div>

		<div class="span4">
			<div class="control-group">
				<div class="controls">
			    	<input type="button" id="refresh" name="refresh" class="btn btn-info span12" value="Refresh after action" />
				</div>
			</div>
		</div>
	</div>

	<input type="hidden" id="actionType" value="##ACTION.TYPE##" />
	<input type="hidden" id="actionPrice" value="##ACTION.PRICE##" />
	<input type="hidden" id="actionPayed" value="##ACTION.PAYED##" />
</form>

<script type="text/javascript">
	$(document).ready(function(){
		var action = new Array();
		$("#calculate").click(function(){
			$.post("/maestro/orders_bills/postCalculateReturn", $("#calculateForm").serialize(), function(data){
				if (data.success == true) {
					$("#calculateForm").replaceWith(data.html);
				}
			}, "json");
		});

		$("#refresh").click(function(){
			redir();
		});

		$("#addbill").click(function(){
			actionType = $("#actionType").val();
			actionPrice = $("#actionPrice").val();
			actionPayed = $("#actionPayed").val();

			if (actionType == 3 || actionType == 4) {
				str = actionType == 3 
					? "vračilo"
					: "zasežena sredstva";

				if (confirm("Dodam " + str + " v vrednosti " + actionPrice + "?")) {
					$.post("/maestro/orders_bills/insert", { orders_bill: { order_id: $("#id").val(), price: actionPrice, payed: actionPayed, type: actionType }}, function(){
						$("#refresh").click();
					});
				}
			} else if (actionType < 1 || actionType > 4) {
				alert("Manjkajoči podatki.");
			} else {
				alert("Polavtomatsko lahko dodam samo vračilo in zasežena sredstva.");
			}
		});
	});
</script>