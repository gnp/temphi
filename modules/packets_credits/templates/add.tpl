
<div class="control-group">
	<label class="control-label" for="id">Id</label>
	<div class="controls">
    	<input type="text" id="id" name="offers_credits[id]" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="datetime">Datetime</label>
	<div class="controls">
    	<input type="text" id="datetime" name="offers_credits[datetime]" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="price">Price</label>
	<div class="controls">
    	<input type="text" id="price" name="offers_credits[price]" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="offer_id">OfferId</label>
	<div class="controls">
    	<input type="text" id="offer_id" name="offers_credits[offer_id]" />
	</div>
</div>
