<?php

class PacketsCreditsController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function index() {
	}
	
	function one() {
	}
	
	function maestro() {
		$q = $this->PacketsCredits->get("all");
		
		$return["title"] = "PacketsCredits";
		$return["th"] = array("ID", "");
		$return["btn header"][] = "add";
		while ($r = $this->db->f($q)) {
			$return["row"][] = array(
				"td" => array(
					$r['id'],
					$r[''],
				),
				"btn" => array("update", "delete")
			);
		}
		
		return $this->Maestro->makeListing($return);
	}
	
	function add() {
		$tpl = new Template("add", "PacketsCredits");

		return $this->Maestro->makeAdd(array(
			"content" => $tpl->display(),
			"title" => "offers_credit",
		));
	}
	
	function insert() {
		$id = $this->PacketsCredits->insert($_POST['offers_credit']);
		
		$this->Maestro->makeInsert(array("id" => $id));
	}

	function edit() {
		$tpl = new Template("edit", "PacketsCredits");

		$r = $this->PacketsCredits->get("first", NULL, array("id" => Router::get("id")));

		return $this->Maestro->makeEdit(array(
			"content" => $tpl->parse($r),
			"title" => "offers_credit",
		));
	}

	function update() {
		$this->PacketsCredits->update($_POST['offers_credit'], $_POST['offers_credit']['id']);

		return $this->Maestro->makeUpdate(array(
			"id" => $_POST['offers_credit']['id']
		));
	}

	function delete() {
		$this->PacketsCredits->delete(Router::get("id"));
	}
}

class PacketsCreditsModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "";

		$this->fields = array(
			"id" => array(
				"not null" => true,
			),
			"datetime" => array(
			),
			"price" => array(
			),
			"offer_id" => array(
				"not null" => true,
			),

		);
	}
}

?>