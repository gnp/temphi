<?php

class GamesAnswersController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function maestro() {
		$sql = "SELECT g.title, go.title AS gotitle, go.correct, ga.datetime, ga.ip, u.name, u.surname, u.email " .
				"FROM games g " .
				"INNER JOIN games_options go ON go.game_id = g.id " .
				"INNER JOIN games_answers ga ON ga.games_option_id = go.id " .
				"LEFT OUTER JOIN users u ON u.id = ga.user_id " .
				"WHERE g.id = " . Router::get("id");
		$q = $this->db->q($sql);
		
        $return["title"] = "Participants";
        $return["module"] = "games_answers";
		$return["th"] = array("Answer", "Name", "Surname", "Email", "Datetime", "IP");
		//$return["btn header"][] = "add";
		while ($r = $this->db->f($q)) {
			$return["row"][] = array(
				"td" => array(
					$r['gotitle'],
					$r['name'],
					$r['surname'],
					$r['email'],
					$r['datetime'],
					$r['ip'],
				),
				//"btn" => array("update", "delete"),
				"class" => $r['correct'] == 1 ? "success" : "error",
			);
			$return["title"] = "Participans @ " . $r['title'];
		}
		
		$return['breadcrumbs'] = $this->Maestro->makeBreadcrumbs(array(
			"Games" => "/maestro/games",
			"Edit game" => "/maestro/games/edit/" . Router::get("id"),
			"Answers" => "#",
		));
		
		return $this->Maestro->makeListing($return);
	}
}

class GamesAnswersModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "";

		$this->fields = array(
			"game_id" => array(
				"not null" => true,
			),
			"user_id" => array(
				"not null" => true,
			),
			"games_option_id" => array(
				"not null" => true,
			),
			"datetime" => array(
				"not null" => true,
				"default" => DT_NOW,
			),
			"ip" => array(
				"not null" => true,
				"default" => $_SERVER['REMOTE_ADDR'],
			),
		);
	}
}

?>