<?php

class GalleriesController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	//Print of all albums
	function gallery() {
		$tpl = new Template("gallery", "galleries");
		Core::setParseVar("css page", "gallery");
		
		$this->db->q("SELECT g.id, g.title, gp.url
										FROM galleries g
										INNER JOIN galleries_pictures gp
										 	ON g.id = gp.gallery_id
										WHERE g.dt_published BETWEEN '1970-00-00 00:00:00' AND NOW()
										GROUP BY g.id
										ORDER BY g.dt_published DESC");
		
		//Parsing single albums
		$cn=0;
		while($r = $this->db->f()) {
			$r['picture'] = images.$r['url'];
			$r['url'] = Router::make("album", array("url" => SEO::niceUrl($r['title']),"id" => $r['id']));
			
			//remove margin-left from first column
			$r['classa'] = $cn%3==0?'new':false;
			
			$tpl->parse($r, "albums");
			$cn++;
		}

		SEO::title(SITE . " galerije");
		
		//Return gallery
		return $tpl->parse(array(
			"albums" => $tpl->display("albums")
		));
	}
	
	function index() {
		$this->db->q("SELECT g.id, g.title, gp.id AS gallery_picture_id, gp.gallery_id, gp.url, gp.url_thumb
										FROM galleries g
										INNER JOIN galleries_pictures gp
											ON g.id = gp.gallery_id
										WHERE g.dt_published < NOW()
										ORDER BY RAND()
										LIMIT 4");
		
		$tpl = new Template("index", "galleries");
		
		$cn = -1;
		while($rGallery = $this->db->f()){
			$cn++;
			//if(file_exists(images.$rGallery['url_thumb'])){
			//	$rGallery['picture'] = images.$rGallery['url_thumb'];
			//}
			//else {
				$rGallery['picture'] = images.$rGallery['url'];
			//}
			$rGallery['url'] = Router::make("album", array("url" => SEO::niceUrl($rGallery['title']), "id" => $rGallery['id']));
			
			if($cn % 2 == 0) $rGallery['break'] = '</div><div class="row-fluid">';
			else $rGallery['break'] = '';
			$tpl->parse($rGallery, 'galleries');
		}
		
		$tpl->commitPart("galleries");
		return $tpl->display();
	}
	
	function onOffer($offerID) {
		if (!Validate::isInt($offerID))
			return;
		
		$qPics = $this->OffersPictures->get("all", NULL, array("offer_id" => $offerID), array("limit" => 30));
		
		if (!$this->OffersPictures->c($qPics))
			return;
		
		$tpl = new Template("onOffer", "galleries");
		
		while ($rPic = $this->db->f($qPics))
			$tpl->parse($rPic, "picture");
		
		$tpl->commit("picture");
		
		return $tpl->display();
	}
	
	//Print of an album inside the gallery
	function album() {
		$tpl = new Template("album", "galleries");
		
		//Get title of a gallery
		$qC = $this->db->q("SELECT title
										FROM galleries
										WHERE id = ".Router::getUrlData('id')."
											AND dt_published BETWEEN '1970-00-00 00:00:00' AND NOW()
										LIMIT 1");
		
		//If album was not suppose to show up or it doesn't exist
		if(!$this->db->c($qC))
			Status::code(404, "UPS, ne najdem albuma.");
		
		$rGallery = $this->db->f($qC);

		$tpl->parse($rGallery);
		
		//Parsing of all the pictures in album
		$q = $this->db->q("SELECT id, url
										FROM galleries_pictures
										WHERE gallery_id = ".Router::getUrlData('id')."
										ORDER BY position ASC");
		while($r = $this->db->f($q)) {
			$r['picture'] = images.$r['url'];
			$tpl->parse($r, "pictures");
		}
		
		$qAlbums = $this->db->q("SELECT DISTINCT(g.id), g.title AS sTitle, gp.url
										FROM galleries g
										INNER JOIN galleries_pictures gp
										 	ON g.id = gp.gallery_id
										WHERE g.dt_published BETWEEN '1970-00-00 00:00:00' AND NOW()
											AND g.id != ".Router::getUrlData('id')."
										GROUP BY g.id
										ORDER BY g.dt_published DESC");
		
		//Parsing single albums
		$cn=0;
		while($r = $this->db->f($qAlbums)) {
			$r['picture'] = images.$r['url'];
			$r['url'] = Router::make("album", array("url" => SEO::niceUrl($r['sTitle']),"id" => $r['id']));
			
			//remove margin-left from first column
			$r['classa'] = $cn%3==0?'new':false;
			
			$tpl->parse($r, "albums");
			$cn++;
		}
		
		
		
		//Return
		Core::setParseVar("css page", "album");

		SEO::title($rGallery['title'] . " z " . SITE);
		
		return $tpl->parse(array(
			$tpl->display("news"),
			"pictures" => $tpl->display("pictures"),
			"albums" => $tpl->display("albums"),
			"content right" => Core::loadView("sidebar","offers"),
		));
		
	}
	
	function maestro() {
		$q = $this->Galleries->get("all");
		
		$return["title"] = "Galleries";
		$return["th"] = array("ID", "Title", "Status");
		$return["btn header"][] = "add";
		while ($r = $this->db->f($q)) {
			$return["row"][] = array(
				"td" => array(
					$r['id'],
					$r['title'],
					'<span class="btnToggle btn btn-small" data-url="/galleries/togglepublished/' . $r['id'] . '/" data-value="' . $r['dt_published'] . '"><i class="icon-check"></i></span>',
				),
				"btn" => array("update", "delete")
			);
		}
		
		return $this->Maestro->makeListing($return);
	}

	function togglepublished() {
		return $this->Galleries->update(array(
			"dt_published" => Router::get("active") ? DT_NOW : DT_NULL,
		), Router::get("id"));
	}
	
	function add() {
		$tpl = new Template("add", "Galleries");

		return $this->Maestro->makeAdd(array(
			"content" => $tpl->parse(array(
				"dt_published" => DT_NOW,
			)),
			"title" => "gallery",
		));
	}
	
	function insert() {
		$id = $this->Galleries->insert($_POST['gallery']);
		
		$this->Maestro->makeInsert(array("id" => $id));
	}

	function edit() {
		$tpl = new Template("edit", "Galleries");

		$r = $this->Galleries->get("first", NULL, array("id" => Router::get("id")));
		
		Core::setParseVar("multiupload id", $r['id']);
			
		return $this->Maestro->makeEdit(array(
			"content" => $tpl->parse($r) .
				$this->mediaEdit(array("gallery_id" => $r['id'])) .
				Core::loadView("edit", "tags", array("multiple" => "galleries", "single" => "gallery", "id" => $r['id'])),
			"title" => "gallery",
		));
	}
	
	function mediaEdit($data = array()) {
		if (!isset($data['gallery_id']) || !Validate::isInt($data['gallery_id']))
			return NULL;
			
		$q = $this->GalleriesPictures->get("all", NULL, array("gallery_id" => $data['gallery_id']), array("order by" => "position"));
		
		$tpl = new Template("mediaedit", "galleries");
		while ($r = $this->db->f($q)) {
			$tpl->parse(array(
				"delete url" => "/maestro/galleries_pictures/delete/" . $r['id'],
				"src" => "/media/" . $r['url'],
				"id" => $r['id'],
			), "media");
		}
		
		return $tpl->parse(array(
			"media" => $tpl->display("media"),
			"gallery id" => $data['gallery_id'],
		));
	}

	function update() {
		$this->Galleries->update($_POST['gallery'], $_POST['gallery']['id']);

		Core::loadView("update", "tags", array("multiple" => "galleries", "single" => "gallery", "id" => $_POST['gallery']['id']));

		return $this->Maestro->makeUpdate(array(
			"id" => $_POST['gallery']['id']
		));
	}

	function delete() {
		$this->Galleries->delete(Router::get("id"));
	}
	
	function updatemediapositions() {
		Core::loadLayout(NULL);

		foreach ($_POST['positions'] AS $position => $id)
			$this->GalleriesPictures->update(array(
				"position" => $position,
			), $id);
	}
}

class GalleriesModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->countForms['min'] = "gallery";
		
		$this->mk = "title";
		
		$this->fields = array(
			"title" => array(
				"not null" => true,
			),
			"dt_published" => array(
				"default" => date("Y-m-d H:i:s"),
			),
		);
	}
}

?>