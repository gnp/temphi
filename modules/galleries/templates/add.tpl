
<div class="control-group">
	<label class="control-label" for="title">Title</label>
	<div class="controls">
    	<input type="text" id="title" name="gallery[title]" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="dt_published">Published</label>
	<div class="controls">
    	<input type="text" id="dt_published" name="gallery[dt_published]" class="jdt span6" value="##DT_PUBLISHED##" />
	</div>
</div>