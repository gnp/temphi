<script>
  $(window).load(function() {
    resize(".gallery img", 0.98);
  });
  $(document).ready(function(){
    resize(".gallery img", 0.98);
  });
  $(window).resize(function(){
    resize(".gallery img", 0.98);
  });
</script>
<div class="gallery row-fluid" id="gallery-module">
  <div class="row-fluid">
    ##GALLERIES START##
    ##BREAK##
    <div class="span6">
      <a href="##URL##" title="##TITLE##">
        <img alt="/" src="/cache/img/w/125##PICTURE##" />
      </a>
    </div>
    ##GALLERIES END##
  </div>
</div><!-- /gallery-module -->