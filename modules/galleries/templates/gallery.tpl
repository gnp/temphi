<h1>GNP galerija <img src="/img/icons/gallery.png"></h1>
<script>
  $(window).load(function() {
    resize(".picture-frame a img", 0.37);
  });
  $(document).ready(function(){
    resize(".picture-frame a img", 0.37);
  });
  $(window).resize(function(){
    resize(".picture-frame a img", 0.37);
  });
</script>
<div class="row-fluid">
	##ALBUMS START##
	<div class="span4 picture-frame ##CLASSA##" style="overflow: hidden;">
    <a href="##URL##"><img src="/cache/img/w/370##PICTURE##" title="##TITLE##" /></a>
    <div class="heading">
      <div class="background">
      </div>
      <a href="##URL##"><p>##TITLE##</p></a>
    </div>
  </div>
  ##ALBUMS END##
</div>
<div class="clear"></div>