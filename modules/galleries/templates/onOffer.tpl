<!-- modal-gallery is the modal dialog used for the image gallery -->
<div id="modal-gallery" class="modal modal-gallery hide fade" tabindex="-1">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
    </div>
    <div class="modal-body"><div class="modal-image"></div></div>
</div>

<div class="gallery grayBg whiteA" id="fotke">
	<h2>CHECKOUT GALLERY</h2>
	<div class="" data-toggle="modal-gallery" data-target="#modal-gallery">
		##PICTURE START##
    	<div class="imageBox">
      	<div>
      		<a href="/media/##URL##" data-gallery="gallery"> <img class="first" src="/cache/img/w/105/media/##URL##" /></a>
      	</div>
      </div>
    ##PICTURE END##
	</div>
  <div class="clear"></div>
</div><!-- /.gallery-->