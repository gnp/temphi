<!-- modal-gallery is the modal dialog used for the image gallery -->
<div id="modal-gallery" class="modal modal-gallery hide fade" tabindex="-1">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <div class="path">
        	<a href="/galerija">Galerija</a> > <a data-dismiss="modal">##TITLE##</a>
					<div class="fb-like" data-href="https://www.gremonaparty.com##URL##" data-layout="button_count" data-send="false" data-width="150" data-show-faces="true" data-font="arial"></div>
        </div>
    </div>
    <div class="modal-body"><div class="modal-image"></div></div>
</div>

<div class="titleH">
  <a class="title" href="/galerija">
    <h1>GNP GALERIJE</h1>
    <img src="/img/icons/gallery.png">
  </a>
  <h2>> ##TITLE##</h2>
</div>
<div class="row-fluid">
	<div class="span7">
    <div class="album-list" data-toggle="modal-gallery" data-target="#modal-gallery">
      ##PICTURES START##
      <div class="image-frame">
        <a href="##PICTURE##" data-gallery="gallery"><img src="/cache/img/w/128##PICTURE##" /></a>
      </div>
      ##PICTURES END##
    </div>
      <div class="clear"></div>
	</div>
  <div class="span4 offset1">
  	##CONTENT RIGHT##
  </div>
</div>
<div class="row gallery">
	<h2>Sorodne galerije <img src="/img/icons/gallery.png" /></h2>
  ##ALBUMS START##
  <div class="span4 picture-frame ##CLASSA##">
    <a href="##URL##"><img src="##PICTURE##" title="##STITLE##" /></a>
    <div class="heading">
      <div class="background">
      </div>
      <a href="##URL##"><p>##STITLE##</p></a>
    </div>
  </div>
  ##ALBUMS END##
</div>
<div class="clear"></div>
<script>
  $(window).load(function() {
    resize(".picture-frame a img", 0.55);
  });
  $(document).ready(function(){
    resize(".picture-frame a img", 0.55);
  });
  $(window).resize(function(){
    resize(".picture-frame a img", 0.55);
  });
</script>