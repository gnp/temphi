<input type="hidden" id="id" name="addition[id]" value="##ID##" />

<div class="control-group">
	<label class="control-label" for="title">Addition</label>
	<div class="controls">
    	<input type="text" id="title" name="addition[title]" value="##TITLE##" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="value">Default value</label>
	<div class="controls">
    	<input type="text" id="value" name="addition[value]" value="##VALUE##" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="short">Short</label>
	<div class="controls">
    	<input type="text" id="short" name="addition[short]" value="##SHORT##" class="span6" />
	</div>
</div>

<div class="control-group">
  <label class="control-label" for="description">Description</label>
  <div class="controls">
      <input type="text" id="description" name="addition[description]" class="span6" value="##DESCRIPTION##" />
  </div>
</div>