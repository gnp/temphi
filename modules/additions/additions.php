<?php

class AdditionsController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function index() {
		$i = 0;
		$i++;
		$i++;
		return $i;
	}
    
	function one() {
	}
	
	function maestro() {
		$q = $this->Additions->get("all", NULL, NULL, array("order by" => "position"));
		
		$return["title"] = "Additions";
		$return["th"] = array("ID", "Title");
		$return["btn header"][] = "add";
		$return["tbody class"] = "sortable";
		while ($r = $this->db->f($q)) {
			$return["row"][] = array(
				"td" => array(
					$r['id'],
					$r['title'],
				),
				"btn" => array("update", "delete"),
			);
		}
		$tplSort = new Template("onsortupdate", "additions");
		$return['append'] = $tplSort->display();
		
		return $this->Maestro->makeListing($return);
	}
	
	function add() {
		$tpl = new Template("add", "Additions");

		return $this->Maestro->makeAdd(array(
			"content" => $tpl->display(),
			"title" => "addition",
		));
	}
	
	function insert() {
		$id = $this->Additions->insert($_POST['addition']);
		
		$this->Maestro->makeInsert(array("id" => $id));
	}

	function edit() {
		$tpl = new Template("edit", "Additions");

		$r = $this->Additions->get("first", NULL, array("id" => Router::get("id")));

		return $this->Maestro->makeEdit(array(
			"content" => $tpl->parse($r),
			"title" => "addition",
		));
	}

	function update() {
		$this->Additions->update($_POST['addition'], $_POST['addition']['id']);

		return $this->Maestro->makeUpdate(array(
			"id" => $_POST['addition']['id']
		));
	}

	function delete() {
		$this->Additions->delete(Router::get("id"));
	}
	
	function updatepositions() {
		foreach ($_POST['positions'] AS $position => $id)
			$this->Additions->update(array(
				"position" => $position
			), $id);
	}
}

class AdditionsModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "title";

		$this->fields = array(
			"title" => array(
				"not null" => true,
			),
      "value" => array(
      ),
      "short" => array(
      ),
      "description" => array(
      ),
      "position" => array(
      	"default" => 1,
      ),
		);
	}
}

?>