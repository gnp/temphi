<?php

class OrdersUsersAdditionsController extends UFWController {
	function __construct() {
		parent::__construct();
	}
}

class OrdersUsersAdditionsModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "";

		$this->fields = array(
			"orders_user_id" => array(
				"not null" => true,
			),
			"addition_id" => array(
				"not null" => true,
			),

		);
	}
}

?>