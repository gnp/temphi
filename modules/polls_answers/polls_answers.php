<?php

class PollsAnswersController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function index() {
	}
	
	function one() {
	}
	
	function maestro() {
		$sql = "SELECT p.title, po.title AS potitle, pa.datetime, pa.ip " .
				"FROM polls p " .
				"INNER JOIN polls_options po ON po.poll_id = p.id " .
				"INNER JOIN polls_answers pa ON pa.polls_option_id = po.id " .
				"INNER JOIN users u ON u.id = pa.user_id " .
				"WHERE p.id = " . Router::get("id");
		$q = $this->db->q($sql);
		
		$return["th"] = array("Answer", "Datetime", "IP");
		//$return["btn header"][] = "add";
		while ($r = $this->db->f($q)) {
			$return["row"][] = array(
				"td" => array(
					$r['potitle'],
					$r['datetime'],
					$r['ip'],
				),
				//"btn" => array("update", "delete"),
			);
			$return["title"] = "Participans @ " . $r['title'];
		}
		
		$return['breadcrumbs'] = $this->Maestro->makeBreadcrumbs(array(
			"Polls" => "/maestro/polls",
			"Edit poll" => "/maestro/polls/edit/" . Router::get("id"),
			"Answers" => "#",
		));
		
		return $this->Maestro->makeListing($return);
	}
}

class PollsAnswersModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "";

		$this->fields = array(
			"poll_id" => array(
				"not null" => true,
			),
			"user_id" => array(
				"not null" => true,
			),
			"polls_option_id" => array(
				"not null" => true,
			),
			"datetime" => array(
				"not null" => true,
			),
			"ip" => array(
				"not null" => true,
			),
		);
	}
}

?>