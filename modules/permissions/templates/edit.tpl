<input type="hidden" id="id" name="permissions[id]" value="##ID##" />

<div class="control-group">
	<label class="control-label" for="id">Id</label>
	<div class="controls">
    	<input type="text" id="id" name="permissions[id]" value="##ID##" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="status_id">StatusId</label>
	<div class="controls">
    	<input type="text" id="status_id" name="permissions[status_id]" value="##STATUS_ID##" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="title">Title</label>
	<div class="controls">
    	<input type="text" id="title" name="permissions[title]" value="##TITLE##" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="type">Type</label>
	<div class="controls">
    	<input type="text" id="type" name="permissions[type]" value="##TYPE##" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="permission">Permission</label>
	<div class="controls">
    	<input type="text" id="permission" name="permissions[permission]" value="##PERMISSION##" />
	</div>
</div>
