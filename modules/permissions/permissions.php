<?php

class PermissionsController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function index() {
	}
	
	function one() {
	}
	
	function maestro() {
		$q = $this->Permissions->get("all");
		
		$return["title"] = "Permissions";
		$return["th"] = array("ID", "title");
		$return["btn header"][] = "add";
		while ($r = $this->db->f($q)) {
			$return["row"][] = array(
				"td" => array(
					$r['id'],
					$r['title'],
				),
				"btn" => array("update", "delete")
			);
		}
		
		return $this->Maestro->makeListing($return);
	}
	
	function add() {
		$tpl = new Template("add", "Permissions");

		return $this->Maestro->makeAdd(array(
			"content" => $tpl->display(),
			"title" => "permission",
		));
	}
	
	function insert() {
		$id = $this->Permissions->insert($_POST['permission']);
		
		$this->Maestro->makeInsert(array("id" => $id));
	}

	function edit() {
		$tpl = new Template("edit", "Permissions");

		$r = $this->Permissions->get("first", NULL, array("id" => Router::get("id")));

		return $this->Maestro->makeEdit(array(
			"content" => $tpl->parse($r),
			"title" => "permission",
		));
	}

	function update() {
		$this->Permissions->update($_POST['permission'], $_POST['permission']['id']);

		return $this->Maestro->makeUpdate(array(
			"id" => $_POST['permission']['id']
		));
	}

	function delete() {
		$this->Permissions->delete(Router::get("id"));
	}
}

class PermissionsModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "title";

		$this->fields = array(
			"id" => array(
				"not null" => true,
			),
			"status_id" => array(
				"not null" => true,
			),
			"title" => array(
			),
			"type" => array(
				"default" => "-1",
			),
			"permission" => array(
				"not null" => true,
			),

		);
	}
}

?>