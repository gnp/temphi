<?php

class OffersIncludesController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function edit($data = array()) {
		if (!isset($data['offer_id']) || !Validate::isInt($data['offer_id']))
			return;
		
		$tpl = new Template("edit", "offers_includes");
			
		$sql = "SELECT a.id, a.title, a.value AS defvalue, oi.value AS setvalue, oi.id AS oiid " .
			"FROM additions a " .
			"LEFT OUTER JOIN offers_includes oi ON (oi.addition_id = a.id AND oi.offer_id = " . $data['offer_id'] .  ") " .
			"LEFT OUTER JOIN offers o ON (o.id = oi.offer_id AND o.id = " . $data['offer_id'] .  ") " .
			"ORDER BY a.position";
		$this->db->q($sql);
		
		while ($r = $this->db->f()) {
			$r['cb addition'] = is_null($r['oiid'])
				? NULL
				: ' checked="checked"';
			
			$r['value'] = !is_null($r['oiid'])
				? $r['setvalue']
				: $r['defvalue'];
			
			$r['disabled'] = is_null($r['oiid'])
				? ' disabled="disabled"'
				: NULL;
			
			$tpl->parse($r, "addition");
		}
		
		return $tpl->parse(array(
			"addition" => $tpl->display("addition")
		));
	}

	function update() {
		if (!Validate::isInt($_POST['offer']['id']))
			return;
		
		$sql = "DELETE FROM offers_includes WHERE offer_id = " . $_POST['offer']['id'] . (isset($_POST['offers_include']) && !empty($_POST['offers_include']) ? " AND addition_id NOT IN(" . implode(",", array_keys($_POST['offers_include'])) . ")" : NULL);
		$this->db->q($sql);
		
		if (isset($_POST['offers_include']))
		foreach ($_POST['offers_include'] AS $addition_id => $setval) {
			if (!Validate::isInt($addition_id))
				continue;
			
			//$sqlCheck = "SELECT id FROM offers_includes WHERE addition_id = " . $addition_id . " AND offer_id = " . $_POST['offer']['id'];
			//$qCheck = $this->db->q($sqlCheck);
			//$rCheck = $this->db->f($qCheck);
			
			$rCheck = $this->OffersIncludes->get("first", NULL, array("addition_id" => $addition_id, "offer_id" => $_POST['offer']['id']));
			
			if (empty($rCheck))
				$this->OffersIncludes->insert(array(
					"offer_id" => $_POST['offer']['id'],
					"addition_id" => $addition_id,
					"value" => $setval["value"],
				));
			else
				$this->OffersIncludes->update(array(
					"value" => $setval["value"],
				), $rCheck['id']);
		}

	}
}

class OffersIncludesModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "";

		$this->fields = array(
			"addition_id" => array(
				"not null" => true,
			),
			"offer_id" => array(
				"not null" => true,
			),
			"value" => array(
			),

		);
	}
}

?>