<!--<div class="row-fluid">
	<div class="span6">-->
		<legend>Includes</legend>
		##ADDITION START##
		<div class="control-group">
			<label class="control-label" for="offers_include_cb_##ID##">##TITLE##</label>
			<div class="controls">
				<div class="input-prepend divCbIncludes">
					<span class="add-on">
		    			<input type="checkbox" value="1" id="offers_include_cb_##ID##" name="offers_include[##ID##][addition_id]"##CB ADDITION## />
		    		</span>
		    		<input type="text" id="offers_include_##ID##" name="offers_include[##ID##][value]" value="##VALUE##"##DISABLED##/>
				</div>
			</div>
		</div>
		##ADDITION END##
<!--	</div>
</div>-->
<script type="text/javascript">
	$(document).ready(function(){
		$(".divCbIncludes span input").change(function(){
			if ($(this).attr("checked") == "checked") {
				$(this).parent().next().attr("disabled", false);
			} else {
				$(this).parent().next().attr("disabled", true);
			}
		});
	});
</script>