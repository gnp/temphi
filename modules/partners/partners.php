<?php

class PartnersController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function index() {
	}
	
	function official() {
		$tpl = new Template("official", "partners");
		
		return $tpl->display();
	}
	
	function footer() {
		$tpl = new Template("footer", "partners");
		
		$this->db->q("SELECT title, picture, url
										FROM partners
										WHERE published = 1
										ORDER BY RAND()
										LIMIT 4");
		while($r = $this->db->f()) {
			$r['picture'] = imagePartners.$r['picture'];
			
			$tpl->parse($r,"list");
		}
		$tpl->commit("list");
		
		return $tpl->display();
	}
	
	function one() {
	}
	
	function maestro() {
		$q = $this->Partners->get("all");
		
		$return["title"] = "Partners";
		$return["th"] = array("ID", "Title", "Status");
		$return["btn header"][] = "add";
		while ($r = $this->db->f($q)) {
			$return["row"][] = array(
				"td" => array(
					$r['id'],
					$r['title'],
					'<span class="btnToggle btn btn-small" data-url="/partners/togglepublished/' . $r['id'] . '/" data-value="' . $r['published'] . '"><i class="icon-check"></i></span>',
				),
				"btn" => array("update", "delete")
			);
		}
		
		return $this->Maestro->makeListing($return);
	}

	function togglepublished() {
		return $this->Partners->update(array(
			"published" => Router::get("active"),
		), Router::get("id"));
	}
	
	function add() {
		$tpl = new Template("add", "Partners");

		return $this->Maestro->makeAdd(array(
			"content" => $tpl->display(),
			"title" => "partner",
		));
	}
	
	function insert() {
		$id = $this->Partners->insert($_POST['partner']);
		
		$this->Maestro->makeInsert(array("id" => $id));
	}

	function edit() {
		$tpl = new Template("edit", "Partners");

		$r = $this->Partners->get("first", NULL, array("id" => Router::get("id")));

		$r['cb published'] = $r['published'] == 1
			? ' checked="checked"'
			: NULL;
		$r['picture2'] = "/media/" . $r['picture'];
		$r['pic display'] = !empty($r['picture']) ? "block" : "none";
			
		return $this->Maestro->makeEdit(array(
			"content" => $tpl->parse($r),
			"title" => "partner",
		));
	}

	function update() {
		$this->Partners->update($_POST['partner'], $_POST['partner']['id']);

		return $this->Maestro->makeUpdate(array(
			"id" => $_POST['partner']['id']
		));
	}

	function delete() {
		$this->Partners->delete(Router::get("id"));
	}
}

class PartnersModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "title";
		
		$this->fields = array(
			"title" => array(
				"not null" => true,
			),
			"picture" => array(
			),
			"url" => array(
				"not null" => true,
			),
			"published" => array(
				"default" => 1,
			),
		);
	}
}

?>