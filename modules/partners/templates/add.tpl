<div class="control-group">
	<label class="control-label" for="title">Title</label>
	<div class="controls">
    	<input type="text" id="title" name="partner[title]" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="url">URL</label>
	<div class="controls">
    	<input type="text" id="url" name="partner[url]" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="picture">Picture</label>
	<div class="controls">
    	<input type="hidden" id="picture" name="partner[picture]" />
		<input type="button" class="btn btn-primary elFinderData" value="Open file browser" data-id="" data-files="" data-multi="-1" data-replace="newImageField" data-field="partner[picture]" data-url="" />
		<img src="" style="display: none; margin-top: 10px;" id="newImageField" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="published">Published</label>
	<div class="controls">
		<input type="hidden" id="publishedCB" name="partner[published]" value="-1" />
    	<input type="checkbox" id="published" name="partner[published]" value="1" />
	</div>
</div>
