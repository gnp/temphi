<div class="partners">
  <h3>Partnerji</h3>
  ##LIST START##
  <a href="##URL##" target="_blank"><img alt="##TITLE##" src="##PICTURE##" /></a>
  ##LIST END##
  <div class="partyinlj">
  	<a href="http://www.pubcrawls.eu/ljubljana">Pub Crawl Ljubljana</a>
  </div>
  <a href="http://drogart.org" target="_blank" style="margin-top: 15px; display: block; color: #fff; width: 49%; float: left;">
    <img src="/img/drogart.png" /><span style="margin-left: 10px;">Združenje Drogart</span>
  </a>
  <a href="http://moneta.si" target="_blank" style="margin-top: 12px; display: block; color: #fff; width: 49%; float: left;">
    <img src="/media/other/moneta-top-logo.png" /><span>Tu lahko plačaš z Moneto!</span>
  </a>
</div>