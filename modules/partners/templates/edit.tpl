<input type="hidden" id="id" name="partner[id]" value="##ID##" />

<div class="control-group">
	<label class="control-label" for="title">Title</label>
	<div class="controls">
    	<input type="text" id="title" name="partner[title]" value="##TITLE##" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="url">URL</label>
	<div class="controls">
    	<input type="text" id="url" name="partner[url]" value="##URL##" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="picture">Picture</label>
	<div class="controls">
    	<input type="hidden" id="picture" name="partner[picture]" value="##PICTURE##" />
		<input type="button" class="btn btn-primary elFinderData" value="Open file browser" data-id="" data-files="" data-multi="-1" data-replace="newImageField" data-field="partner[picture]" data-url="" />
		<img src="##PICTURE2##" id="newImageField" style="display: ##PIC DISPLAY##; margin-top: 10px;" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="published">Published</label>
	<div class="controls">
		<input type="hidden" id="publishedCB" name="partner[published]" value="-1" />
    	<input type="checkbox" id="published" name="partner[published]" value="1"##CB PUBLISHED## />
	</div>
</div>

