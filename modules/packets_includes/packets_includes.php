<?php

class PacketsIncludesController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function edit($data = array()) {
		if (!isset($data['packet_id']) || !Validate::isInt($data['packet_id']))
			return;
		
		$tpl = new Template("edit", "packets_includes");
			
		$sql = "SELECT a.id, a.title, a.value AS defvalue, pi.value AS setvalue, pi.id AS piid " .
			"FROM additions a " .
			"LEFT OUTER JOIN packets_includes pi ON (pi.addition_id = a.id AND pi.packet_id = " . $data['packet_id'] .  ") " .
			"LEFT OUTER JOIN packets p ON (p.id = pi.packet_id AND p.id = " . $data['packet_id'] .  ") " .
			"ORDER BY a.position";
		$this->db->q($sql);
		
		while ($r = $this->db->f()) {
			$r['cb addition'] = is_null($r['piid'])
				? NULL
				: ' checked="checked"';
			
			$r['value'] = !is_null($r['piid'])
				? $r['setvalue']
				: $r['defvalue'];
			
			$r['disabled'] = is_null($r['piid'])
				? ' disabled="disabled"'
				: NULL;
			
			$tpl->parse($r, "addition");
		}
		
		return $tpl->parse(array(
			"addition" => $tpl->display("addition")
		));
	}

	function update() {
		if (!Validate::isInt($_POST['packet']['id']))
			return;
		
		$sql = "DELETE FROM packets_includes WHERE packet_id = " . $_POST['packet']['id'] . (isset($_POST['packets_include']) && !empty($_POST['packets_include']) ? " AND addition_id NOT IN(" . implode(",", array_keys($_POST['packets_include'])) . ")" : NULL);
		$this->db->q($sql);
		
		if (isset($_POST['packets_include']))
		foreach ($_POST['packets_include'] AS $addition_id => $setval) {
			if (!Validate::isInt($addition_id))
				continue;
			
			//$sqlCheck = "SELECT id FROM packets_includes WHERE addition_id = " . $addition_id . " AND packet_id = " . $_POST['packet']['id'];
			//$qCheck = $this->db->q($sqlCheck);
			//$rCheck = $this->db->f($qCheck);
			
			$rCheck = $this->PacketsIncludes->get("first", NULL, array("addition_id" => $addition_id, "packet_id" => $_POST['packet']['id']));
			
			if (empty($rCheck))
				$this->PacketsIncludes->insert(array(
					"packet_id" => $_POST['packet']['id'],
					"addition_id" => $addition_id,
					"value" => $setval["value"],
				));
			else
				$this->PacketsIncludes->update(array(
					"value" => $setval["value"],
				), $rCheck['id']);
		}

	}
}

class PacketsIncludesModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "";

		$this->fields = array(
			"addition_id" => array(
				"not null" => true,
			),
			"packet_id" => array(
				"not null" => true,
			),
			"value" => array(
			),

		);
	}
}

?>