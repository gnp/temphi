<?php

class CronController extends UFWController {
  function __construct() {
    parent::__construct();
  }

  function cron() {
  }

  function log($msg, $e, $fatal = false) {
    $content = $msg . ($e ? json_encode($e) : '');

    file_put_contents($content, LOG_PATH . date("Ymdhis") . "-" . substr(sha1(microtime()), 5, 5) . substr(sha1($content), 5, 5));
  
    if ($fatal) {
      $mail = new UFWMail();
      $mail->Subject('GNP Cron failed')->To('Bojan Rajh <schtr4jh@schtr4jh.net>')->Body($content)->Send();
    }
  }

  function setPayedOrders() {
    $sqlOrders = "SELECT o.id, SUM(IF(ob.dt_confirmed > '" . DT_NULL . "', 1, 0)) AS payed, COUNT(ob.id) AS a, of.dt_start, o.price " .
      "FROM orders o " .
      "INNER JOIN offers of ON of.id = o.offer_id " .
      "INNER JOIN orders_bills ob ON ob.order_id = o.id " .
      "WHERE o.dt_confirmed > '" . DT_NULL . "' " .
      "AND o.dt_payed <= '" . DT_NULL . "' " .
      "AND o.dt_payed <= '" . DT_NULL . "' " .
      "AND ob.type IN (1,2) " .
      "GROUP BY o.id " .
      "HAVING SUM(IF(ob.dt_confirmed > '" . DT_NULL . "', 1, 0)) = COUNT(ob.id) " .
      "AND ROUND(SUM(ob.price), 2) = ROUND(o.price, 2)";
  }

  function backupFiles() {
    $folders = array(
      "cache" => WWW_PATH . "cache",
    );

    foreach ($folders AS $key => $path) {
      $success = true;
      $data = null;
      try {
        $file_name = $key . '-' . date("Ymdhis") . ".tar";
        $archive_name = APP_PATH . 'backups' . DS . 'files' . DS . $file_name;
        $dir_path = $path;

        $archive = new PharData($archive_name);
        $archive->buildFromDirectory($dir_path);
        $archive->compress(Phar::GZ);
        unlink($archive_name);

        if (!is_file($archive_name)) {
          $this->log('Cannot create ' . $key . ' backup', null, true);
          $success = false;
        } else {
          $this->remoteBackup($archive_name . ".gz", $file_name . ".gz");
        }
      } catch (Exception $e) {
        $this->log('Cannot create ' . $key . ' backup', $e, true);
        $success = false;
        $data = $e;
      }
    }
    
    return $this->output(array("success" => (bool)$success, "data" => $data));
  }

  function backupDb() {
    $success = true;
    $data = null;
    try {
      $file = date("Ymdhis") . '.sql';
      $filename = APP_PATH . 'backups' . DS . 'db' . DS . $file;
      $config = Settings::get("db_config");

      $result = exec('mysqldump ' . $config['db'] . ' --password=' . $config['pass'] . ' --user=' . $config['user'] . ' --single-transaction > ' . $filename, $output);

      if ($output) {
        $this->log('Cannot create DB backup', $output, true);
        $success = false;
        $data = $output;
      } else {
        $this->remoteBackup($filename, $file);
      }
    } catch (Exception $e) {
      $this->log('Cannot create DB backup', $e, true);
      $success = false;
      $data = $e;
    }
    return $this->output(array("success" => (bool)$success, "data" => $data));
  }

  function backupBackups() {

  }

  function remoteBackup($archive_name, $file_name) {
    $file = $archive_name;
    $remote_file = "/backups/" . $file_name;

    // set up basic connection
    $conn_id = ftp_ssl_connect("schtr4jh.net");

    // login with username and password
    $login_result = ftp_login($conn_id, "schtr4jh", "xkce569f");

    // passive mode
    ftp_pasv($conn_id, true);

    // upload a file
    $ok = ftp_put($conn_id, $remote_file, $file, FTP_ASCII);

    // close the connection
    ftp_close($conn_id);


    if (!$ok) {
      $this->log('Cannot create remote backup ' . $archive_name . ":" . $file_name, null, true);
    }

    return $ok;
  }

  function warnLateReservations() {
    $success = $this->Orders->warnLateReservations($data);

    return $this->output(array("success" => (bool)$success, "data" => $data));
  }

  function output($output) {
    die(json_encode($output));
  }
}

?>