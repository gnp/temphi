<input type="hidden" id="packet_id" name="packets_tab[packet_id]" value="##PACKET_ID##" />

<div class="control-group">
	<label class="control-label" for="predefinder">Predefined</label>
	<div class="controls">
    	##PREDEFINED LIST##
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="title">Title</label>
	<div class="controls">
    	<input type="text" id="title" name="packets_tab[title]" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="content">Content</label>
	<div class="controls">
    	<textarea name="packets_tab[content]" id="content" class="mceEditor"></textarea>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="published">Published</label>
	<div class="controls">
		<input type="hidden" value="-1" id="published_cb" name="packets_tab[published]" />
    	<input type="checkbox" value="1" id="published" name="packets_tab[published]" />
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$("#tabs_predefined").change(function(){
			if ($(this).val() != -1) {
				$.post("/packets_tabs/getpredefined", {name: $(this).val()}, function(data){
					data = fromJSON(data);
					
					$("#content").val(data['svalue']);
					$("#title").val(data['title']);
					tinyMCE.activeEditor.setContent(data['svalue']);
					validator._validateForm();
				});
			}
		});
	});
</script>