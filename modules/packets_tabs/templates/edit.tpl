<input type="hidden" id="id" name="packets_tab[id]" value="##ID##" />

<div class="control-group">
	<label class="control-label" for="title">Title</label>
	<div class="controls">
    	<input type="text" id="title" name="packets_tab[title]" value="##TITLE##" class="span6" />
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="content">Content</label>
	<div class="controls">
    	<textarea name="packets_tab[content]" id="content" class="mceEditor">##CONTENT##</textarea>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="published">Published</label>
	<div class="controls">
		<input type="hidden" value="-1" id="published_cb" name="packets_tab[published]" />
    	<input type="checkbox" value="1" id="published" name="packets_tab[published]" ##CB PUBLISHED## />
	</div>
</div>
