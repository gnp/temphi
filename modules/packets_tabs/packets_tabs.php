<?php

class PacketsTabsController extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function index() {
	}
	
	function one() {
	}

	function onOffer($packetID) {
		$qTabs = $this->PacketsTabs->get("all", NULL, array("packet_id" => $packetID, "published" => 1), array("order by" => "position"));

		$tpl = new Template("offers_one", "packets_tabs");

		while ($rTab = $this->db->f($qTabs)) {
			$tpl->parse($rTab, "row");
		}

		return $tpl->parse(array(
			"row" => $tpl->display("row"),
		));
	}
	
	function maestro($data = array()) {
    	if (!Validate::isInt($data['packet_id']))
			return NULL;
		
		$q = $this->PacketsTabs->get("all", NULL, array("packet_id" => $data['packet_id']), array("order by" => "PacketsTabs.position"));
		
		$return["title"] = "Tabs";
        $return["module"] = "packets_tabs";
		$return["th"] = array("ID", "Title", "Status");
        $return["btn header"][] = array(
			"txt" => '<i class="icon-plus"/>',
			"url" => "/maestro/packets_tabs/add/" . $data['packet_id'],
			"class" => "success"
		);
		$return["tbody class"] = "sortable";
		while ($r = $this->db->f($q)) {
			$return["row"][] = array(
				"td" => array(
					$r['id'],
					$r['title'],
					'<span class="btnToggle btn btn-small" data-url="/packets_tabs/togglepublished/' . $r['id'] . '/" data-value="' . $r['published'] . '"><i class="icon-check"></i></span>',
				),
				"btn" => array("update", "delete")
			);
		}
		
		$tplSort = new Template("onsortupdate", "packets_tabs");
		$return['append'] = $tplSort->display();
		
		return $this->Maestro->makeListing($return);
	}
    
	function togglepublished() {
		return $this->PacketsTabs->update(array(
			"published" => Router::get("active") ? 1 : 0,
		), Router::get("id"));
	}
	
	function updatepositions() {
		Core::loadLayout(NULL);
		foreach ($_POST['positions'] AS $position => $id)
			$this->PacketsTabs->update(array(
				"position" => $position
			), $id);
	}
	
	function add() {
		$tpl = new Template("add", "packets_tabs");

		return $this->Maestro->makeAdd(array(
			"content" => $tpl->parse(array(
				"packet_id" => Router::get("id"),
				"predefined list" => $this->PacketsTabs->getList(array(
					"data" => array(
						"warnings" => "Warnings",
						"payments" => "Payments",
					),
					"default" => " -- select predefined tab -- ",
					"id" => "tabs_predefined",
					"class" => "span6",
				)),
			)),
			"title" => "tab",
		));
	}
	
	function insert() {
		$id = $this->PacketsTabs->insert($_POST['packets_tab']);
		
		$this->Maestro->makeInsert(array("id" => $id));
	}

	function edit() {
        $tpl = new Template("edit", "packets_tabs");

        $r = $this->PacketsTabs->get("first", NULL, array("id" => Router::get("id")));
            
        $r['cb published'] = $r['published'] == 1
            ? ' checked="checked"'
            : NULL;

        return $this->Maestro->makeEdit(array(
            "content" => $tpl->parse($r),
            "title" => "tab",
            "breadcrumbs" => $this->Maestro->makeBreadcrumbs(array(
				"Offers" => "/maestro/offers",
				"Edit packet" => "/maestro/packets/edit/" . $r['packet_id'],
				"Edit tab" => "#",
			)),
        ));
	}

	function update() {
		$this->PacketsTabs->update($_POST['packets_tab'], $_POST['packets_tab']['id']);

		return $this->Maestro->makeUpdate(array(
			"id" => $_POST['packets_tab']['id']
		));
	}

	function delete() {
		$this->PacketsTabs->delete(Router::get("id"));
	}
	
	function getpredefined() {
		$data = $this->Settings->findArr("tab-" . $_POST['name']);
		
		return JSON::to($data);
	}
}

class PacketsTabsModel extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "title";

		$this->fields = array(
			"packet_id" => array(
				"not null" => true,
			),
			"title" => array(
				"not null" => true,
			),
			"content" => array(
				"not null" => true,
			),
			"published" => array(
				"default" => "-1",
			),
			"position" => array(
				"default" => 1,
			),
		);
	}
}

?>