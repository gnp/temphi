<?php

// root folder of config, layouts and modules with ending /
// www folder
// ufw folder

function autoStripSlashes($var) {
    if (is_array($var))
        foreach ($var AS $key => $val)
            $var[$key] = autoStripSlashes($val);
    else
        $var = stripslashes($var);

    return $var;
}

if (in_array(strtolower(ini_get('magic_quotes_gpc')), array(1, 'on', TRUE))) {
    $_POST = autoStripSlashes($_POST);
    $_GET = autoStripSlashes($_GET);
    $_COOKIE = autoStripSlashes($_COOKIE);
}

ini_set('date.timezone', "Europe/Ljubljana");

define('BR', "<br />");
define('DS', DIRECTORY_SEPARATOR);

      error_reporting(E_ALL);
      ini_set("display_errors", E_ALL);
      
if (isset($_GET['ref']) && $_GET['ref']) {
    setcookie('referer', $_GET['ref'], 0, '/');
}

include_once '../framework/core.php';

$core = new Core();

?>