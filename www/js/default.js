// JavaScript Document

/*$(document).ready(function() {
	setTimeout(function(){
	    if($('#' + window.location.hash.replace('#', ''))) {
	         scrollTo(window.location.hash.replace('#', ''));
	    }
	}, 500)
});*/

$(document).ready(function() {
	//open login modal if url anchor is #loginModal
	if(document.URL.substring(document.URL.lastIndexOf("#")+1) == "loginModal")
		$("#loginModal").modal('show');
	
	$("input").focus(function(e) {
    $(".mobile-bottom").addClass('hidden');
  });
	$("input").focusout(function(e) {
    $(".mobile-bottom").removeClass('hidden');
	});
	
	window.setTimeout(function() {newFloatingMenu("#fixedSignup", 5)},300);
	window.setTimeout(function() {newFloatingMenu(".container.offers-menu",0)},300);
	
	
		if(document.URL.indexOf("/ponudba/"))
			$(".main-menu").css('position', 'absolute');

    $("form[name='login-form']").submit(function(e) {
      e.preventDefault();
      
      $.post("/users/login", {username:$("form[name=login-form] #email").val(), password:$("form[name=login-form] #password").val()}, function(data) {
        data = fromJSON(data);
        
        if(data.success == true) {
          redir(data.redirect);
        }
        else {
          if ($("#loginAlert").length > 0)
            $("#loginAlert").html(data.text);
          else
            $("#submit").before('<div class="alert alert-danger" id="loginAlert" data-dismiss="alert">'+data.text+'</div>');
        }
      }); 
      
      return false;
    });

    $("form[name='forgot-pass-form']").submit(function(e) {
      e.preventDefault();
      
      $.post("/users/forgotpassword", {username:$("#emailfp").val()}, function(data) {
        data = fromJSON(data);
        
        if(data.success == true) {
          if ($("#forgotPasswordAlert").length > 0)
            $("#forgotPasswordAlert").html(data.text);
          else
            $("#submitfp").before('<div class="alert alert-success" id="forgotPasswordAlert" data-dismiss="alert">'+data.text+'</div>');
        }
        else {
          if ($("#forgotPasswordAlert").length > 0)
            $("#forgotPasswordAlert").html(data.text);
          else
            $("#submitfp").before('<div class="alert alert-danger" id="forgotPasswordAlert" data-dismiss="alert">'+data.text+'</div>');
        }
      }); 
      
      return false;
    });
    
	$('.package .more a').click(function(e) {
		e.preventDefault();
		if ($(this).parents('.package').children('.advance').css('display') == 'none')
			$(this).parents('.package').children('.advance').show(400);
		else
			$(this).parents('.package').children('.advance').hide(400);
		return false;
	});
	$('.advance .hideIt').click(function(e) {
		e.preventDefault();

		$(this).parent('.advance').hide(400);

		return false;
	});
});

function logError(data) {
	data.current = $("body").html();
	$.post("/log-error", data, function(){

	});
}

function newFloatingMenu(selector, topPadding, id) {
	 var offset = $(selector).offset();
	 update();
	 $(window).scroll(function() {
		 update();
	 });
	 function update() {
			 if ($(window).scrollTop() > (offset != null && typeof offset.top != "undefined" ? offset.top : 0)) {
					 $(selector).css('position','fixed');
					 $(selector).css('top','0');
			 } else {
					 $(selector).css('position','relative');
					 $(selector).css('top','0');
			 };
	 }
}


	function getOriginalWidthOfImg(img_element, att) {
		var t = new Image();
		t.src = img_element.attr('src');
		if(att == 'height')
			return t.height;
		else
			return t.width;
	}
	
	function resize(selector, ratio) {
		/*$(selector).each(function(){
			holder = $(this).parents("div").first();
			image = $(this);

			var img = new Image();
			img.src = $(image).attr("src");

			imageWidth = parseInt(img.width);
			imageHeight = parseInt(img.height);

			$(holder).height((parseInt($(holder).width())*ratio) + "px");

			holderWidth = parseInt($(holder).width());
			holderHeight = parseInt($(holder).height());

			if (imageHeight/imageWidth > ratio) {
				$(image).css("width", holderWidth + "px").css("height", "auto").css("min-width", "0");
				$(image).css("margin-top", ((holderHeight-(imageHeight*(holderWidth/imageWidth)))/2) + "px");
			} else {
				$(image).css("height", holderHeight + "px").css("width", "auto").css("min-height", "0");
				$(image).css("margin-left", ((holderWidth-(imageWidth*(holderHeight/imageHeight)))/2) + "px");
			}
		});
		/*$(selector).each(function() {
			pic_h = getOriginalWidthOfImg($(this), 'height');
			pic_w = getOriginalWidthOfImg($(this));
			pic_r = pic_h / pic_w;
			
			fullWidth = $(this).parents('div').width();
			fullHeight = fullWidth*ratio;
			
			$(this).parent().parent('div').height(fullHeight);
				
			if(pic_h > pic_w) {
				pic_w = fullWidth;
				pic_h = pic_r * pic_w;
			}
			else {
				pic_h = fullWidth;
				pic_w = pic_h / pic_r;
			}
			
			$(this).width(pic_w);
			$(this).height(pic_h);
			
			if(fullHeight < pic_h) {
				$(this).css('margin-top', (fullHeight-pic_h) / 2);
			}
			if(fullWidth < pic_w) {
				$(this).css('margin-left', (fullWidth-pic_w) / 2);
			}
		});*/
	}