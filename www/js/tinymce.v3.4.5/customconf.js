/*tinyMCE.init({
        mode : "specific_textareas",
        editor_selector : "mceEditor",
        theme : "advanced",
        content_css : "/css/tinymce.css",
        theme_advanced_toolbar_location : "top",
        theme_advanced_styles : "Bela=clrWhite, Svetlo siva=clrGreyL, Siva=clrGrey, Temno siva=clrGreyD, Črna=clrBlack, Pink=clrPink, Rdeča=clrRed, Modra=clrBlue, Zelena=clrGreen, Temno modra=clrBlueD, Rumena=clrYellow, Vijolična=clrPurple, Oranžna=clrOrange, Rjava=clrBrown",
        theme_advanced_font_sizes : "Big text=30px,Small text=small,My Text Size=.mytextsize"
});*/


tinymce.init({
        mode : "specific_textareas",
        selector : ".mceEditor",
		element_format : "xhtml",
		schema: "html5",
		entity_encoding : "raw",
        style_formats: [
	        {title: 'H1', block: 'h1'},
	        {title: 'H2', block: 'h2'},
	        {title: 'H3', block: 'h3'},
	        //{title: 'H4', block: 'h4'},
	        {title: 'Normal', inline: 'span'},
	        {title: 'Extra small', classes: 'fsXS', inline: 'span'},
	        {title: 'Small', classes: 'fsS', inline: 'span'},
	        {title: 'Medium', classes: 'fsM', inline: 'span'},
	        {title: 'Large', classes: 'fsL', inline: 'span'},
	        {title: 'Extra large', classes: 'fsXL', inline: 'span'},
	        {title: 'Extra extra large', classes: 'fsXXL', inline: 'span'},
	        {title: 'Black', classes: 'clrBlack', inline: 'span'},
	        {title: 'White', classes: 'clrWhite', inline: 'span'},
	        {title: 'Red', classes: 'clrRed', inline: 'span'},
	        {title: 'Green', classes: 'clrGreen', inline: 'span'},
	        {title: 'Blue', classes: 'clrBlue', inline: 'span'},
	        {title: 'Yellow', classes: 'clrYellow', inline: 'span'},
	        {title: 'Orange', classes: 'clrOrange', inline: 'span'}
	    ],
	    plugins: [
						"advlist autolink lists link image charmap print preview anchor paste",
						"searchreplace visualblocks code fullscreen",
						"insertdatetime media table contextmenu"
					],
	    toolbar: "bold italic underline strikethrough superscript subscript | styleselect forecolor fontsize | alignleft aligncenter alignright alignjustify | selectall cut copy paste | link image media table charmap | blockquote numlist bullist | undo redo removeformat code",
        menubar: " ",
        content_css : "/css/tinymce.css",
        setup : function(ed) {
			ed.on('change', function(e) {
				if (typeof validator !== "undefined") {
					$("#" + e.target.id).html(tinymce.activeEditor.getContent({format : 'raw'}));
					validator._validateForm();
				}
			});
			ed.on('init', function(e) {
				ed.pasteAsPlainText = true;
			});
        },
	    
        //theme_advanced_blockformats : "p,h1,h2,h3,h4,div",
	    paste_text_sticky : true,
	    convert_fonts_to_spans: false
});