function recalculateNumbers() {
	var num = 1;
	$(".ordersUserNum").each(function(){
		$(this).html(num);
		num++;
	});
}
window.onpopstate = function (event) {
	if (event.state.narocilnica) {
		$("#leftcontent").slideUp(function(){
			$("#maincontainer").removeClass(event.state.narocilnica.n).addClass(event.state.narocilnica.o);
			$(this).removeClass(event.state.narocilnica.cn).addClass(event.state.narocilnica.co);
			$(this).html(event.state.narocilnica.html).slideDown();

			runGA();
		});
	}
}

$(document).ready(function(){
	// disable all customers inputs
	$("#customers").find("input, select").prop("disabled", true);
	
	$(document).delegate(".btnDeleteOrdersUser", "click", function(){
		if (confirm("Ste prepričani, da želite izbrisat potnika?"))
			$("#customer_" + $(this).data("hash")).slideUp(function(){
				$(this).remove();
				recalculateNumbers();
			});
	});
	
	$(document).delegate(".ordersUserAddition", "click", function(){
		hash = $(this).attr("name").substring($(this).attr("name").indexOf("[") + 1, $(this).attr("name").indexOf("]"));

		if ($(this).prop("checked") == true) {
			$("#payment_" + hash).html(makePrice(parseFloat($("#payment_" + hash).html()) + parseFloat($(this).data("value"))));
			$("#payment").html(makePrice(parseFloat($("#payment").html()) + parseFloat($(this).data("value"))));
		} else {
			$("#payment_" + hash).html(makePrice(parseFloat($("#payment_" + hash).html()) - parseFloat($(this).data("value"))));
			$("#payment").html(makePrice(parseFloat($("#payment").html()) - parseFloat($(this).data("value"))));
		}
	});
	
	/*$(document).delegate(".customerEmail", "change", function(){
		var hash = $(this).parent().parent().data("hash");
		
		$.post("/users/getUserData", { email: $(this).val() }, function(data){
			data = fromJSON(data);
			
			if (data.success != true) {
				//alert(data.text);
			} else {
				$("[name='order[" + hash + "][name]']").val(data.user.name);
				$("[name='order[" + hash + "][surname]']").val(data.user.surname);
			}
		});
	});*/
	
	$(document).delegate("select.orderPacket", "change", function() {
		var hash = $(this).parent().parent().data("hash");

		if (hash.length != 40) {
			alert("Manjka hash.");
			return false;
		}
		
		var sumOldAdditions = 0.0;
		$("#orderAdditions_" + hash).find("input:checked").each(function(){
			sumOldAdditions = parseFloat(sumOldAdditions) + parseFloat($(this).data("value"));
		});
		
		$.post(
			"/narocilnica/json/packetchange",
			{
				packet: $(this).val() > 0 ? $(this).val() : 0,
				department: getRadio($("#orderDepartments_" + hash).find("input:checked")),
				additions: getCheckboxes($("#orderAdditions_" + hash).find("input:checked")),
				hash: hash,
			},
			function(data) {
				data = fromJSON(data);
				
				if (data.success != true) {
					alert(data.text);
					return false;
				}
				
				data.payment = data.payment == null ? 0 : data.payment;
				data.payment = parseFloat(data.payment);
				
				$("#orderDepartments_" + hash).html(data.departments);
				$("#orderAdditions_" + hash).html(data.additions);
				$("#orderIncludes_" + hash).html(data.includes);
				
				var sumNewAdditions = 0.0;
				$("#orderAdditions_" + hash).find("input:checked").each(function(){
					sumNewAdditions = parseFloat(sumNewAdditions) + parseFloat($(this).data("value"));
				});
				
				oldCustomerPayment = parseFloat($("#payment_" + hash).html());
				oldSumPayment = parseFloat($("#payment").html());
				
				newCustomerPayment = data.payment + sumNewAdditions;
				newSumPayment = oldSumPayment - oldCustomerPayment + data.payment + sumNewAdditions;
				
				$("#payment_" + hash).html(newCustomerPayment + " €");
				$("#payment").html(newSumPayment + " €");
			}
		);
	});
	
	$(document).delegate("#btnSubmitOrder", "click", function(){
		var success = true;
		$(".customer").each(function(){
			hash = $(this).data("hash");
			
			if (isEmpty($("[name='order[" + hash + "][email]']").val())) {
				if (isEmpty($("[name='order[" + hash + "][name]']").val()) || isEmpty($("[name='order[" + hash + "][surname]']").val())) {
					success = false;
				}
			}
		});
		
		if (success != true)
			alert("Pri vsakem naročniku je vnos emaila ali imena in priimka obvezen.");
		else {
			$.post("/predracun", $("#orderform").serialize(), function(data){
				history.replaceState({narocilnica: {html: $("#leftcontent").html(), o: "order", n: "estimateform", co: "span12", cn: "span8"}}, "title narocilnica", "/narocilnica");
				if (data.success != true) {
					alert(data.text);
					return false;
				}
				
				$("#leftcontent").slideUp(function(){
					$("#maincontainer").removeClass(data.css.o).addClass(data.css.n);
					//$(this).removeClass("span12").addClass("span8");
					$(this).html(data.html).slideDown(function(){
						scrollTo("#leftcontent", -60);
					});

					history.pushState({narocilnica: {html: data.html, o: "estimateform", n: "order", co: "span8", cn: "span12"}}, "title predracun", "/predracun");
					runGA();
				});
			}, "json").fail(function(){
				logError({
					type: "request",
					title: "narocilnica@127",
					post: { hash: hash, bills: $("#bills").val() },
					response: data
				});

				alert("Prosimo, poskusite znova. Če se napaka ponovi, nas lahko pokličete na telefon 040-148-148.");
			});
		}
		
		return false;
	});
	
	$(document).delegate("#addCustomer", "click", function(){
		$.post(
			"/narocilnica/json/addcustomer",
			{
				num: $(".customer").length,
				offer: $("#offer_id").val(),
			},
			function(data) {
				data = fromJSON(data);
				
				if (data.success != true) {
					alert(data.text);
					return false;
				}
				
				/*if ($(".customer").length > 1)
					$(".removeCustomer").last().after(data.html);
				else
					$("#customers").children().first().after(data.html);*/
				$("#appendCustomer").before(data.html);
				
				oldSumPayment = parseFloat($("#payment").html());
				newSumPayment = oldSumPayment + parseFloat(data.payment);
				
				$("#payment").html(newSumPayment + " €");
				
				recalculateNumbers();
			}
		);
	});
	
	$(document).delegate(".removeCustomer", "click", function(){
		if (confirm("Želiš res odstranit frenda?")) {
			var hash = $(this).data("hash");
			//$(this).hide().remove();
			$(this).slideUp(function(){$(this).remove();});
			$("#customer_" + hash).parent().slideUp(function(){$(this).remove();});
		}
	});
	
	$(document).delegate("#confirmPayee", "click", function(){
		hash = $("#payee").data("hash");
		empty = new Array();
		
		if (isEmpty($("[name='order[" + hash + "][email]']").val()))
			empty.push("email");

		if (isEmpty($("[name='order[" + hash + "][name]']").val()))
			empty.push("ime");
			
		if (isEmpty($("[name='order[" + hash + "][surname]']").val()))
			empty.push("priimek");
			
		if (isEmpty($("[name='order[" + hash + "][address]']").val()))
			empty.push("naslov");
			
		if (isEmpty($("[name='order[" + hash + "][post]']").val()))
			empty.push("pošta");
			
		if (isEmpty($("[name='order[" + hash + "][phone]']").val()))
			empty.push("telefonska številka");
		
		if (!isEmpty(empty)) {
			alert("Sledeči podatki so obvezni: " + empty.join(", "));
			return false;
		}
		
		$("#confirmPayee").slideUp();
		$("#customers").css("opacity", 1);
		$("#customers").find("input, select").prop("disabled", false);
		$("#customers").find("input, select").attr("disabled", false);
	});
});
