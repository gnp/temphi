$(document).ready(function(e) {
  $("#contact-form").validationEngine('attach',{scroll: false, onValidationComplete: function(form, status) {
    if(status) {
      var form = "#contact-form";
      $('.error').remove();
      $(form + " .sendy").val('Pošiljam').addClass('processing');
      $.post("/send-contact", $(form).serialize(), function(data){
        data = fromJSON(data);
        
        if (data.success != false) {
          $("#form").slideUp();
          $("#success").slideDown();
          $(form + " .sendy").val('Poslano').addClass('processing');
        }
        else {
          $(form + ", #fail").slideDown();
          $(form + " .sendy").removeClass('processing').val("Pošlji ponovno");
          $(form + " .sendy").after('<p class="error">Ups, neki je šlo narobe. Če se ponovi, kopiraj sporočilo in pošlji na party@gremonaparty.com</p>');
        }
      });
    }
  }});
});