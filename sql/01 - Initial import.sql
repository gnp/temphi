-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Gostitelj: localhost
-- Čas nastanka: 20 jan 2014 ob 19.03
-- Različica strežnika: 5.5.34
-- Različica PHP: 5.3.10-1ubuntu3.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Zbirka podatkov: `dev_gnp`
--

-- --------------------------------------------------------

--
-- Struktura tabele `abbreviations`
--

CREATE TABLE IF NOT EXISTS `abbreviations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) CHARACTER SET latin1 NOT NULL,
  `short` varchar(32) COLLATE utf8_slovenian_ci NOT NULL,
  `clicks` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `short` (`short`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `additions`
--

CREATE TABLE IF NOT EXISTS `additions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) COLLATE utf8_slovenian_ci NOT NULL,
  `value` varchar(32) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `short` varchar(32) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=67 ;

--
-- Odloži podatke za tabelo `additions`
--

INSERT INTO `additions` (`id`, `title`, `value`, `short`, `position`) VALUES
(2, 'Prevoz z avtobusom', '1', 'Prevoz', 0),
(3, 'Prevoz iz Celja', NULL, 'Prevoz iz CE', 11),
(4, 'Prevoz iz Maribora', NULL, 'Prevoz iz MB', 8),
(5, 'Prevoz iz Ljubljane', NULL, 'Prevoz iz LJ', 7),
(6, 'Prevoz iz Jesenic', NULL, 'Prevoz iz JE', 9),
(7, 'Prevoz iz Kranja', NULL, 'Prevoz iz KR', 10),
(8, 'Osnovno nezgodno zavarovanje', NULL, 'Osnovno nezgodno zavarovanje', 60),
(9, 'Zavarovanje rizika odpovedi', NULL, 'ZRO', 59),
(10, '2x nočitev v hotelu', NULL, '2x nočitev v hotelu', 15),
(11, 'Organizacija in vodenje', NULL, 'Organizacija in vodenje', 61),
(12, 'Prevoz z kombijem', NULL, 'Prevoz z kombijem', 1),
(13, '1x nočitev v hotelu', NULL, '1x nočitev v hotelu', 14),
(14, '3x nočitev v hotelu', NULL, '3x nočitev v hotelu', 16),
(15, '3x nočitev v hostlu', NULL, '3x nočitev v hostlu', 22),
(16, '1x nočitev v hostlu', NULL, '1x nočitev v hostlu', 20),
(17, '2x nočitev v hostlu', NULL, '2x nočitev v hostlu', 21),
(18, '7x nočitev v hotelu', NULL, '7x nočitev v hotelu', 19),
(22, 'BB (nočitev z zajtrkom)', NULL, 'BB (nočitev z zajtrkom)', 23),
(20, 'HB (pol-penzion)', NULL, 'HB (pol-penzion)', 24),
(21, 'FB (poln-penzion)', NULL, 'FB (poln-penzion)', 25),
(46, 'Vstopnica za klub (2x)', NULL, 'Vstopnica za klub (2x)', 32),
(24, 'Animacija', NULL, 'Animacija', 57),
(25, 'Dvoposteljna soba', NULL, 'Dvoposteljna soba', 26),
(26, 'Lokalni prevozi na destinaciji', NULL, 'Lokalni prevozi na destinaciji', 54),
(27, 'Najem opreme za kampiranje', NULL, 'Najem opreme za kampiranje', 56),
(28, 'Prevoz z letalom', NULL, 'Prevoz z letalom', 4),
(29, 'Prevoz z ladjo', NULL, 'Prevoz z ladjo', 5),
(30, 'Prevoz z vlakom', NULL, 'Prevoz z vlakom', 6),
(31, 'Prevozi na destinaciji (po programu)', NULL, 'Prevozi na destinaciji', 53),
(32, 'Transfer do letališča', NULL, 'Transfer do letališča', 12),
(33, 'Uporaba vozička za prtljago', NULL, 'Uporaba vozička za prtljago', 55),
(34, 'Boat party', NULL, 'Boat party', 50),
(35, 'Vstopnica za event', NULL, 'Vstopnica za event', 30),
(36, 'Vstopnica za Pre-Party', NULL, 'Vstopnica za Pre-Party', 42),
(37, 'Vstopnica za After-Party', NULL, 'Vstopnica za After-Party', 43),
(38, 'Vstopnica za kamp', NULL, 'Vstopnica za kamp', 40),
(39, 'VIP vstopnica', NULL, 'VIP vstopnica', 37),
(40, 'Vstopnica za klub', NULL, 'Vstopnica za klub', 31),
(41, 'Vstopnica za klube', NULL, 'Vstopnica za klube', 34),
(42, 'Vstopnica za Fan pit', NULL, 'Vstopnica za Fan pit', 35),
(43, 'Vstopnica za kamp (Dreamville pass)', NULL, 'Vstopnica za kamp (Dreamville pa', 41),
(44, 'Pool party', NULL, 'Pool party', 51),
(45, 'Soba z balkonom', NULL, 'Soba z balkonom', 27),
(47, 'Zavarovanje z asistenco v tujini', NULL, 'Zavarovanje z asistenco v tujini', 58),
(48, 'Vstopnica za klub (5x)', NULL, 'Vstopnica za klub (5x)', 33),
(49, 'Pozna odjava iz hotela', NULL, 'Pozna odjava iz hotela', 28),
(50, 'Paket pijače za na pot', NULL, 'Paket pijače za na pot', 13),
(51, 'VIP vstopnica (12.7.)', NULL, 'VIP vstopnica (12.7.)', 38),
(52, 'VIP vstopnica (13.7.)', NULL, 'VIP vstopnica (13.7.)', 39),
(54, 'Wavebraker ticket', '17', 'Wavebraker ticket', 36),
(55, 'Dbest party vodiči', NULL, NULL, 2),
(56, 'Deluxe vstopnica', NULL, NULL, 3),
(57, '5x nočitev v apartmaju', NULL, '5x nočitev v apartmaju', 17),
(58, 'Vstopnica za festival', NULL, 'Vstopnica za festival', 29),
(59, 'Festivalski bus', NULL, 'Festivalski bus', 52),
(60, '5x nočitev v hotelu', NULL, '5x nočitev v hotelu', 18),
(61, 'Ski pass - 3 dni', '137', 'Ski pass - 3 dni', 45),
(62, 'Ski pass - 2 dni', '87.00', 'Ski pass - 2 dni', 44),
(63, 'Ski pass - 4 dni', '169', 'Ski pass - 4 dni', 46),
(64, 'Ski pass - 5 dni', '202', 'Ski pass - 5 dni', 47),
(65, 'Ski pass - 6 dni', '235', 'Ski pass - 6 dni', 48),
(66, 'Ski pass - 7 dni', '265', 'Ski pass - 7 dni', 49);

-- --------------------------------------------------------

--
-- Struktura tabele `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) COLLATE utf8_slovenian_ci NOT NULL,
  `position` int(11) DEFAULT NULL,
  `published` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=8 ;

--
-- Odloži podatke za tabelo `categories`
--

INSERT INTO `categories` (`id`, `title`, `position`, `published`) VALUES
(1, 'Festivali', 2, 1),
(2, 'Clubbing', NULL, 1),
(3, 'Dubstep // Drum&Bass', 5, 1),
(4, 'Koncerti', 6, 1),
(5, 'Harder Styles', 3, 1),
(6, 'House // Trance // Progressive', 1, 1),
(7, 'Techno // Tech house // Minimal', 4, 1);

-- --------------------------------------------------------

--
-- Struktura tabele `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `title` varchar(64) COLLATE utf8_slovenian_ci NOT NULL,
  `departure` tinyint(1) DEFAULT '-1',
  `code` varchar(4) COLLATE utf8_slovenian_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cities_countries1_idx` (`country_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=24 ;

--
-- Odloži podatke za tabelo `cities`
--

INSERT INTO `cities` (`id`, `country_id`, `title`, `departure`, `code`) VALUES
(1, 1, 'Ljubljana', 1, '1000'),
(2, 2, 'Split', -1, '1000'),
(3, 6, 'Amsterdam', -1, '1000'),
(4, 9, 'Praga', -1, '1000'),
(5, 6, 'Hilvarenbeek', -1, '1000'),
(6, 4, 'Vösendorf', -1, '1000'),
(7, 4, 'Schwarzl See', -1, '1000'),
(8, 3, 'Kastellaun', -1, '1000'),
(9, 5, 'Wachtebeke', -1, '1000'),
(10, 10, 'Ibiza', -1, '1000'),
(11, 6, 'Eersel', -1, '1000'),
(12, 5, 'Boom', -1, '1000'),
(13, 2, 'Zrće / Pag', -1, '1000'),
(14, 6, 'Arnhem', -1, '1000'),
(15, 11, 'Budimpešta', -1, '1000'),
(16, 4, 'Mayrhofen', -1, '1000'),
(17, 7, 'Bologna', -1, '1000'),
(18, 3, 'Mannheim', -1, '100'),
(19, 1, 'Sežana', 1, '6210'),
(20, 1, 'Maribor', 1, '2000'),
(21, 1, 'Celje', 1, '3000'),
(22, 1, 'Kranj', 1, '4000'),
(23, 4, 'Baden', -1, '1000');

-- --------------------------------------------------------

--
-- Struktura tabele `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) COLLATE utf8_slovenian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=12 ;

--
-- Odloži podatke za tabelo `countries`
--

INSERT INTO `countries` (`id`, `title`) VALUES
(1, 'Slovenija'),
(2, 'Hrvaška'),
(3, 'Nemčija'),
(4, 'Avstrija'),
(5, 'Belgija'),
(6, 'Nizozemska'),
(7, 'Italija'),
(8, 'Srbija'),
(9, 'Češka'),
(10, 'Španija'),
(11, 'Madžarska');

-- --------------------------------------------------------

--
-- Struktura tabele `elfinder_file`
--

CREATE TABLE IF NOT EXISTS `elfinder_file` (
  `id` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(7) unsigned NOT NULL,
  `name` varchar(256) NOT NULL,
  `content` longblob NOT NULL,
  `size` int(10) unsigned NOT NULL DEFAULT '0',
  `mtime` int(10) unsigned NOT NULL,
  `mime` varchar(256) NOT NULL DEFAULT 'unknown',
  `read` enum('1','0') NOT NULL DEFAULT '1',
  `write` enum('1','0') NOT NULL DEFAULT '1',
  `locked` enum('1','0') NOT NULL DEFAULT '0',
  `hidden` enum('1','0') NOT NULL DEFAULT '0',
  `width` int(5) NOT NULL,
  `height` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `parent_name` (`parent_id`,`name`),
  KEY `parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `feedbacks`
--

CREATE TABLE IF NOT EXISTS `feedbacks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `datetime` datetime NOT NULL,
  `content` text COLLATE utf8_slovenian_ci NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`),
  KEY `fk_feedbacks_users1_idx` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `galleries`
--

CREATE TABLE IF NOT EXISTS `galleries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) COLLATE utf8_slovenian_ci NOT NULL,
  `dt_published` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `galleries_pictures`
--

CREATE TABLE IF NOT EXISTS `galleries_pictures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_id` int(11) NOT NULL,
  `url` varchar(255) COLLATE utf8_slovenian_ci NOT NULL,
  `main` tinyint(1) DEFAULT '-1',
  `thumb` tinyint(1) DEFAULT '-1',
  `position` int(11) DEFAULT '1',
  `url_main` varchar(255) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `url_thumb` varchar(255) COLLATE utf8_slovenian_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_galleries_pictures_galleries1_idx` (`gallery_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `galleries_tags`
--

CREATE TABLE IF NOT EXISTS `galleries_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_galleries_tags_galleries1_idx` (`gallery_id`),
  KEY `fk_galleries_tags_tags1_idx` (`tag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `games`
--

CREATE TABLE IF NOT EXISTS `games` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) COLLATE utf8_slovenian_ci NOT NULL,
  `content` text COLLATE utf8_slovenian_ci NOT NULL,
  `question` varchar(128) COLLATE utf8_slovenian_ci NOT NULL,
  `dt_published` datetime DEFAULT NULL,
  `picture` varchar(255) COLLATE utf8_slovenian_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `games_answers`
--

CREATE TABLE IF NOT EXISTS `games_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `ip` varchar(40) COLLATE utf8_slovenian_ci NOT NULL,
  `games_option_id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_games_answers_games_options1_idx` (`games_option_id`),
  KEY `fk_games_answers_games1_idx` (`game_id`),
  KEY `fk_games_answers_users1_idx` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `games_options`
--

CREATE TABLE IF NOT EXISTS `games_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `game_id` int(11) NOT NULL,
  `correct` tinyint(1) DEFAULT '-1',
  PRIMARY KEY (`id`),
  KEY `fk_games_options_games1_idx` (`game_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `logins`
--

CREATE TABLE IF NOT EXISTS `logins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `ip` varchar(40) COLLATE utf8_slovenian_ci NOT NULL,
  `datetime` datetime NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  `hash` varchar(40) COLLATE utf8_slovenian_ci NOT NULL,
  `dt_logged_out` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_logins_users1_idx` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `mails`
--

CREATE TABLE IF NOT EXISTS `mails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) COLLATE utf8_slovenian_ci NOT NULL,
  `subject` varchar(64) COLLATE utf8_slovenian_ci NOT NULL,
  `content` text COLLATE utf8_slovenian_ci NOT NULL,
  `sender` varchar(64) COLLATE utf8_slovenian_ci NOT NULL,
  `identifier` varchar(64) COLLATE utf8_slovenian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=24 ;

--
-- Odloži podatke za tabelo `mails`
--

INSERT INTO `mails` (`id`, `title`, `subject`, `content`, `sender`, `identifier`) VALUES
(13, 'Čakamo plačilo rezervacije', 'Plačilo rezervacije za aranžma ##OFFER.TITLE##', '<p dir="ltr">Živjo!</p>\r\n<p dir="ltr">še vedno nismo prejeli tvojega plačila rezervacije v višini ##RESERVATION##. Dokler ga ne prejmemo, tvoje naročilo za aranžma ##OFFER.TITLE## žal ni potrjeno... Torej, smo žurerji ‘al nismo?! :P</p>\r\n<p dir="ltr"><b> </b></p>\r\n<p dir="ltr">Rezervacijo lahko plačaš z Moneto, ali pa z nakazilom na TRR račun. Informacije o naročilu ter povezavo do plačila z Moneto najdeš v svojem <a href="##URL##profil">profilu</a>. V priponki pošiljamo še predračun.</p>\r\n<p dir="ltr"><b> </b></p>\r\n<p dir="ltr">Če si rezervacijo že plačal, prosimo pošlji potrdilo o plačilu, lahko da se je kej zamuštral. :)</p>', 'party@gremonaparty.com', 'waiting-reservation'),
(6, 'Obvestilo o sodelovanju v nagradni igri', 'Hvala za sodelovanje v nagradni igri', '<p dir="ltr">Živjo!</p>\r\n<p dir="ltr">Uspešno si sodeloval/a v nagradni igri ##GAME.TITLE##. Poteguješ se za nagrade:</p>\r\n<p dir="ltr">##prva nagrada##</p>\r\n<p dir="ltr">##druga nagrada##</p>\r\n<p dir="ltr">##tretja nagrada##</p>\r\n<p dir="ltr">Spremljaj našo Facebook stran, rezultate bomo objavili ##datum##.</p>\r\n<p dir="ltr">Držimo pesti zate!</p>', 'party@gremonaparty.com', 'game-vote'),
(7, 'Avtomatska registracija - frend', 'Avtomatska registracija na gremonaparty.com', '<p dir="ltr">Ti imaš pa res super frende!</p>\r\n<p dir="ltr">Nekdo, ki trdi, da je tvoj prijatelj, te je uspešno prijavil na aranžma ##OFFER.TITLE##. Woohoo! :)</p>\r\n<p dir="ltr"> </p>\r\n<p dir="ltr">**S tem si bil/a tudi avtomatsko registriran/a v sistem Gremonaparty.com, ki ti omogoča pregled tvojih naročil. Za prijavo lahko uporabiš email ##USER.EMAIL## ter naključno generirano geslo ##PASSWORD##, ki si ga lahko kasneje spremeniš v <a href="##URL##profil">profilu</a>. Pomembno je, da v svoj profil vneseš še ime, priimek ter naslov - ker hočemo vedet, s kom žuramo :P</p>', 'party@gremonaparty.com', 'signup-friend'),
(8, 'Avtomatska registracija - naročnik', 'Avtomatska registracija na gremonaparty.com', '<p dir="ltr">Woop woop! Zdej si pa del naše party družbe! :)</p>\r\n<p dir="ltr"><br />Prijavil/a si se na aranžma ##OFFER.TITLE## in zato si bil/a avtomatsko registriran/a v sistem Gremonaparty.com, ki ti med drugim omogoča popoln nadzor nad tvojimi naročili - tam imaš seznam vseh svojih žurov - kje smo že in kje še bomo skup žurali :)</p>\r\n<p dir="ltr">Za prijavo lahko uporabiš email ##USER.EMAIL## ter tole naključno generirano geslo ##PASSWORD##, ki pa si ga lahko kasneje valda spremeniš v <a href="##URL##profil">profilu</a>.</p>', 'party@gremonaparty.com', 'signup-payee'),
(9, 'Naročilo je sprejeto', 'Naročilo na aranžma ##OFFER.TITLE## je sprejeto', '<p dir="ltr">Holá, ##USER.NAME##!</p>\r\n<p dir="ltr">Prejeli smo tvoje naročilo na aranžma ##OFFER.TITLE## - to pomeni, da bomo kmalu skup žural!</p>\r\n<p dir="ltr">Tvoja prijava bo potrjena takoj, ko izvedeš plačilo rezervacije. To lahko storiš kar preko monete ali z nakazilom na naš TRR. Informacije o naročilu ter povezavo do plačila z Moneto najdeš v svojem <a href="##URL##profil">profilu</a> in v pripetem predračunu.</p>\r\n<p dir="ltr">Rok za plačilo rezervacije je 24 ur. Ko bomo plačilo zabeležili, te obvestimo - ne pusti, da te kdo prehiti in čim prej plačaj rezervacijo!</p>\r\n<p dir="ltr">Se vidimo kmalu!</p>\r\n<p dir="ltr"><br /><br /></p>', 'party@gremonaparty.com', 'order-added'),
(10, 'Naročilo je potrjeno - Moneta', 'Naročilo na aranžma ##OFFER.TITLE## je potrjeno', '<p dir="ltr">Hudooo!</p>\r\n<p dir="ltr">Prejeli smo plačilo rezervacije aranžmaja ##OFFER.TITLE## z Moneto, s tem je tvoje party naročilo potrjeno - woohoo!</p>\r\n<p dir="ltr">Prosimo te, da preostanek plačila poravnaš pravočasno skladno z roki v predračunu. Status naročila lahko spremljaš v svojem <a href="##URL##profil">profilu</a>.</p>', 'party@gremonaparty.com', 'order-confirmed-moneta'),
(11, 'Naročilo je potrjeno - Frend', 'Naročilo na aranžma ##OFFER.TITLE## je potrjeno', '<p dir="ltr">Holá, ##USER.NAME##!</p>\r\n<p>Hvala za nakazilo rezervacije za aranžma ##OFFER.TITLE##, s tem je tvoje naročilo potrjeno - woohoo! :)</p>\r\n<p>Status naročila lahko spremljaš v svojem <a href="##URL##profil">profilu</a>.</p>', 'party@gremonaparty.com', 'order-confirmed-friend'),
(12, 'Naročilo je potrjeno - Ročno', 'Naročilo na aranžma ##OFFER.TITLE## je potrjeno', '<p dir="ltr">Paaarty!!</p>\r\n<p dir="ltr">Hvala za plačilo rezervacije aranžmaja ##OFFER.TITLE## s TRR nakazilom. S tem je tvoje naročilo potrjeno - woop woop! :)</p>\r\n<p dir="ltr">Prosimo te, da preostanek plačila poravnaš pravočasno skladno z roki v predračunu. Status naročila lahko spremljaš v svojem <a href="##URL##profil">profilu</a>.</p>', 'party@gremonaparty.com', 'order-confirmed-upn'),
(14, 'Čakamo na podatke frendov', 'Potrebujemo podatke tvojih frendov', '<p dir="ltr">Hej hej!</p>\r\n<p dir="ltr">Ob pregledu naročila na aranžma ##OFFER.TITLE## smo ugotovili, da nisi vnesel vseh e-naslovov svojih frendov. Da jih bomo lahko obveščali o statusu naročila, nujno potrebujemo email naslove. Prosimo, da čimprej obiščeš svoj <a href="##URL##profil">profil</a> ter dodaš manjkajoče podatke.</p>\r\n<p dir="ltr">Potem bomo pa kmalu skup žural! ;)</p>\r\n<p> </p>', 'party@gremonaparty.com', 'waiting-friends-data'),
(15, 'Obvestilo o plačilu obroka', 'Prejeli smo plačilo za aranžma ##OFFER.TITLE##', '<p dir="ltr">Holá!</p>\r\n<p dir="ltr">Hvala za plačilo obroka za aranžma ##OFFER.TITLE##. Prosimo, da prihajajoče obroke poravnaš pravočasno, skladno z roki na predračunu. Status naročila lahko spremljaš v svojem <a href="##URL##profil">profilu</a>.</p>', 'party@gremonaparty.com', 'order-payment'),
(16, 'Obvestilo o plačilu aranžmaja', 'Aranžma ##OFFER.TITLE## je plačan', '<p dir="ltr">Holá!</p>\r\n<p dir="ltr">zahvaljujemo se ti za plačilo aranžmaja ##OFFER.TITLE##. Informacije o potovanju ti pošljemo najkasneje 5 dni pred odhodom. Status naročila lahko spremljaš v svojem <a href="##URL##profil">profilu</a>.</p>', 'party@gremonaparty.com', 'order-payed'),
(17, 'Zahtevek za spremembo aranžmaja je sprejet', 'Sprememba naročila na aranžma ##OFFER.TITLE## je zabeležena', '<p dir="ltr">Holá!</p>\r\n<p dir="ltr">Tvoj zahtevek za spremembo naročila na aranžma ##OFFER.TITLE## je sprejet. Obvestili te bomo, ko bo zahtevek obdelan. Status naročila lahko spremljaš v svojem <a href="##URL##profil">profilu</a>.</p>', 'party@gremonaparty.com', 'order-change-added'),
(18, 'Zahtevek za spremembo aranžmaja je potrjen', 'Sprememba naročila na aranžma ##OFFER.TITLE## je potrjena', '<p dir="ltr">Woohoo! :)</p>\r\n<p dir="ltr">Tvoj zahtevek za spremembo naročila na aranžma  ##OFFER.TITLE## je potrjen. V priponki pošiljamo tudi spremenjen predračun. Status naročila lahko spremljaš v svojem <a href="##URL##profil">profilu</a>.</p>', 'party@gremonaparty.com', 'order-change-confirmed'),
(19, 'Zahtevek za spremembo aranžmaja je zavrnjen', 'Sprememba naročila na aranžma ##OFFER.TITLE## je zavrnjena', '<p dir="ltr">O shit :/</p>\r\n<p dir="ltr">Žal je tvoj zahtevek za spremembo naročila na aranžma  ##OFFER.TITLE## zavrnjen. Status naročila lahko spremljaš v svojem <a href="##URL##profil">profilu</a>.</p>', 'party@gremonaparty.com', 'order-change-rejected'),
(20, 'Čakamo na uporabnikove podatke', 'Potrebujemo tvoje podatke', '<p dir="ltr">Holá,</p>\r\n<p dir="ltr">ob pregledu naročila na aranžma  ##OFFER.TITLE## smo ugotovili, da še nimamo vseh tvojih podatkov. Da te bomo lahko obveščali o statusu naročila, nujno potrebujemo email naslov, za udeležbo na partyu pa tudi ime, priimek ter telefonsko številko. Pliz čimprej obišči svoj <a href="##URL##profil">profil</a> in dodaj manjkajoče podatke.</p>\r\n<p> </p>', 'party@gremonaparty.com', 'waiting-users-data'),
(21, 'Naročilo je preklicano', 'Naročilo na aranžma ##OFFER.TITLE## je preklicano', '<p dir="ltr">Nee, zakaaaj!?</p>\r\n<p>Tvoje naročilo na aranžma ##OFFER.TITLE## je preklicano. Kadarkoli ga lahko ponovno aktiviraš v svojem <a href="##URL##profil">profilu</a>.</p>', 'party@gremonaparty.com', 'order-canceled'),
(22, 'Naročilo je zavrnjeno', 'Naročilo na aranžma ##OFFER.TITLE## je zavrnjeno', '<p dir="ltr">Whaaat!?</p>\r\n<p dir="ltr">Naročilo na aranžma ##OFFER.TITLE## je žal zavrnjeno.</p>\r\n<p>Razlog: ##RAZLOG##</p>', 'party@gremonaparty.com', 'order-rejected'),
(23, 'Prenova spletnega portala GremoNaParty.com - novo geslo', 'Prenova spletnega portala GremoNaParty.com', '<p><em>Živjo ##USER.NAME##</em>!</p>\r\n<p><em>ker smo res super potovalna agencija, smo temeljito prenovili našo <a href="http://goo.gl/CIjsb" target="_blank"><strong>spletno stran</strong></a>! :)</em></p>\r\n<p><em>Poleg novega dizajna in načina predstavitve aranžmajev smo dodelali in spremenili proces naročanja, implementirali plačilo rezervacije za takojšnjo zagotovitev mest in za potnike naredili vmesnik za spremljanje statusa naročil, zraven vsega tega pa smo dodali še <strong>galerije </strong>najboljših utrinkov z naših potovanj.</em></p>\r\n<p><em>Za prijavo v uporabniški vmesnik lahko uporabiš email <strong>##USER.EMAIL##</strong> in naključno generirano geslo <strong>##PASSWORD##</strong>. Kasneje si ga itak lahko spremeniš.</em></p>\r\n<p><em>Zdaj pa hitro<strong> <a href="http://goo.gl/zJjZE" target="_blank">klikni tukaj</a></strong></em><em>, novo spletno stran preklikaj po dolgem in počez, svoj vtis pa nam sporoči na naši <a href="http://goo.gl/OGM0y" target="_blank"><strong>facebook</strong> </a>strani.</em></p>\r\n<p><em>PS: To obvestilo ste prejeli vsi carji in carice, ki ste v preteklosti potovali z nami ali pa vsaj oddali naročilo. Obljubimo, da bomo od zdaj naprej spemali samo prijavljene na našo mailing listo. =)</em></p>\r\n<p><em>PS2: Naši vrli politiki so poskrbeli, da ti piškotov ne smemo servirat brez tvojega dovoljenja :) Bodi kul in sprejmi naše piškote, saj bomo le tako lahko poskrbeli za še boljšo uporabniško izkušnjo.</em></p>', 'party@gremonaparty.com', 'posodobljeno-geslo');

-- --------------------------------------------------------

--
-- Struktura tabele `mails_sents`
--

CREATE TABLE IF NOT EXISTS `mails_sents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mail_id` int(11) NOT NULL,
  `subject` varchar(128) COLLATE utf8_slovenian_ci NOT NULL,
  `content` text COLLATE utf8_slovenian_ci NOT NULL,
  `from` varchar(64) COLLATE utf8_slovenian_ci NOT NULL,
  `to` varchar(128) COLLATE utf8_slovenian_ci NOT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_mails_sent_mails1_idx` (`mail_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `members`
--

CREATE TABLE IF NOT EXISTS `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) COLLATE utf8_slovenian_ci NOT NULL,
  `content` text COLLATE utf8_slovenian_ci,
  `picture` varchar(255) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `published` tinyint(1) DEFAULT '-1',
  `team_id` int(11) NOT NULL,
  `position` int(2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_members_members_groups1_idx` (`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `microphones`
--

CREATE TABLE IF NOT EXISTS `microphones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text COLLATE utf8_slovenian_ci NOT NULL,
  `dt_added` datetime DEFAULT NULL,
  `dt_published` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `moneta`
--

CREATE TABLE IF NOT EXISTS `moneta` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `confirmationid` varchar(64) COLLATE utf8_slovenian_ci NOT NULL DEFAULT '',
  `confirmationsignature` varchar(255) COLLATE utf8_slovenian_ci NOT NULL DEFAULT '',
  `tarifficationerror` int(10) unsigned NOT NULL DEFAULT '0',
  `startdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `confirmdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `refreshcounter` int(10) unsigned NOT NULL DEFAULT '0',
  `purchasestatus` varchar(32) COLLATE utf8_slovenian_ci NOT NULL DEFAULT '',
  `providerdata` text COLLATE utf8_slovenian_ci NOT NULL,
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_confirmationid` (`confirmationid`),
  KEY `order_id` (`order_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) COLLATE utf8_slovenian_ci NOT NULL,
  `content` text COLLATE utf8_slovenian_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `dt_published` datetime DEFAULT NULL,
  `content_short` text COLLATE utf8_slovenian_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `news_tags`
--

CREATE TABLE IF NOT EXISTS `news_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `news_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_news_tags_news1_idx` (`news_id`),
  KEY `fk_news_tags_tags1_idx` (`tag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `offers`
--

CREATE TABLE IF NOT EXISTS `offers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `subtitle` varchar(256) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `city_id` int(11) NOT NULL,
  `dt_start` datetime DEFAULT NULL,
  `dt_end` datetime DEFAULT NULL,
  `short_content` text COLLATE utf8_slovenian_ci,
  `content` text COLLATE utf8_slovenian_ci,
  `dt_published` datetime DEFAULT NULL,
  `published` tinyint(1) DEFAULT '-1',
  `pickup_line` varchar(64) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `firstpage` tinyint(1) DEFAULT '-1',
  `top` tinyint(1) DEFAULT '-1',
  `dt_opened` datetime DEFAULT NULL,
  `dt_closed` datetime NOT NULL,
  `soundcloud` varchar(128) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `picture` varchar(255) COLLATE utf8_slovenian_ci NOT NULL,
  `order_limit` int(11) NOT NULL DEFAULT '-1',
  `price_text` text COLLATE utf8_slovenian_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `lineup` text COLLATE utf8_slovenian_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_offers_cities1_idx` (`city_id`),
  KEY `category_id` (`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `offers_pictures`
--

CREATE TABLE IF NOT EXISTS `offers_pictures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `offer_id` int(11) NOT NULL,
  `url` varchar(128) COLLATE utf8_slovenian_ci NOT NULL,
  `main` tinyint(1) DEFAULT '-1',
  `thumb` tinyint(1) DEFAULT '-1',
  `position` int(11) DEFAULT '1',
  `url_main` varchar(64) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `url_thumb` varchar(64) COLLATE utf8_slovenian_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_galleries_pictures_galleries1_idx` (`offer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `offers_tags`
--

CREATE TABLE IF NOT EXISTS `offers_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_id` int(11) NOT NULL,
  `offer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_offers_tags_tags1_idx` (`tag_id`),
  KEY `fk_offers_tags_offers1_idx` (`offer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `num` varchar(16) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `offer_id` int(11) NOT NULL,
  `dt_added` datetime NOT NULL,
  `dt_confirmed` datetime DEFAULT NULL,
  `dt_rejected` datetime DEFAULT NULL,
  `dt_canceled` datetime DEFAULT NULL,
  `dt_payed` datetime DEFAULT NULL,
  `dt_finished` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `hash` varchar(40) COLLATE utf8_slovenian_ci NOT NULL,
  `price` float DEFAULT NULL,
  `bill_url` varchar(128) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `estimate_url` varchar(128) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `original` float DEFAULT NULL,
  `dt_locked` datetime DEFAULT NULL,
  `wishes` text COLLATE utf8_slovenian_ci,
  PRIMARY KEY (`id`),
  KEY `fk_orders_offers1_idx` (`offer_id`),
  KEY `fk_orders_users1_idx` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `orders_bills`
--

CREATE TABLE IF NOT EXISTS `orders_bills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `dt_added` datetime NOT NULL,
  `dt_valid` datetime DEFAULT NULL,
  `dt_confirmed` datetime DEFAULT NULL,
  `price` float NOT NULL,
  `notes` text COLLATE utf8_slovenian_ci,
  `dt_override` datetime DEFAULT NULL,
  `bill_id` varchar(32) COLLATE utf8_slovenian_ci NOT NULL,
  `url` varchar(64) COLLATE utf8_slovenian_ci NOT NULL,
  `payed` float DEFAULT NULL,
  `reservation` tinyint(1) NOT NULL DEFAULT '-1',
  `type` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_orders_bills_orders1_idx` (`order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `orders_users`
--

CREATE TABLE IF NOT EXISTS `orders_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `packet_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `dt_added` datetime NOT NULL,
  `dt_confirmed` datetime DEFAULT NULL,
  `dt_rejected` datetime DEFAULT NULL,
  `dt_canceled` datetime DEFAULT NULL,
  `order_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `notes` text COLLATE utf8_slovenian_ci,
  PRIMARY KEY (`id`),
  KEY `fk_orders_users_orders1_idx` (`packet_id`),
  KEY `fk_orders_users_users1_idx` (`user_id`),
  KEY `order_id` (`order_id`),
  KEY `city_id` (`city_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `orders_users_additions`
--

CREATE TABLE IF NOT EXISTS `orders_users_additions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_user_id` int(11) NOT NULL,
  `addition_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_orders_users_additions_orders_users1_idx` (`orders_user_id`),
  KEY `fk_orders_users_additions_additions1_idx` (`addition_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `packets`
--

CREATE TABLE IF NOT EXISTS `packets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) COLLATE utf8_slovenian_ci NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `offer_id` int(11) NOT NULL,
  `dt_published` datetime NOT NULL,
  `position` int(11) NOT NULL,
  `ticket` tinyint(1) NOT NULL DEFAULT '-1',
  `price_old` decimal(10,2) NOT NULL,
  `order_limit` int(11) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`),
  KEY `offer_id` (`offer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `packets_additions`
--

CREATE TABLE IF NOT EXISTS `packets_additions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `addition_id` int(11) NOT NULL,
  `packet_id` int(11) NOT NULL,
  `value` varchar(32) COLLATE utf8_slovenian_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_offers_additions_additions1_idx` (`addition_id`),
  KEY `fk_offers_additions_offers1_idx` (`packet_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `packets_cities`
--

CREATE TABLE IF NOT EXISTS `packets_cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `packet_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `dt_departure` datetime DEFAULT NULL,
  `fee` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_offers_cities_offers1_idx` (`packet_id`),
  KEY `fk_offers_cities_cities1_idx` (`city_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `packets_credits`
--

CREATE TABLE IF NOT EXISTS `packets_credits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime` datetime DEFAULT NULL,
  `price` float DEFAULT NULL,
  `packet_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_offers_credits_offers1_idx` (`packet_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `packets_includes`
--

CREATE TABLE IF NOT EXISTS `packets_includes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `packet_id` int(11) NOT NULL,
  `addition_id` int(11) NOT NULL,
  `value` varchar(32) COLLATE utf8_slovenian_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_offers_includes_offers1_idx` (`packet_id`),
  KEY `fk_offers_includes_additions1_idx` (`addition_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `packets_tabs`
--

CREATE TABLE IF NOT EXISTS `packets_tabs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `packet_id` int(11) NOT NULL,
  `title` varchar(64) COLLATE utf8_slovenian_ci NOT NULL,
  `content` text COLLATE utf8_slovenian_ci NOT NULL,
  `published` tinyint(1) DEFAULT '-1',
  `position` int(2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_offers_tabs_offers1_idx` (`packet_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) COLLATE utf8_slovenian_ci NOT NULL,
  `content` text COLLATE utf8_slovenian_ci NOT NULL,
  `url` varchar(128) COLLATE utf8_slovenian_ci NOT NULL,
  `system` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url` (`url`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=6 ;

--
-- Odloži podatke za tabelo `pages`
--

INSERT INTO `pages` (`id`, `title`, `content`, `url`, `system`) VALUES
(2, 'Izjava o varovanju osebnih podatkov', '<p>Naša spletna stran uporablja piškotke, da lahko izboljšamo<strong> uporabniško izkušnjo</strong>, <strong>analiziramo promet</strong> ter uporabljamo <strong>vtičnike družbenih omrežij</strong>. V prvi tabeli so navedeni piškotki, ki sodijo med izjeme, v drugi tabeli pa so navedeni piškotki, za katere potrebujemo vaše dovoljenje.</p>\r\n<p> </p>\r\n<p><strong>Dovoljeni piškotki oziroma piškotki, ki spadajo med izjeme</strong></p>\r\n<table class="table table-condensed">\r\n<tbody>\r\n<tr><th>Ime piškotka</th><th>Trajanje</th><th>Uporaba</th></tr>\r\n<tr>\r\n<td>zekom</td>\r\n<td>do konca seje</td>\r\n<td>Dovoljenje za piškote (Gremonaparty.com)</td>\r\n</tr>\r\n<tr>\r\n<td>hash</td>\r\n<td>1 leto</td>\r\n<td>Avtentikacija uporabnika (Gremonaparty.com)</td>\r\n</tr>\r\n<tr>\r\n<td>PHPSESSID</td>\r\n<td>do konca seje</td>\r\n<td>Seja aplikacije (Gremonaparty.com)</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p> </p>\r\n<p><strong>Piškotki, za katere potrebujemo vaše dovoljenje</strong></p>\r\n<table class="table table-condensed">\r\n<tbody>\r\n<tr><th>Ime piškotka</th><th>Trajanje</th><th>Uporaba</th></tr>\r\n<tr>\r\n<td>__utmb</td>\r\n<td>30 minut</td>\r\n<td>Analiza obiska (Google Analytics)</td>\r\n</tr>\r\n<tr>\r\n<td>__utmc</td>\r\n<td>Do zaprtja seje</td>\r\n<td>Analiza obiska (Google Analytics)</td>\r\n</tr>\r\n<tr>\r\n<td>_ga</td>\r\n<td>Do zaprtja seje</td>\r\n<td>Analiza obiska (Google Analytics)</td>\r\n</tr>\r\n<tr>\r\n<td>__utmz</td>\r\n<td>6 mesecev</td>\r\n<td>Analiza obiska (Google Analytics)</td>\r\n</tr>\r\n<tr>\r\n<td>__utma</td>\r\n<td>2 leti</td>\r\n<td>Analiza obiska (Google Analytics)</td>\r\n</tr>\r\n<tr>\r\n<td>__utmv</td>\r\n<td>2 leti</td>\r\n<td>Analiza obiska (Google Analytics)</td>\r\n</tr>\r\n</tbody>\r\n</table>', 'zakon-o-uporabi-piskotov', -1),
(3, 'Private party pack', '<p>Vsebina je v pripravi =)</p>', 'private-party-pack', -1),
(4, 'Party prevozi', '<p>Vsebina je v pripravi =)</p>', 'party-prevozi', -1),
(5, 'Splošni pogoji poslovanja', '<p>Splošne pogoje poslovanja v .pdf obliki lahko prenesete <a href="../../../media/files/splosni_pogoji_poslovanja.pdf" target="_blank">tukaj</a>.</p>\r\n<p> </p>\r\n<h3 style="text-align: center;">1. Splošna določila</h3>\r\n<p> </p>\r\n<p style="text-align: left;">Blagovna znamka gremonaparty.com z vsemi svojimi sestavnimi deli (spletna stran www.gremonaparty.com, Facebook stran www.facebook.com/gremonaparty in vsi drugi prodajni ter komunikacijski kanali) je v upravljanju podjetja RENTOR Mario Benić s.p., PODRUŽNICA RENTOOR TURIZEM, Vožarski pot 3, 1000 Ljubljana, MŠ: 3334872001, DŠ: 81835078 (v nadaljevanju RENTOOR). </p>\r\n<p style="text-align: left;">Splošni pogoji in navodila so sestavni del aranžmajev in pogodbe, ki jo s postopki od naročila do plačila in končno izvedbe storitev skleneta potnik in RENTOOR ali pooblaščena turistična agencija. Ta pogodba je sklenjena in veljavna šele ob plačilu predračuna s strani potnika delno ali v celoti. Če se v posameznem programu katero določilo razlikuje od Splošnih pogojev poslovanja, velja določilo programa. Pri posredovanju aranžmajev drugih organizatorjev veljajo njihovi splošni pogoji in navodila. RENTOOR si pridržuje pravico v kateremkoli trenutku in brez predhodnega opozorila spremeniti Splošne pogoje poslovanja.</p>\r\n<p style="text-align: center;"> </p>\r\n<h3 style="text-align: center;">2. Prijava na aranžma</h3>\r\n<p> </p>\r\n<p style="text-align: left;">Prijava je možna na dva načina, opisana v nadaljevanju. Z izpolnitvijo prijavnice udeležba potnika na potovanju še ni potrjena, to se zgodi šele ob izdaji predračuna s strani RENTOOR in plačilom predračuna delno ali v celoti s strani potnika. Prijava na aranžma je zasebna storitev in ni del cene aranžmaja. </p>\r\n<p style="text-align: left;">Preko spletne naročilnice na spletni strani www.gremonaparty.com: prijava je veljavna, če so pravilno izpolnjeni vsi potrebni podatki za prijavo. Če je prijava uspešna, je naročnik o tem obveščen po elektronski pošti. Dolžnost potnika je, da zagotovi pravilne osebne podatke, še posebej pa elektronski naslov in naslov za pošiljanje predračunov. Na eno ime je lahko prijavljenih več oseb, kjer je oseba, ki je naročila aranžma, nosilec naročila in odgovorna kontaktna oseba za ostale prijavljene, v kolikor ni drugače dogovorjeno. Potnik se zavezuje ob prijavi plačati stroške prijavnine v višini 4,90 EUR.</p>\r\n<p style="text-align: left;">Preko elektronske pošte na party@gremonaparty.com: potnik lahko pošlje prijavo na potovanje na e-mail, v katerem mora prav tako kot v spletni naročilnici posredovati naslednje podatke: ime, priimek, elektronski naslov, naslov in hišno številko, poštno številko, pošto in telefonsko številko ter podrobnosti naročila (odhodni kraj, morebitna doplačila). Potnik je dolžan posredovati pravilne osebne podatke, še posebej pa elektronski naslov in naslov za pošiljanje predračuna. Tudi ob prijavi preko elektronske pošte se potnik zavezuje plačati stroške prijavnine v višini 4,90 EUR.</p>\r\n<p style="text-align: left;">Ob prijavi na potovanje postanejo naročnik in vsi morebitni potniki, ki jih je prijavil na potovanje, registrirani uporabniki spletne strani Gremonaparty.com. To pomeni, da so člani baze uporabnikov RENTOOR. Po prijavi vsak potnik dobi avtomatsko generirano sporočilo z uporabniškim imenom in geslom, ki si ga kasneje lahko spremeni. S tem lahko potniki lahko svoja naročila in osebne podatke naknadno urejajo in spreminjajo. To je mogoče po prijavi v sistem z uporabniškim imenom in geslom.</p>\r\n<p style="text-align: center;"> </p>\r\n<h3 style="text-align: center;">3. Plačila</h3>\r\n<p> </p>\r\n<p style="text-align: left;">Splošen rok plačila aranžmaja je 5 dni po izdaji predračuna, s tem da le ta lahko zapade v plačilo najkasneje 15 dni pred izvedbo aranžmaja. Če je do izvedbe aranžmaja manj kot 30 dni, velja rok plačila 2 dni od izdaje predračuna. <br />Za uspešno dokončanje prijave je potrebno plačilo rezervacije potovanja v višini 19,90 eur. Rezervacijo je možno plačati preko sistema Moneta ali na bančni račun (najkasneje v 24 urah od prijave). Rezervacija potovanja se odšteje od skupne vrednosti aranžmaja in ni dodaten strošek za potnika. Preostali znesek je možno plačati enkratno ali po obrokih. Pri opisu vsakega aranžmaja na spletni strani je določeno na koliko obrokov je možno plačilo. Na izdanem predračunu so določeni roki plačila posameznih obrokov. RENTOOR si pridržuje pravico do sprememb pravil glede obročnega plačevanja v posameznih primerih, če je tako določeno v vsebini opisa aranžmaja ali v primeru posebnega dogovora s potnikom.</p>\r\n<p style="text-align: left;">Pravila glede obročnega odplačevanja so: </p>\r\n<p style="text-align: left;">- 1. obrok mora biti plačan v roku 5 dni od izdaje predračuna in v vrednosti najmanj 40% celotne vrednosti naročenih aranžmajev zmanjšane za vrednost rezervacije potovanja <br />- Potnik mora spoštovati roke plačila naslednjih obrokov, kot je določeno na predračunu <br />- Celotna vrednost predračuna mora biti plačana vsaj 15 dni pred odhodom <br />Pravila se izjemoma spremenijo, kadar je do izvedbe aranžmaja manj kot 30 dni: <br />- 1. obrok mora biti tako plačan v roku 2 dni od izdaje predračuna in v vrednosti najmanj 40% celotne vrednosti predračuna zmanjšane za vrednost rezervacije potovanja <br />- Celotna vrednost predračuna mora biti plačana vsaj 10 dni pred odhodom. <br />Zamuda rokov plačila (kateregakoli od obrokov ali celotne vrednosti aranžmaja), določenih na izdanem predračunu, se šteje kot odpoved potovanja s strani potnika. Skladno s tem se upoštevajo vsa določila 8. člena teh Splošnih pogojev.</p>\r\n<p style="text-align: left;"> </p>\r\n<p style="text-align: left;">Morebitna vračila denarja potnikom bo agencija RENTOOR izvedla najkasneje v 30 dneh od potrditve vračila denarja.</p>\r\n<p style="text-align: center;"> </p>\r\n<h3 style="text-align: center;">4. Storitve, ki so vključene v ceno aranžmaja</h3>\r\n<p> </p>\r\n<p style="text-align: left;">V ceno aranžmaja so vključene vse tiste storitve, ki so navedene na spletni strani www.gremonaparty.com v opisu posameznega aranžmaja pod “Cena vključuje”.</p>\r\n<p style="text-align: center;"> </p>\r\n<h3 style="text-align: center;">5. Možna doplačila</h3>\r\n<p> </p>\r\n<p style="text-align: left;">Posebne storitve, ki niso vključene v ceno in jih mora potnik plačati posebej, so navedene v opisu posameznega aranžmaja na spletni strani pod “Možna doplačila”. To so običajno doplačila za: nastanitev (če ni vključena v ceno aranžmaja), VIP vstopnice, enoposteljno sobo, obisk nočnih lokalov s programov, dodatne vstopnine in oglede, letališko in varnostno takso, posebno prehrano, ipd. Potnik ob prijavi navede želje po posebnih storitvah in jih doplača v predračunu. Na samem potovanju pa te storitve doplača turističnemu vodniku v ustrezni valuti.</p>\r\n<p style="text-align: center;"> </p>\r\n<h3 style="text-align: center;">6. Cene</h3>\r\n<p> </p>\r\n<p style="text-align: left;">Pri cenah, ki so navedene v tuji valuti, upoštevamo srednji tečaj te valute pri Banki Slovenije na dan plačila, razen če je v programu drugače navedeno. Organizator lahko v programu predvidi, da potnik plača storitve, ki bodo opravljene v tujini, neposredno tuji osebi in na način, ki je predviden v programu. <br />V skladu z 900. členom obligacijskega zakonika, si pridržujemo pravico do zvišanja dogovorjene cene, če je po sklenitvi pogodbe prišlo do sprememb v menjalnem tečaju valute ali do spremembe cen zunanjih izvajalcev storitev, ki vplivajo na ceno potovanja. Če zvišanje dogovorjene cene preseže 10 odstotkov, ima potnik pravico odstopiti od pogodbe, ne da bi moral povrniti škodo. O zvišanju cen obvestimo potnike preko naslova elektronske pošte, navedenega v naročilnici.</p>\r\n<p style="text-align: center;"> </p>\r\n<h3 style="text-align: center;">7. Obvestilo pred odhodom</h3>\r\n<p> </p>\r\n<p style="text-align: left;">Končno obvestilo o odhodu prejme potnik na e-mail najkasneje 5 dni pred odhodom. Izjema so potovanja po Sloveniji, kjer prejme potnik obvestilo najkasneje 2 dni pred odhodom. Če potnik obvestila ne prejme, je dolžan obvestiti RENTOOR preko na elektronski naslov party@gremonaparty.com. Morebitno škodo, ki bi nastala ob posredovanju nepopolnega ali nepravilnega naslova potnika za namen izpolnitve pogodbe, nosi potnik sam.</p>\r\n<p style="text-align: center;"> </p>\r\n<h3 style="text-align: center;">8. Odpoved ali sprememba potovanja s strani potnika</h3>\r\n<p> </p>\r\n<p style="text-align: left;">Potnik ima pravico do odpovedi ali spremembe potovanja, v kolikor pisno ali ustno odpove potovanje, na katerega se je prijavil. V tem primeru je upravičen do vračila vplačanih zneskov, vendar v najvišji vrednosti 60% cene aranžmaja. Preostalih 40% vrednosti cene aranžmaja zadrži RENTOOR za kritje stroškov odpovedi potovanja. <br />Višina zneska vračila vplačil, do katerega je potnik upravičen navkljub odpovedi potovanja, je odvisna od časa, v katerem je potnik odpovedal potovanje. Če odpoved nastopi: <br />- do 21 dni pred odhodom – vračilo 60% cene aranžmaja <br />- od 20 do 15 dni pred odhodom – vračilo 40% cene aranžmaja <br />- od 14 do 10 dni pred odhodom – vračilo 20% cene aranžmaja <br />- manj kot 10 do 5 dni pred odhodom – vračilo 10% cene aranžmaja <br />- manj kot 5 dni pred odhodom – potnik ni upravičen do vračila do tedaj vplačane kupnine</p>\r\n<p style="text-align: left;">Osnova za izračun vrednosti vračil po zgoraj omenjenih odstotkih je skupna vrednost naročenih storitev na predračunu zmanjšana za vrednost rezervacije potovanja v višini 19,90 EUR. Potnik v primeru lastne odpovedi potovanja ni upravičen do povračila rezervacije potovanja.</p>\r\n<p style="text-align: left;">V primerih, ko je predmet pogodbe o potovanju med drugim še nakup vstopnic ali letalske vozovnice, ima RENTOOR pravico tudi do zadržanja celotne vrednosti vstopnice oz. letalske vozovnice. V kolikor je potnik do trenutka odjave vplačal manjši del vrednosti od tiste, do vračila katere je po zgornjih pogojih upravičen, ne dobi povračila kupnine.</p>\r\n<p style="text-align: left;">Vsak potnik lahko odpove naročilo aranžmaja, če nastopi smrt, nesreča, nepričakovano poslabšanje zdravstvenega stanja ali hujša nesreča ali smrt ožjih svojcev. V primeru, da nastopi katera izmed naštetih situacij, je potnik dolžan o tem obvestiti RENTOOR in predložiti ustrezna dokazila. V tem primeru se mu vrne celotna do tistega trenutka vplačana kupnina aranžmaja, zmanjšana za rezervacijo potovanja v vrednosti 19,90 EUR in stroške morebitnih vstopnic in/ali letalskih kart.</p>\r\n<p style="text-align: left;">V primeru, da potnik na lastno željo med samo izvedbo potovanja pisno odstopi od nadaljnjega potovanja, ni upravičen do povračila kupnine. </p>\r\n<p style="text-align: center;"> </p>\r\n<h3 style="text-align: center;">9. Riziko odpovedi</h3>\r\n<p> </p>\r\n<p style="text-align: left;">Potnik lahko izbere doplačilo »Riziko odpovedi«, višina katerega je določena v opisu aranžmaja pod rubriko »Možna doplačila«. Praviloma znaša 10% cene aranžmaja. S programom se lahko posebej določi, da odpovedni riziko ni mogoč. Z rizikom odpovedi se potnik zavaruje za povračilo do tedaj plačane kupnine po pogojih v nadaljevanju. Ne glede na plačani riziko odpovedi ima RENTOOR v primeru potnikove odpovedi potovanja pravico do zadržanja vrednosti rezervacije potovanja v višini 19,90 EUR ter pravico do zadrževanja celotnega zneska vplačanega odpovednega rizika. V primeru, ko je predmet pogodbe o potovanju med drugim še nakup vstopnic ali letalske vozovnice, ima RENTOOR pravico tudi do zadržanja celotne vrednosti vstopnice oz. letalske vozovnice. Potnik ima iz naslova vplačanega odpovednega rizika torej pravico do povračila vplačanega zneska za dogovorjene turistične storitve, zmanjšane za rezervacijo potovanja, vplačila odpovednega rizika in vrednosti vstopnin ter morebitne letalske karte. RENTOOR ne odgovarja za druge morebitne stroške, ki jih je imel potnik zaradi načrtovanega potovanja ali druge turistične storitve po pogodbi o potovanju (npr. stroški cepljenja, vizum, potni stroški, ipd.). Riziko odpovedi ni mogoče uveljaviti v primeru, da pride do odpovedi potovanja manj kot 24 ur pred odhodom. Riziko odpovedi je mogoče uveljaviti le, če so poravnane vse obveznosti s strani potnika.</p>\r\n<p style="text-align: center;"> </p>\r\n<h3 style="text-align: center;">10. Odpoved ali sprememba potovanja s strani organizatorja potovanja</h3>\r\n<p> </p>\r\n<p style="text-align: left;">RENTOOR ima pravico brez razloga spremeniti način izvajanja storitev ali v celoti odpovedati izvedbo storitev pred in po izdaji predračuna. Če pride do spremembe načina izvajanja storitev ali spremembe v ceni potovanja je RENTOOR potnika dolžan obvestiti o možnosti in pogojih izvedbe aranžmaja, potnik pa ima pravico odločiti se, ali se bo potovanja udeležil ali ga odpovedal. V primeru delne odpovedi potovanja ali odpovedi potovanja v celoti zaradi sprememb organizatorja potovanja svoje ima potnik pravico prejeti delno ali celotno vračilo kupnine, zmanjšane za stroške prijavnine v vrednosti 4,90 EUR. Če pride do odpovedi storitev s strani RENTOOR po plačilu potnika se RENTOOR zavezuje vrniti celotno kupnino potniku, vendar brez stroškov prijavnine v vrednosti 4,90 EUR, hkrati pa nima nobene druge odgovornosti do potnika. V primeru odpovedi potovanja s strani organizatorja le ta ni dolžan plačati odškodninskih zahtevkov. Organizator je potnike dolžan obvestiti o spremembi ali odpovedi potovanja najkasneje pet dni pred predvidenim odhodom in jim vrniti celoten znesek vplačila.</p>\r\n<p style="text-align: center;"> </p>\r\n<h3 style="text-align: center;">11. Odpoved ali sprememba potovanja zaradi višje sile</h3>\r\n<p style="text-align: left;">Če pride do odpovedi ali spremembe potovanja zaradi višje sile (naravne katastrofe, vojna, odpoved dogodka s strani organizatorja dogodka, ipd.) je RENTOOR potniku odvisno od posamezne situacije dolžan vrniti delno ali celotno kupnino, vendar brez stroškov prijavnine v višini 4,90 EUR.</p>\r\n<p style="text-align: center;"> </p>\r\n<h3 style="text-align: center;">12. Reklamacije</h3>\r\n<p> </p>\r\n<p style="text-align: left;">Reklamacije v primeru pomanjkljivih storitev, storitev neustrezne kakovosti ali ob neupravičenem ne-opravljanju določenih storitev na potovanju morajo biti takoj podane vodniku, neposrednemu izvajalcu storitev, predstavniku organizacije ali pooblaščeni lokalni agenciji. V kolikor se predmet reklamacije ne da odpraviti, je potnik dolžan še v času potovanja podati pisno pritožbo, katere prejem mora biti potrjen s strani vodnika ali predstavnika organizatorja potovanja. V kolikor takšna potrditev s strani vodnika ali predstavnika ne bi bila možna, je potrebno takoj obvestiti RENTOOR preko telefona (klic ali sms). Organizator bo upošteval samo reklamacije, ki so ugotovljene na kraju samem, potnik pa vloži reklamacijo pisno v roku dveh mesecev od opravljene storitve. Organizator poda odgovor in sklep o obravnavanju reklamacije v rokih, določenih z veljavno zakonodajo. Potnik medtem ne sme posredovati podatkov o reklamaciji katerikoli drugi osebi, sodnim ustanovam ali javnim glasilom. Če ne dobi odgovora na reklamacijo v rokih, določenih z veljavno zakonodajo, je upravičen svojo reklamacijo uveljavljati pri pristojnih organizacijah. V primeru, da bi lahko bila reklamacija glede na vsebino rešena na kraju samem, potnik pa ni grajal napake, se šteje, da se je potnik strinjal s tako pomanjkljivo opravljeno storitvijo in je s tem izgubil pravico do vlaganja kasnejše pritožbe. Reklamacija mora biti utemeljena. Višina odškodnine za upravičeno reklamacijo je omejena z vplačanim zneskom aranžmaja, vendar brez stroška prijavnine v višini 4,90 EUR.</p>\r\n<p style="text-align: center;"> </p>\r\n<h3 style="text-align: center;">13. Private party pack aranžmaji</h3>\r\n<p> </p>\r\n<p style="text-align: left;">Vsebina v pripravi.</p>\r\n<p style="text-align: center;"> </p>\r\n<h3 style="text-align: center;">14. Storitev Party prevozi</h3>\r\n<p> </p>\r\n<p style="text-align: left;">Vsebina v pripravi.</p>\r\n<p style="text-align: center;"> </p>\r\n<h3 style="text-align: center;">15. Ponudbe v zadnjem hipu – last minute</h3>\r\n<p> </p>\r\n<p style="text-align: left;">Te so posebna kategorija aranžmajev, kjer so potniku na razpolago zadnja razpoložljiva mesta v določenem kraju in v določeni kategoriji objekta. Odpoved last minute aranžmaja ni mogoča. Prav tako niso mogoče reklamacije na vsebino last minute ponudbe, temveč le na navedeno kategorijo in navedene vključene storitve.</p>\r\n<p style="text-align: center;"> </p>\r\n<h3 style="text-align: center;">16. Potne listine</h3>\r\n<p> </p>\r\n<p style="text-align: left;">Za potne liste in vizume skrbi potnik sam. Za določeno organizirano potovanje lahko za vizume poskrbi tudi organizator. V primeru, da potnik zaradi nepravilno ali pomanjkljivo urejenih dokumentov ne more odpotovati ali mora potovanje prekiniti, nosi sam vse stroške. Če potnik na potovanju izgubi dokumente ali mu jih ukradejo in so nepogrešljivi za nadaljnje potovanje ali vrnitev v domovino, si mora na lastne stroške priskrbeti nove. V primeru, da mora potnik čakati na nove dokumente več kot 12 ur, je organizator upravičen s celotno skupino nadaljevati potovanje, medtem, ko se mora potnik vrniti v domovino na svoje stroške. Potnik v tem primeru ni upravičen do povračila kupnine.</p>\r\n<p style="text-align: center;"> </p>\r\n<h3 style="text-align: center;">17. Zdravstveni predpisi</h3>\r\n<p> </p>\r\n<p style="text-align: left;">Potnik ima pravico do zdravstvene zaščite doma in v tujini v obsegu in na način, ki je določen s predpisi Zavoda za zdravstveno varstvo na območju, kjer je potnik redno zdravstveno zavarovan. V državah brez konvencije o zdravstveni zaščiti nosi stroške zdravljenja potnik sam. Za vstop v nekatere države je potrebno cepljenje, ki ga potnik mora opraviti. Informacije pridobi na Zavodu za zdravstveno varstvo. Na potovanju pa mora imeti s seboj veljavno mednarodno knjižico z vpisanimi opravljenimi cepljenji.</p>\r\n<p style="text-align: center;"> </p>\r\n<h3 style="text-align: center;">18. Prtljaga</h3>\r\n<p> </p>\r\n<p style="text-align: left;">Organizator potovanja ne odgovarja za prtljago potnikov. V primeru opravljanja prevoza s strani organizatorja s kombijem, je za vsak kos prtljage odgovoren potnik sam. V primeru opravljanja prevoza s strani zunanjega izvajalca z avtobusom je odgovornost za prtljago določena skladno s pogoji poslovanja prevoznika. Organizator potovanja prav tako ne more biti odgovoren za krajo prtljage in drugih dragocenosti na potovanju. V primeru kraje, izgube ali uničenja prtljage mora potnik skupaj s predstavnikom prevozne organizacije, hotela ali policije sestaviti zapisnik. Organizator potovanja je lahko samo posrednik med potnikom in prevoznikom, hotelirjem ali policijo, vendar brez vseh materialnih obveznosti. V primeru letalskih prevozov ima potnik pravico do prevoza prtljage skladno z navodili v opisu . Za vsak naslednji kilogram se mora doplačati razlika prevozniku na letališču.</p>\r\n<p style="text-align: center;"> </p>\r\n<h3 style="text-align: center;">19. Letalski prevozi</h3>\r\n<p> </p>\r\n<p style="text-align: left;">Letalski prevozi so v celoti v pristojnosti posameznega letalskega prevoznika, zato za morebitne spremembe pri letalskih prevozih ne moremo prevzeti odgovornosti. Še posebej pri čarterskih poletih se lahko zgodi, da se načrtovana ura poleta spremeni tudi tik pred odhodom. Kot organizatorji potovanja se bomo vedno potrudili najti najboljšo možno rešitev tudi v takem primeru, prerazporediti določene oglede, če bo to potrebno. Vendar pa potniki v taki situaciji za nastale spremembe ne morejo kriviti RENTOOR in niso upravičeni do odškodnine v zvezi z nastalo spremembo.</p>\r\n<p style="text-align: center;"> </p>\r\n<h3 style="text-align: center;">20. Uporaba podatkov</h3>\r\n<p> </p>\r\n<p style="text-align: left;">RENTOOR vse pridobljene podatke o potnikih uporablja v skladu z Zakonom o varstvu osebnih podatkov. Šteje se, da potnik s prijavo, opisano v 3. Členu teh Splošnih pogojev soglaša, da RENTOOR hrani in uporablja njegove osebne podatke z namenom, da se izpolnijo pogodbene obveznosti in pravice ter tudi za namene neposrednega trženja in obveščanja o ponudbi RENTOOR in njegovih poslovnih partnerjev. V kolikor se potnik ne strinja z uporabo podatkov v namene trženja in obveščanja, lahko to izjavi pisno ali ustno. Osebne podatke uporabnikov, pridobljene ob izvajanju promocijskih aktivnost lahko RENTOOR hrani in uporablja za namene neposrednega trženja in obveščanja o ponudbi RENTOOR in njegovih poslovnih partnerjev. Uporabnik lahko kadarkoli zahteva odjavo od prejemanja obvestil oz. izbris iz podatkovne baze uporabnikov.</p>\r\n<p style="text-align: center;"> </p>\r\n<h3 style="text-align: center;">21. Sodna pristojnost</h3>\r\n<p> </p>\r\n<p style="text-align: left;">Morebitne spore iz pogodbe, sklenjene med RENTOOR in potnikom, bosta pogodbeni stranki reševali sporazumno. V primeru, da to ni mogoče, je pristojno sodišče.</p>', 'splosni-pogoji', -1);

-- --------------------------------------------------------

--
-- Struktura tabele `partners`
--

CREATE TABLE IF NOT EXISTS `partners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) COLLATE utf8_slovenian_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `url` varchar(128) COLLATE utf8_slovenian_ci NOT NULL,
  `published` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_id` int(11) NOT NULL,
  `title` varchar(64) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `type` tinyint(1) DEFAULT '-1',
  `permission` text COLLATE utf8_slovenian_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_permissions_statuses_idx` (`status_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `polls`
--

CREATE TABLE IF NOT EXISTS `polls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) COLLATE utf8_slovenian_ci NOT NULL,
  `dt_published` datetime DEFAULT NULL,
  `question` varchar(256) COLLATE utf8_slovenian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `polls_answers`
--

CREATE TABLE IF NOT EXISTS `polls_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poll_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `polls_option_id` int(11) NOT NULL,
  `datetime` datetime NOT NULL,
  `ip` varchar(40) COLLATE utf8_slovenian_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_polls_answers_polls1_idx` (`poll_id`),
  KEY `fk_polls_answers_users1_idx` (`user_id`),
  KEY `fk_polls_answers_polls_options1_idx` (`polls_option_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `polls_options`
--

CREATE TABLE IF NOT EXISTS `polls_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poll_id` int(11) NOT NULL,
  `title` varchar(64) COLLATE utf8_slovenian_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_polls_options_polls1_idx` (`poll_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) COLLATE utf8_slovenian_ci NOT NULL,
  `skey` varchar(64) COLLATE utf8_slovenian_ci NOT NULL,
  `svalue` text COLLATE utf8_slovenian_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_UNIQUE` (`skey`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=4 ;

--
-- Odloži podatke za tabelo `settings`
--

INSERT INTO `settings` (`id`, `title`, `skey`, `svalue`) VALUES
(2, 'Warnings', 'tab-warnings', '<p>Content tab warning</p>'),
(3, 'Payments', 'tab-payments', 'Content tab payments');

-- --------------------------------------------------------

--
-- Struktura tabele `statuses`
--

CREATE TABLE IF NOT EXISTS `statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) COLLATE utf8_slovenian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=3 ;

--
-- Odloži podatke za tabelo `statuses`
--

INSERT INTO `statuses` (`id`, `title`) VALUES
(1, 'Administrator'),
(2, 'User');

-- --------------------------------------------------------

--
-- Struktura tabele `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) COLLATE utf8_slovenian_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `teams`
--

CREATE TABLE IF NOT EXISTS `teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) COLLATE utf8_slovenian_ci NOT NULL,
  `published` tinyint(1) DEFAULT '-1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabele `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `password` varchar(40) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `email` varchar(128) COLLATE utf8_slovenian_ci NOT NULL,
  `name` varchar(64) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `surname` varchar(64) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `dt_birth` datetime DEFAULT NULL,
  `address` text COLLATE utf8_slovenian_ci,
  `phone` varchar(64) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT '1',
  `post` varchar(255) COLLATE utf8_slovenian_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `fk_users_statuses1_idx` (`status_id`),
  KEY `fk_users_cities1_idx` (`city_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=2356 ;

--
-- Odloži podatke za tabelo `users`
--

INSERT INTO `users` (`id`, `status_id`, `city_id`, `password`, `email`, `name`, `surname`, `dt_birth`, `address`, `phone`, `enabled`, `post`) VALUES
(1, 1, 1, '8095360ef708668f9eb0d2b92657194ad03dbd3d', 'schtr4jh@schtr4jh.net', 'Bojan', 'Rajh', '0000-00-00 00:00:00', 'Šmartno ob Paki 103', '40269596', 1, '3327 Šmartno ob Paki'),
(10, 1, 1, 'cf4fffdb059ad2a1f6bd2bb045c5e235eebf758b', 'tadejkocnik@gmail.com', 'Tadej', 'Kočnik', NULL, NULL, NULL, -1, ''),
(9, 1, 1, 'd1ce51c339ce378759a1915fcdbb867ee05ab2e6', 'mario@gremonaparty.com', 'Mario', 'Benić', NULL, NULL, NULL, 1, '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
