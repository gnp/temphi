INSERT INTO `gnp_dev`.`translations` (`id`, `slug`, `title`, `content`)
VALUES (NULL, 'packet_without_additions', 'Naročilnica osnovna cena brez dodatkov', 'Osnovna cena brez dodatkov:');

/* 11. 5. 2014 */

UPDATE `gnp_dev`.`translations` SET `content` = 'Izvedi več' WHERE `translations`.`id` = 16;
UPDATE `gnp_dev`.`translations` SET `content` = 'Preveri ponudbo potovanj' WHERE `translations`.`id` = 57;
UPDATE `gnp_dev`.`translations` SET `content` = 'Preveri cene potovanj in vstopnic' WHERE `translations`.`id` = 62;

INSERT INTO `gnp_dev`.`translations` (`id`, `slug`, `title`, `content`) VALUES (NULL, 'offer navigation', NULL, 'Navigacija');