CREATE TABLE IF NOT EXISTS `paypal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_hash` varchar(40) NOT NULL,
  `paypal_hash` varchar(40) NOT NULL,
  `paypal_id` varchar(64) NOT NULL,
  `status` varchar(16) NOT NULL,
  `dt_started` datetime NOT NULL,
  `dt_confirmed` datetime NOT NULL,
  `price` float NOT NULL,
  `paypal_payer_id` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`,`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;