CREATE TABLE IF NOT EXISTS `offers_payment_methods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `method` varchar(16) COLLATE utf8_slovenian_ci NOT NULL,
  `bill` varchar(16) COLLATE utf8_slovenian_ci NOT NULL,
  `enabled` int(11) NOT NULL,
  `offer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `offer_id` (`offer_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci AUTO_INCREMENT=1 ;