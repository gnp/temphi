<?php

class UFWModel {
	public $name; // model's name
	public $table; // // model's db table
	protected $pk; // db primary key
	protected $mk; // db main key

	protected $hasMany = array();
	protected $belongsTo = array();
	protected $hasAndBelongsToMany = array();
	protected $hasOne = array();

	protected $fields = array();
	protected $data = array();
	protected $countForms = array();
	
	function __construct() {
		$this->name = str_replace("Model", "", get_class($this));
		$this->table = Core::fromCamel($this->name);

		$this->hasMany = array();
		$this->belongsTo = array();
		$this->hasAndBelongsToMany = array();
		$this->hasOne = array();

		$this->fields = array();
		
		$this->countForms = array(
			'min' => Core::fromCamel(substr($this->name, -1) == "s" ? substr($this->name, 0, -1) : $this->name),
			'max' => $this->name
		);

		$this->pk = 'id';
		$this->mk = 'title'; // default: name | title
	}

	public function __get($name) {
		if (Core::$db === FALSE) {
			return FALSE;
		}
		
		$currentObjectName = str_replace("Model", "", get_class($this));
		
		if ($currentObjectName == Core::toCamel($currentObjectName)) {
		    if (Core::issetModule($currentObjectName))
                return Core::getModule($currentObjectName, FALSE);
            else if (Core::issetCtrl(lcfirst($currentObjectName)))
                return Core::getCtrl($currentObjectName, FALSE);
            else if (Core::issetModel(lcfirst($currentObjectName)))
                return Core::getModel($currentObjectName, FALSE);
        }
        
		return FALSE;
	}

	public function __call($name, $args) {
		$currentObjectName = str_replace("Model", "", get_class($this));
		
		if (Core::issetModule($currentObjectName) && method_exists(Core::getModule($currentObjectName, FALSE), $name)) {
			return call_user_func_array(array(Core::getModule($currentObjectName), $name), $args);
		}

		return FALSE;
	}

	public function isConnected($to) {
		$to = Core::toCamel($to);
		if (array_key_exists($to, $this->hasMany) || array_key_exists($to, $this->belongsTo) || array_key_exists($to, $this->hasAndBelongsToMany) || array_key_exists($to, $this->hasOne)) {
			return TRUE;
		}

		return FALSE;
	}

	public function getConnection($key) {
		if (array_key_exists($key, $this->hasMany))
			return $this->hasMany[$key];
		else if (array_key_exists($key, $this->belongsTo))
			return $this->belongsTo[$key];
	}

	public function validate($field, $data) {
		$error = FALSE;
		if (!isset($this->fields[$field]) || !is_array($this->fields[$field]) || empty($this->fields[$field])) {
			return TRUE;
		} else {
			$errors = array();
			foreach ($this->fields[$field] AS $type => $check) {
				if ($type == "int" || $type == "float") {
					if ($type == "int" && !Validate::isInt($data)) {
						$error = TRUE;
						$errors[] = $this->table . "." . $field . " should be integer.";
					} else if ($type == "float" && !Validate::isFloat($data)) {
						$error = TRUE;
						$errors[] = $this->table . "." . $field . " should be float.";
					} else {
						if (isset($check['min'])) {
							if (Validate::isInt($check['min'])) {
								if ($data < $check['min']) {
									$error = TRUE;
									$errors[] = $this->table . "." . $field . " should be min " . $check['min'] . ".";
								}
							} else {
								Core::setWarning("Wrong configuration for " . $this->name . "." . $field . ".min.");
							}
						}
						
						if (isset($check['max'])) {
							if (Validate::isInt($check['max'])) {
								if ($data > $check['min']) {
									$error = TRUE;
									$errors[] = $this->table . "." . $field . " should be max " . $check['max'] . ".";
								}
							} else {
								Core::setWarning("Wrong configuration for " . $this->name . "." . $field . ".max.");
							}
						}
					}
				}
				
				if ($type == "ip") {
					if (!Validate::isIP($data)) {
						$error = TRUE;
						$errors[] = $this->table . "." . $field . " should be IP.";
					}
				}
				
				if ($type == "mail") {
					if (!Validate::isMail($data)) {
						$error = TRUE;
						$errors[] = $this->table . "." . $field . " should be mail.";
					}
				}
				
				if ($type == "url") {
					if (!Validate::isUrl($data)) {
						$error = TRUE;
						$errors[] = $this->table . "." . $field . " should be IP.";
					}
				}
				
				if ($type == "bool") {
					if (Validate::isBool($data)) {
						$error = TRUE;
						$errors[] = $this->table . "." . $field . " should be boolean.";
					}
				}
				
				if ($type == "datetime") {
					if (Validate::isDatetime($data)) {
						$error = TRUE;
						$errors[] = $this->table . "." . $field . " should be datetime.";
					}
				}
				
				if ($type == "text" || $type == "alphanumeric" || $type == "alpha" || $type == "password") {
					if (isset($check['min'])) {
						if (Validate::isInt($check['min'])) {
							if (mb_strlen($data) < $check['min']) {
								$error = TRUE;
								$errors[] = $this->table . "." . $field . " should be min " . $check['min'] . " characters long.";
							}
						} else {
							Core::setErr("Wrong configuration for " . $this->name . "." . $field . ".min.");
						}
					}
					
					if (isset($check['max'])) {
						if (Validate::isInt($check['max'])) {
							if (mb_strlen($data) > $check['max']) {
								$error = TRUE;
								$errors[] = $this->table . "." . $field . " should be max " . $check['max'] . " characters long.";
							}
						} else {
							Core::setErr("Wrong configuration for " . $this->name . "." . $field . ".max.");
						}
					}
				}
			}
		}

		return !$error ? TRUE : $errors;
	}

    public function isValidInsert($data) {
        $errors = array();
        foreach ($this->fields AS $field => $conf) {
            if (isset($conf['default']) && !isset($data[$field]))
                $data[$field] = $conf['default'];

            if (isset($conf['not null']) && $conf['not null'] == TRUE && (!isset($data[$field]) || empty($data[$field])))
                $errors[] = $field . " shouldn't be empty";
            else if ($this->validate($field, isset($data[$field]) ? $data[$field] : NULL) !== TRUE)
                $errors[] = $field . " doesn't validate";
        }
        
        return empty($errors) ? TRUE : $errors;
    }
    public function isValidUpdate($data) {
        $success = TRUE;
        foreach ($data AS $field => $conf) {
            if (isset($conf['update']) && !isset($data[$field]))
                $data[$field] = $conf['update'];

            if (isset($conf['not null']) && $conf['not null'] == TRUE && (isset($data[$field]) && empty($data[$field])))
                return FALSE;
            else if ($this->validate($field, isset($data[$field]) ? $data[$field] : NULL) !== TRUE)
                return FALSE;
        }
        
        return TRUE;
    }

	public function insert($data, $validate = TRUE, $escape = TRUE, $makeQuotes = TRUE) {
		$error = FALSE;
		$invalid = array();
		$generated = array(); // generated data
		foreach ($this->fields AS $field => $conf) {
			if (isset($conf['default']) && !array_key_exists($field, $data))
				$data[$field] = $conf['default'];
            
            if (isset($conf['unique']) && $conf['unique'] == TRUE) {
                $sqlUnique = "SELECT COUNT(id) AS cnt FROM " . $this->table . " WHERE " . $field . " = '" . $this->escape($data[$field]) . "'";
                $qUnique = $this->q($sqlUnique);
                $rUnique = $this->f($qUnique);
                
                if (!empty($rUnique) && $rUnique['cnt'] != 0) {
                    $error = TRUE;
                    $invalid[] = $this->name . "." . $field . " should be unique.";
                }
            }
            
			if ($validate == TRUE) {
				if (isset($conf['not null']) && $conf['not null'] == TRUE && (array_key_exists($field, $data) && empty($data[$field]))) {
					$error = TRUE;
					$invalid[] = $this->name . "." . $field . " is required but empty.";
				} else {
					$tmpData = array_key_exists($field, $data) ? $data[$field] : NULL;
					$cool = $this->validate($field, $tmpData);
	
					if ($cool == TRUE) {
					    $generated[$field] = $this->makeQuotes($this->escape($tmpData, $escape), !isset($conf['quotes']) || $conf['quotes'] == TRUE);
					} else {
						$error = TRUE;
						$invalid[] = implode("<br /> " . $cool);
					}
				}
			} else {
				$generated[$field] = $this->makeQuotes($this->escape($data[$field], $escape), $makeQuotes);
			}
		}

		if ($error === FALSE) {
			if (!empty($generated)) {
				$sql = "INSERT INTO `" . $this->table . "` (`" . implode("`,`", array_keys($generated)) . "`) VALUES (" . implode(", ", $generated) . ")";
				$this->q($sql);

				if (mysql_error()) {
                    echo mysql_error();
                    Debug::addError(mysql_error());
					return FALSE;
				}

				return mysql_insert_id();
			} else {
			    echo "Query is empty. Have you really sent data?";
				Debug::addError("Query is empty. Have you really sent data?");
				return NULL;
			}
		} else {
		    echo implode("<br />", $invalid);
			Debug::addError(implode("<br />", $invalid));
			return FALSE;
		}
	}

	public function update($data, $primaryKey, $validate = TRUE, $escape = TRUE, $makeQuotes = TRUE, $where = NULL, $validKeys = NULL) {
		if (!Validate::isInt($primaryKey)) {
			Debug::addError("Primary key not set.");
			return FALSE;
		}
		
		// update only allowed keys
		if (is_array($validKeys)) {
			$tempData = array();
			foreach ($validKeys AS $validKey) {
				if (isset($data[$validKey]))
					$tempData[$validKey] = $data[$validKey];
			}
			$data = $tempData;
		}
		
		$error = FALSE;
		$invalid = array();
		$generated = array(); // generated data
		
		foreach ($this->fields AS $field => $conf) {
			if (isset($conf['update']) && !isset($data[$field]))
				$data[$field] = $conf['update'];
            
            if ($validate == TRUE) {
                if (!array_key_exists($field, $data)) {
					continue;
                }
                
				if (isset($conf['not null']) && $conf['not null'] == TRUE && (isset($data[$field]) && empty($data[$field]))) {
					$error = TRUE;
					$invalid[] = $this->name . "." . $field . " is required but empty.";
				}
            
                if (isset($conf['unique']) && $conf['unique'] == TRUE) {
                    $sqlUnique = "SELECT COUNT(id) AS cnt FROM " . $this->table . " WHERE " . $field . " = '" . $this->escape($data[$field]) . "' AND id != " . $primaryKey;
                    $qUnique = $this->q($sqlUnique);
                    $rUnique = $this->f($qUnique);

                    if (!empty($rUnique) && $rUnique['cnt'] != 0) {
                        $error = TRUE;
                        $invalid[] = $this->name . "." . $field . " should be unique.";
                    }
                }
				
				if (array_key_exists($field, $data)) {
					$cool = $this->validate($field, $data[$field]);
	
					if ($cool == TRUE) {
						$generated[] = $field . " = " . $this->makeQuotes($this->escape($data[$field], $escape), $makeQuotes);
					} else {
						$error = TRUE;
						$invalid[] = implode("<br /> " . $cool);
					}
				}
			} else {
				if (isset($data[$field])) {
					$generated[] = '`' . $field . '`' . " = " . $this->makeQuotes($this->escape($data[$field], $escape), $makeQuotes);
				}
			}
		}

		if ($error === FALSE) {
			if (!empty($generated)) {
				$sql = "UPDATE `" . $this->table . "` " .
					"SET " . implode(", ", $generated) . " " .
					"WHERE " . $this->pk . " = " .  $primaryKey . $where;
				//die();
				$this->q($sql);
				
				if (mysql_error()) {
					//die("SQL error!" . "<br />" . mysql_error());
					Debug::addError(mysql_error());
					return FALSE;
				}
				
				return TRUE;
			} else {
				//die("Empty query");
				//Core::setErr("Query is empty. Have you really sent data?", FALSE);
				Debug::addError("Query is empty. Have you really sent data?");
				return NULL;
			}
		} else {
			//die("Invalid query" . print_r($invalid));
			Debug::addError(implode("<br />", $invalid));
			return FALSE;
		}
	}
	
	public function delete($id, $where = NULL) {
		$sql = "DELETE FROM " . $this->table . " " .
			"WHERE id " . (!is_array($id) ? " = " . $id : (" IN(" . implode($id, ",") . ")"))
			. (is_null($where) ? NULL : (" AND " . $where));

		return $this->q($sql);
	}
	
	public function prepareWhere($where, $escape = TRUE, $quote = TRUE, $glue = "and") {
		$numInt = 0;
	 	$group = array();
	 	foreach ($where AS $key => $val) {
	 		if (($key == "and" || $key == "or") && is_array($val)) {
	 			$group[] = "(" . $this->prepareWhere($val, $escape, $quote, strtoupper($key)) . ")";
	 		} else if ($key === $numInt) {
 				$group[] = $val;
 				$numInt++;
	 		} elseif (!is_array($val)) {
	 			$val = $escape == TRUE ? $this->escape($val) : $val;
	 			$val = $quote == TRUE ? $this->makeQuotes($val) : $val;
 				$group[] = $this->name . "." . $key . " = " . $val . "";
	 		} else {
	 			$group[] = $key . " IN(" . $this->prepareWhere($val, $escape, $quote, ",") . ")";
	 		}
	 	}
		return "(" . implode(") " . strtoupper($glue) . " (", $group) . ")";
	}

	public function get($type, $what = NULL, $where = NULL, $args = NULL) {
		$escape = isset($args['escape']) && Validate::isBool($args['escape']) ? Validate::toBool($args['escape']) : TRUE;
		$quote = isset($args['quote']) && Validate::isBool($args['quote']) ? Validate::toBool($args['quote']) : TRUE;
		$glue = isset($args['glue']) ? strtoupper($args['glue']) : "and";

    	// $type = tolower($type);
    	$gen['LIMIT'] = NULL;
    	$gen['SELECT'] = "*";
    	$gen['FROM'] = " FROM " . $this->table . " AS " . $this->name . "";
    	$gen['JOIN'] = NULL;
    	$gen['GROUP BY'] = NULL;
    	$gen['ORDER BY'] = " ORDER BY " . $this->pk . " DESC";

    	if ($type == "all") {
    		// do nothing =)
    	} elseif ($type == "first" || $type == "last") {
    		$gen['LIMIT'] = " LIMIT 1";
    		$gen['ORDER BY'] = $type == "first" ? " ORDER BY " . $this->name . "." . $this->pk . " ASC" : " ORDER BY " . $this->pk . " DESC";
    	} elseif ($type == "random") {
    		// make query, which select all ids and then select random of it and make new query
    	} elseif ($type == "count") {
    		$gen['SELECT'] = " COUNT(*)";
    	} elseif ($type == "distinct") {
    		$gen['SELECT'] = " DISTINCT *";
    	}

    	if (is_array($what)) {
    		$generated = array();
    		foreach ($what AS $key => $val) {
    			$generated[] = $val;
    		}

    		$gen['SELECT'] = " " . implode(", ", $generated);
    	} else if (!is_null($what)) {
    		$gen['SELECT'] = $what;
    	}

    	$gen['WHERE'] = !empty($where) ? 
			Validate::isInt($where)
				? " WHERE " . $this->pk . " = " . $where
				: " WHERE " . $this->prepareWhere($where, $escape, $quote, $glue) : NULL;

    	if (isset($args['group by'])) {
    		$generated = array();
    		foreach ($args['group by'] AS $key => $val) {
    			$generated[] = $val;
    		}
    		$gen['GROUP BY'] = " GROUP BY " .  implode(", ", $generated);
    	}

    	$gen['HAVING'] = isset($args['having']) ? " HAVING " . $this->prepareWhere($args['having'], $escape, $quote, $glue) : NULL;

    	$gen['ORDER BY'] = isset($args['order by']) ? " ORDER BY " . (is_array($args['order by']) ? implode(", ", $args['order by']) : $args['order by']) : (isset($gen['ORDER BY']) ? $gen['ORDER BY'] : NULL);

    	$gen['LIMIT'] = isset($args['limit']) ? " LIMIT " . (is_array($args['limit']) ? implode(", ", $args['limit']) : $args['limit']) : (isset($gen['limit']) ? $gen['limit'] : NULL);

    	$gen['SUB'] = isset($args['sub']) ? " " . $args['sub'] : NULL;

    	$sql = "SELECT " . $gen['SELECT'] . " " . $gen['FROM'] . $gen['JOIN'] . $gen['WHERE'] . $gen['GROUP BY'] . $gen['HAVING'] . $gen['ORDER BY'] . $gen['LIMIT'] . $gen['SUB'];

    	if ($type == "first" || $type == "last")
    		return Core::$db->f(Core::$db->q($sql));

    	return Core::$db->q($sql);
	}

	public function sql($sql = NULL) {
		return Core::$db !== FALSE ? Core::$db->sql($sql) : FALSE;
	}

	public function q($sql = NULL) {
		return Core::$db !== FALSE ? Core::$db->q($sql) : FALSE;
	}

	public function f($q = NULL) {
		return Core::$db !== FALSE ? Core::$db->f($q) : FALSE;
	}

	public function fx($q) {
		$r = Core::$db !== FALSE ? Core::$db->f($q) : FALSE;

		if ($r !== FALSE) {
			$i = 0;
			$return = array();
			foreach ($r AS $key => $val) {
				$table = mysql_field_table($q, $i);
				$return[(isset(Core::$classes["Controller"][Core::toCamel($table)]) ? Core::$classes["Controller"][Core::toCamel($table)]->name : $table)][$key] = $val;
				$i++;
			}
			return $return;
		}
		return FALSE;
	}

	public function c($q) {
		return Core::$db !== FALSE ? Core::$db->c($q) : FALSE;
	}

	public function a() {
		return Core::$db !== FALSE ? Core::$db->a() : FALSE;
	}

	public function escape($val) {
		return Core::$db !== FALSE ? Core::$db->escape($val) : $val;
	}

	public function makeQuotes($val, $makeQuotes = TRUE) {
		if ($makeQuotes === FALSE) {
			return $val;
		} else if (is_null($val)) {
			return "NULL";
		} else if (empty($val)) {
			return "NULL";
		} else if (Validate::isNumeric($val)) {
			return $val;
		} else if (Validate::isBool($val)) {
			return Validate::fromBool($val);
		} else if (is_float($val)) {
            return number_format($val, 4, '.', '');
        } else {
			return "'" . $val . "'";
		}
	}

	public function getTreeList($data = NULL) {
		$data['parent'] = isset($data['parent']) ? $data['parent'] : -1;
		$data['depth'] = isset($data['depth']) ? $data['depth'] + 1 : 0;
		$data['separator'] = isset($data['separator']) ? $data['separator'] : "-";

		$sql = "SELECT " . $this->pk . ", " . $this->mk . " " . 
			"FROM " . $this->table . " " .
			("WHERE page_id = " . $data['parent'] . " " . (isset($data['where']) ? " " . $data['where'] . " " : NULL)) .
			"ORDER BY " . (isset($data['order']) ? $data['order'] : $this->mk);
		$q = $this->q($sql);
		
		if ($data['depth'] == 0)
			$contentList = '<select' . (isset($data['name']) ? ' name="' . $data['name'] . '"' : NULL) . (isset($data['id']) ? ' id="' . $data['id'] . '"' : NULL) . '>';
		else
			$contentList = NULL;

		$contentList .= isset($data['default']) && $data['parent'] == -1
			? '<option value="-1"' . (isset($data['selected']) && $data['selected'] == -1 ? ' selected="selected"' : NULL) . '>' . $data['default'] . '</option>'
			: NULL;
			
		while ($r = $this->f($q)) {
			$contentList .= '<option value="' . $r[$this->pk] . '"' . (isset($data['selected']) && $data['selected'] ==  $r[$this->pk] ? ' selected="selected"' : NULL) . '>' . str_repeat($data['separator'], $data['depth']) . $r[$this->mk] . '</option>';
			$data['parent'] = $r[$this->pk];
			$contentList .= $this->getTreeList($data);
		}
		
		if ($data['depth'] == 0)
			$contentList .= '<select>';
		
		return $contentList;
	}

	public function getArrList($fields = NULL, $where = NULL, $args = NULL, $key = NULL, $val = NULL) {
		$fields = empty($fields) ? $this->pk . ", " . $this->mk : $fields;
		$where = empty($where) ? NULL : $where;
		$args = empty($args) ? NULL : $args;
		$key = empty($key) ? $this->pk : $key;
		$val = empty($val) ? $this->mk : $val;
		
		$q = $this->get("all", $fields, $where, $args);
		
		$arr = array();
		while ($r = $this->f($q))
			$arr[$r[$key]] = $r[$val];
		
		return $arr;
	}
	
	public function getList($data = NULL) {
		$contentList = '<select ' . (!isset($data['disabled']) ? NULL : ($data['disabled'] == TRUE ? 'disabled="disabled" ' : NULL)) . (isset($data['name']) ? ' name="' . $data['name'] . '"' : NULL) . (isset($data['class']) ? ' class="' . $data['class'] . '"' : NULL) . (isset($data['id']) ? ' id="' . $data['id'] . '"' : NULL) . (isset($data['multiple']) ? ' multiple="' . $data['multiple'] . '"' : NULL) . '>';
		$contentList .= isset($data['default']) 
			? '<option value="-1"' . (isset($data['selected']) && $data['selected'] == -1 ? ' selected="selected"' : NULL) . '>' . $data['default'] . '</option>'
			: NULL;
			
		if (isset($data['data']) && is_array($data['data'])) {
			foreach ($data['data'] AS $key => $val)
				$contentList .= '<option value="' . $key . '"' . (isset($data['selected']) && ((!is_array($data['selected']) && $data['selected'] == $key) || (is_array($data['selected']) && in_array($key, $data['selected']))) ? ' selected="selected"' : NULL) . '>' . $val . '</option>';
		} else {
			$sql = "SELECT " . $this->pk . ", " . (isset($data['mk']) ? $data['mk'] . " AS " : NULL) . $this->mk . " " . 
				"FROM " . $this->table . " " .
				(isset($data['where']) ? " WHERE " . $data['where'] . " " : NULL) .
				"ORDER BY " . (isset($data['order']) ? $data['order'] : $this->mk);
			$q = $this->q($sql);
				
			while ($r = $this->f($q))
				$contentList .= '<option value="' . $r[$this->pk] . '"' . (isset($data['selected']) && ((!is_array($data['selected']) && $data['selected'] == $r[$this->pk]) || (is_array($data['selected']) && in_array($r[$this->pk], $data['selected']))) ? ' selected="selected"' : NULL) . '>' .  $r[$this->mk] . '</option>';
		}
		
		$contentList .= '</select>';
		
		return $contentList;
	}

	public function genValidateJS($formName = "maestroForm", $JS = TRUE) {
		$elements = array();
		foreach ($this->fields AS $field => $conf) {
			$rules = array();

			/*if (isset($conf['tinymce']) && $conf['tinymce'] == TRUE)
				$rules[] = "callback_krasneki";
			else */
			
			if (isset($conf['not null']) && $conf['not null'] == TRUE)
				$rules[] = "required";
				
			if (isset($conf['float'])) {
				$rules[] = "decimal";
				
				if (isset($conf['float']['min'])) {
					$rules[] = "greater_than[" . ($conf['float']['min']-0.00000001) . "]";
				}
				if (isset($conf['float']['max'])) {
					$rules[] = "less_than[" . ($conf['float']['max']+0.00000001) . "]";
				}
			}
			
			if (isset($conf['int'])) {
				$rules[] = "integer";
				
				if (isset($conf['int']['min'])) {
					$rules[] = "greater_than[" . ($conf['int']['min']-1) . "]";
				}
				if (isset($conf['int']['max'])) {
					$rules[] = "less_than[" . ($conf['int']['max']+1) . "]";
				}
			}
			
			if (isset($conf['email'])) {
				$rules[] = "valid_email";
			}
			
			if (isset($conf['ip'])) {
				$rules[] = "valid_ip";
			}
			
			if (isset($conf['text'])) {
				if (isset($conf['text']['min'])) {
					$rules[] = "min_length[" . ($conf['text']['min']) . "]";
				}
				if (isset($conf['text']['max'])) {
					$rules[] = "max_length[" . ($conf['text']['max']) . "]";
				}
			}

			if (!empty($rules))
				$elements[] = '{name: \'' . Core::fromCamel($this->countForms['min']) . '[' . $field . ']\', display: \'' . $field . '\', rules: \'' . implode("|", $rules) . '\'}';
		}

		if (empty($elements))
			return NULL;

		return ($JS == TRUE ? '<script type="text/javascript">$(document).ready(function(){' : NULL) . ' validator = new FormValidator("' . $formName . '", [' . implode(",", $elements) . '], function (errors) { FormValidatorError(errors); }); validateForm();' . ($JS == TRUE ? '});</script>' : NULL);
	}
	
	function genRequiredStar(){
		$rules = array();
		foreach ($this->fields AS $field => $conf) {

			if (isset($conf['not null']) && $conf['not null'] == TRUE)
				//$rules[] = '$("label#' . $field . '[name^=\'' . $this->countForms['min'] . '\']").html(function(){return $(this).val() + "*";});';
				$rules[] = '$("label.control-label[for^=\'' . $field . '\']").html(function(){return $("label.control-label[for=\'' . $field . '\']").html() + " *";});';
		}
		
		return !empty($rules) ? '<script type="text/javascript">$(document).ready(function(){ ' . implode($rules) . ' });</script>' : NULL;
	}
}

?>