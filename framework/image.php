<?php

class Image {
	static $img;
	
	static $path;
	static $file;
	
	static $type;
	
	function __construct() {
		
	}
	
	function load($file, $path) {
	    if (strpos($file, "http://") !== FALSE || strpos($file, "https://") !== FALSE) { // online
	        $temp = explode("/", $file);
            $file = end($temp);
            $path = substr(implode("/", $temp), 0, strlen($path) - strlen($file));
	    }
        
		self::$path = $path;
		self::$file = $file;

		if (!is_file(self::$path . self::$file))
			return;
		
		$imageInfo = @getimagesize(self::$path . self::$file);
		
		self::$type = $imageInfo[2];
		
		if (self::$type == IMAGETYPE_JPEG)
			self::$img = imagecreatefromjpeg(self::$path . self::$file);
		else if (self::$type == IMAGETYPE_GIF)
			self::$img = imagecreatefromgif(self::$path . self::$file);
		else if (self::$type == IMAGETYPE_PNG)
			self::$img = imagecreatefrompng(self::$path . self::$file);
		
		$im  = imagecreatetruecolor(self::getW(), self::getH());
		$transparent = imageColorAllocateAlpha($im, 255, 255, 255, 127);
		imagecolortransparent($im, $transparent);
		imagecopy($im, self::$img, 0, 0, 0, 0, self::getW(), self::getH());
	    imagealphablending($im, false);
	    imagesavealpha($im, true);
		self::$img = $im;
	}
	
	static function save($file, $path = NULL, $type = NULL, $compression = 75, $permissions = 0775) {
		if ($type == NULL)
			$type = IMAGETYPE_PNG;
		
		is_null($path) ? $path = WWW_PATH : NULL;

		$path = rawurldecode($path);
		$file = rawurldecode($file);
		
		$path2create = pathinfo($path . $file);
		
		@mkdir($path2create['dirname'], 0775, TRUE);
		
		if ($type == IMAGETYPE_JPEG)
			imagejpeg(self::$img, $path . $file, $compression);
		else if ($type == IMAGETYPE_GIF)
			imagegif(self::$img, $path . $file);         
		else if ($type == IMAGETYPE_PNG)
			imagepng(self::$img, $path . $file);
		
		@chmod($path . $file, $permissions);
	}
	
	static function output($type = IMAGETYPE_PNG) {
		header('Pragma: public');
		header('Cache-Control: max-age=86400');
		if ($type == IMAGETYPE_JPEG) {
			header('Content-Type: image/jpeg');
			imagejpeg(self::$img);
		} else if ($type == IMAGETYPE_GIF) {
			header('Content-Type: image/gif');
			imagegif(self::$img);         
		} else if ($type == IMAGETYPE_PNG) {
			header('Content-Type: image/png');
			imagepng(self::$img);
		}
	}
	
	static function getW() {
		return imagesx(self::$img);
	}
	
	static function getH() {
		return imagesy(self::$img);
	}
	
	static function getRatio() {
		return self::getWidth() / self::getHeight();
	}
	
	static function destroy() {
		imagedestroy(self::$img);
	}
	
	static function resizeToHeight($h) {
		self::resize(self::getW() * ($h / self::getH()), $h);
	}
	
	static function resizeToWidth($w) {
		self::resize($w, self::getH() * ($w / self::getW()));
	}

	static function returnString() {
		return;
	}
	
	static function rotate($deg) { // needs feature fix
		$source = NULL;
		if (substr(self::$file, -3) == "png")
	    	$source = imagecreatefrompng(self::$path . self::$file);
		else if (substr(self::$file, -3) == "jpg") {
	    	$source = imagecreatefromjpeg(self::$path . self::$file);
			/*$mt = microtime();
			self::save($mt, WWW_PATH . "cache" . DS . "temp" . DS);
		    $source = imagecreatefromstring(file_get_contents(WWW_PATH . "cache" . DS . "temp" . DS . $mt));
		    @unlink(WWW_PATH . "cache" . DS . "temp" . DS . $mt);*/
		}

	    imagealphablending($source, false);
	    imagesavealpha($source, true);

	    /*$rotation = imagerotate($source, $deg, imageColorAllocateAlpha($source, 0, 0, 0, 127));
	    imagealphablending($rotation, false);
	    imagesavealpha($rotation, true);

	    self::$img = $rotation;*/

	    $i = imagerotate($source, $deg, imageColorAllocateAlpha($source, 255, 255, 255, 127));
	    imagealphablending($i, false);
	    imagesavealpha($i, true);

	    self::$img = $i;

	    /*header('Content-type: image/png');
	    imagepng($rotation);
	    imagedestroy($source);
	    imagedestroy($rotation);
		die();

		self::$img = imagerotate(self::$img, $deg, 1);*/
	}

	static function scale($scale) {
		self::resize(self::getW() * $scale/100, self::getheight() * $scale/100);
	}
	
	static function resize($w, $h) {
		$im  = imagecreatetruecolor($w, $h);
		imagecolortransparent($im, imagecolorallocate($im, 0, 0, 0));
		imagecopyresampled($im, self::$img, 0, 0, 0, 0, $w, $h, self::getW(), self::getH());
		self::$img = $im;
	}
    
    static function watermark($watermark) { // working?
        $watermark = imagecreatefrompng($watermark); // create watermark
        $watermark_width = imagesx($watermark);  
        $watermark_height = imagesy($watermark);  
        
        $image = imagecreatetruecolor($watermark_width, $watermark_height);  
        $image = self::$img; 
        
        $dest_x = self::getW() - $watermark_width - 5;  
        $dest_y = self::getH() - $watermark_height - 5;  
        
        imagecopymerge($image, $watermark, $dest_x, $dest_y, 0, 0, $watermark_width, $watermark_height, 100);
        
        self::$img = $image;
    }
}

?>