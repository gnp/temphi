<?php

class UFWHTML {
    static function input($data) {
    	$return = '<input ';

        $return .= self::makeAttributes($data, array("type", "class", "value", "id", "name", "placeholder", "step", "checked"));

        $return .= ' />';

        return $return;
    }
    
    static function textarea($attributes) {
        $valid = array("class", "id", "name", "placeholder");

        $finalConf = array();
        foreach($attributes AS $key => $val) {
            if (in_array($key, $valid))
                $finalConf[] = $key . "=" . '"' . htmlspecialchars($val) . '"';
        }

        return '<textarea ' . implode(" ", $finalConf) . '>' . (isset($attributes['value']) ? $attributes['value'] : NULL) . '</textarea>';
    }
	
	static function select($data = NULL) {
		$return = '<select';
		
		$return .= self::makeAttributes($data, array("id", "class", "name", "data"));

		$return .= '>';
		
		if (isset($data['options']))
		foreach ($data['options'] AS $id => $option) {
			if (is_array($option)) {
				$option['value'] = $id;
				$option['selected'] = isset($data['selected']) && $data['selected'] == $id ? TRUE : FALSE;
				$return .= self::option($option);
			} else {
				$return .= self::option(array(
					"value" => $id,
					"text" => $option,
					"selected" => isset($data['selected']) && $data['selected'] == $id ? TRUE : FALSE
				));
			}
		}

		$return .= '</select>';
		
		return $return;
	}
	
	static function option($data) {
		$return = '<option';
		
		$return .= self::makeAttributes($data, array("value", "data"));
		
		if (isset($data['selected']) && in_array($data['selected'], array(TRUE, 1, "selected")))
			$return .= " selected";
		
		$return .= '>';
		
		if (isset($data['text']))
			$return .= $data['text'];
		
		$return .= '</option>';
		
		return $return;
	}
	
	static function a($data) {
		$return = '<a';
		
		$return .= self::makeAttributes($data, array("id", "class", "href"));
		
		$return .= '>';
		
		if (isset($data['text']))
			$return .= $data['text'];
		
		$return .= '</a>';
		
		return $return;
	}
	
	static function makeAttributes($data, $keys = array()) {
		$return = NULL;
		
		foreach ($data AS $key => $val)
			if (empty($keys) || in_array($key, $keys))
				$return .= ' ' . ($key == "data" ? "data-" : NULL) . $key . '="' . $val . '"';
		
		return $return;
	}

	static function checked($val) {
		return in_array($val, array(1, TRUE, "1")) ? ' checked="checked"' : NULL;
	}

	static function selected($val) {
		return in_array($val, array(1, TRUE, "1")) ? ' selected="selected"' : NULL;
	}
}

class HTML extends UFWHTML {}

?>