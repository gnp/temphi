<?php

require_once CORE_PATH . "helpers" . DS . "cache.php";

class FileCache extends Cache {
	private static $path = "filecache";
	private static $enabled = FALSE;
	
	public function set($key, $val, $ttl = 3600) {
		if (!is_dir(WWW_PATH . "cache" . DS . self::$path . DS . self::getFilePath($key, TRUE)))
			mkdir(WWW_PATH . "cache" . DS . self::$path . DS . self::getFilePath($key, TRUE), 0775, TRUE);
		
		file_put_contents(WWW_PATH . "cache" . DS . self::$path . DS . self::getFilePath($key), $val);
	}

	public function get($key) {
		if (is_file(WWW_PATH . "cache" . DS . self::$path . DS . self::getFilePath($key)))
			return file_get_contents(WWW_PATH . "cache" . DS . self::$path . DS . self::getFilePath($key));
		
		return NULL;
	}

	public function delete($key) {
		if (is_file(WWW_PATH . "cache" . DS . self::$path . DS . self::getFilePath($key)))
			return unlink(WWW_PATH . "cache" . DS . self::$path . DS . self::getFilePath($key));
		
		return NULL;
	}

	public function exists($key, $ttl = 3600) {
		if (!self::$enabled)
			return FALSE;
		
		if (is_file(WWW_PATH . "cache" . DS . self::$path . DS . self::getFilePath($key))
		&& filemtime(WWW_PATH . "cache" . DS . self::$path . DS . self::getFilePath($key)) + $ttl > mktime())
			return TRUE;
		
		return FALSE;
	}

	public function clear() {
		return FALSE;
	}
	
	private function getFilePath($file, $fileonly = FALSE) {
		$file = sha1($file);
		return substr($file, 0, 2) . "/" . substr($file, 2, 2) . ($fileonly == FALSE ? "/" . substr($file, 4) : NULL);
	}
	
	public function setPath($path) {
		self::$path = $path;
	}
	
	public function setEnabled($enabled) {
		self::$enabled = $enabled;
	}
}

?>
