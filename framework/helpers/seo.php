<?php

class UFWSEO {
	private static $keywords;
	private static $description;
	private static $title;
	private static $image;
	private static $facebook;
	
	public static function niceUrl($string) {
		$string = strtolower($string);
        $string = str_replace(array("č", "š", "ž", "€", "Č", "Š", "Ž"), array("c", "s", "z", "eur", "c", "s", "z"), $string); //test
	    $string = preg_replace("/[^a-z0-9\s-]/", "", $string);
	    $string = trim(preg_replace("/[\s-]+/", " ", $string));
	    $string = preg_replace("/\s/", "-", $string);
	    return $string;
	}
	
	public static function setFacebook($bool = TRUE) {
		self::$facebook = $bool;
	}
	
	public static function description($description = NULL) {
		if (is_null($description))
			return htmlspecialchars_decode(self::$description);
		
		self::$description = htmlspecialchars($description);
	}
	
	public static function keywords($keywords = NULL) {
		if (is_null($keywords))
			return htmlspecialchars_decode(self::$keywords);
		
		self::$keywords = htmlspecialchars($keywords);
	}
	
	public static function title($title = NULL) {
		if (is_null($title))
			return htmlspecialchars_decode(self::$title);
		
		self::$title = htmlspecialchars($title);
	}
	
	public static function image($image = NULL) {
		if (is_null($image))
			return htmlspecialchars_decode(self::$image);
		
		//self::$image = htmlspecialchars($image);
		self::$image = "http://hr.gonparty.eu/img/logo_200x200.png";
	}
	
	public static function getHTML() {
		if (empty(self::$title))
			self::$title = SITE;

		return '<title>' . self::$title . '</title>
		<meta name="description" content="' . self::$description . '" />
		<meta name="keywords" content="' . self::$keywords . '" />' .
		(self::$facebook == TRUE ? ('<meta property="og:title" content="' . self::$title . '" />
		<meta property="og:site_name" content="' . self::$title . '" />
		<meta property="og:description" content="' . self::$description . '" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="' . substr(URL, 0, -1) . Router::currentURL() . '" />
		<meta property="fb:admins" content="1197210626" />
		<meta property="fb:app_id" content="' . Settings::get("facebook_appid") . '" />
		<meta property="og:image" content="' . self::$image . '" />') : NULL);
	}
}

class SEO extends UFWSEO {}

?>