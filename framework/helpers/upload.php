<?php

class UFWUpload {
	private $extensions = array();
	private $path = NULL;
	private $folder = NULL;
	private $file = array();
	private $name = NULL;
	private $extension = NULL;
	private $err = array();
	
	function setFile($name, $reverse = FALSE, $array = FALSE) {
		$this->file = $this->getFile($name, $reverse, $array);
        
        return $this;
	}
	
	private function getFile($name, $reverse = FALSE, $array = FALSE) {
		return $array === FALSE 
			? ($reverse === FALSE
				? (isset($_FILES[$name])
					? $_FILES[$name]
					: array("not name")
				)
				: (isset($_FILES[$name]['name'][$reverse])
					? array(
						"name" => $_FILES[$name]['name'][$reverse],
						"type" => $_FILES[$name]['type'][$reverse],
						"tmp_name" => $_FILES[$name]['tmp_name'][$reverse],
						"error" => $_FILES[$name]['error'][$reverse],
						"size" => $_FILES[$name]['size'][$reverse]
						)
					: array("not rev")
				)
			)
			: (isset($_FILES[$name]['name'][$array][$reverse])
				? array(
					"name" => $_FILES[$name]['name'][$array][$reverse],
					"type" => $_FILES[$name]['type'][$array][$reverse],
					"tmp_name" => $_FILES[$name]['tmp_name'][$array][$reverse],
					"error" => $_FILES[$name]['error'][$array][$reverse],
					"size" => $_FILES[$name]['size'][$array][$reverse]
					)
				: array("not arr")
			);
	}
	
	function issetFile($name, $reverse = FALSE, $array = FALSE) {
		$file = $this->getFile($name, $reverse, $array);

		return isset($file['size']) && !empty($file['size']);
	}
	
	function make() {
		$this->extension = strtolower(end(explode(".", $this->file['name'])));
		
		$err = array();
		if (!in_array($this->extension, $this->extensions)) {
			$this->err[] = "Wrong filetype.";
			return FALSE;
		}
		
		if (!is_dir(WWW_PATH . $this->path . "/" . $this->folder))
			if (!mkdir(WWW_PATH . $this->path . "/" . $this->folder, 0755, TRUE)) {
				$this->err[] = "Cannot create dir.";
				return FALSE;
			}
		
		$name = UFWSEO::niceUrl($this->file['name']) . "." . $this->extension;
				
		if (file_exists(WWW_PATH . $this->path . "/" . $this->folder . "/" . $name))
			for ($i = 0; $i < 1000; $i++)
			if (!file_exists(WWW_PATH . $this->path . "/" . $this->folder . "/" . $i . "_" . $name)) {
				$name = $i . "_" . $name;
				break;
			}
		
		if (file_exists(WWW_PATH . $this->path . "/" . $this->folder . "/" . $name)) {
			$this->err[] = "Cannot place image.";
			return FALSE;
		}

		if (!is_writeable(WWW_PATH . $this->path . "/" . $this->folder . "/") || !move_uploaded_file($this->file['tmp_name'], WWW_PATH . $this->path . "/" . $this->folder . "/" . $name)) {
			$this->err[] = "Cannot move image.";
			return FALSE;
		}
		
		if (!chmod(WWW_PATH . $this->path . "/" . $this->folder . "/" . $name, 775)) {
			$this->err[] = "Cannot chmod image.";
			return FALSE;
		}
		
		$this->name = $this->folder . "/" . $name;
		
		return TRUE;
	}
	
	function setExtensions($arr = array()) {
		$this->extensions = $arr;
		return $this;
	}
	
	function setPath($path = NULL) {
		$this->path = $path;
		
		return $this;
	}
	
	function getName($path = FALSE) {
		return ($path == TRUE ? "/" . $this->path : "") . $this->name;
	}
	
	function setFolder($id) {
		$id = sha1($id);
		
		$this->folder = substr($id, 0, 2) . "/" . substr($id, 2, 2);
		
		return $this;
	}
	
	function getErrors() {
		$err = $this->err;
		$this->err = array();
		return $err;
	}
}

class Upload extends UfwUpload {}

?>