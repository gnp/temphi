<?php

// Requires Validate Class
class Auth {
	private static $conf = array(
		"pass" => array(
			"min" => 3,
			"max" => NULL,
			"type" => "text",
			"db" => "password",
		),
		"user" => array(
			"min" => 3,
			"max" => 40,
			"type" => "alphanum",
			"db" => "username",
		),
		"hash" => array(
			"min" => 40,
			"max" => 40,
			"type" => "alphanum",
			"db" => "hash",
		),
		"login" => array(
			"url" => "/users/login",
			"url_success" => "/" 
		),
		"logout" => array(
			"url" => "/users/logout",
			"url_success" => "/"
		),
	);
	
	private static $isLoggedIn = FALSE;
	
	function getConf($key1 = NULL, $key2 = NULL) {
		if (is_null($key1))
			return $this->conf;
		else if (isset($this->conf[$key1])) {
			if (is_null($key2))
				return $this->conf[$key1];
			else if (isset($this->conf[$key1][$key2]))
				return $this->conf[$key1][$key2];
		}
		
		return NULL;
	}
	
	static function getUserID() {
		return self::isLoggedIn() && isset($_SESSION['User']['id']) ? $_SESSION['User']['id'] : -1;
	}
	
	public function getStatus($array = FALSE) {
		return self::isLoggedIn() && isset($_SESSION['Auth']['status_id']) ? ($array == TRUE ? $_SESSION['Auth']['status_id'] : $_SESSION['Auth']['status_id'][0]) : -1;
	}
	
	function __construct() {
		self::$isLoggedIn = FALSE; // for security
	}

	public static function isLoggedIn($redirect = FALSE) {
		if (!isset($_SESSION['User'])
		|| empty($_SESSION['Auth']['hash'])
		|| !self::isValidHash($_SESSION['Auth']['hash'])) {
			if ($redirect == TRUE)
				Status::redirect(Router::make("login"));
			
			return FALSE;
		} else if (self::$isLoggedIn === TRUE) {
			return TRUE;
		}
		
		$sql = "SELECT u.id " .
			"FROM users u " .
			"LEFT JOIN logins l ON l.user_id = u.id " .
			"WHERE u.id = '" . Core::$db->escape($_SESSION['Auth']['user']) . "' " .
			"AND l.hash = '" . Core::$db->escape($_SESSION['Auth']['hash']) . "' " .
			"AND l.active = 1 ";
		$q = Core::$db->q($sql);

		self::$isLoggedIn = Core::$db->c($q) == 1 ? TRUE : FALSE;
		
		if (self::$isLoggedIn !== TRUE && $redirect == TRUE)
			Status::redirect(self::getLoginURL());
		
		return self::$isLoggedIn;
	}

	public function isStatus($statuses) {
		if (!self::isLoggedIn())
			return FALSE;
			
		if (!is_array($statuses))
			$statuses = array($statuses);

		foreach ($statuses AS $status)
			if (in_array($status, self::getStatus(TRUE)))
				return TRUE;

		return FALSE;
	}
	
	public static function simpleLogin($username, $password) {
		if (!(self::isValidUsername($username) && self::isValidPassword($password)))
			return FALSE;

		$sql = "SELECT u.* " .
			"FROM users u " .
			"WHERE u." . self::$conf['user']['db'] . " = '" . mysql_real_escape_string($username) . "' " .
			"AND u." . self::$conf['pass']['db'] . " = '" . self::hashPassword($password) . "'";

		return self::makeLogin($sql, TRUE);
	}
    
    public function tryAutoLogin($simple = FALSE) {
        if (isset($_COOKIE['hash']))
            return self::loginByHash($_COOKIE['hash'], $simple);
        
        return FALSE;
    }
    
    public function loginByHash($hash = NULL, $simple = FALSE) {
        if (is_null($hash) && isset($_COOKIE['hash']))
            $hash = $_COOKIE['hash'];
        
        if (!self::isValid($_COOKIE['hash'], "hash"))
            return FALSE;
        
        $sql = "SELECT user_id FROM logins WHERE hash = '" . mysql_real_escape_string($hash) . "' AND active = 1";
        $q = Core::$db->q($sql);
        $r = Core::$db->f($q);
        
        while ($r2 = Core::$db->f($q))
            return FALSE;
        
        return self::loginbyUserID($r['user_id']);
    }

	public static function login($username, $password) {
		if (!(self::isValidUsername($username) && self::isValidPassword($password)))
			return FALSE;
			
		$sql = "SELECT u.*, s.id AS sid, s.title " .
			"FROM users u " .
			"INNER JOIN statuses s ON s.id = u.status_id " .
			"WHERE u." . self::$conf['user']['db'] . " = '" . mysql_real_escape_string($username) . "' " .
			"AND u." . self::$conf['pass']['db'] . " = '" . self::hashPassword($password) . "'";
			
		return self::makeLogin($sql);
	}

	public static function hashLogin() {
		if (!isset($_COOKIE['hash']) || !self::isValidHash($_COOKIE['hash']))
			return FALSE;

		$hash =  $_COOKIE['hash'];

		$sql = "SELECT u.*, s.id AS sid, s.title " .
			"FROM users u " .
			"INNER JOIN statuses s ON s.id = u.status_id " .
			"INNER JOIN logins l ON l.user_id = u.id " .
			"WHERE l.hash = '" . $hash . "' " .
			"AND l.active = 1";

		return self::makeLogin($sql);
	}

	public static function makeLogin($sql, $simple = FALSE) {
		$q = Core::$db->q($sql);

		if (Core::$db->c($q) === 1) {
			$r = Core::$db->f($q);
			$_SESSION['User'] = $r;

			$hash = sha1(microtime()); // @ToDo :: sha1(microtime()) - is strong enough?
			$id = $r['id'];
			
			$_SESSION['Auth'] = array();
			$_SESSION['Auth']['hash'] = $hash;
			$_SESSION['Auth']['user'] = $id;

			if (isset($r['sid']))
			do {
				$_SESSION['Auth']['status_id'][] = $r['sid'];
			} while ($r = Core::$db->f($q));
			
			setcookie("hash", $hash, strtotime("+30 days"), "/") OR Debug::addError("Cannot set hash login cookie!");
			
			///*if ($simple == FALSE) {
				$sql = "INSERT INTO logins (user_id, datetime, hash, active, ip) VALUES (" . $_SESSION['User']['id'] . ", '" . date("Y-m-d H:i:s") . "', '" . $hash . "', 1, '" . $_SERVER['REMOTE_ADDR'] . "')";
				$q = @Core::$db->q($sql);
			//}*/

			self::$isLoggedIn = TRUE;

			return TRUE;
		}
		
		return FALSE;
	}
	
    public static function getUser($data = NULL) {
        if (is_null($data)) return $_SESSION['User'];
        else if (isset($_SESSION['User'][$data])) return $_SESSION['User'][$data];
        else return FALSE;
    }
    
    function loginByUserID($id, $simple = FALSE) {
        $sql = "SELECT u.*, s.id AS sid, s.title " .
            "FROM users u " .
            "INNER JOIN statuses s ON s.id = u.status_id " .
            "WHERE u.id = '" . $id . "'";
            
        return self::makeLogin($sql, $simple);
    }
    
    function updateSessionByUserID($id = NULL, $simple = FALSE) {
    	if ($id == NULL)
			$id = Auth::getUserID();
		
        $sql = "SELECT u.*, s.id AS sid, s.title " .
            "FROM users u " .
            "INNER JOIN statuses s ON s.id = u.status_id " .
            "WHERE u.id = '" . $id . "'";
        $q = Core::$db->q($sql);

        if (Core::$db->c($q) === 1) {
            $_SESSION['User'] = Core::$db->f($q);
            return TRUE;
        }
        
        return FALSE;
    }
	
	public static function fullLogin($simple = FALSE, $username = NULL, $password = NULL, $successCallback = NULL) {
		$username = is_null($username) ? $_POST['username'] : $username;
		$password = is_null($password) ? $_POST['password'] : $password;

		if (self::isLoggedIn())
			Status::redirect(self::getSuccessLoginRedirect());
		else {
			if ($simple == FALSE)
				self::login($username, $password);
			else
				self::simpleLogin($username, $password);
				
			if (self::isLoggedIn()) {
				if (is_callable($successCallback)) $successCallback();
				
				Status::redirect(self::getSuccessLoginRedirect());
			} else
				Status::redirect(self::getLoginURL());
		}
	}
	
	public static function fullLogout() {
		self::logout();
		header("Location: " . self::getSuccessLogoutRedirect());
	}
	
	public static function setLoginURL($arr) {
		self::$conf['login']['url'] = $arr;
	}
	
	public static function getLoginURL() {
		return self::$conf['login']['url'];
	}
	
	public static function setSuccessLoginRedirect($arr) {
		self::$conf['login']['url_success'] = $arr;
	}
	
	public static function getSuccessLoginRedirect() {
		return self::$conf['login']['url_success'];
	}
	
	public static function setLogoutURL($arr) {
		self::$conf['logout']['url'] = $arr;
	}
	
	public static function getLogoutURL() {
		return  self::$conf['logout']['url'];
	}
	
	public static function setSuccessLogoutRedirect($arr) {
		self::$conf['logout']['url_success'] = $arr;
	}
	
	public static function getSuccessLogoutRedirect() {
		return  self::$conf['logout']['url_success'];
	}
	
	public static function setUserField($field) {
			self::$conf['user']['db'] = $field;
	}
	
	public static function setPassField($field) {
			self::$conf['pass']['db'] = $field;
	}

	public static function logout() {
	    $sql = "UPDATE logins SET active = -1 WHERE hash = '" . mysql_real_escape_string($_SESSION['Auth']['hash']) . "' AND user_id = '" . mysql_real_escape_string($_SESSION['User']['id']) . "'";
	    Core::$db->q($sql);
	    
	    session_destroy();
        unset($_SESSION);
		
        foreach ($_COOKIE AS $key => $cookie)
        	if ($key != "zekom")
            	setcookie($key, "", strtotime("-7 days"));

		self::$isLoggedIn = FALSE;

		return TRUE;
	}

	public static function hashPassword($password) {
		return sha1($password); // @ToDo :: sha1($password) - is strong enough?
	}

	public function makePassword() {
		return substr(sha1(microtime()), 0, 10); // @ToDo :: sha1(microtime()) - is strong enough?
	}

	public static function isValidUsername($username) {
		return self::isValid($username, "user");
	}

	public static function isValidPassword($password) {
		return self::isValid($password, "pass");
	}

	public static function isValidHash($hash) {
		return self::isValid($hash, "hash");
	}

	private static function isValid($input, $type) {
		if (!in_array($type, array("pass", "user", "hash"))) return FALSE;

		if (isset(self::$conf[$type])) {
			if (isset(self::$conf[$type]['min'])
			&& Validate::isInt(self::$conf[$type]['min'])
			&& self::$conf[$type]['min'] > 0
			&& mb_strlen($input) < self::$conf[$type]['min'])
				return FALSE;

			if (isset(self::$conf[$type]['max'])
			&& Validate::isInt(self::$conf[$type]['max'])
			&& self::$conf[$type]['max'] > 0
			&& mb_strlen($input) > self::$conf[$type]['max'])
				return FALSE;
		}

		return TRUE;
	}
}

?>