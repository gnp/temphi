<?php

class Paginator {
	private $cAll = 0; // number of all elements
	private $limit = 2; // elements per page
	private $maxDiff = 5; // max difference in paginator
	
	private $links = array();
	
	private $currentPage = 1;
	
	private $url = "";
	
	private $settings = array();
	
	function __construct($config = array()) {
		$this->config($config);
	}
	
	public function setSettings($arr) {
		foreach ($arr AS $key => $val)
			$this->settings[$key] = $val;
	}
	
	public function make() {
		$numberOfPages = ceil($this->cAll / $this->limit);

		$start = $this->currentPage - $this->maxDiff;
		$start = $start < 1 ? 1 : $start;
		
		$end = $this->currentPage + $this->maxDiff;
		$end = $end > $numberOfPages ? $numberOfPages : $end;
		
		$tplPaginator = new UFWTemplate("paginator", "default");
		
		$links = array();
		
		//if ($this->currentPage > 1) // if we can go at least 1 page back
		//	$links["<"] = $this->getUrl($this->currentPage - 1);
			
		//if ($this->currentPage > 2) // if we can go at least 2 pages back
		//	$links["<<"] = $this->getUrl(1);
			
		for ($i = $start; $i <= $end; $i++) {
			$links[$i] = $this->getUrl($i) ;
		}
		
		// if we can go at least 1 pages forward
		//if ($this->currentPage < $numberOfPages)
		//	$links[">"] = $this->getUrl($this->currentPage + 1);
			
		// if we can go at least 2 pages forward
		//if ($this->currentPage + 1 < $numberOfPages)
		//	$links[">>"] =  $this->getUrl($numberOfPages);
		
		$return = array();
		$content = NULL;
		
		foreach ($links AS $title => $href) {
			if ($title != $this->currentPage) {
				$return[$title] = $tplPaginator->parse(array(
					"href" => $href,
					"title" => $title,
					"class" => "ufwPaginator notactive"
				),
				"unactive") . "\n";
			} else {
				$return[$title] = $tplPaginator->parse(array(
					"href" => $href,
					"title" => $title,
					"class" => "ufwPaginator active "
				),
				"active") . "\n";
			}
			$content .= $return[$title];
		}
		
		return $content;
	}
	
	private function getUrl($page) {
		return str_replace("#*#", $page, $this->url);
	}
	
	public function config($conf) {
		$arr = array(
			"all" => "cAll",
			"limit" => "limit",
			"diff" => "maxDiff",
			"page" => "currentPage",
			"url" => "url"
		);
		
		if (is_array($conf))
		foreach ($arr AS $key => $val)
			if (isset($conf[$key]))
				$this->{$val} = $conf[$key];
	}
	
	public function getSqlLimit($implode = TRUE) {
		$arr = array($this->currentPage < 2 ? 0 : ($this->limit * ($this->currentPage - 1)), $this->limit);
		return $implode == TRUE ? implode(", ", $arr) : $arr;
	}
}

?>