<?php

class SloDate {
	private static $daysOfWeek = array(
		0 => "-- izberite dan --",
		1 => "Ponedeljek",
		2 => "Torek",
		3 => "Sreda",
		4 => "Četrtek",
		5 => "Petek",
		6 => "Sobota",
		7 => "Nedelja",
	);
	private static $namesOfMonths = array(
		0 => "-- izberite dan --",
		1 => "Januar",
		2 => "Februar",
		3 => "Marec",
		4 => "April",
		5 => "Maj",
		6 => "Junij",
		7 => "Julij",
		8 => "Avgust",
		9 => "September",
		10 => "Oktober",
		11 => "November",
		12 => "December",
	);
	
	public static function getDayOfWeek($day, $short = FALSE) { return $short !== TRUE ? self::$daysOfWeek[(int)$day] : substr(self::$daysOfWeek[(int)$day], 0, 3); }
	
	public static function getNameOfMonth($month, $short = FALSE) { return $short !== TRUE ? self::$namesOfMonths[(int)$month] : substr(self::$namesOfMonths[(int)$month], 0, 3); }
}

?>