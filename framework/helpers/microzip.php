<?php

class MicroZip {
	private $files = array();
	function __construct() {

	}

	public function add($files = array(), $path = NULL) {
		if (is_null($path))
			$path = WWW_PATH . "cache" . DS . "print" . DS;

		foreach ($files AS $file)
			if (file_exists($path . $file))
				$this->files[] = $path . $file;
	}

	public function save($file = NULL, $path = NULL) {
		if (is_null($path))
			$path = WWW_PATH . "cache" . DS . "print" . DS;
		
		if (empty($this->files))
			return NULL;

		if (empty($file))	
			$file = date("Y-m-d H-i-s") . ".zip";

		$zip = new ZipArchive();

		if (!$zip->open($path . $file, ZIPARCHIVE::CREATE))
			return FALSE;

		foreach ($this->files AS $f) {
			$zip->addFile($f, end(explode("/", $f)));
		}

		if ($zip->close())
			return $path . $file;
		else
			return FALSE;
	}
}

?>