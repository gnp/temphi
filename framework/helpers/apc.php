<?php

/* @Author: Bojan Rajh
 * @Datetime: February 2011
 * @Description: Class APC extends UFW's abstract class Cache {cache.php}
*/

/* @To-Do START

APC::conf
Gets configuration variables and save the configuration.
Help: http://www.php.net/manual/en/apc.configuration.php

APC::__constructor()
Calls APC:: conf()

@To-Do END */

require_once "cache.php";

class APC extends Cache {
	public function set($key, $val, $ttl = 3600) {
		return TRUE;
		/*if (!apc_store($key, $val, $ttl)) {
			//App::addErr("Can't store key '" . $key . "' in APC Cache!");
			return FALSE;
		}

		return TRUE;*/
	}

	public function get($key) {
		return TRUE;
		$val = apc_fetch($key);

		if ($val) {
			return $val;
		}

		//App::addErr("Can't fetch key '" . $key . "' from APC Cache!");
		return FALSE;
	}

	public function delete($key) {
		return TRUE;
		$res = apc_delete($key);

		if ($res) {
			return TRUE;
		}

		//App::addErr("Can't delete key '" . $key . "' from APC Cache!");
		return FALSE;
	}

	public function exists($key, $return = FALSE) {
		return TRUE;
		
		/*if (apc_exists($key)) {
			if ($return === FALSE) {
				return TRUE;
			}
			
			return $this->get($key);
		}

		return FALSE;*/
	}

	public function clear() {
		return TRUE;
		$res = apc_clear_cache();
		
		if (!$res) {
			App::addErr("Can't clear APC Cache!");
			return FALSE;
		}

		return TRUE;
	}
}
?>
