<?php

/* @Author: Bojan Rajh
 * @Datetime: February 2011
 * @Description: UFW's abstract class Cache
*/

/* @To-Do START
@To-Do END */

abstract class Cache {
	abstract public function set($key, $val, $ttl = 3600);

	abstract public function get($key);

	abstract public function delete($key);

	abstract public function exists($key, $return = FALSE);

	abstract public function clear();
}
?>
