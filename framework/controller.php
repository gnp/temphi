<?php

class UFWController {
	protected $name;
	protected $db;
	protected $expand;

	public function __construct() {
		$this->name = str_replace("Controller", "", get_class($this));
		$this->db = Core::$db;
		$this->expand = $this->name;
	}
	
	/* Autoload models and modules */
	public function __get($name) {
		if ($name == "db") {
			return Core::$db;
		} else if (Core::issetModel($name)) {
			return Core::getModel($name);
		} else if (Core::loadModule($name) && Core::issetModel($name)) {
			return Core::getModel($name);
		} else if (Core::issetModel($this->expand)) {
			return Core::getModel($this->expand);
		} else if (Core::loadModule($this->expand) && Core::issetModel($this->expand)) {
			return Core::getModel($this->expand);
		}
	}

	public function __call($name, $args) {
		if ((Core::issetModel($name) || (Core::loadModel($name) && Core::issetModel($name))) && method_exists(Core::getModel($name), $name)) {
			return call_user_method($name, Core::getModel($name), $args);
		}
	}
}

?>
