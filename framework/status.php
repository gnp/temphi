<?php

class Status {
	static $http = array (
       100 => "HTTP/1.1 100 Continue",
       101 => "HTTP/1.1 101 Switching Protocols",
       200 => "HTTP/1.1 200 OK",
       201 => "HTTP/1.1 201 Created",
       202 => "HTTP/1.1 202 Accepted",
       203 => "HTTP/1.1 203 Non-Authoritative Information",
       204 => "HTTP/1.1 204 No Content",
       205 => "HTTP/1.1 205 Reset Content",
       206 => "HTTP/1.1 206 Partial Content",
       300 => "HTTP/1.1 300 Multiple Choices",
       301 => "HTTP/1.1 301 Moved Permanently",
       302 => "HTTP/1.1 302 Found",
       303 => "HTTP/1.1 303 See Other",
       304 => "HTTP/1.1 304 Not Modified",
       305 => "HTTP/1.1 305 Use Proxy",
       307 => "HTTP/1.1 307 Temporary Redirect",
       400 => "HTTP/1.1 400 Bad Request",
       401 => "HTTP/1.1 401 Unauthorized",
       402 => "HTTP/1.1 402 Payment Required",
       403 => "HTTP/1.1 403 Forbidden",
       404 => "HTTP/1.1 404 Not Found",
       405 => "HTTP/1.1 405 Method Not Allowed",
       406 => "HTTP/1.1 406 Not Acceptable",
       407 => "HTTP/1.1 407 Proxy Authentication Required",
       408 => "HTTP/1.1 408 Request Time-out",
       409 => "HTTP/1.1 409 Conflict",
       410 => "HTTP/1.1 410 Gone",
       411 => "HTTP/1.1 411 Length Required",
       412 => "HTTP/1.1 412 Precondition Failed",
       413 => "HTTP/1.1 413 Request Entity Too Large",
       414 => "HTTP/1.1 414 Request-URI Too Large",
       415 => "HTTP/1.1 415 Unsupported Media Type",
       416 => "HTTP/1.1 416 Requested range not satisfiable",
       417 => "HTTP/1.1 417 Expectation Failed",
       500 => "HTTP/1.1 500 Internal Server Error",
       501 => "HTTP/1.1 501 Not Implemented",
       502 => "HTTP/1.1 502 Bad Gateway",
       503 => "HTTP/1.1 503 Service Unavailable",
       504 => "HTTP/1.1 504 Gateway Time-out"        
   );
   static $delayedRedirect = FALSE;
  
	static function code($code, $text = NULL, $terminate = TRUE) {
    /*if ($code == 400 || $code == 404 || empty($code)) {
        error_log($code . " " . $text . " " . $_SERVER['REQUEST_URI']);
        if (!Core::dev())
        self::rewrite();
    }*/
		if (isset(self::$http[$code])) {
      if (!in_array($code, array(303, 302, 301))) {
        file_put_contents(LOG_PATH . "codes" . DS . $code . "-" . date("Ymdhis") . "-" . substr(sha1(microtime()), 0, 10), json_encode($_SERVER) . json_encode($_GET) . json_encode($_POST) . json_encode($_SESSION) . json_encode($_COOKIE));
      }

      if (!Core::getLayout()) {
        Core::__LayoutPrepareLoad("main:full");
      }
             if (method_exists(Core::getLayout(), "err" . $code)) {
                  Core::getLayout()->{"err" . $code}();
             } else {
              @ob_end_clean();
              
              header(self::$http[$code]);
              
              header(str_replace("HTTP/1.1", "Status:", self::$http[$code]));
              
              $tpl = new Template($code, "default", FALSE);

              // gnp
              if ($tpl->isEmpty()) { // fallback 
                $tpl = new Template("404", "default", FALSE);
              }


              if ($tpl->isEmpty()) // fallback
                $tpl = new Template("status");

              $tpl->parse(array(
                    "code" => $code,
                     "site title" => SITE . " - " . str_replace("HTTP/1.1 ", NULL, self::$http[$code]),
                     "title" => str_replace("HTTP/1.1 ", NULL, self::$http[$code]),
                     "href" => URL,
                     "site" => SITE,
                     "description" => !empty($text) ? $text : (Core::dev() ? print_r(debug_backtrace(), TRUE) . Debug::getCurrent() : NULL),
                     "ufw debug" => Core::dev() ? (Debug::getSession() . Debug::getCurrent()) : NULL,
              ));

              Core::setParseVar("css page", "r404");
              
              $content = $tpl->display();

              Core::setParseVar("content", $content);
       }
       
       if ($terminate == TRUE) {
        // potrebe gnp
        //Core::loadLayout("main:full");
        //Core::__LayoutPrepareLoad("main:full");
        
        //Core::setParseVar("css page", "r404");

        Core::__LayoutAfterLoad();
        Core::preFinalize();
        Core::finalize();
        die("<!-- err " . $code . $text . " -->");
       }
		} else
			return FALSE;
	}

    static function rewrite($url = NULL) {
      $url = is_null($url) ? URL : $url;
      header("HTTP/1.1 301 Moved Permanently"); 
      header("Location: " .  $url);
      die(); 
    }

    static function redirect($location, $immediately = TRUE) {
        if ($immediately == TRUE || !empty(self::$delayedRedirect)) {
            Debug::push();
			//debug_print_backtrace();
			//die((!empty(self::$delayedRedirect) ? self::$delayedRedirect : $location));
			//echo "Location: " . (!empty(self::$delayedRedirect) ? self::$delayedRedirect : $location);
      //
            if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                header('Content-Type: application/json');
                echo json_encode(array('redirect' => $location));
                die();
            }
            header("Location: " . (!empty(self::$delayedRedirect) ? self::$delayedRedirect : $location));
            die();
        } else {
            self::$delayedRedirect = $location;
        }
    }
    
    static function delayedRedirect() {
        if (self::$delayedRedirect != FALSE) {
            Debug::push();
            header("Location: " . self::$delayedRedirect);
            die("Location: " . $location);
        }
    }
}

?>
