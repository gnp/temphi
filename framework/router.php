<?php

class Router {
    private static $routes = array();
    private static $arrRoutes = array();
    private static $arrRoutesByName = array();
    private static $urlData = array();
    private static $arrUrl = array();
	private static $prefix = NULL;
    
    /*
     * $url [string]        URL added to route
     * $settings [array]    model, view, layout and data
     * 
     * Adds URL to routing logic.
     */
    public static function add($url, $settings = array()) {
        // validate data
        if (!Validate::isString($url) || !is_array($settings))
            return;
           
        // we overwrite or set settings
        self::$routes[$url] = $settings;
        self::explode($url);
		
		if (isset($settings["name"]))
			self::$arrRoutesByName[$settings["name"]] = $url;
    }
	
	public static function setPrefix($prefix) {
		self::$prefix = $prefix;
	}
	
	public static function getPrefix($backslash = false) {
		return self::$prefix . ($backslash ? "/" : NULL);
	}
	
	public static function make($key, $args = array(), $domain = FALSE) {
		if (!isset(self::$arrRoutesByName[$key]))
			return NULL;
		
		$url = self::$arrRoutesByName[$key];
		$finalUrl = self::$prefix;

		foreach (self::$arrRoutes[$url] AS $urlPart) {
			$finalUrl .= "/";
			if (isset($urlPart[0]) && $urlPart[0] == "[") {
				foreach ($args AS $argKey => $argVal) {
					if (strpos($urlPart, $argKey) === 1) {
						$finalUrl .= $argVal;
						break;
					}
				}
			} else
				$finalUrl .= $urlPart;
		}
		
		return ($domain ? substr(URL, 0, -1) : "") . $finalUrl;
	}
	
	public static function currentURL($domain = FALSE) {
		return ($domain ? substr(URL, 0, -1) : NULL) . $_SERVER['REQUEST_URI'];
	}
    
    /*
     * $url [string]
     * 
     * Explodes URL for easier matching.
     */
    private static function explode($url) {
        // validate data
        if (!Validate::isString($url))
            return;
        
        self::$arrRoutes[$url] = array_slice(explode("/", $url), 1);
    }
    
    /*
     * $key [string]
     * 
     * Returns data parsed from url ([id:int] ...)
     */ 
    public static function getUrlData($key, $safe = TRUE) {
        // validate data
        if (!Validate::isString($key)) {
            if (Validate::isInt($key)) {
                return isset(self::$arrUrl[$key]) ? self::$arrUrl[$key] : NULL;
            }
            return;
        }
        
        return isset(self::$urlData[$key]) ? ($safe == TRUE ? mysql_real_escape_string(self::$urlData[$key]) : self::$urlData[$key]) : NULL;
    }
    
    public static function get($key = NULL) { return !is_null($key) ? self::getUrlData($key) : implode("/", self::$arrUrl); }
    
    /*
     * $customUrl [string] NULL     if set, $customUrl is used instead of $_SERVER['REQUEST_URI']
     *
     * Finds routing. 
    */
    public static function getMatch($customUrl = "") {
        // we need path and query
        $expl = explode("/", $_SERVER['SERVER_PROTOCOL'], 1);
        $url = parse_url(end($expl) . "://" . $_SERVER['SERVER_ADDR'] . (is_null($customUrl) ? $_SERVER['REQUEST_URI'] : $customUrl));
        $url['path'] = $_SERVER['REQUEST_URI'];
        
        $customUrl = empty($customUrl) 
                ? $url['path']
                : $customUrl;
        
        if (mb_substr($customUrl, -1) == "/")
            $customUrl = mb_substr($customUrl, 0, -1);

		if (!empty(self::$prefix))
			$customUrl = str_replace(self::$prefix, NULL, $customUrl);

        if (strpos($customUrl, "?"))
            $customUrl = substr($customUrl, 0, strpos($customUrl, "?"));
		
        $arrUrl = explode("/", mb_substr($customUrl, 1));
		
        self::$arrUrl = $arrUrl;
            
        $arrFinalSettings = array();
        if (isset(self::$routes["/" . $customUrl])) {
            $arrFinalSettings = self::$routes["/" . $customUrl];
        } else {
                
            foreach (self::$arrRoutes AS $routeURL => $arrRouteURLs) {
                // match only set urls
                //var_dump($arrRouteURLs);
                //var_dump($arrUrl);
                if (count($arrRouteURLs) < count($arrUrl) || (count($arrRouteURLs) > count($arrUrl) && end($arrRouteURLs) != "*"))
                    continue;
				
				//echo "111";

                $urlData = array();
                $error = FALSE;
                foreach ($arrRouteURLs AS $routeIndex => $routePart) {
                    if ($routePart == "*") {
                        $urlData = array_merge($urlData, self::$routes[$routeURL]);
                        break;
                    } else if (!isset($arrUrl[$routeIndex])) {
                    	if (empty($routePart)) {
	                        $urlData = array_merge($urlData, self::$routes[$routeURL]);
	                        break;
                    	} else {
	                        $error = TRUE;
	                        break; // error
                    	}
                    } else if (preg_match('/\[(.*?)\]/', $routePart, $pregMatch)) {
                        if (mb_strpos($pregMatch[1], ":") == FALSE) {
                            $urlData[$pregMatch[1]] = $arrUrl[$routeIndex];
                        } else { // we have to explode, find and validate
                            $arrSettings = explode(":", $pregMatch[1]);

                            $key = $arrSettings[0];
                            if (isset($arrSettings[1])) {
                                $type = $arrSettings[1];
                                
                                if (!empty($type)) {
                                	if ($type == "int") {
                                		if (!Validate::isInt($arrUrl[$routeIndex])) {
                                            $error = TRUE;
                                            break; // error because it isn't int
                                		}
                                	}
                                    // currently not implementer @ToDo!!!
                                }
                                
                                if (isset($arrSettings[2])) {
                                    $value = $arrSettings[2];
                                    
                                    if (mb_strpos($value, "|") == FALSE) { // if 1 value
                                        if ($value == $arrUrl[$routeIndex]) { // exact match of url part
                                             $urlData[$key] = $arrUrl[$routeIndex];
                                        } else {
                                            $error = TRUE;
                                            break; // error because it didn't match
                                        }
                                    } else { // if many values
                                        $values = explode("|", $value);
                                        
                                        if (in_array($arrUrl[$routeIndex], $values)) // find permitted url
                                            $urlData[$key] = $arrUrl[$routeIndex];
                                        else if (in_array("!" . $arrUrl[$routeIndex], $values)) {
                                            $error = TRUE;
                                            break; // error because it didn't match
                                        } else {
                                            $error = TRUE;
                                            break; // error because it didn't match
                                        }
                                    }
                                } else {
                                    $urlData[$key] = $arrUrl[$routeIndex];
                                }
                            } else {
                                $urlData[$key] = $arrUrl[$routeIndex];
                            }
                        }
                    } else if ($routePart != $arrUrl[$routeIndex]) {
                        $error = TRUE;
                        break;
                    }
                }

                if (isset(self::$routes[$routeURL]["method"])) {
                    $methods = explode("|", strtoupper(self::$routes[$routeURL]["method"]));
                    
                    if (!in_array($_SERVER['REQUEST_METHOD'], $methods))
                        $error = TRUE;
                }

                if ($error == FALSE) {
                    $arrFinalSettings = array_merge($urlData, self::$routes[$routeURL]);
                    break;
                }
            }
        }

        if (!isset($arrFinalSettings['module']) || !isset($arrFinalSettings['view']) || empty($arrFinalSettings['module']) || empty($arrFinalSettings['view']))
            Status::code(400, "Module and/or view aren't set (" . @$arrFinalSettings['module'] . ":" . @$arrFinalSettings['view'] . "). Check routing. " . (Core::dev() ? ("<pre>" . print_r($arrFinalSettings, TRUE) . "</pre>") : NULL));
		
        self::$urlData = $arrFinalSettings;
        Core::$calls = $arrFinalSettings;
        Core::$attr = explode("/", $customUrl);
        unset(Core::$attr[0]);
    }
}

?>