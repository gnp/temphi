<?php

class Core {
	public static $calls;
    
	public static $attr;

	private static $modules = array();
	
	private static $models = array();

	private static $layout = NULL;

	private static $content = NULL;

	private static $routes = array();

	private static $finalized = FALSE;
	
	public static $db;
	
	private static $development = FALSE;

	private static $exception = NULL;
	
	// parsed at the end
	private static $defaults = array();

	public static $globals = array();

  private static $layoutData = array();

	public static function setGlobal($set) {
		if (!is_array($set))
			$set = array($set);

		foreach ($set AS $key => $val)
			self::$globals[$key] = $val;
	}

	public static function getGlobal($key) {
		return isset(self::$globals[$key]) ? self::$globals[$key] : NULL;
	}
	
	/* autogenerate modules */
	public static function generateModules($modules = array()) {
		$sqlTables = "SHOW tables;";
		$qTables = self::$db->q($sqlTables);
		while ($rTable = self::$db->f($qTables)) {
			$table = array_pop($rTable);

			if (!is_dir(APP_PATH . "modules" . DS . $table)) {
				$sqlFields = "DESCRIBE " . $table;
				$qFields = self::$db->q($sqlFields);
				$mk = FALSE;
				$contentFields = NULL;
				$contentEdit = array();
				$tplAdd = new Template("moduletpladd");
				$tplEdit = new Template("moduletpledit");
				while ($rField = self::$db->f($qFields)) {
					if (strtolower($rField['Field']) !== "id" && $rField['Field'] != "PRI") {
						$contentFields .= "\t\t\t" . '"' . $rField['Field'] . '" => array(' . "\n";
						
                        // datetimes
                        if ($rField['Field'] == "dt_added" && is_null($rField['Default']))
                            $contentFields .= "\t\t\t\t" . '"default" => date("Y-m-d H:i:s"),' . "\n";
                        else if ($rField['Field'] == "dt_updated")
                            $contentFields .= "\t\t\t\t" . '"update" => date("Y-m-d H:i:s"),' . "\n";
                        
                        // null
                        if ($rField['Null'] == "NO" || $rField['Key'] == "MUL")
                            $contentFields .= "\t\t\t\t" . '"not null" => true,' . "\n";
                        
                        // unique
                        if ($rField['Key'] == "UNI")
                            $contentFields .= "\t\t\t\t" . '"unique" => true,' . "\n";
						
                        // varchar max length
                        if (strpos(strtolower($rField['Field']), "varchar") === 0) {
                            $contentFields .= "\t\t\t\t" . '"max" => ' . substr($rField['Field'], 7, -1) . ',' . "\n";
                        }
                        
                        // index min length
                        if (strpos($rField['Field'], "_id") === strlen($rField['Field']) - 3) {
                            $contentFields .= "\t\t\t\t" . '"min" => 1,' . "\n";
                        }
                        
                        // default
						if (!is_null($rField['Default']))
							$contentFields .= "\t\t\t\t" . '"default" => ' . (Validate::isInt($rField['Default']) ? $rField['Default'] : ('"' . $rField['Default'] . '"')) . ',' . "\n";
						
						$contentFields .= "\t\t\t" . '),' . "\n";
						
						if (!$mk && ($rField['Field'] == "title" || $rField['Field'] == "name" || $rField['Field'] == "username"))
							$mk = $rField['Field'];

						$editor = NULL;
                        if (strpos($rField['Field'], "dt_") === 0) {
                            $editor = HTML::input(array(
                                "type" => "text",
                                "name" => mb_substr($table, 0, -1) . "[" . $rField['Field'] . "]",
                                "class" => "jdt",
                                "id" => $rField['Field'],
                            ));
                            $editor2 = HTML::input(array(
                                "type" => "text",
                                "name" => mb_substr($table, 0, -1) . "[" . $rField['Field'] . "]",
                                "class" => "jdt",
                                "id" => $rField['Field'],
                                "value" => "##" . strtoupper($rField['Field']) . "##",
                            ));
                        } else if ($rField['Type'] == "text") {
                            $editor = HTML::textarea(array(
                                "name" => mb_substr($table, 0, -1) . "[" . $rField['Field'] . "]",
                                "id" => $rField['Field'],
                                "class" => "mceEditor",
                            ));
                            $editor2 = HTML::textarea(array(
                                "name" => mb_substr($table, 0, -1) . "[" . $rField['Field'] . "]",
                                "id" => $rField['Field'],
                                "value" => "##" . strtoupper($rField['Field']) . "##",
                                "class" => "mceEditor",
                            ));
                        } else if (strpos($rField['Type'], "int") === 0) {
                            $editor = HTML::input(array(
                                "type" => "number",
                                "step" => "1",
                                "name" => mb_substr($table, 0, -1) . "[" . $rField['Field'] . "]",
                                "id" => $rField['Field'],
                            ));
                            $editor2 = HTML::input(array(
                                "type" => "number",
                                "step" => "1",
                                "name" => mb_substr($table, 0, -1) . "[" . $rField['Field'] . "]",
                                "id" => $rField['Field'],
                                "value" => "##" . strtoupper($rField['Field']) . "##",
                            ));
                        } else {
                            $editor = HTML::input(array(
                                "type" => "text",
                                "name" => mb_substr($table, 0, -1) . "[" . $rField['Field'] . "]",
                                "id" => $rField['Field'],
                            ));
                            $editor2 = HTML::input(array(
                                "type" => "text",
                                "name" => mb_substr($table, 0, -1) . "[" . $rField['Field'] . "]",
                                "id" => $rField['Field'],
                                "value" => "##" . strtoupper($rField['Field']) . "##",
                            ));
                        }
						
						$tplAdd->parse(array(
							"editor" => $editor,
                            "for" => $rField['Field'],
                            "title" => Core::toCamel($rField['Field']),
						), "control group");
						
						$tplEdit->parse(array(
                            "editor" => $editor2,
                            "for" => $rField['Field'],
                            "title" => Core::toCamel($rField['Field']),
						), "control group");
					}
				}
			
				$tplPhp = new Template("modulephp");
				
				mkdir(APP_PATH . "modules" . DS . $table . DS . "templates", 0777, TRUE);
				
				file_put_contents(APP_PATH . "modules" . DS . $table . DS . $table . ".php", $tplPhp->parse(array(
					"module" => self::toCamel($table),
					"mk" => $mk,
					"fields" => $contentFields,
					"small" => substr($table, 0, -1),
				)));
				
				file_put_contents(APP_PATH . "modules" . DS . $table . DS . "templates" . DS . "add.tpl", $tplAdd->parse(array(
					"control group" => $tplAdd->display("control group"),
				)));
				
				file_put_contents(APP_PATH . "modules" . DS . $table . DS . "templates" . DS . "edit.tpl", $tplEdit->parse(array(
					"control group" => $tplEdit->display("control group"),
					"table" => $table,
				)));
			}
		}
	}

	function __construct($customUrl = NULL) {
		self::$finalized = FALSE;
		
		if (session_id() == '')
			session_start();

    $ds = substr($_SERVER['DOCUMENT_ROOT'], -1) == "/"
      ? ""
      : DS;

		if (!defined("APP_PATH"))
    		define('APP_PATH', $_SERVER['DOCUMENT_ROOT'] . $ds);
		
		if (!defined("WWW_PATH"))
    		define('WWW_PATH', $_SERVER['DOCUMENT_ROOT'] . $ds . "www" . DS);
	
		if (!defined("CORE_PATH"))
    		define('CORE_PATH', $_SERVER['DOCUMENT_ROOT'] . $ds . "framework" . DS);

    if (!defined("SRC_PATH"))
      define("SRC_PATH", $_SERVER['DOCUMENT_ROOT'] . $ds . "modules" . DS);

    if (!defined("LOG_PATH"))
      define("LOG_PATH", $_SERVER['DOCUMENT_ROOT'] . $ds . "logs" . DS);

		if (is_file(APP_PATH . "vendor/autoload.php")) {
			require APP_PATH . "vendor" . DS . "autoload.php";
		}

		include_once CORE_PATH . 'debug.php';

		include_once CORE_PATH . 'settings.php';

		include_once CORE_PATH . 'validate.php';

		include_once CORE_PATH . 'router.php';

		include_once CORE_PATH . 'controller.php';

		include_once CORE_PATH . 'model.php';

		include_once CORE_PATH . 'view.php';

		include_once CORE_PATH . 'template.php';

		include_once CORE_PATH . 'layout.php';

		include_once CORE_PATH . 'status.php';

		include_once CORE_PATH . 'developer.php';
		
		if (is_file(APP_PATH . "config" . DS . "functions.php"))
			include_once APP_PATH . "config" . DS . "functions.php";
		
		if (is_file(APP_PATH . "config" . DS . "settings.php")) {
			include_once APP_PATH . "config" . DS . "settings.php";
		}
		
		include_once APP_PATH . 'config' . DS  . 'router.php';

		include_once APP_PATH . 'config' . DS  . 'secrets.php';

		if (substr($_SERVER['DOCUMENT_ROOT'], -1) != "/")
			$_SERVER['DOCUMENT_ROOT'] != "/";

		if (!defined("DEVELOPING"))
			define("DEVELOPING", FALSE);
		
		if (DEVELOPING === TRUE) { // secrets.php
			error_reporting(E_ALL);
			ini_set("display_errors", E_ALL);
			self::$development = TRUE;
		} else {
			error_reporting(0);
			ini_set("display_errors", 0);
			self::$development = FALSE;
		}

        Router::getMatch($customUrl);
		
		//header('Content-Type: text/html; charset=' . self::$defaults['charset']);

		spl_autoload_register(function($class) {
			if (strpos($class, "Model") || strpos($class, "Controller")) {
				$class = str_replace(array("Model", "Controller"), NULL, $class);

				if (is_file(APP_PATH . "modules" . DS . Core::fromCamel($class) . DS . Core::fromCamel($class) . ".php"))
					require_once APP_PATH . "modules" . DS . Core::fromCamel($class) . DS . Core::fromCamel($class) . ".php";
			} else if (strpos($class, "Layout")) {
				$class = str_replace(array("Layout"), NULL, $class);
				
				if (is_file(APP_PATH . "layouts" . DS . Core::fromCamel($class) . ".php"))
					require_once APP_PATH . "layouts" . DS . Core::fromCamel($class) . ".php";
			}
		});
		
		setlocale (LC_ALL, 'sl_SI.UTF-8');
		setlocale (LC_TIME, 'sl_SI');
		
		self::$calls['layout'] = isset(self::$calls['layout']) ? self::$calls['layout'] : NULL;
		self::$calls['module'] = isset(self::$calls['module']) ? self::$calls['module'] : "default";
		self::$calls['view'] = isset(self::$calls['view']) ? self::$calls['view'] : "index";

		self::__LayoutPrepareLoad(self::$calls['layout']);
		
		// tu se samo naloži
		try {
			$module = self::loadModule(self::$calls['module']);
			
			if ($module !== TRUE)
				Status::code(400, "Cannot load module '" . self::$calls['module'] . "'." . (DEVELOPING ? ("<pre>" . print_r(Core::getException(), TRUE) . print_r(debug_backtrace(), TRUE) . "</pre>") : NULL));
		} catch (Exception $e) {
			Status::code(400, $e->getMessage() . (DEVELOPING ? ("<pre>" . print_r($e, TRUE) . print_r(debug_backtrace(), TRUE) . "</pre>") : NULL));
		}

		// v tej vrstici se zažene glavni modul oziroma pogled/funkcija
		try {
			$view = self::loadView(self::$calls['view'], self::$calls['module'], empty(self::$calls['data']) ? NULL : self::$calls['data'], TRUE);

			if ($view === FALSE)
				Status::code(400, "Cannot load main view '" . self::$calls['module'] . ":" . self::$calls['view'] . "'." . (DEVELOPING ? ("<pre>" . print_r(Core::getException(), TRUE) . print_r(debug_backtrace(), TRUE) . "</pre>") : NULL));
		} catch (Exception $e) {
			Status::code(400, $e->getMessage() . (DEVELOPING ? ("<pre>" . print_r($e, TRUE) . print_r(debug_backtrace(), TRUE) . "</pre>") : NULL));
		}
		
		if (self::$finalized !== TRUE) {
			self::__LayoutAfterLoad();
            
      Status::delayedRedirect();

     	self::preFinalize();
		
			self::finalize();
		}
	}

	public static function __LayoutPrepareLoad($layout, $error = TRUE) {
		if (self::loadLayout($layout) !== TRUE) {
			if ($error == TRUE)
				Status::code(400, "Cannot load layout '" . $layout . "'.");
		} else {
			if (method_exists(self::$layout, "__beforeLoad")) // če morš kj sparsat/izvest po tem ko se izvede glavni pogled, dodaš to funkcijo v layout
				self::$layoutData = array_merge(self::$layoutData, (array)self::$layout->__beforeLoad());
            
			if (method_exists(self::$layout, "__beforeIndex")) // called before loading main view
				self::$layoutData = array_merge(self::$layoutData, (array)self::$layout->__beforeIndex());
		}
	}

	public static function __LayoutAfterLoad() {
		if (method_exists(self::$layout, "__afterLoad")) // če morš kj sparsat/izvest po tem ko se izvede glavni pogled, dodaš to funkcijo v layout
      self::$layoutData = array_merge((array)self::$layoutData, (array)self::$layout->__afterLoad());
	}

	public static function preFinalize() {
 		self::$defaults["ufw debug"] = self::dev() ? Debug::getSession() . Debug::getCurrent() : Debug::getSession();
    self::$defaults["ufw developer"] = self::$development == TRUE ? Developer::get() : NULL;
		self::$defaults["base href"] = Router::getPrefix(TRUE);

    // twig fallback
    self::$defaults["ufwDebug"] = @self::$defaults["ufw debug"];
    self::$defaults["ufwDeveloper"] = @self::$defaults["ufw developer"];
    self::$defaults["baseHref"] = @self::$defaults["base href"];

    self::$defaults["siteTitle"] = @self::$defaults["site title"];
    self::$defaults["cssPage"] = @self::$defaults["css page"];
    self::$defaults["contentRight"] = @self::$defaults["content right"];

    Debug::push();
	}

	public static function getLayout() {
		return self::$layout;
	}
	
	public static function configDatabase($type) {
		if ($type == "Mysql")
			include_once CORE_PATH . 'mysql.php';
	}
	
	public static function constructDatabase() {
		self::$db = new DB();
	}
	
	public static function configHelpers($helpers) {
		$availableHelpers = array(
			"Apc",
			"Auth",
			"Cache",
			"FileCache",
			"Image",
			"Mail",
			"Optimize",
			"Paginator",
			"SloDate",
			"Social",
			"Upload",
			"HTML",
            "SEO",
            "JSON",
            "MicroZip",
		);
		
		foreach ($helpers AS $helper)
			if (in_array($helper, $availableHelpers))
				include_once CORE_PATH . "helpers" . DS . strtolower($helper) . ".php";
	}
	
	public static function configDefaults($defaults) {
		foreach ($defaults AS $key => $val)
			self::$defaults[$key] = $val;
	}
	
	public static function dev() {
		return self::$development;
	}

	public static function setFinalized() {
		self::$finalized = TRUE;
	}

  public static function parseTranslation($content) {
    $translations = self::getModel("translations");

    preg_match_all("|\{\{ __\('([abcdefghijklmnopqrstuvwxyz_-]*)\'\) \}\}|", $content, $matches);

    if ($matches)
    foreach ($matches[1] AS $i => $slug) {
      $content = str_replace($matches[0][$i], self::parseTranslation($translations->getContentBySlug($slug)), $content);
    }

    return $content;
  }
	
  public static function finalize() {
    $echoContent = null;
    if (!empty(self::$layout)) {
      if (self::$layout instanceof TwigTpl) {
        $tempContent =  isset(self::$defaults['content']) ? self::$defaults['content'] : NULL;
        try {
          self::$layout->addData(self::$defaults);
          self::$layout->addData(self::$layoutData);

          $layoutContent = self::$layout->parse();

          if (!$layoutContent && $tempContent) {
            $echoContent = $tempContent;
          } else {
            $echoContent = $layoutContent;
          }
        } catch (Exception $e) {
          if (self::dev()) {
            die($e->getMessage());
          } else {
            die("going 404 anyway...");
            Status::code(404, "Application error");
          }
        }
      } else {
        $tempContent =  isset(self::$defaults['content']) ? self::$defaults['content'] : NULL;
        self::$layout->parse(array(
          "content" => $tempContent,
        ));
        
        if (isset(self::$defaults['content']))
          unset(self::$defaults['content']);
              
        $finalContent = self::$layout->parse(self::$defaults);

        $echoContent = $finalContent ?: $tempContent;
      }
    } else if (isset(self::$defaults['content'])) {
      $echoContent = self::$defaults['content'];
    }

    echo self::parseTranslation($echoContent);;

    return self::$finalized = $echoContent ? true : false;
  }
	
	static function url($index = NULL) {
		return Router::get($index);
	}

	public static function issetDatabaseConnection() {
		return is_resource(self::$db);
	}

	public static function reqFile($file, $err = NULL) {
		if (is_file($file)) {
			require_once $file;
			return TRUE;
		}

		return FALSE;
	}

	public static function camelize($val, $from = NULL, $ucfirst = FALSE) {
		if ($from == NULL) {
			if ($val == strtolower($val)) {
				return $ucfirst == FALSE ? Core::toCamel($val) : ucfirst(Core::toCamel($val));
			} else {
				return $ucfirst == FALSE ? Core::fromCamel($val) : ucfirst(Core::fromCamel($val));
			}
		} elseif ($from == "to") {
			return $ucfirst == FALSE ? Core::toCamel($val) : ucfirst(Core::toCamel($val));
		} else {
			return $ucfirst == FALSE ? Core::fromCamel($val) : ucfirst(Core::fromCamel($val));
		}
	}

	public static function toCamel($text) {
		$text = str_split($text, 1);

		foreach ($text AS $index => $char) {
			if ($char == "_" && isset($text[$index + 1])) {
				$text[$index + 1] = mb_strtoupper($text[$index + 1]);
			}
		}

		return ucfirst(str_replace("_", "", implode($text)));
	}

	public static function fromCamel($text) {
		$return = NULL;
		$text = str_split($text, 1);

		foreach ($text AS $index => $char) {
			if ($char != strtolower($char) && $index != 0) {
				$return .= "_";
			}

			$return .= $char;
		}

		return strtolower($return);
	}

	public static function getLocalFile($file = NULL, $subfix = NULL, $fatal = FALSE) {
		if (empty($subfix))
			$subfix = APP_PATH;

		if (is_file($subfix . $file))
			return file_get_contents($subfix . $file);
		else
			return FALSE;
	}

    public static function issetModel($model) {
        return isset(self::$models[Core::toCamel($model)]);
    }

	public static function getModel($model, $check = TRUE) {
		return !$check || self::issetModel($model) ? self::$models[Core::toCamel($model)] : FALSE;
	}

    public static function issetModule($module) {
        return isset(self::$modules[Core::toCamel($module)]);
    }

    public static function getModule($module, $check = TRUE) {
        return !$check || self::issetModule($module) ? self::$modules[Core::toCamel($module)] : FALSE;
    }

    public static function issetCtrl($ctrl) {
        return isset(self::$models[Core::toCamel($ctrl)]);
    }

    public static function getCtrl($ctrl, $check = TRUE) {
        return !$check || self::issetCtrl($ctrl) ? self::$models[Core::toCamel($ctrl)] : FALSE;
    }

/*
 * sets parsing of variable in layout
 * $key --> ##KEY##
 * $val --> $key is replaced with $val
 * $append --> TRUE/FALSE --> if true, appends to existing content, otherwise content is rewritten ... kkrkol
 * $before --> TRUE/FALSE --> if true, content is added at the beginning
 * $splitter --> whatever :D
 * 
 * */
	public static function setParseVar($key, $val, $append = FALSE, $before = FALSE, $splitter = NULL) {
		 if ($append == FALSE || !isset(self::$defaults[$key]))
		 	self::$defaults[$key] = $val;
		 else {
		 	if ($before == TRUE)
		 		self::$defaults[$key] = $val . (!empty(self::$defaults[$key]) ? $splitter : NULL) . self::$defaults[$key];
		 	else
		 		self::$defaults[$key] .= (!empty(self::$defaults[$key]) ? $splitter : NULL) . $val;
		 }
	}

/*
 * called in controller where we want to use other layout
 * 
 * 
 * */
	public static function loadLayout($layout) {
    $layoutFunction = "index";
    if (strpos($layout, ":") !== FALSE) {
      $layout = explode(":", $layout);
      $layoutFunction = $layout[1];
      $layout = $layout[0];
    }
        
		if ($layout) {
			$name = self::toCamel($layout) . "Layout";

      if (!class_exists($name) && is_file(APP_PATH . "layouts" . DS . self::fromCamel($layout) . ".php")) {
        require_once APP_PATH . "layouts" . DS . self::fromCamel($layout) . ".php";
      }
			
			self::$layout = new $name;

      if (self::$layout instanceof TwigTpl) {
        //var_dump(self::$layout);
      } else {
        self::$layout->set($layout);
      }

			self::$layoutData = array_merge((array)self::$layoutData, (array)self::$layout->{$layoutFunction}());
			return TRUE;
		} else if (is_null($layout)) {
			self::$layout = NULL;
			return TRUE;
		}

		return FALSE;
	}

	public static function loadModule($module, $conf = array()) {
		if (empty($conf))
			$conf = array(
				"ctrl" => TRUE,
				"model" => TRUE
			);
		

		$ok = FALSE;
		if (isset($conf['ctrl'])) {
 			$name = self::toCamel($module) . "Controller";

      if (is_file(APP_PATH . "modules" . DS . $module . DS . $module . ".php"))
        include_once APP_PATH . "modules" . DS . $module . DS . $module . ".php";

			self::$modules[self::toCamel($module)] = new $name;
			$ok = TRUE;
		}

		if (isset($conf['model']) && class_exists(self::toCamel($module) . "Model")) {
			$name = self::toCamel($module) . "Model";
			self::$models[self::toCamel($module)] = new $name;
			$ok = TRUE;
		}

		return $ok;
	}

	public static function loadCtrl($ctrl) {
		return self::loadModule($ctrl, array("ctrl"));
	}

	public static function loadModel($model) {
		return self::loadModule($model, array("model"));
	}

	public static function loadView($view, $module, $arr = NULL, $main = FALSE) {
		try {
			if (!isset(self::$modules[self::toCamel($module)])) {
				if (!self::loadModule($module)) {
					return FALSE;
				}
			}
					
			if (method_exists(self::$modules[self::toCamel($module)], $view)) {
				$content = is_null($arr) ? self::$modules[self::toCamel($module)]->$view() : self::$modules[self::toCamel($module)]->$view($arr);
				
				if ($main) {
					if (!is_array($content))
						self::$defaults['content'] = $content;
					else {
						foreach ($content AS $keyToParse => $valToParse)
							self::$defaults[$keyToParse] = $valToParse;
					}
				}
				
				return $content;
			} else {
				return FALSE;
			}
		} catch (Exception $e) {
			Core::setException($e);
			return FALSE;
			//throw new Exception('Something really gone wrong', 0, $e);
		}
		
		return "FALSE";
	}

	static function setException(Exception $e) {
		self::$exception = $e;
	}

	static function getException() {
		return self::$exception;
	}
}

?>