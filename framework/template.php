<?php

class UFWTemplate extends UFWView {
	function __construct($tpl = NULL, $module = NULL, $err = TRUE) {
		if (is_null($tpl)) {
			
		} else if (!($this->content = Core::getLocalFile($tpl . ".tpl", !is_null($module) ? (APP_PATH . "modules" . DS . Core::fromCamel($module) . DS . "templates" . DS) : (CORE_PATH . "defaults" . DS)))) {
			$this->content = NULL;
			if ($err)
				Debug::addError("Cannot load template '" . (!is_null($module) ? (APP_PATH . "modules" . DS . Core::fromCamel($module) . DS . "templates" . DS) : (CORE_PATH . "defaults" . DS)) . $tpl . ".tpl" . "'");
		}
	}
}

class Template extends UFWTemplate { }

class TwigTpl extends UFWView {
	protected $view;
	protected $data;
	protected $override = false;

	function __construct($view = NULL, $data = array()) {
		$this->view = $view;

		$this->data = $data;
	}
	
	function setContent($content) {
		$this->override = $content;
	}

	function setData($arrData) {
		$this->data = $arrData;
	}

        // Strict Standards: Declaration of TwigTpl::display() should be compatible with UFWView::display($part = NULL, $clear = true)
        function display($part = NULL, $clear = TRUE) {
                return $this->parse(array());
        }

	function addData($arrData) {
		foreach ($arrData AS $key => $data) {
			try {
				if (!isset($this->data[$key]))
					$this->data[$key] = $data;
				else if (is_array($this->data[$key]))
					$this->data[$key][] = $data;
				else {
					$this->data[$key] = $data;
				}
			} catch (Exception $e) {
				Debug::addError($e->getMessage());
			}
		}
		return $this;
	}

	function getData() {
		return $this->data;
	}
	
	// $part & $saveAs are here because Strict Standards: Declaration of TwigTpl::parse() should be compatible with UFWView::parse($data = Array, $part = NULL, $saveAs = NULL)
	function parse($data = array(), $part = NULL, $saveAs = NULL) {
		$this->addData($data);
		$this->addData(array("url" => URL));

		if (is_null($this->override)) {
			return null;
		} else {
			$twig = new Twig_Environment(new Twig_Loader_Chain(array(
				new Twig_Loader_Filesystem(array(APP_PATH)),
				new Twig_Loader_Filesystem(array(SRC_PATH)),
				new Twig_Loader_String()
				)), array('debug' => true));
		}

		Core::loadModule("translations");

		$twig->addFunction(new Twig_SimpleFunction("__", function($slug, $replace = array()){
            $m = Core::getModel("translations");
            $content = $m->getContentBySlug($slug);
            return str_replace(array_keys($replace), $replace, $content);
        }, array('is_safe' => array('html'))));
	
		try {
			return $twig->render($this->view, $this->data);
		} catch (Twig_Error_Syntax $e) {
			return "<pre>" . htmlspecialchars($e->getMessage()) . "</pre>";
		}
	}

	function __toString() {
		try {
			return $this->parse();
		} catch (Exception $e) {
			var_dump($e);
		}
	}
}

?>