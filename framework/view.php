<?php

class UFWView {
	public $content;
	public $partContent = array();
	public $partContents = array();
	
	public function __get($name) {
		if ($name == "db") {
			return Core::$db;
		}
	}

	public function isEmpty() {
		return empty($this->content);
	}
	
	public function setContent($content) {
		$this->content = $content;
	}

	public function setPart($part) {
		$this->partContent[$part] = $this->getTplContent($part);
	}

	protected function getTplContent($part = NULL, $content = NULL) {
		if ($part != NULL) {
			$start = mb_strpos(is_null($content) ? $this->content : $content, "##" . mb_strtoupper($part) . " START##") + mb_strlen("##" . mb_strtoupper($part) . " START##");
			$end = mb_strpos(is_null($content) ? $this->content : $content, "##" . mb_strtoupper($part) . " END##");

			if ($end > $start) {
				return mb_substr(is_null($content) ? $this->content : $content, $start, $end - $start);
			}
		}
		return false;
	}

	protected function commitParts() {
		foreach ($this->partContents AS $key => $val) {
			$this->commit($key);
		}
	}

	public function hasData($part) {
		return isset($this->partContents[$part]) && !empty($this->partContents[$part]);
	}

	public function commit($part = NULL, $data = NULL) {
		if (!empty($part)) {
			$data = $data !== NULL ? $data : isset($this->partContents[$part]) ? implode($this->partContents[$part]) : NULL;
			//$this->partContent[$part] = str_replace($this->partContent[$part], implode($this->partContents[$part]), $this->partContent[$part]);
			//$this->partContent[$part] = $data;
            $this->parse(array(
                $part => $data,
            ));
			unset($this->partContents[$part]);
			return TRUE;
		}
		return FALSE;
	}
    
    public function commitPart($part) {
        $this->commit($part);
    }
	
	public function clear($part) {
		if (isset($this->partContents[$part]))
			unset($this->partContents[$part]);
	}

	public function receive($part = NULL) {
		return !is_null($part) && isset($this->partContent[$part]) ? $this->partContent[$part] : FALSE;
	}

	public function display($part = NULL, $clear = TRUE) {
		return !is_null($part) ? (array_key_exists($part, $this->partContents) && is_array($this->partContents[$part]) ? (implode($this->partContents[$part]) . ($clear === TRUE ? $this->clear($part) : NULL)) : NULL) : $this->content;
	}

	public function finalize() {
		$this->commitParts();

		foreach ($this->partContent AS $key => $val) {
			$this->content = str_replace(array("##" . strtoupper($key) . "##"), $val, $this->content);

			if ($key != NULL) {
				$start = mb_strpos($this->content, "##" . strtoupper($key) . " START##");
				$end = mb_strpos($this->content, "##" . strtoupper($key) . " END##") + mb_strlen("##" . strtoupper($key) . " END##");

				$this->content = str_replace(mb_substr($this->content, $start, $end - $start), $val, $this->content);
			}
		}

		$content = $this->content;

		// translations
    preg_match_all("|\{\{ __\('([abcdefghijklmnopqrstuvwxyz_-]*)\'\) \}\}|", $content, $matches);

    Core::loadModule("translations");
    $translations = Core::getModel("translations");

    if ($matches)
    foreach ($matches[1] AS $i => $slug) {
      $content = str_replace($matches[0][$i], $translations->getContentBySlug($slug), $content);
    }
    // translations
    
    $this->content = $content;

		return $this->content;
	}

	public function set($lyt, $ext = ".tpl") {
		if (is_null($lyt))
			$lyt = str_replace("Layout", "", get_class($this));

		if (!($this->content = Core::getLocalFile(Core::fromCamel($lyt) . $ext, APP_PATH . "layouts" . DS))) {
			$this->content = NULL;
			Debug::setErr("Cannot load layout." . APP_PATH . "layouts" . DS . Core::fromCamel($lyt) . $ext);
		}
	}
	
	public function manualAddToPart($content = NULL, $part = NULL) {
		if (!is_null($part))
			$this->partContents[$part][] = $content;
		else
			$this->content .= $content;
	}

	public function parse($data = array(), $part = NULL, $saveAs = NULL) {
		if (is_null($part)) {
			$content = $this->content;
		} else {
			$this->setPart($part);
			$content = $this->partContent[$part];
		}
		
		$saveAs = is_null($saveAs) ? $part : $saveAs;
		
		if (is_array($data))
			foreach ($data AS $key => $val) {
				if (!is_array($val)) {
					if (mb_strpos($content, "##" . strtoupper($key) . "##") !== FALSE) {
						$content = str_replace("##" . strtoupper($key) . "##", $val, $content);
					} else {
						$content = str_replace("##" . strtoupper($key) . " START##" . $this->getTplContent($key, $content) . "##" . strtoupper($key) . " END##", $val, $content);
					}
				} else {
					foreach ($val AS $key2 => $val2) {
						if (is_array($val2)) {
							foreach ($val2 AS $key3 => $val3) {
								if (!is_array($val3))
									$content = str_replace("##" . strtoupper($key2) . "." . strtoupper($key3) . "##", $val3, $content);
							}
						} else {
// 							if($key2 == "pin") echo "CCC: $key2 | $val2".BR;
							$content = str_replace("##" . strtoupper($key) . "." . strtoupper($key2) . "##", $val2, $content);
						}
					}
				}
			}
		else if (is_null($data)) {
			if (mb_strpos($content, "##" . strtoupper($part) . "##") !== FALSE) {
				$content = str_replace("##" . strtoupper($part) . "##", NULL, $content);
			} else {
				$content = str_replace("##" . strtoupper($part) . " START##" . $this->getTplContent($part, $content) . "##" . strtoupper($part) . " END##", NULL, $content);
			}
		}
		
// 		echo "AAAAAAAAAAAAAA"; dump($content); echo "BBBBBBBBBBBBBBBBBBBBB";

		// translations
    preg_match_all("|\{\{ __\('([abcdefghijklmnopqrstuvwxyz_-]*)\'\) \}\}|", $content, $matches);

    Core::loadModule("translations");
    $translations = Core::getModel("translations");

    if ($matches)
    foreach ($matches[1] AS $i => $slug) {
      $content = str_replace($matches[0][$i], $translations->getContentBySlug($slug), $content);
    }
    // translations

		if ($saveAs != NULL) {
			$this->partContents[$saveAs][] = $content;
		} else {
			$this->content = $content;
		}

		return $content;
	}
}

?>