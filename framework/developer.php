<?php

class DeveloperDB {
	private static $arrQueries = array();
	
	private static $arrErrno = array();
	private static $arrError = array();
	
	private static $arrSuccess = array();
	private static $length = array();
	
	static function start() {
		self::$length = microtime();
	}
	
	static function debugLastQuery($sql = NULL) {
		self::$length = microtime()-self::$length;
		self::$arrQueries[] = htmlspecialchars($sql, ENT_QUOTES, 'UTF-8');;
		if (mysql_errno() === 0)
			self::debugLastSuccess();
		else
			self::debugLastError();
	}
	
	static function debugLastSuccess() {
		self::$arrSuccess[count(self::$arrQueries) - 1] = self::$length . "s";
	}
	
	static function debugLastError() {
		self::$arrErrno[count(self::$arrQueries) - 1] = mysql_errno();
		self::$arrError[count(self::$arrQueries) - 1] = mysql_error();
	}
	
	static function getHTML() {
		$string = "<legend>Database</legend>" . "<table><tr><td>Query</td><td>Error #</td></tr>";
		foreach (self::$arrQueries AS $key => $val) {
			if (!empty($val))
				$string .= "<tr><td>" . $val . "</td><td>" . (isset(self::$arrErrno[$key]) ? (self::$arrErrno[$key] . " :: " . self::$arrError[$key]) : self::$arrSuccess[$key]) . "</td></tr>";
		}
		
		return $string . "</table>";
	}
}

class DeveloperRequest {
	static function getHTML() {
		return "<legend>POST</legend>" . Developer::fromArrayToString($_POST) .
			"<legend>GET</legend>" . Developer::fromArrayToString($_GET) .
			"<legend>SESSION</legend>" . Developer::fromArrayToString($_SESSION) .
			"<legend>SERVER</legend>" . Developer::fromArrayToString($_SERVER);
	}
}

class Developer {
	static function fromArrayToString($arr, $depth = 0) {
		$string = "<table>";
		foreach ($arr AS $key => $val)
			$string .= "<tr style='vertical-align: top;'><td>" . (is_array($val) ? NULL : $key) . "</td><td>" . (is_array($val) 
				? self::fromArrayToString($val, $depth+1) 
				: $val) . "</td></tr>";
		
		return $string . "</table>";
	}
	
	static function get() {
		return '<div style="background: #ddd; color: #333; width: 80%; display: block; float: left; clear: both; padding: 5%; margin: 5%;">' .
			DeveloperDB::getHTML() .
			DeveloperRequest::getHTML() .
			'</div>';
	}
}

?>