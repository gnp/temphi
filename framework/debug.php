<?php 

class Debug {
	// sets session
	static function push() {
		
	}
	
	// resets session
	static function clean() {
		$_SESSION['Debug'] = array();
	}
	
	// adds error, warning or success
	static function add($type, $content, $display = TRUE) {
		if (!in_array($type, array("e", "w", "s")))
			return FALSE;
	
        if (!isset($_SESSION['Debug'][$type]) || !in_array($content, $_SESSION['Debug'][$type]))
            $_SESSION['Debug'][$type][] = $content;
	}
	
	static function addError($content, $display = TRUE, $system = TRUE) {
		Debug::add("e", $content, $display);
	}
	
	static function addWarning($content, $display = TRUE) {
		Debug::add("w", $content, $display);
	}
	
	static function addSuccess($content, $display = TRUE) {
		Debug::add("s", $content, $display);
	}
	
	// red/yellow/green popup (succes actions, warnings ...)
	
	static function getSession() {
		$contentRet = NULL;
		
		if (isset($_SESSION['Debug']))
		foreach ($_SESSION['Debug'] AS $type => $contents)
			foreach ($contents AS $content)
				$contentRet .= '<div style="clear: both; display: block; margin: 5px;" class="ufwDebug' . ($type == "e" ? "AlwaysShow" : NULL) . ' btn btn-' . ($type == "e" ? "danger" : ($type == "w" ? "warning" : "success")) . '">' . $content . '</div>';
		
        $_SESSION['Debug'] = array();
		return $contentRet;
	}
	
	static function getCurrent() {
		return NULL;
	}
}

?>