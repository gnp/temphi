<?php

class HelperFacebook {
	private static $metaProperty = array();
	
	public static function like($url) {
		return '<iframe class="fl db" src="//www.facebook.com/plugins/like.php?app_id=244152332297581&amp;href=' . $url . '&amp;send=false&amp;layout=standard&amp;width=400&amp;show_faces=true&amp;action=like&amp;colorscheme=dark&amp;font=arial&amp;height=25" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:400px; height:25px;" allowTransparency="true"></iframe>';
	}
	
	public static function likebox() {
	}
	
	/*
	 * Input:
	 * $key		key for facebook meta tag 
	 * $value	value of $key facebook meta tag
	 * 
	 * Description:
	 * Stores $value in facebook's $key meta tag.
	 * 
	 * Output: Boolean
	 * TRUE		successful
	 * FALSE	not successful (not valid $key)
	 * 
	 * Author:
	 * Bojan Rajh - May 2012
	*/
	public static function setMeta($key, $value = NULL) {
		if (in_array($key, array(
			"title", "description", "image",
			"audio", "audio:type", "audio:title", "audio:artist", "audio:album",
			"video", "video:height", "video:width", "video:type"
			))) {
			self::$metaProperty[$key] = '<meta property="og:' . $key . '" content="' . $value . '" />';
			
			return TRUE;
		}
		
		return FALSE;
	}
	
	/*
	 * Output: Mixed
	 * NULL		none of facebook meta tags are set
	 * String	imploded facebook meta tags
	 */
	public static function getMeta() {
		return !(empty(self::$metaProperty)) ? implode("\n", self::$metaProperty) : NULL;
	}
}

?>