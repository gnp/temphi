<?php

class ##MODULE##Controller extends UFWController {
	function __construct() {
		parent::__construct();
	}
	
	function index() {
		$tpl = new Template("index", "##MODULE##");
		
		return $tpl->display();
	}
	
	function one() {
		$tpl = new Template("one", "##MODULE##");
		
		$r = $this->##MODULE##->get("first", NULL, array("id" => Router::get("id")));
		
		if (empty($r))
			Status::code(404, "##MODULE## #" . Router::get("id") . " not found.");
		
		return $tpl->parse($r);
	}
	
	function maestro() {
		$q = $this->##MODULE##->get("all");
		
		$return["title"] = "##MODULE##";
		$return["th"] = array("ID", "##MK##");
		$return["btn header"][] = "add";
		while ($r = $this->db->f($q)) {
			$rowBtn = array("update", "delete");
			
			$return["row"][] = array(
				"td" => array(
					$r['id'],
					$r['##MK##'],
				),
				"btn" => $rowBtn,
			);
		}
		return $this->Maestro->makeListing($return);
	}
	
	function add() {
		$tpl = new Template("add", "##MODULE##");

		return $this->Maestro->makeAdd(array(
			"content" => $tpl->display(),
			"title" => "##SMALL##",
		));
	}
	
	function insert() {
		if (!$this->##MODULE##->isValidInsert($_POST['##SMALL##']))
            Status::code(404, "Invalid insert data!");
            
		$id = $this->##MODULE##->insert($_POST['##SMALL##']);

		$this->Maestro->makeInsert(array("id" => $id));
	}

	function edit() {
		$tpl = new Template("edit", "##MODULE##");

		$r = $this->##MODULE##->get("first", NULL, array("id" => Router::get("id")));
		
		if (empty($r))
			Status::code(404, "##MODULE## #" . Router::get("id") . " not found.");

		return $this->Maestro->makeEdit(array(
			"content" => $tpl->parse($r),
			"title" => "##SMALL##",
		));
	}

	function update() {
		if (!Validate::isInt($_POST['##SMALL##']['id']))
            Status::code(404, "Invalid ID!");
		
		if (!$this->##MODULE##->isValidUpdate($_POST['##SMALL##']))
            Status::code(404, "Invalid update data!");
            
		$this->##MODULE##->update($_POST['##SMALL##'], $_POST['##SMALL##']['id']);

		return $this->Maestro->makeUpdate(array(
			"id" => $_POST['##SMALL##']['id']
		));
	}

	function delete() {
		$this->##MODULE##->delete(Router::get("id"));
	}
}

class ##MODULE##Model extends UFWModel {
	function __construct() {
		parent::__construct();
		
		$this->mk = "##MK##";

		$this->fields = array(
##FIELDS##
		);
	}
}

?>