<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>##SITE TITLE##</title>
	</head>
	<body style="margin: 0; padding: 0; font-family: 'Trebuchet MS', Helvetica, sans-serif; background: #ccc; font-size: 18px; font-weight: normal; text-decoration: none; color: #000;">
		<div style="width: 66%; margin: 10% auto;">
			<p style="font-size: 30px; font-weight: bold; text-transform: uppercase;">Whooops ... ##TITLE##</p>
			<p>It seems that the page you were trying to reach doesn't exist, has just moved or you made invalid request.</p>
			<p>##DESCRIPTION##</p>
			<a href="##HREF##" style="text-decoration: none; color: inherit;">Click here if you want to return to ##SITE##.</a>
		</div>
	</body>
</html>