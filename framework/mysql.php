<?php

class Mysql {
	private static $conf = array();
	private static $connection = array();
	
	private static $r;
	private static $q;
	private static $sql;
	
	public static function config($config = array(), $index = 0) {
		self::$conf[$index] = array(
			"host" => "localhost",
			"user" => "root",
			"pass" => "rootpass",
			"port" => 3306,
			"db" => "database",
			"prefix" => NULL,
			"encoding" => "utf8",
			"collate" => "utf8_slovenian_ci"
		);
		
		// override defaults
		foreach ($config AS $key => $val)
			if (isset(self::$conf[$index][$key]))
				self::$conf[$index][$key] = $val;
		
		Core::constructDatabase();
	}

	public function __construct($index = 0) {
		// creates connection
		DeveloperDB::start();
		self::$connection[$index] = @mysql_connect(self::$conf[$index]['host'] . ":" . self::$conf[$index]['port'], self::$conf[$index]['user'], self::$conf[$index]['pass']);
		DeveloperDB::debugLastQuery();
		
		// selects database
		DeveloperDB::start();
		@mysql_select_db(self::$conf[$index]['db'], self::$connection[$index]);
		DeveloperDB::debugLastQuery();
		
		// sets encoding
		DeveloperDB::start();
		@mysql_query("SET NAMES '" . self::$conf[$index]['encoding'] . "' COLLATE '" . self::$conf[$index]['collate'] . "'", self::$connection[$index]);
		DeveloperDB::debugLastQuery();
	}

	public function __destruct() {
		foreach (self::$connection AS $index => $connection)
			if (is_resource($connection))
				mysql_close(self::$connection[$index]) OR Debug::addError("Can't close MySQL connection.");
	}

	public static function isActive($index = 0) {
		return isset(self::$connection[$index]) && is_resource(self::$connection[$index]) ? TRUE : FALSE;
	}

	public static function q($sql, $index = 0) {
		if (self::isActive()) {
			DeveloperDB::start();
			$q = mysql_query($sql, self::$connection[$index]);
			DeveloperDB::debugLastQuery($sql);
			
			if (!mysql_error()) {
				self::$q = $q;
				return self::$q;
			} else {
				return FALSE;
			}
		}
		return FALSE;
	}

	public static function f($q = NULL) {
		if (self::isActive()) {
			$q == NULL ? $q = self::$q : NULL;
			
			if (is_resource($q)) {
				return mysql_fetch_array($q, MYSQL_ASSOC);
			} else {
				//Debug::addError(mysql_errno());
				return FALSE;
			}
		}
		return FALSE;
	}

	public static function sql($sql = NULL) {
		self::$sql = $sql;

		return TRUE;
	}

	public static function c($q = NULL) {
		if (self::isActive()) {
			$q == NULL ? $q = self::$q : NULL;
			
			if (is_resource($q)) {
				return mysql_num_rows($q);
			} else {
				Debug::addError(mysql_errno());
				return FALSE;
			}
		}
		return FALSE;
	}

	public static function a() {
		if (self::isActive())
			return end(self::f(self::q("SELECT FOUND_ROWS();")));

		return FALSE;
	}

	public static function escape($val) {
		if (self::isActive())
			return is_float($val) ? $val : mysql_real_escape_string($val);

		return FALSE;
	}

	//* magic! *//
	private $select, $table, $join, $where, $having, $groupBy, $orderBy, $limit;

	public function select($select = "*") {
		print $sql = "SELECT " . $select . " " .
			"FROM " . $this->from . " " .
			(!empty($this->join) ? implode($this->join, " ") : NULL);
	}

	public function from($model) {
		$this->from = $model->table;

		return $this;
	}

	public function innerJoin($first, $key) {
		$key = Core::toCamel($key);
		if ($first->isConnected($key)) {
			$conf = $first->getConnection($key);

			if (!Core::issetModule($key))
				Core::loadModule($key);

			$second = Core::getModel($key);

			$this->join[] = "INNER JOIN " . $second->table . 
			" ON " . $second->table . "." . $conf["foreign"] . " = " . $first->table . "." . $conf['primary'];
		
		}

		return $this;
	}

	public function update() {

	}

	public function delete() {

	}
}

class DB extends Mysql { }

?>